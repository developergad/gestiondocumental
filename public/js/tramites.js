var arrUrl = window.location.href.split("/");
var URLdomain = window.location.host;
var protocol = window.location.protocol;
var URLactual;
let posicion = URLdomain.indexOf('localhost');
var archivo = null;

if(posicion !== -1) URLactual = `${protocol}//${URLdomain}/sistemasti58/public`;
else URLactual = `${protocol}//${URLdomain}`;

$(document).ready(function() {
  $('#div-telefono').removeClass('col-lg-2');
  $('#div-telefono').addClass('col-lg-8');
  requiredInputs(true)

  let cr = $('#cedula_remitente').val()

  if(cr != undefined && cr.length >= 10)
  {
    if(arrUrl[4] == 'tramites' && arrUrl[5] == 'create') cargarAcuerdo(cr)
    $('#btn_acuerdo').attr('disabled', false)
  }
  else
  {
    $('#btn_acuerdo').attr('disabled', true)
  }

  $('#tipo_documento').change(function() {
    let td = $(this).val()

    if(td == 'RUC') {
      $('#label_remitente').text('Razón social*:')
      $('#label_cedula_remitente').text('Número de RUC*:')
      $('#cedula_remitente').attr('Ingrese número de RUC*')
    }
    else {
      $('#label_remitente').text('Remitente*:')
      $('#label_cedula_remitente').text('Número de cédula*:')
      $('#cedula_remitente').attr('Ingrese número de cédula*')
    }
  })

  $("#position").css("display", "none");

  $('input#cedula_remitente').keypress(function (event) {
    let tipoDocumento = $('#tipo_documento').val()
    if (tipoDocumento == 'CEDULA' && this.value.length === 10) {
      return false;
    }
    else if (tipoDocumento == 'RUC' && this.value.length === 13) {
      return false;
    }
  });

  $('input#telefono').keypress(function (event) {
    if (this.value.length === 10) {
      return false;
    }
  });

  $('input[name="telefono_2"]').keypress(function (event) {
    if (this.value.length === 10) {
      return false;
    }
  });

  let recomendacion = document.getElementById("recomendacion");
  if(recomendacion) recomendacion.rows = 4;

  let observacion = document.getElementById("observacion");
  if(observacion) observacion.rows = 4;

  let direccion_informe = document.getElementById("direccion_informe");
  if(direccion_informe) direccion_informe.rows = 4;

  let obs_general = document.getElementById("observacion_general");
  if(obs_general) obs_general.rows = 4;

  $('#btn_reservar').click(function(event) {
    event.preventDefault()
    let form = $('#form1')
    let area = $('#id_area').val()
    let tipoTramite = $('#tipo_tramite').val()

    if(area == '' || tipoTramite == '')
    {
      Swal.fire({
        title: 'Debe seleccionar un área y tipo de trámite para continuar.!',
        type: 'warning',
        allowOutsideClick: false,
        allowEscapeKey: false,
        confirmButtonText: 'OK'
      })
    }
    else showAlert('reservartramite', form.serialize(), 'tramites')
  })

  $('#btn_guardar').click(function() {
    requiredInputs(true)
    // $('#form1').attr('action', `${URLactual}/tramites/tramites`)
  })

  $("#cedula_remitente").on("keyup", function() {
    let cedula = $(this).val();
    let fecha = $("#fecha_ingreso").val();
    let documento = $("#tipo_documento").val()

    if ((cedula.length === 13 && documento == 'RUC') || (cedula.length <= 10 && documento == 'CEDULA') ) { //&& fecha != undefined
      cargarAcuerdo(cedula);
      $.ajax({
        type: "GET",
        url: `${URLactual}/tramites/consultar_cedula/${cedula}`,
        beforeSend: function() {
          $("#position").css("display", "block");
        },
        error: function(err) {
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        }
      }).done(function(data) {
        $('#btn_acuerdo').attr('disabled', false)
        if (data.ok) {
          if (data.message.id) {
            $("#remitente").val(data.message.name);
            $("#correo_electronico").val(data.message.email);
            $("#telefono").val(data.message.telefono);
            $("#direccion").val(data.message.direccion.toUpperCase());

            if(data.message.referencia != null && data.message.referencia != '')
            {
              $("input[name='referencia']").val(data.message.referencia.toUpperCase());
            }
            if(data.message.parroquia != null)
            {
              obtenerBarrios(data.message.parroquia, data.message.barrio)
              $('#id_parroquia').val(data.message.parroquia).trigger('chosen:updated');
              $('#id_barrio').val(data.message.barrio).trigger('chosen:updated');
            }
          } else {
            if(documento == 'CEDULA')
            {
              $("#remitente").val(data.message.nombre);
            }
            else if(documento == 'RUC')
            {
              $("#remitente").val(data.message.razonSocial);
            }
          }
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        } else {
          toastr["error"](data.message);
          $("#remitente").val("");
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
        }
      });
    }
  });

  $('#submit').click(function(event) {
    event.preventDefault()
    let form = $('#my-dropzone')
  })

  $('#id_parroquia').change(function() {
    let parroquia = $(this).val();

    obtenerBarrios(parroquia);
  })

  $("#cedula_remitente").on("blur", function() {
    let cedula = $("#cedula_remitente").val();
    let fecha = $("#fecha_ingreso").val();
    let tipoDocumento = $('#tipo_documento').val()

    if (tipoDocumento == 'CEDULA' && cedula.length != 10 && fecha != undefined) {
      // $("#cedula_remitente").focus();
      toastr["error"]("Número de cédula no válida.");
    }
  });

  $("#tipo_tramite").on("change", function() {
    let valor = $(this).val();
    let fecha_inicial = $('#fecha_ingreso').val();

    if(valor != '')
    {
      $.ajax({
        type: "GET",
        url: `${URLactual}/tramites/calcular_fecha_despachar/${valor}/${fecha_inicial}`,
        beforeSend: function() {
          $("#position").css("display", "block");
        },
        error: function(err) {
          $("#position").css("display", "block");
          $(".loader").fadeOut("slow");
          $("#fecha_fin").val("");
        }
      }).done(function(data) {
        if (data.ok)
        {
          $('#fecha_fin').addClass('disabled')
          $("#fecha_fin").val(data.fecha_finalizacion);
          if(data.disabled == 'NO')
          {
            // $('#fecha_fin').val('')
            $('#fecha_fin').removeClass('disabled')
          }
        }
        else
        {
          let div = $('#div-fecha_fin')
          let element = `<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" name="fecha_fin" id="fecha_fin" data-placement="top" data-toogle="tooltip" class="form-control datefecha" readonly>`

          // div.append(element);
          $('#fecha_fin').val('')
          $('#fecha_fin').removeClass('disabled')
          $('#fecha_fin').attr('placeholder', 'Ingrese fecha estimada de finalización (YYYY-MM-DD)')
        }
        $("#position").css("display", "block");
        $(".loader").fadeOut("slow");
      });
    }
    else $("#fecha_fin").val('');
  });

  $('#id_area').change(function() {
    let area = $(this)
    let tiposTramite = $('#tipo_tramite');
    let respuesta = $('#obtener_respuesta');
    let correo = $('#correo_electronico').val();

    if(area.val() != '')
    {
      if(area.val() == 32 && correo == '')
      {
        respuesta.find('option').remove()
        respuesta.append('<option value="SI">SI</option>');
        respuesta.append('<option value="NO">NO</option>');
      }
      else if(correo == '')
      {
        respuesta.find('option').remove()
        respuesta.append('<option value="SI">SI</option>');
      }
      $.ajax({
        type: 'GET',
        url: `${URLactual}/tramites/obtenertipostramite/${area.val()}`,
        beforeSend: () => {
          area.prop('disabled', true);
        }
      }).done((data) => {
        if(data.ok) {
          area.prop('disabled', false)
          tiposTramite.find('option').remove();
          tiposTramite.append('<option value="">Escoja opción...</option>');
          $(data.tiposTramite).each(function(i, v) {
              tiposTramite.append('<option value="' + v.id + '">' + v.valor + '</option>');
          })
          tiposTramite.trigger('chosen:updated');

          tiposTramite.prop('disabled', false);
        }
        else
        {
          Swal.fire({
            title: 'No se pueden cargar los tipos de trámite, inténtelo de nuevo!',
            type: 'warning',
            allowOutsideClick: false,
            width: '500px',
            allowEscapeKey: false
          })
        }
      })
    }
    else
    {
      tiposTramite.find('option').remove();
      tiposTramite.append('<option value="">Escoja opción...</option>');
    }
  })

  $('#archivo_finalizacion').change(function(e) {
    let file = e.target.files[0];
    archivo = file
  })

  $('#finalizarImcompleto').click(function(e) {
    e.preventDefault()
    let tramite_id = $('#tramite_id').val()
    let archivo_finalizacion = $('#archivo_finalizacion')

    let formulario = new FormData()
    if(archivo_finalizacion.val() != undefined)
    {
      archivo_finalizacion = archivo_finalizacion[0].files[0]
      formulario.append('archivo_finalizacion', archivo_finalizacion);
    }

    Swal.fire({
      title: '¿Seguro de continuar?',
      text: 'No todos las direcciones asignadas han respondido',
      showCancelButton: true,
      width: '500px',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
      showLoaderOnConfirm: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      preConfirm: () => {
        return new Promise(function(resolve) {
            $.ajax({
              type: 'POST',
              contentType: false,
              data: formulario,
              processData: false,
              cache: false,
              url: `${URLactual}/tramites/finalizarimcompleto/${tramite_id}`,
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            }).done(function(response) {
              if(response.ok) {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                }).then(result => {
                  if(result.value)
                  {
                    if(response.redireccionar != null)
                    {
                      location.href = `${URLactual}${response.redireccionar}`
                    }
                    else location.reload();
                  }
                });
              }
              else {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                })
              }
            })
        })
      }
    })
  })

  $('#archivo').change(function() {
    showIconImage(this)
  })
});

function obtenerBarrios(parroquia, id_barrio = null) {
  let barrio = $('#id_barrio');
  let selected = '';

  if(parroquia == "")
    {
      barrio.find('option').remove();
      barrio.append('<option value="">Escoja opción...</option>');
    }
    else
    {
      if(parroquia == 10)
      {
        $('#label_id_barrio').text('Ciudad*:')
      }
      else $('#label_id_barrio').text('Barrio*:')

      $.ajax({
        type: 'GET',
        url: `${URLactual}/tramites/obtenerbarrios/${parroquia}`
      }).done(function(data){
        barrio.find('option').remove();
        barrio.append('<option value="">Escoja opción...</option>');
        $(data).each(function(i, v) {
            selected = id_barrio == v.id ? 'selected' : '';
            if(parroquia == 10) barrio.append(`<option value="${v.id}" ${selected}>${v.nombre_canton}</option>`);
            // else barrio.append('<option value="' + v.id + '">' + v.barrio + '</option>');
            else barrio.append(`<option value="${v.id}" ${selected}>${v.barrio}</option>`);
            barrio.trigger('chosen:updated');
        })
      });
    }
}

function generarAcuerdo(obj) {
  let cedulaRuc = $('#cedula_remitente').val()
  let remitente = $('#remitente').val()
  let telefono = $('#telefono').val()
  let correo_electronico = $('#correo_electronico').val()

  if(cedulaRuc == '') alerta('Ingrese el número de cédula o RUC')
  else if(remitente == '') alerta('Ingrese el nombre del remitente o Razón Social')
  else if(telefono == '') alerta('Ingrese el número de teléfono')
  else if(correo_electronico == '') alerta('Ingrese el correo electrónico')
  else
  {
    let form = $('#form1')
    let elementURL =`${URLactual}/tramites/ContratoElectronico?`+ form.serialize();
    // window.open(elementURL);
    $.colorbox({iframe: true, href: elementURL, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
  }
}

function cargarAcuerdo(cedula) {
  $.ajax({
      type: 'GET',
      url: `${URLactual}/tramites/cargaracuerdo/${cedula}`
  }).done((data) => {
      if(data.ok) {
          $('#fila_acuerdo').remove();
          let html = `
          <tr id="fila_acuerdo">
              <td>${data.archivo.nombre}</td>
              <td>${data.archivo.created_at}</td>
              <td>
                  <a href="${URLactual}/archivos_sistema/${data.archivo.ruta}" type="button"
                      class="btn btn-success dropdown-toggle divpopup" target="_blank">
                      <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                  </a>
              </td>
              <td>${data.archivo.usuario}</td>
              <td>
                  <i onclick="borrar_acuerdo(${data.archivo.id}, 'fila_acuerdo')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i>
              </td>
          </tr>`;
          $("#tabla_acuerdo tbody").append(html);
          toastr["success"](data.message);
      }
      else {
        $('#fila_acuerdo').remove();
        let html = `<tr id="fila_acuerdo">
        <td></td>
            <td></td>
            <td>
                <form action="{{ route('subiracuerdo.tramite') }}" method="post" enctype="multipart/form-data" id="form-acuerdo">
                    <input type="file" accept="application/pdf" onChange="subirAcuerdo(this, 'fila_acuerdo')" required class="form-control" name="acuerdo" id="acuerdo">
                </form>
            </td>
            <td>${data.usuario}</td>
            <td>
            </td>
        </tr>`;
        $("#tabla_acuerdo tbody").append(html);
      }
  })
}

function alerta(message) {
  Swal.fire({
    title: message,
    allowOutsideClick: false,
    width: '500px',
    allowEscapeKey: false
  })
}

function donacion(tramite_id) {
  let observacion = $('#observacion').val()
  if(observacion == '')
  {
    Swal.fire({
      title: 'Ingrese una observación antes de guardar como donación el trámite',
      allowOutsideClick: false,
      width: '500px',
      allowEscapeKey: false
    })
  }
  else
  {
    showAlert(`enviardonacion/${tramite_id}`, { observacion }, 'tramitesrespondidos');
  }
}

function concejales(tramite_id) {
  showAlert(`tramitesconcejales/${tramite_id}`, { }, 'tramitesconcejales');
}

function devolverVentanilla(tramite_id) {
  showAlertWhitTextArea(`devolverventanilla/${tramite_id}`, 'tramitesrespondidos')
}

function archivarTramiteAnalista(tramite_id) {
  showAlertWhitTextArea(`archivartramiteanalista/${tramite_id}`, 'asignados')
}

function finalizarSubdirector(tramite_id) {
  let observacion = $('#analista_informe').val()
  showAlert(`finalizarsubdirector/${tramite_id}`, { observacion }, 'asignados', 'Su respuesta será enviada al ciudadano');
}

function enviarSecretaria(tramite_id) {
  let observacion = $('#analista_informe').val()
  showAlert(`enviarsecretaria/${tramite_id}`, { observacion }, 'asignados', 'Su respuesta será enviada a secretaría general');
}

function enviarCiudadano(tramite_id, devolver = false) {
  let observacion = $('#observacion').val()

  if(devolver == 'true' && observacion == '')
  {
    Swal.fire({
      title: 'Ingrese una observación antes de devolver el trámite',
      allowOutsideClick: false,
      width: '500px',
      allowEscapeKey: false
    })
  }
  else
  {
    let data = new FormData();
    data.append('observacion', observacion);
    data.append('devolver', devolver);

    Swal.fire({
      title: '¿Seguro de continuar?',
      text: 'Su respuesta será enviada al ciudadano',
      showCancelButton: true,
      width: '500px',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
      showLoaderOnConfirm: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      preConfirm: () => {
        return new Promise(function(resolve) {
            $.ajax({
              type: 'POST',
              url: `${URLactual}/tramites/enviarrespuestaciudadano/${tramite_id}`,
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              data: data,
              cache: false,
              processData: false,
              contentType: false
            }).done(function(response) {
              if(response.ok) {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                }).then(result => {
                  if(result.value)
                  {
                    location.href = `${URLactual}/tramites/tramitesingresados`;
                  }
                });
              }
              else {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                })
              }
            })
        })
      }
    })
  }
}

function archivarTramite(tramite) {
  showAlertWhitTextArea(`archivartramite/${tramite}`, 'tramitesarchivados')
}

function devolverTramiteInforme(tramite, redirect = 'solicitudesinforme') {
  showAlertWhitTextArea(`devolvertramiteinforme/${tramite}`, redirect)
}

function archivarTramiteInforme(tramite) {
  let observacion = $('#direccion_informe').val()
  if(observacion == '')
  {
    Swal.fire({
      title: 'Ingrese una observación antes de archivar el trámite',
      allowOutsideClick: false,
      width: '500px',
      allowEscapeKey: false
    })
  }
  else
  {
    Swal.fire({
      title: '¿Seguro de continuar?',
      text: 'El trámite será archivado y no podrá ser editado nuevamente.',
      showCancelButton: true,
      width: '500px',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
      reverseButtons: true,
      showLoaderOnConfirm: true,
      allowOutsideClick: false,
      allowEscapeKey: false,
      preConfirm: () => {
        return new Promise(function(resolve) {
            $.ajax({
              type: 'POST',
              url: `${URLactual}/tramites/archivartramiteinforme/${tramite}`,
              headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
              data: { observacion }
            }).done(function(response) {
              if(response.ok) {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                }).then(result => {
                  if(result.value)
                  {
                    location.href = `${URLactual}/tramites/tramitesarchivados`;
                  }
                });
              }
              else {
                Swal.fire({
                  title: response.message,
                  allowOutsideClick: false,
                  width: '500px',
                  allowEscapeKey: false
                })
              }
            })
        })
      }
    })
  }
}

function showIconImage(input) {
  //let img = ["jpg", "JPG", "jpeg", "JPEG", "png", "PNG"];
  let img = ['pdf', 'PDF', 'xlsx', 'xls', 'docx'];
  let input_result = $("#" + input.name);
  if (input_result[0].files && input_result[0].files[0]) {
      let fileName = input_result[0].files[0].name;
      let extn = fileName.split('.').pop();
      if (isExtension(extn, img)) {
          let totalByte = input_result[0].files[0].size;
          let totalSize = totalByte / Math.pow(1024, 2);
          let tamanio = 40;
          if (totalSize.toFixed(2) <= tamanio) {
              input_result.next().text(fileName);
              $('#div-icon-' + input.name).show();
          } else {
              input_result.next().text('Seleccione un archivo');
              $('#div-icon-' + input.name).hide();
              input_result.val("");
              swal.fire({
                  title: 'Tamaño máximo superado',
                  text: "Solo se admiten documentos PDF que no superen los " + tamanio + " Megabytes",
                  type: 'error',
                  confirmButtonText: 'Ok',
                  reverseButtons: true
              });
          }
      } else {
          input_result.next().text('Seleccione un archivo');
          $('#div-icon-' + input.name).hide();
          input_result.val("");
          swal.fire({
              title: 'Formato incorrecto',
              text: "Solo se admiten archivos en formato PDF.",
              type: 'error',
              confirmButtonText: 'Ok',
              reverseButtons: true
          });
      }
  } else {
      input_result.next().text('Seleccione un archivo');
      $('#div-icon-' + input.name).hide();
      input_result.val("");
  }
}

function isExtension(ext, extnArray) {
  var result = false;
  var i;
  if (ext) {
      ext = ext.toLowerCase();
      for (i = 0; i < extnArray.length; i++) {
          if (extnArray[i].toLowerCase() === ext) {
              result = true;
              break;
          }
      }
  }
  return result;
}

/**Funciones Coordinadores */
function enviarAprobar(id, area) {
  let parametros = getParameterByName('id')
  showAlert(`enviaraprobacion/${id}`, { parametros, area }, 'tramitesrespondidos');
}

function devolverDirector(id, direccion) {
  showAlertWhitTextArea(`devolverdirector/${id}/${direccion}`, 'reload')
}

/**Funciones Directores */
function validar() {
  let input = document.getElementById('swal-input2')
  input.value = input.value.replace(/[^0-9.]/g, '')
}

function cambiar() {
  let cantidad = $('#swal-input2').val()
  cantidad = cantidad.replace(',', '')
}

function finalizarConPago(id_tramite) {
  Swal.fire({
    title: '¿Seguro de continuar?',
    showCancelButton: true,
    width: '600px',
    html: `<textarea id="swal-input1" class="swal2-textarea" placeholder="Ingrese una observación"></textarea>
          <label for="swal-input2">Valor a pagar</label>
          <input id="swal-input2" class="swal2-input" maxlength="8" oninput="validar()">`,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Continuar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    preConfirm: () => {
      let observacion = $('#swal-input1').val();
      let valorCancelar = $('#swal-input2').val();

      return new Promise(function(resolve) {
          $.ajax({
            type: 'POST',
            url: `${URLactual}/tramites/finalizarconpago/${id_tramite}`,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { observacion, valorCancelar }
          }).done(function(response) {
            if(response.ok) {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                allowEscapeKey: false
              }).then(result => {
                if(result.value)
                {
                  location.href = `${URLactual}/tramites/tramitesingresados`
                }
              });
            }
            else {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                allowEscapeKey: false
              })
            }
          })
      })
    },
    allowOutsideClick: false,
    allowEscapeKey: false
  })
}

function desvincularTramite(id_tramite) {
  showAlertWhitTextArea(`desvinculartramite/${id_tramite}`, 'tramitesrevision')
}

function notificarCoordinador(id, direccion) {
  let observacion = $('#observacion').val()
  showAlert(`notificarcoordinador/${id}/${direccion}`, { observacion }, 'tramitesrespondidos', 'Se notificará al coordinador su requerimiento');
}

function solicitarInforme(id_tramite, analista = 'NO') {
  let direcciones = $('#direcciones').val()
  let observacion = $('#observacion').val()
  $('#modal-direcciones').modal('hide')
  let redirectTo = analista == 'NO' ? 'tramitesenrevision' : 'asignados';

  if(direcciones != null)
  {
    showAlert(`solicitarinforme/${id_tramite}`, {direcciones, observacion}, redirectTo, 'Se notificará a las direcciones seleccionadas su petición', $('#modal-direcciones'))
  }
  else
  {
    Swal.fire({
      title: 'Debe seleccionar al menos una direción.!',
      allowOutsideClick: false,
      width: '500px',
      allowEscapeKey: false
    }).then(val => {
      $('#modal-direcciones').modal('show')
    })
  }
}

function actualizarNumTramiteExterno() {
  let fecha = new Date()
  let dia = fecha.getDate() <= 9 ? `0${fecha.getDate()}` : fecha.getDate()
  let mes = fecha.getMonth() + 1 <= 9 ? `0${fecha.getMonth() + 1}` : fecha.getMonth() + 1
  let anio = fecha.getFullYear()
  let hora = fecha.getHours() <= 9 ? `0${fecha.getHours()}` : fecha.getHours()
  let minutos = fecha.getMinutes() <= 9 ? `0${fecha.getMinutes()}` : fecha.getMinutes()
  let numtramite = `TE${dia}${mes}${anio}${hora}${minutos}`

  $('#numtramite').val(numtramite)
}

function asignarTramiteAnalista(id_tramite) {
  let analistas = $('#analistas').val()
  let observacion = $('#observacion_director').val()
  $('#modal-analistas').modal('hide')

  if(analistas != null)
  {
    showAlert(`asignaranalista/${id_tramite}`, {analistas, observacion}, 'reload', 'Se notificará al analista seleccionado su petición', $('#modal-analistas'))
  }
  else
  {
    Swal.fire({
      title: 'Debe seleccionar a uno o más analistas para asignar el trámite.!',
      allowOutsideClick: false,
      width: '500px',
      allowEscapeKey: false
    }).then(val => {
      $('#modal-analistas').modal('show')
    })
  }
}

function guardarInforme(id, redirect = 'solicitudesinforme') {
  let observacion = $('#direccion_informe').val()
  showAlert(`guardarinforme/${id}`, { observacion }, redirect);
}


function guardaInformeAnalista(id, archivar = 'NO') {
  // if(archivar == 'SI')
  // {
  //   showAlertWhitTextArea(`guardarinformeanalista/${id}?archivar=${archivar}`, 'asignados');
  // }
  // else
  // {
  // }
  let observacion = $('#analista_informe').val()
  showAlert(`guardarinformeanalista/${id}`, { observacion }, 'asignados');
}

function obtenerDirecciones() {
  $('#modal-direcciones').modal();
}

function obtenerAnalistas() {
  $('#modal-analistas').modal();
}

function finalizarTramiteDirector(id, tipo, correo = 'SI') {
  let observacion = $('#observacion').val()

  if(correo == 'NO')
  {
    let msg = 'No se proporcionó un correo electrónico, el trámite finalizará pero deberá notificar al ciudadano de forma física';
    showAlert(`finalizartramitedirector/${id}`, { tipo, observacion }, 'tramitesrespondidos', msg);
  }
  else
  {
    showAlert(`finalizartramitedirector/${id}`, { tipo, observacion }, 'tramitesrespondidos');
  }
}

/**Métodos generales */
function showAlert(url, data, redirectTo, text = '', modal = null, type = 'POST') {
  Swal.fire({
    title: '¿Seguro de continuar?',
    text: text,
    showCancelButton: true,
    width: '500px',
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Continuar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    preConfirm: () => {
      return new Promise(function(resolve) {
          $.ajax({
            type: type,
            data: data,
            url: `${URLactual}/tramites/${url}`,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
          }).done(function(response) {
            if(response.ok) {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                width: '500px',
                allowEscapeKey: false
              }).then(result => {
                if(result.value)
                {
                  if(response.redirect)
                  {
                    location.href = `${URLactual}/tramites/${response.redirect}`
                  }
                  else
                  {
                    if(redirectTo == 'reload') location.reload();
                    else if(redirectTo == 'no-action')
                    {
                      resolve()
                    }
                    else location.href = `${URLactual}/tramites/${redirectTo}`
                  }
                }
              });
            }
            else {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                width: '500px',
                allowEscapeKey: false
              })
            }
          }).then((value) => {
            if(modal != null && value.dismiss == 'cancel') modal.modal('show')
          })
      })
    },
    allowOutsideClick: false,
    allowEscapeKey: false
  })
}

function showAlertWhitTextArea(url, redirectTo, type = 'POST') {
  Swal.fire({
    title: '¿Seguro de continuar?',
    showCancelButton: true,
    width: '600px',
    input: 'textarea',
    inputPlaceholder: 'Ingrese observación',
    inputAttributes: {
        'aria-label': 'Ingrese observación'
    },
    inputValidator: (value) => {
        return new Promise((resolve) => {
            value.length > 0 ? resolve() : resolve('Debe ingresar una observación');
        });
    },
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Continuar',
    cancelButtonText: 'Cancelar',
    reverseButtons: true,
    showLoaderOnConfirm: true,
    preConfirm: () => {
      let observacion = $('.swal2-textarea').val();
      return new Promise(function(resolve) {
          $.ajax({
            type: type,
            url: `${URLactual}/tramites/${url}`,
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data: { observacion }
          }).done(function(response) {
            if(response.ok) {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                allowEscapeKey: false
              }).then(result => {
                if(result.value)
                {
                  if(redirectTo == 'reload') location.reload()
                  else location.href = `${URLactual}/tramites/${redirectTo}`
                }
              });
            }
            else {
              Swal.fire({
                title: response.message,
                allowOutsideClick: false,
                allowEscapeKey: false
              })
            }
          })
      })
    },
    allowOutsideClick: false,
    allowEscapeKey: false
  })
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function calcularEdad(fecha) {
  var values = fecha.split("-");
  var dia = values[2];
  var mes = values[1];
  var ano = values[0];

  var fecha_hoy = new Date();
  var ahora_ano = fecha_hoy.getYear();
  var ahora_mes = fecha_hoy.getMonth() + 1;
  var ahora_dia = fecha_hoy.getDate();

  var edad = (ahora_ano + 1900) - ano;
  if (ahora_mes < mes) {
      edad--;
  }
  if ((mes == ahora_mes) && (ahora_dia < dia)) {
      edad--;
  }
  if (edad > 1900) {
      edad -= 1900;
  }

  var meses = 0;

  if (ahora_mes > mes && dia > ahora_dia)
      meses = ahora_mes - mes - 1;
  else if (ahora_mes > mes)
      meses = ahora_mes - mes
  if (ahora_mes < mes && dia < ahora_dia)
      meses = 12 - (mes - ahora_mes);
  else if (ahora_mes < mes)
      meses = 12 - (mes - ahora_mes + 1);
  if (ahora_mes == mes && dia > ahora_dia)
      meses = 11;

  var dias = 0;
  if (ahora_dia > dia)
      dias = ahora_dia - dia;
  if (ahora_dia < dia) {
      ultimoDiaMes = new Date(ahora_ano, ahora_mes - 1, 0);
      dias = ultimoDiaMes.getDate() - (dia - ahora_dia);
  }

  return edad;
}

function requiredInputs(valor) {
  $('#tipo_tramite').attr('required', valor);
  $('#asunto').attr('required', valor);
  $('#peticion').attr('required', valor);
  $('#cedula_remitente').attr('required', valor);
  // $('#remitente').attr('required', valor);
  $('#telefono').attr('required', valor);
  $('#id_parroquia').attr('required', valor);
  $('#id_barrio').attr('required', valor);
  $('#direccion').attr('required', valor);
  $('#prioridad').attr('required', valor);
  // $('#direccion_atender').attr('required', valor);
  $('#fecha_fin').attr('readonly', false);
}
