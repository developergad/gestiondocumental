var URLdomain = window.location.host;
var protocol = window.location.protocol;
var URLactual;
let posicion = URLdomain.indexOf('localhost');

var arrUrl = window.location.href.split("/");
let posicion_ = URLdomain.indexOf('194');
let posicionPuerto = URLdomain.indexOf(':80');

if (posicion !== -1) URLactual = `${protocol}//${URLdomain}/gestiondocumental/public`;
else if (posicion_ !== -1) URLactual = `${protocol}//${URLdomain}/gestiondocumental/public`;
else if (posicionPuerto !== -1) URLactual = `${protocol}//${URLdomain}`;
else URLactual = `${protocol}//${URLdomain}`;
// console.log(URLactual);

$(document).ready(function() {
    // console.log(arrUrl[6]);

    if (arrUrl[5] == 'create' || arrUrl[5] == 'edit' || arrUrl[6] == 'edit') {
        cargarParaCopia();
    }


    // $("#numtramite_externo").blur(function(e) {
    //     $.ajax({
    //         type: "GET",
    //         url: "/validarTramiteExterno",
    //         data: {
    //             numtramite: $("#numtramite_externo").val()
    //         },
    //         success: function(response) {
    //             if (response.estado == false) {
    //                 $("#numtramite_externo").focus();
    //                 $("#numtramite_externo").val('');
    //                 toastr["error"]("No se encuentra el número de trámite ingresado");
    //             }

    //         }
    //     });

    // });

    $('#btn_guardar').click(function(e) {
        e.preventDefault()
        let form = $('#form')

        Swal.fire({
            title: '¿Seguro de continuar?',
            type: 'question',
            showCancelButton: true,
            width: '500px',
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continuar',
            cancelButtonText: 'Cancelar',
            reverseButtons: true,
        }).then((result) => {
            if (result.value) {
                $(this).attr('disabled', true)
                form.submit()
            }
        })
    })

    $('.input-group.datetime').datetimepicker({
        format: 'YYYY-MM-DD HH:mm',
        locale: 'es',
        sideBySide: true,
        // ignoreReadonly: true
    }).on('keypress paste', function(e) {
        // alert('NOOOO');
        e.preventDefault();
        return false;
    });
    var nextElem = $('.input-group.datetime');
    var fecha_inicio = new Date();
    // console.log(fecha_inicio.format("yyyy-MM-dd hh:mm"));

    if (nextElem.length > 0) {
        if ($("#fecha").val() == "") {
            // modifico_fecha
            nextElem.data("DateTimePicker").date(new Date())
            $.ajax({
                type: "GET",
                url: `${URLactual}/tramites/getDate`,
                success: function(response) {
                    var fecha = new Date();
                    let dia = fecha.getDate() < 9 ? `0${fecha.getDate()}` : fecha.getDate()
                    let mes = fecha.getMonth() + 1 < 9 ? `0${fecha.getMonth() + 1}` : fecha.getMonth() + 1
                    let anio = fecha.getFullYear()
                    let hora = fecha.getHours()
                    let minutos = fecha.getMinutes() < 9 ? `0${fecha.getMinutes()}` : fecha.getMinutes();
                    var fecha_nv = anio + '-' + mes + '-' + dia + ' ' + hora + ':' + minutos

                    console.log(fecha_nv);
                    console.log(response);
                    if (fecha_nv != response) {
                        console.log('si');
                        $("#modifico_fecha").val('SI');
                    } else {
                        $("#modifico_fecha").val('NO');
                    }
                }
            });
        }
    }

    $("#fecha").attr('readonly', true);





    $("#btn_firmar").click(function(e) {
        firmar();
    });
    $("#btn_firmarcorreo").click(function(e) {
        generarCodigo();
    });
    $("#btn_validar_documento").click(function(e) {
        validarDocumento();
    });

    $('#div_numtramite_externo').hide()
    $('#div_nombre_informe').hide()

    $('#tipo_tramite_interno').change(function() {
        let tipo_tramite = $(this).val()
        let memo = $('#div_memo_tramite')
        let oficio = $('#div_oficio_tramite')

        if (tipo_tramite == 'MEM' || tipo_tramite == 'INF') {
            memo.fadeIn()
            oficio.fadeOut()

        } else {
            // memo.fadeOut()
            oficio.fadeIn()
        }
        if (tipo_tramite != 'INF') {
            $('#div_numtramite_externo').hide()
            $('#div_nombre_informe').hide()
        } else {
            $('#div_numtramite_externo').show()
            $('#div_nombre_informe').show()
        }
        actualizarNumTramite();
    });
    let tipo_tramite = $('#tipo_tramite_interno').val()
    let memo = $('#div_memo_tramite')
    let oficio = $('#div_oficio_tramite')

    if (tipo_tramite == 'MEM' || tipo_tramite == 'INF') {
        memo.fadeIn()
        oficio.fadeOut()
        if (tipo_tramite == 'INF') {
            $('#div_numtramite_externo').show()
            $('#div_nombre_informe').show()
        }

    } else {
        // memo.fadeOut()
        oficio.fadeIn()
    }
    $("#btn_firmarcorreo").hide();
    $("input[name='tipo_firma']").click(function() {
        var radioValue = $("input[name='tipo_firma']:checked").val();

        if (radioValue) {
            if (radioValue == 'TOKEN') {
                $("#btn_firmar").show();
                $("#btn_firmarcorreo").hide();
            } else {
                $("#btn_firmar").hide();
                $("#btn_firmarcorreo").show();
            }
            $("#documento").val('');
        }
    });

    $("input[name='tipo_envio']").click(function() {
        tipo_envio();
    });

    // $("input[type='radio']").trigger('click');
    $("#btn_guardar").attr('disabled', true);

    $("#btn_ver_borrador").click(function(e) {
        firmar(3)
    });
    tipo_envio();

    let tramites_numero = $("#numtramite").val();
    if (tramites_numero == "" || tramites_numero == null || tramites_numero == undefined) {}
    actualizarNumTramite();
})

function tipo_envio() {
    var radioValue = $("input[name='tipo_envio']:checked").val();

    if (radioValue) {
        if (radioValue == 'BORRADOR') {
            // alert(radioValue);
            cambiarRemitente('NO')
            $("#btn_guardar").attr('disabled', false);
        } else if (radioValue == 'BORRADOR_DIRECTOR') {
            cambiarRemitente('SI')
            $("#btn_guardar").attr('disabled', false);
        } else {
            cambiarRemitente('NO')
            $("#btn_guardar").attr('disabled', true);;
        }
    }
}

function cargarParaCopia() {
    let tipo_tramite = $('#tipo_tramite_interno').val()

    let para = $('#delegados_para').val() != '' ? JSON.parse($('#delegados_para').val()) : ''
    let lista = $('#para_lista')

    let copia = $('#delegados_copia').val() != '' ? JSON.parse($('#delegados_copia').val()) : ''
    let lista_copia = $('#copia_lista')

    let memo = $('#div_memo_tramite')
    let oficio = $('#div_oficio_tramite')

    if (tipo_tramite == 'OFI') {
        // memo.fadeOut()
        oficio.fadeIn()

        if (para != '') {
            $.each(para, function(i, v) {
                // lista.append(`<li id="li_${i+1}">${v.titulo} ${v.nombre} - ${v.cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${i+1}')">x</button></li>`)
                lista.append(`<li id="li_${i + 1}_para">${v.titulo} ${v.nombre} - ${v.cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${i + 1}_para', 'para',${i + 1})">x</button></li>`)
            })
        }

        if (copia != '') {
            $.each(copia, function(i, v) {
                // lista_copia.append(`<li id="li_${i+1}_copia">${v.titulo} ${v.nombre} - ${v.cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${i+1}_copia')">x</button></li>`)
                lista_copia.append(`<li id="li_${i + 1}_copia" >${v.titulo} ${v.nombre} - ${v.cargo}<button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${i + 1}_copia', 'copia',${i + 1})">x</button></li>`)
            })
        }
    }
}

function eliminarBorrador(id) {
    let data = new FormData();
    let csrf_token = document.querySelector('meta[name=csrf-token]').content
    data.append('id', id);
    data.append("_token", csrf_token);

    Swal.fire({
        title: 'Está seguro en realizar esta acción?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, borrar!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: "POST",
                url: URLactual + "/tramites/eliminarBorrador",
                data: data,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                cache: false,
                statusCode: {
                    404: function() {
                        toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                    },
                    401: function() {
                        toastr["error"]("Inicie sesión de nuevo por favor... :(");
                    }
                },
                beforeSend: function(response) {},
                success: function(response) {
                    if (response.estado) {
                        toastr["success"](response.msg);
                        try {
                            tabla.draw();
                        } catch (error) {
                            console.log(error);
                            location.reload(true);

                        }
                    } else {
                        toastr["error"](response.msg);
                    }
                },
                error: function(param) {
                    toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                }
            });

        }
    })
}

function agregarPara() {
    let titulo = $('#titulo').val()
    let nombre = $('#nombres').val()
    let cargo = $('#cargo').val()
    let institucion = $('#institucion').val()
    let para = $('#delegados_para')
    let lista = $('#para_lista')

    if (titulo == '' || nombre == '' || cargo == '' || institucion == '') {
        toastr["error"]("Complete los campos");
    } else {
        let dataInput = para.val() == '' ? [] : JSON.parse(para.val());
        dataInput.push({ titulo, nombre, cargo, institucion })
        para.val(JSON.stringify(dataInput))
        let contador = JSON.parse(para.val())

        // lista.append(`<li id="li_${contador.length}_para">${titulo} ${nombre} - ${cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${contador.length}_para', 'para')">x</button></li>`)
        lista.append(`<li id="li_${contador.length}_para">${titulo} ${nombre} - ${cargo}  - ${institucion}<button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${contador.length}_para', 'para',${contador.length})">x</button></li>`)
        limpiarCampos('para')
    }
}

function agregarCopia() {
    let titulo = $('#titulo_copia').val()
    let nombre = $('#nombres_copia').val()
    let cargo = $('#cargo_copia').val()
    let institucion = $('#institucion_copia').val()
    let copia = $('#delegados_copia')
    let lista = $('#copia_lista')

    if (titulo == '' || nombre == '' || cargo == '' || institucion == '') {
        toastr["error"]("Complete los campos");
    } else {
        let dataInput = copia.val() == '' ? [] : JSON.parse(copia.val());
        dataInput.push({ titulo, nombre, cargo, institucion })
        copia.val(JSON.stringify(dataInput))
        let contador = JSON.parse(copia.val())
            // lista.append(`<li id="li_${contador.length}_copia">${titulo} ${nombre} - ${cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${contador.length}_copia', 'copia')">x</button></li>`)
        lista.append(`<li id="li_${contador.length}_copia">${titulo} ${nombre} - ${cargo} <button type="button" class="btn btn-xs btn-primary" onclick="quitarLI('li_${contador.length}_copia', 'copia',${contador.length})">x</button></li>`)
        limpiarCampos('copia')
    }
}

function quitarLI(id, tipo, index) {
    let nuevo_array = [];
    if (tipo == 'para') {
        let para = JSON.parse($('#delegados_para').val())
        $.each(para, function(indexInArray, valueOfElement) {

            console.log('-------------------');
            console.log(index);
            console.log(indexInArray);
            console.log(valueOfElement);
            console.log('-------------------');

            if (index != indexInArray + 1) {
                nuevo_array[indexInArray] = valueOfElement;
            }
        });
        // let nuevo = para.splice(index, index);
        $('#delegados_para').val(JSON.stringify(nuevo_array))
        console.log($('#delegados_para').val());
    } else {
        let copia = JSON.parse($('#delegados_copia').val())
            // console.log(copia);
        $.each(copia, function(indexInArray, valueOfElement) {
            console.log('-------------------');
            console.log(index);
            console.log(indexInArray);
            console.log(valueOfElement);
            console.log('-------------------');
            if (index != indexInArray + 1) {
                nuevo_array[indexInArray] = valueOfElement
            }
        });
        // let nuevo = copia.splice(index, index);
        $('#delegados_copia').val(JSON.stringify(nuevo_array))
        console.log($('#delegados_copia').val());
    }
    $(`#${id}`).remove();
}

function limpiarCampos(tipo) {
    if (tipo == 'para') {
        $('#titulo').val('')
        $('#nombres').val('')
        $('#cargo').val(null)
        $('#institucion').val('')
    } else {
        $('#titulo_copia').val('')
        $('#nombres_copia').val('')
        $('#cargo_copia').val(null)
        $('#institucion_copia').val('')
    }
}

function archivarTramiteInterno(id) {
    Swal.fire({
        title: '¿Desea archivar el trámite?',
        showCancelButton: true,
        width: '500px',
        input: 'textarea',
        inputPlaceholder: 'Ingrese observación',
        inputAttributes: {
            'aria-label': 'Ingrese observación'
        },
        inputValidator: (value) => {
            return new Promise((resolve) => {
                value.length > 0 ? resolve() : resolve('Debe ingresar una observación');
            });
        },
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cancelar',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        preConfirm: () => {
            let observacion = $('.swal2-textarea').val();
            return new Promise(function(resolve) {
                $.ajax({
                    type: 'POST',
                    url: `${URLactual}/tramites/archivartramiteinterno/${id}`,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { observacion }
                }).done(function(response) {
                    if (response.ok) {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then(result => {
                            if (result.value) {
                                tablaTramitesIntenos.draw()
                                    // location.reload()
                            }
                        });
                    } else {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }
                })
            })
        }
    })
}

function obtenerAnalistas() {
    $('#modal-analistas').modal();

    // $('#modal-analistas').on('shown.bs.modal', function () {
    //     $('#id_tramite').val(id_tramite)
    // })
}

function asignarTramiteAnalista(id_tramite) {
    // let id_tramite = $('#id_tramite').val()
    let analistas = $('#analistas').val()
    let observacion = $('#observacion_director').val()
    $('#modal-analistas').modal('hide')

    if (analistas != null) {
        showAlert(`asignaranalistainternos/${id_tramite}`, { analistas, observacion }, 'reload', 'Se notificará al analista seleccionado su petición', $('#modal-analistas'))
    } else {
        Swal.fire({
            title: 'Debe seleccionar a uno o más analistas para asignar el trámite.!',
            allowOutsideClick: false,
            width: '500px',
            allowEscapeKey: false
        }).then(val => {
            $('#modal-analistas').modal('show')
        })
    }
}

function desvincularTramiteInterno(id) {
    showAlertWhitTextArea(`desvinculartramiteinterno/${id}`, 'tramitesinternos/recibidos')
}

function firmar(tipo = 1, codigo = null) {
    let numtramite = $("#numtramite").val();

    let de = $("#de").val();
    let elaboradoPor = $("#elaborado_por").val();
    let enviarDirector = $('#enviar_director').val()
    let para = $("#asignados").val();
    let copia = $("#copia").val();
    let asunto = $("#asunto").val();
    let peticion = $("#peticion").val();
    let prioridad = $("#prioridad").val();
    let anexos_json = $('#anexos_json').val();
    let fecha = $('#fecha').val();
    let tipo_tramite_interno = $('#tipo_tramite_interno').val();
    let tipo_autenticacion = $('#tipo_autenticacion').val();
    let numtramite_externo = $('#numtramite_externo').val();
    let nombre_informe = $('#nombre_informe').val();
    if (de == '' || para == '' || asunto == '' || peticion == '' || prioridad == '') {
        $("#btn_guardar").attr('disabled', true);
        toastr["error"]("Complete todos los campos por favor");
        return false;
    }

    let data = new FormData();
    let csrf_token = document.querySelector('meta[name=csrf-token]').content
    data.append('numtramite', numtramite);
    data.append("_token", csrf_token);
    data.append('de', de);
    data.append('elaborado_por', elaboradoPor);
    data.append('enviar_director', enviarDirector);
    data.append('para', para);
    data.append('copia', copia);
    data.append('asunto', asunto);
    data.append('peticion', peticion);
    data.append('prioridad', prioridad);
    data.append('anexos_json', anexos_json);
    data.append('tipo_tramite_interno', tipo_tramite_interno);
    data.append('tipo', tipo);
    data.append('codigo', codigo);
    data.append('fecha', fecha);
    data.append('tipo_firma', tipo_autenticacion);
    data.append('numtramite_externo', numtramite_externo);
    data.append('nombre_informe', nombre_informe);

    let delegados_para = $('#delegados_para').val();
    let delegados_copia = $('#delegados_copia').val();
    if (tipo_tramite_interno == 'OFI') {
        data.append('delegados_para', delegados_para);
        data.append('delegados_copia', delegados_copia);
    }

    $.ajax({
        type: "POST",
        url: URLactual + "/tramites/generarDocumento",
        data: data,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        cache: false,
        statusCode: {
            404: function() {
                toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                $("#btn_firmar").attr('disabled', false);
                $("#documento").val('');
            },
            401: function() {
                toastr["error"]("Inicie sesión de nuevo por favor... :(");
                $("#btn_firmar").attr('disabled', false);
                $("#documento").val('');
            }
        },
        beforeSend: function(response) {
            $("#btn_firmar").attr('disabled', true);
            $("#documento").val('');
            $("#gif_loader").show();
        },
        success: function(response) {
            $("#gif_loader").hide();
            if (response.estado) {
                if (tipo == 1) {
                    botonesFirmar();
                    window.open(response.msg + '?v=' + Math.random());
                }
                if (tipo == 3) {
                    window.open(response.msg + '?v=' + Math.random());
                    $("#btn_firmar").attr('disabled', false);
                } else {
                    botonesFirmar();
                    toastr["success"]('Documento generardo correctamente');
                }
                $("#documento").val(response.documento);
            } else {
                $("#btn_firmar").attr('disabled', false);
                toastr["error"](response.msg);
                if (tipo == 2) {

                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: "btn btn-success",
                            cancelButton: "btn btn-danger",
                            popup: "sweet_popupImportant"

                        },
                        buttonsStyling: false
                    });

                    swalWithBootstrapButtons.fire({
                        title: "INGRESE EL CÓDIGO DE VALIDACIÓN",
                        input: 'text',
                        inputAttributes: {
                            autocapitalize: 'off'
                        },
                        showCancelButton: true,
                        confirmButtonText: "Generar documento",
                        cancelButtonText: "No, cancelar",
                        showLoaderOnConfirm: true,
                        preConfirm: (obs) => {
                            if (obs) {
                                firmar(tipo = 2, codigo = obs)
                            }
                        }
                    });

                }
                $("#documento").val('');
            }


        },
        error: function(param) {
            $("#gif_loader").hide();
            $("#btn_firmar").attr('disabled', false);
            $("#documento").val('');
            toastr["error"]("Si el texto fue copiado de un documento de Word por favor dar click derecho, luego seleccionar pegar como texto sin formato, luego darle el formato correspondiente con el editor web de esta página.");
        }
    });
}

function botonesFirmar() {
    $("#btn_validar_documento").attr('disabled', false);
    $("#btn_firmar").attr('disabled', false);
}

function generarCodigo() {
    let data = new FormData();
    let numtramite = $("#numtramite").val();
    let csrf_token = document.querySelector('meta[name=csrf-token]').content
    data.append('numtramite', numtramite);
    data.append("_token", csrf_token);
    $.ajax({
        type: "POST",
        url: URLactual + "/tramites/generarCodigo",
        data: data,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        cache: false,
        statusCode: {
            404: function() {
                toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                $("#btn_firmar").attr('disabled', false);
            },
            401: function() {
                toastr["error"]("Inicie sesión de nuevo por favor... :(");
                $("#btn_firmar").attr('disabled', false);
            }
        },
        beforeSend: function(response) {
            $("#btn_firmar").attr('disabled', true);
            $("#gif_loader").show();
        },
        success: function(response) {
            $("#gif_loader").hide();
            if (response.estado) {

                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: "btn btn-success",
                        cancelButton: "btn btn-danger",
                        popup: "sweet_popupImportant"

                    },
                    buttonsStyling: false
                });

                swalWithBootstrapButtons.fire({
                    title: "INGRESE EL CÓDIGO DE VALIDACIÓN ENVIADO A SU CORREO INSTITUCIONAL",
                    text: response.msg,
                    input: 'text',
                    inputAttributes: {
                        autocapitalize: 'off'
                    },
                    showCancelButton: true,
                    confirmButtonText: "Generar documento",
                    cancelButtonText: "No, cancelar",
                    showLoaderOnConfirm: true,
                    preConfirm: (obs) => {
                        if (obs) {
                            firmar(tipo = 2, codigo = obs)
                        } else {

                        }
                    }
                });

            } else {
                toastr["error"](response.msg);
            }
        },
        error: function(param) {
            $("#btn_firmar").attr('disabled', false);
            $("#gif_loader").hide();
        }
    });
}

function validarDocumento() {
    let documento = $("#documento").val();
    if (documento == "") {
        toastr["error"]("Falta el campo documento, por favor firmar.");
    } else {
        $.ajax({
            type: "GET",
            url: URLactual + "/tramites/validarDocumento?documento=" + documento,
            data: {

            },
            statusCode: {
                404: function() {
                    toastr["error"]("Se produjo un error página no encontrada, disculpe las molestias... :(");
                    $("#btn_guardar").attr('disabled', true);
                },
                401: function() {
                    toastr["error"]("Inicie sesión de nuevo por favor... :(");
                    $("#btn_guardar").attr('disabled', true);
                }
            },
            beforeSend: function(response) {
                $("#btn_guardar").attr('disabled', true);
            },
            success: function(response) {
                if (response.estado) {
                    $("#btn_guardar").attr('disabled', false);
                    $("#documento").val(response.documento);
                    const swalWithBootstrapButtons = Swal.mixin({
                        customClass: {
                            confirmButton: "btn btn-success",
                            cancelButton: "btn btn-danger",
                            popup: "sweet_popupImportant"

                        },
                        buttonsStyling: false
                    });

                    swalWithBootstrapButtons.fire({
                        title: "¿Deséa ver el documento firmado?",
                        showCancelButton: true,
                        confirmButtonText: "Ver",
                        cancelButtonText: "No, cancelar",
                        showLoaderOnConfirm: true,
                        preConfirm: (obs) => {
                            if (obs) {
                                window.open(response.ruta + '?v=' + Math.random());
                            }
                        }
                    });

                } else {
                    $("#btn_guardar").attr('disabled', true);
                    toastr["error"](response.msg);
                }
            },
            error: function(param) {
                $("#btn_guardar").attr('disabled', false);
            }
        });
    }


}

function descargarFormatoAutorizacion(params) {
    alert('SS');

}

function actualizarNumTramite() {
    let fecha = new Date()
    let dia = fecha.getDate() <= 9 ? `0${fecha.getDate()}` : fecha.getDate()
    let mes = fecha.getMonth() + 1 <= 9 ? `0${fecha.getMonth() + 1}` : fecha.getMonth() + 1
    let anio = fecha.getFullYear()
    let hora = fecha.getHours() <= 9 ? `0${fecha.getHours()}` : fecha.getHours()
    let minutos = fecha.getMinutes() <= 9 ? `0${fecha.getMinutes()}` : fecha.getMinutes()
    let tipo_tramite = $('#tipo_tramite_interno').val();

    var nextElem = $('.input-group.datetime');
    if (nextElem.length > 0) {
        nextElem.data("DateTimePicker").date(new Date())
    }
    $.get(URLactual + "/tramites/consultardireccion", null,
        function(data, textStatus, jqXHR) {
            let numtramite = `MTA-${data.mta}-${tipo_tramite}-${dia}${mes}${anio}${hora}${minutos}`
            $('#numtramite').val(numtramite)
        }
    );
    // console.log(numtramite);
}




function showAlertWhitTextArea(url, redirectTo, type = 'POST') {
    Swal.fire({
        title: '¿Seguro de continuar?',
        showCancelButton: true,
        width: '600px',
        input: 'textarea',
        inputPlaceholder: 'Ingrese observación',
        inputAttributes: {
            'aria-label': 'Ingrese observación'
        },
        inputValidator: (value) => {
            return new Promise((resolve) => {
                value.length > 0 ? resolve() : resolve('Debe ingresar una observación');
            });
        },
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            let observacion = $('.swal2-textarea').val();
            return new Promise(function(resolve) {
                $.ajax({
                    type: type,
                    url: `${URLactual}/tramites/${url}`,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { observacion }
                }).done(function(response) {
                    if (response.ok) {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        }).then(result => {
                            if (result.value) {
                                if (redirectTo == 'reload') location.reload()
                                else location.href = `${URLactual}/tramites/${redirectTo}`
                            }
                        });
                    } else {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            allowEscapeKey: false
                        })
                    }
                })
            })
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    })
}

function showAlert(url, data, redirectTo, text = '', modal = null, type = 'POST') {
    Swal.fire({
        title: '¿Seguro de continuar?',
        text: text,
        showCancelButton: true,
        width: '500px',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Continuar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {
            return new Promise(function(resolve) {
                $.ajax({
                    type: type,
                    data: data,
                    url: `${URLactual}/tramites/${url}`,
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                }).done(function(response) {
                    if (response.ok) {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            width: '500px',
                            allowEscapeKey: false
                        }).then(result => {
                            if (result.value) {
                                if (response.redirect) {
                                    location.href = `${URLactual}/tramites/${response.redirect}`
                                } else {
                                    if (redirectTo == 'reload') location.reload();
                                    else location.href = `${URLactual}/tramites/${redirectTo}`
                                }
                            }
                        });
                    } else {
                        Swal.fire({
                            title: response.message,
                            allowOutsideClick: false,
                            width: '500px',
                            allowEscapeKey: false
                        })
                    }
                }).then((value) => {
                    if (modal != null && value.dismiss == 'cancel') modal.modal('show')
                })
            })
        },
        allowOutsideClick: false,
        allowEscapeKey: false
    })
}


function cambiarRemitente(director) {
    if (director == 'SI') {
        $('#btn_firmar').attr('disabled', true)
        $('#btn_firmarcorreo').attr('disabled', true)
        $('#enviar_director').val(director)
    } else {
        $('#btn_firmar').attr('disabled', false)
        $('#btn_firmarcorreo').attr('disabled', false)
    }

    let asignados = $('#asignados');
    let copia = $('#copia');

    $.ajax({
        type: 'GET',
        url: `${URLactual}/tramites/cambiarremitente?director=${director}`
        // beforeSend: () => {
        //     asignados.find('option').remove();
        //     copia.find('option').remove();
        // }
    }).done((data) => {
        $('#de').val(data.remitente)
        // $(data.remitentes).each(function(i, v) {
        //     asignados.append(`<option value="${v.id}">${v.name}</option>`);
        //     asignados.trigger('chosen:updated');
        //     copia.append(`<option value="${v.id}">${v.name}</option>`);
        //     copia.trigger('chosen:updated');
        // })
    })
}
