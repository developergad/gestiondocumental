<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title> MEMORANDO </title>
    {!! Html::style('/css/memo.css') !!}

    {{-- {!! HTML::style('css/bootstrap4/bootstrap.min.css') !!} --}}
</head>
<style>
    * {
        margin: 0;
        padding: 0;
    }


    #body_todo_2 {
        background-repeat: no-repeat;
        background-size: cover;
        font-family: Helvetica;
        /* position: absolute; */

    }

    #footer {
        position: fixed;
        left: 0px;
        bottom: -180px;
        right: 0px;
        height: 150px;
        /* background-color: lightblue; */
    }

    #footer .page:after {
        content: counter(page, upper-roman);
    }

    .footer {
        position: fixed;
        bottom: 2%;
        width: 100%;
        /* left: 100px; */
        font-size: 10;
        padding-left: 35px;
        padding-right: 200px;
        padding-bottom: 2px !important;
        padding-top: 5px !important;
    }

    @page {
        margin: 0px 90px 120px 90px !important;
        padding: 0;
        /* background-image: url("../img/memo.png"); */
    }

    body {
        font-family: Helvetica;
    }

    .alinedado {
        text-align: right;
    }

    .centrado {
        text-align: center;
    }

    header {
        position: fixed;
        /* top: -200px; */
        left: 0;
        right: 0;
        /* height: 200px; */
        /* padding: 10px 50px; */
        /* background-color: #ccc; */
        /* border-bottom: 1px solid #1f1f1f; */
        z-index: -900;
    }

    #principal_div_informe {
        width: 100%;
        height: 80%;
        position: relative;
        top: 18%;
        /*border: 5px solid red;*/
    }


    <?php if ($datos->borrador !=3) {
        echo '#overlay {
position: fixed;
        top: 5px;
        bottom: 0px;
        left: 0;
        width: 18.9cm;
        height: 29.5cm;
        z-index: -1000;
    }

    ';

    }

    else {
        echo '#overlay {
position: fixed;
        bottom: 0px;
        left: 0;
        top: 5px;
        width: 18.9cm;
        height: 29.5cm;
        z-index: -1000;
    }

    ';

    }

    ?>
</style>

<body>
    <div id="overlay">
        @if ($datos->borrador==3)
        @if($datos->tipo=='INFORME')
        <img src="{{asset('img/borrador_informe.png')}}" height="100%" width="100%" />
        @else
        <img src="{{asset('img/borrador2.png')}}" height="100%" width="100%" />
        @endif
        @else
        @if($datos->tipo=='INFORME')
        <img src="{{asset('img/informe.png')}}" height="100%" width="100%" />
        @else
        <img src="{{asset('img/memo.png')}}" height="100%" width="100%" />
        @endif
        @endif
    </div>


                @if($datos->tipo=='INFORME')
                    <header>
                        <table class="table table-bordered"
                            style="z-index: -900; margin-bottom: 0px; margin-top: 20px; border-collapse: collapse; font-size: 10px;  ">
                            <tbody>
                                <tr>
                                    <td class="bordeada" style="width: 40%; text-align: center;"> <img
                                            src="{{asset('img/mantafirmes.png')}}" height="25px" style="padding: 8px;" width="150px" />
                                    </td>
                                    <td class="bordeada" colspan="2" style="color: #012958;"> <b>{{$datos->user->direccion}}<b></td>
                                </tr>
                                <tr>
                                    <td class="bordeada" style="width: 60%;" colspan="2">{{$datos->numtramite}}</td>
                                    <td class="bordeada">
                                        {{(isset($datos->numtramite_externo))? 'Trámite No.: '.$datos->numtramite_externo :''}} </td>
                                </tr>
                                <tr>
                                    <td class="bordeada" style="width: 60%;" colspan="2">INFORME DE {{$datos->nombre_informe}}</td>
                                    <td class="bordeada"></td>
                                </tr>
                            </tbody>

                        </table>
                    </header>

                    <main>
                    <div id="principal_div_informe">

                @else
                    <main>
                    <div id="principal_div">
                @endif


                    <main>
                    @if ($datos->tipo=='OFICIO')
                        <p class="separador_3">
                            <b>Oficio Nro. {{$datos->numtramite}} </b><br>
                            Manta, {{(isset($datos->fecha))?fechas(600,$datos->fecha):fechas(600)}}
                        </p>
                        <br>
                        <br>
                        <br>
                    @elseif($datos->tipo=='MEMORANDO')
                        <p class="separador_2">
                            <b style="font-size: 18px;">{{$datos->tipo}}</b>
                            <br>
                            <b>{{$datos->numtramite}}</b>
                            <br>
                            <br>
                        </p>
                    @else

                    @endif


                    <div class="justificado texto">
                        <table id="tabla_establecimiento">
                            @if ($datos->tipo=='OFICIO')
                            @if(count($datos->para)>=6)
                            <tr>
                                <td style="font-size: 13px" class="td_datos"><b>ASUNTO:</b>
                                    &nbsp;&nbsp;{{$datos->asunto}}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px" class="td_datos">

                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key<=10) { echo $item->titulo.'<br>'.$item->name.' <br>
                                        <b>'.strtoupper($item->cargo).'</b><br>
                                        <b>'.strtoupper($item->institucion).'</b><br><br>';
                                        ;
                                        }
                                        @endphp
                                        @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px" class="td_datos">

                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key>10 && $key<=20) { echo $item->titulo.'<br>'.$item->name.' <br>
                                        <b>'.strtoupper($item->cargo).'</b><br>
                                        <b>'.strtoupper($item->institucion).'</b><br><br>';
                                        ;
                                        }
                                        @endphp
                                        @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 11px" class="td_datos">

                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key>20) { echo $item->titulo.'<br>'.$item->name.' <br>
                                    <b>'.strtoupper($item->cargo).'</b><br>
                                    <b>'.strtoupper($item->institucion).'</b><br><br>';
                                    ;
                                    }
                                    @endphp
                                    @endforeach
                                </td>
                            </tr>
                            @else
                            <tr>
                                <td style="font-size: 13px" class="td_datos"><b>ASUNTO:</b>
                                    &nbsp;&nbsp;{{$datos->asunto}}</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="font-size: 13px" class="td_datos">
                                    @foreach ($datos->para as $item)
                                    @php
                                    echo $item->titulo.'<br>'.$item->name.' <br>
                                    <b>'.strtoupper($item->cargo).'</b><br>
                                    <b>'.strtoupper($item->institucion).'</b><br>';
                                    @endphp
                                    @endforeach
                                    {{((isset($datos->texto_adicional))?$datos->texto_adicional:'En su despacho.')}}
                                </td>
                                <td></td>
                            </tr>
                            @endif
                            @else
                            @if(count($datos->para)>=10)
                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca">
                                    <b> PARA:</b>
                                </td>
                                <td style="font-size: 11px" class="td_datos_2">
                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key<=19) { echo ucwords(strtolower($item->name)).' <br> <b>'.
                                            strtoupper($item->cargo).'</b><br><br>';
                                        }

                                        @endphp
                                        @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca">
                                </td>
                                <td style="font-size: 11px" class="td_datos_2">
                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key>19 && $key<=39) { echo ucwords(strtolower($item->name)).' <br>
                                        <b>'.strtoupper($item->cargo).'</b><br><br>';
                                        }

                                        @endphp
                                        @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca">
                                </td>
                                <td style="font-size: 11px" class="td_datos_2">
                                    @foreach ($datos->para as $key=> $item)
                                    @php
                                    if ($key>39) { echo ucwords(strtolower($item->name)).' <br>
                                    <b>'.strtoupper($item->cargo).'</b><br><br>';
                                    }

                                    @endphp
                                    @endforeach
                                </td>
                            </tr>


                            @else
                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca"><b>PARA:</b>
                                    @foreach ($datos->para as $item)
                                    <br>
                                    <br>
                                    @endforeach</td>
                                @if(isset($datos->grupo_envio) && $datos->grupo_envio != 'ELEGIR')
                                <td style="font-size: 11px" class="td_datos_2">
                                    @php
                                    echo ' <br>
                                    <b>'.strtoupper($datos->grupo_envio).'</b><br><br>';
                                    @endphp
                                </td>
                                @endif
                                <br>
                                <td style="font-size: 11px" class="td_datos_2">
                                    @foreach ($datos->para as $item)
                                    @php
                                    echo ucwords(strtolower($item->name)).' <br>
                                    <b>'.strtoupper($item->cargo).'</b><br><br>';
                                    @endphp
                                    @endforeach
                                </td>
                            </tr>
                            @endif

                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca"><b>FECHA:</b></td>
                                <td style="font-size: 13px;" class="td_datos_2">Manta,
                                    {{(isset($datos->fecha))?fechas(600,$datos->fecha):fechas(600)}}</td>

                            </tr>
                            <tr>
                                <td style="font-size: 13px" class="td_datos_ca"><b>ASUNTO:</b></td>
                                <td style="font-size: 13px" class="td_datos_2">{{$datos->asunto}}</td>
                            </tr>
                            @endif

                        </table>
                    </div>

                    <br>

                    <div class="texto_5">
                        @if ($datos->tipo=='OFICIO')
                            De mi consideración: <br><br>
                        @endif
                        @php
                        echo $datos->peticion;
                        @endphp

                    </div>

                    <div class="texto">
                        <p>
                            <br>
                            <br>
                            @if(Auth::user()->id != 1643)
                                Atentamente,
                            @else
                                Cordial,
                            @endif
                            <br>
                            <br>
                        </p>
                        @if ($datos->borrador!=3)
                            @if ($datos->tipo_firma==2)
                                <span style="color: blue;"><i>Firma generada mediante QR</i></span>
                            @else
                            <span style="color: blue;"><i>Documento firmando electrónicamente</i></span>
                            @endif
                        @endif
                        <br>
                        @if(Auth::user()->id != 1643)
                            <p> {{$datos->user->name}} </p>
                            <p>
                                <b>{{$datos->user->cargo}}</b><br>
                                @if($datos->tipo=='INFORME')
                                GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA
                                @endif
                                <br><b> {{((isset($datos->cargo_adicional))?$datos->cargo_adicional:'')}}</b>

                            </p>
                        @else
                            <p class="">Agustín Intriago Quijano </p>
                            <p class="">
                                <b>ALCALDE DE MANTA</b>
                                <br><b> {{((isset($datos->cargo_adicional))?$datos->cargo_adicional:'')}}</b>
                            </p>
                        @endif

                        <p class="alinedado" style="font-size: 9px;">
                            <br>
                            @if ($datos->anexos)
                            <span><b>Anexos</b></span>
                            <table style="width:100%">
                                <tr>
                                    <td class="td_datos_ca_right"></td>
                                    <td class="td_datos_right">

                                        <table style="width:100%">
                                            @foreach ($datos->anexos as $k=> $item)
                                            <tr>
                                                <td style="font-size: 11px;" class="td_datos_right">
                                                    {{($k+1).': '.$item->descripcion.' '.((isset($item->hojas) && $item->hojas!='')?' - N°. hojas: '.$item->hojas:'')}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                </tr>
                            </table>
                            @endif
                        </p>
                        <p class="alinedado" style="font-size: 9px;">
                            <br>
                            @if (isset($datos->copia[0]))

                            <span><b>Copia</b></span>
                            <table style="width:100%">
                                <tr>
                                    <td class="td_datos_ca_right"></td>
                                    <td class="td_datos_right">
                                        <table style="width:100%">

                                            @foreach ($datos->copia as $item)
                                            <tr>
                                                <td style="font-size: 11px;" class="td_datos_right">
                                                    @php
                                                    echo ucwords(strtolower($item->name)).' <br>
                                                    <b>'.strtoupper($item->cargo).'</b>
                                                    <br>
                                                    <b>'.strtoupper($item->institucion).'</b><br>';
                                                    @endphp
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            @endif
                        </p>

                        @if (isset($datos->elaborado_por))
                        <p class="alinedado" style="font-size: 11px;">
                            <span><b>Elaborado por</b></span>
                        </p>
                        <p style="font-size: 11px;" class="td_datos_right">
                            {{ $datos->elaborado_por }}
                        </p>
                        @endif

                    </div>
                        @if ($datos->tipo_firma==2)
                        <div class="footer">
                            <center>
                                <?php
                        $datosqr = "NÚMERO TRÁMITE: " . $datos->numtramite . "\n";
                        // $datosqr .= "FECHA: " . $datos->fecha_firma . "\n";
                        // $datosqr .= "GENERDADO POR: " . $datos->user->name . "\n";
                        // $datosqr .= "CARGO: " . $datos->user->cargo . "\n";
                        // $datosqr .= "DIRECCIÓN: " . $datos->user->direccion . "\n";
                        $datosqr .= "VIZUALIZAR PDF.: https://docs.google.com/gview?embedded=true&url=" .  'https://sistemasic.manta.gob.ec/archivos_firmados/' . $datos->nombre_documento  . "\n";
                        $qrdato = qrvcard('', $datos->tipo, null, $datosqr);
                        $codqr = '<img src="data:image/png;base64,' . base64_encode(QrCode::format('png')->margin(0)->merge('/public/img/logo_qr.png', .1)->size(110)->generate(trim($qrdato))) . '">';
                        echo $codqr;
                        ?>
                            </center>
                        @endif
                    </div>
                </div>
        </main>
</body>

</html>
