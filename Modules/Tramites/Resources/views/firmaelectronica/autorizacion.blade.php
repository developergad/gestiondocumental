<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title> Autorización </title>
    {!! Html::style('/css/memo.css') !!}
</head>
<style>
    * {
        margin: 0;
        padding: 0;
    }
</style>

<body id="body_todo_2">
    <div id="principal_div_2">
        <div class=" texto_3">
            <p> <b> N° {{$datos->numero}}</b></p>
        </div>
        <div class=" texto_4">
            <br>
            <p class="separador">
                <b>ACUERDO DE RESPONSABILIDAD Y USO DE CÓDIGO QR</b>
            </p>
        </div>
        <div class=" justificado texto_4">

            <br>
            <p>
                El Gobierno Autónomo Descentralizado Municipal del Cantón Manta, ha implementado las condiciones generales relacionadas a la responsabilidad y uso de Código QR para sus Servidores Públicos. Con este antecedente <b>{{$datos->nombre}}</b> en adelante “Servidor Público” con documento de identidad No. <b>{{$datos->cedula}}</b> acuerda las siguientes condiciones a las que se someterá, con relación a la utilización del “Código QR” para el cumplimiento de sus obligaciones laborales, otros deberes formales, emitir informes, documentos, a través de Internet en el Sistema de Gestión Documental institucional <span style="color: blue;">https://sistemasic.manta.gob.ec</span> y acceso a otros servicios que el Gobierno Autónomo Descentralizado Municipal del Cantón Manta ponga a su disposición a través de Internet.
                <br>
                <br>
                El Servidor Público, mediante la suscripción de este acuerdo manifiesta estar interesado en utilizar los mecanismos virtuales habilitados para la emisión de documentos en el Sistema de Gestión Documental institucional, para lo cual expresa su voluntad de utilizar de manera preferente el medio de representación a través de Internet en el usuario personal que el Gobierno Autónomo Descentralizado Municipal del Cantón Manta pone a su disposición en el Sistema de Gestión Documental institucional https://sistemasic.manta.gob.ec expresando y otorgando su consentimiento para ello. El Servidor Público conoce y acepta expresamente que la suscripción de este acuerdo no impide a la Dirección de Tecnología - cuando las circunstancias así lo requieran – realizar la validación de datos y veracidad del código QR. Dicha documentación generada por el Código QR se lo entiende como toda información creada, procesada, enviada, recibida, comunicada o archivada por medios electrónicos, que puede ser intercambiada por cualquier medio. Los documentos desmaterializados de conformidad con lo establecido en la Ley de Comercio Electrónico, Firmas Electrónicas y Mensajes de Datos, tienen el mismo valor jurídico que los documentos escritos, por lo cual el acceso a los mismos será entendido como el acceso al documento original.
                <br>
                <br>
                El Servidor Público entiende y acepta expresamente que las actuaciones administrativas realizada a través del Internet, se entenderá practicada a todos los efectos legales en el momento de su emisión en el Sistema de Gestión Documental institucional de la página web del Gobierno Autónomo Descentralizado Municipal del Cantón Manta <span style="color: blue;"><span style="color: blue;">https://sistemasic.manta.gob.ec</span></span>. El Gobierno Autónomo Descentralizado Municipal del Cantón Manta, verificará por medio de sus herramientas informáticas, el día y hora exactos en el que se produjo dicha emisión, por medio del funcionario competente, la constancia de emisión pertinente, como prueba de haberse esta realizado.
                <br>
                <br>
                <b>Responsabilidad del Servidor Público</b>
                <br>
                <br>
                El Servidor Público asume la responsabilidad total del uso el Código QR, así como de la veracidad de la información en el cumplimiento de sus obligaciones laborales, otros deberes formales y la utilización de los servicios que el Gobierno Autónomo Descentralizado Municipal del Cantón Manta ponga a su disposición a través de Internet.
                <br>
                <br>
                Conforme a los principios de simplicidad administrativa, seguridad en el manejo de la información y neutralidad tecnológica, la seguridad de las transacciones realizadas y servicios prestados por medio del portal electrónico del Gobierno Autónomo Descentralizado Municipal del Cantón Manta se garantizará mediante la clave de usuario del Servidor Público y de su uso se derivarán todas las responsabilidades legales, de conformidad con la ley.
                <br>
                <br>
                <b> Solicitud y uso del código QR</b>
                <br>
                <br>
                El servidor público deberá solicitarla a la Dirección de Desarrollo e Innovación Tecnológica la emisión de su nuevo Código QR el mismo que se asociará con su usuario y contraseña en el Sistema de Gestión Documental institucional <span style="color: blue;">https://sistemasic.manta.gob.ec</span>, asimismo será enviado al correo institucional del solicitante dicho acuerdo para que sea firmado y entregado a la dirección de Administración del Talento Humano y dejar constancia de su validez y aceptación.
                Cada que el servidor público vaya hacer uso de su  <br><br><br><br><br><br><br><br> código QR, será enviado previo a estampar la firma electrónica un código de autorización al correo institucional del servidor público, el cual debe ser ingresado de forma correcta para culminar el proceso de firmar electrónicamente el documento.
                <br>
                <br>
                El Servidor Público titular, debe acceder a la página WEB del GADMC-Manta mediante la clave que el Gobierno Autónomo Descentralizado Municipal del Cantón Manta le asigna al momento de suscribir el presente acuerdo, debiendo la misma ser reemplazada posteriormente por otra secreta que el mismo Servidor Público defina. En caso de olvidar su clave, podrá recuperarla cumpliendo las validaciones y filtros de seguridad que la página electrónica del Gobierno Autónomo Descentralizado Municipal del Cantón Manta le solicite. La responsabilidad derivada de la falta de cuidado, de la indebida reserva, del mal uso o del uso por terceros autorizados o no, mediante mandato del titular del Código QR, ocasionándose o no perjuicios, será exclusivamente del Servidor Público titular de dicho código.
                <br>
                <br>
                El Servidor Público se compromete a ingresar periódicamente al portal electrónico de la institución que señale en este Acuerdo, a fin de revisar las notificaciones que por dicho medio le sean periódicamente asignados de parte de la Dirección competente, documentos y reportes por esta, enviados en la fecha en la que fue efectuada la respectiva notificación, así como también acceder al contenido de las mismas.
                <br>
                <br>
                <b>Aceptación</b>

                La suscripción del acuerdo implicará la aceptación de todas y cada una de las disposiciones establecidas en las Manual de Gestión Documental del Gobierno Autónomo Descentralizado Municipal del cantón Manta, mediante las cuales, se establecieron las políticas de la Gestión Documental en lo que respecta al Sistema de Gestión Documental, mismas que se entienden incorporadas a este texto. Los términos y condiciones están sujetos a las disposiciones contenidas en la Ley de Comercio Electrónico, firmas electrónicas y mensajes de datos y las normas tributarias vigentes en el Ecuador. El Servidor Público suscribe este acuerdo por su propia iniciativa y se somete voluntariamente a lo aquí estipulado.
                <br>
                <br>
                El Servidor Público acepta la validez de este acuerdo, del Código QR que se le proporciona, las notificaciones electrónicas de actuaciones administrativas que le envíen, así como de las declaraciones u otra información que envíe haciendo uso de los sistemas o medios electrónicos que el Gobierno Autónomo Descentralizado Municipal de Manta ponga a su disposición.
                <br>
                <br>
                Cualquier información relativa al uso del Código QR, así como los documentos emitidos electrónicamente, las asignaciones de tareas del Sistema de Gestión Documental del Servidor Público las recibirá en la siguiente dirección de correo Electrónico <b>{{$datos->correo}}</b> o en la última dirección que haya registrado en la página electrónica de la institución.
                <br>
                <br>
                <b>Duración</b>
                <br>
                <br>
                Este acuerdo de responsabilidad y uso de Código QR tendrá una duración de tiempo indefinido, a menos que el Servidor Público sea separado del Gobierno Autónomo Descentralizado Municipal del Cantón Manta. El GADMC-Manta se reserva la potestad de rescindir este acuerdo o de rehacer uno nuevo cuando el Servidor Público no cumpla las cláusulas que en él se especifican, comunicando del particular al Servidor Público con por lo menos 30 días de anticipación.
                <br>
                El Gobierno Autónomo Descentralizado Municipal del Cantón Manta se reserva el derecho de modificar las condiciones del servicio, el periodo, y los procedimientos técnicos de identificación y validación de los datos transmitidos, de acuerdo con sus necesidades.
                <br>
                <br>
                <b>Fecha: Manta, {{fechas(600)}}</b>

            </p>
        </div>


        <div class="texto_4">
            <p>
                <br>
                Atentamente,
                <br>
                <br>
                <br>
                <br>
                <br>
            </p>
            <p> f. __________________________________________________
                <br>Servidor Público
                <p>
                    <b>Nombre del Servidor Público: {{$datos->nombre}}</b><br>
                    <b>Documento de identidad: {{$datos->cedula}}</b><br>
                </p>


        </div>


    </div>

    <div class="footer_2">

        <div width="100%">
            <table width="100%">
                <tr>
                    <td class="sin_bordes" style="padding-left: 0px; padding-left: 0px; padding-bottom:0px;" width="40%;">

                    </td>
                    <td width="60%;" style="padding-left: 0px; padding-left: 0px; padding-bottom:0px; text-align: center">
                        <?php
                        $datosqr = "Cédula: " . $datos->cedula . "\n";
                        $datosqr .= "FECHA: " . date(600) . "\n";
                        $datosqr .= "CARGO: " . $datos->cargo . "\n";
                        $datosqr .= "DIRECCIÓN: " . $datos->direccion . "\n";
                        $datosqr .= "VIZUALIZAR PDF.: https://docs.google.com/gview?embedded=true&url=" . url('') . '/descargarFormatoAutorizacion/' . $datos->cedula  . "\n";
                        $qrdato = qrvcard('', 'AUTORIZACION DE FUNCIONARIO TRAMITES', null, $datosqr);
                        $codqr = '<img src="data:image/png;base64,' . base64_encode(QrCode::format('png')->margin(0)->merge('/public/img/logo_qr.png', .1)->size(154)->generate(trim($qrdato))) . '">';
                        echo $codqr;
                        ?>
                    </td>
                </tr>
                <tr>
                    <td class="sin_bordes" style="padding-left: 0px; padding-left: 0px; padding-bottom:0px;" width="50;">

                    </td>
                    <td width="50%;" style="padding-left: 0px; padding-left: 0px; padding-bottom:0px; text-align: center">

                    </td>
                </tr>

            </table>

        </div>
    </div>
</body>

</html>
