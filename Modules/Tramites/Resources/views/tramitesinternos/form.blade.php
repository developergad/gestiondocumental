

<div class="form-group">
    <label class="col-lg-3 control-label">Fecha:</label>
    <div class=" col-lg-3 ">
        <div class="input-group datetime">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
            </span>
            <input type="text" class="form-control datefecha" id="fecha" name="fecha" value="{{ old('fecha', $tabla->fecha) }}" readonly>
        </div>
        <input type="text" id="modifico_fecha" hidden>

    </div>

</div>
<div class="form-group">
    <label class="col-lg-3 control-label">Tipo de trámite:</label>
    <div class="col-lg-3">
        {!! Form::select('tipo_tramite', $tabla->tipo_tramite, ((isset($tabla->tipo_tramite_a))?$tabla->tipo_tramite_a:null), ['id'=>'tipo_tramite_interno','class' =>
        'form-control',
        'required']) !!}
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">Número de trámite:</label>
    @if ($configuraciongeneral[3] == 'crear')
    <div class="col-lg-3">
        <input type="text" class="form-control" id="numtramite" name="numtramite" value="{{ old('numtramite', $numtramite) }}" readonly>
    </div>
    <div class="col-lg-2">
        <button class="btn btn-info" type="button" onclick="actualizarNumTramite()">
            <i class="fa fa-history"></i>
        </button>
    </div>
    @else
    <div class="col-lg-3">
        <input type="text" class="form-control" id="numtramite" name="numtramite" value="{{ $tabla->numtramite }}" readonly>
    </div>
    @endif
</div>

<div class="form-group" id="div_numtramite_externo">
    <label class="col-lg-3 control-label">Número trámite externo o interno:</label>
    <div class="col-lg-3">
        <input type="text" class="form-control mayuscula" id="numtramite_externo" value="{{ old('numtramite_externo', $tabla->numtramite_externo) }}" name="numtramite_externo" maxlength="50" autocomplete="off">
    </div>
</div>
<div class="form-group" id="div_nombre_informe">
    <label class="col-lg-3 control-label">Nombre del informe:</label>
    <div class="col-lg-7">
        <input type="text" class="form-control mayuscula" id="nombre_informe" value="{{ old('nombre_informe', $tabla->nombre_informe) }}" name="nombre_informe" maxlength="50" required autocomplete="off">
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">De:</label>
    <div class="col-lg-7">
        <input type="hidden" id="enviar_director" value="{{ isset($tabla->elaborado_por) && $tabla->tipo_envio == 'BORRADOR_DIRECTOR' ? 'SI' : (isset($tabla->elaborado_por) && $tabla->tipo_envio == 'BORRADOR_SUBDIRECTOR' ? 'SD' : 'NO') }}">
        @if ($configuraciongeneral[3] == 'crear')
            <input type="text" id="de" class="form-control" value="{{ Auth::user()->name }}" readonly>
            <input type="hidden" class="form-control" id="elaborado_por"
                value="{{ isset($esSecretaria) && $esSecretaria == true ? Auth::user()->id : null }}" readonly>
        @elseif ($configuraciongeneral[3] == 'editar')
            <input type="text" class="form-control" id="de" value="{{ $tabla->remitente->name }}" readonly>
            <input type="hidden" class="form-control" id="elaborado_por" value="{{ $tabla->elaborado_por }}" readonly>
        @endif
    </div>
</div>


<div id="div_memo_tramite">
    <div style="text-align: center;">
        <h3>Servidores públicos del Gad Manta</h3>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Para:</label>
        <div class="col-lg-7">
            @if(isset($responder))
            {!! Form::select('asignados[]', $tabla->destinatarios, $responder->remitente->id,
            ['multiple','id'=>'asignados', 'class' => 'chosen-select']) !!}
            @else
            {!! Form::select('asignados[]', $tabla->destinatarios, isset($asignados) ? $asignados : null,
            ['multiple','id'=>'asignados', 'class' => 'chosen-select']) !!}
            @endif
        </div>
    </div>

    @if (isset($configuraciongeneral[10]))
        <input type="hidden" name="analista_responde" value="{{ $configuraciongeneral[10] }}">
        @if (isset($tabla->observacion))
            <div class="form-group">
                <label class="col-lg-3 control-label">Observación:</label>
                <div class="col-lg-7">
                    <textarea rows="3" class="form-control" readonly>{{ strtoupper($tabla->observacion) }}</textarea>
                    {{-- <p class=""></p> --}}
                </div>
            </div>
        @endif
    @else
        <div class="form-group">
            <label class="col-lg-3 control-label">Con copia:</label>
            <div class="col-lg-7">
                @if (isset($tabla['con_copia']))
                    {!! Form::select('copia[]', $tabla['con_copia'], isset($copias) ?
                    $copias : null, ['multiple','id'=>'copia' ,'class' => 'chosen-select']) !!}
                @else
                    {!! Form::select('copia[]', isset($tabla->copias) ? $tabla->copias : $tabla->destinatarios, isset($copias) ?
                    $copias : null, ['multiple','id'=>'copia' ,'class' => 'chosen-select']) !!}
                @endif
            </div>
        </div>
    @endif

</div>
<div id="div_oficio_tramite" style="display: none;">
    <div style="text-align: center;">
        <h3>Personas externas al Gad Manta</h3>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label">Para:</label>
        <div class="col-lg-2">
            {!! Form::select('titulo', $tabla->titulos, null, ['id'=>'titulo','class' => 'form-control', 'required'])
            !!}
        </div>
        <div class="col-lg-4">
            {!! Form::text('nombres', null, ['class' => 'form-control', 'id' => 'nombres', 'placeholder' =>
            'Ingrese el nombre', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-2">
            {!! Form::text('cargo', null, ['class' => 'form-control', 'id' => 'cargo', 'placeholder' =>
            'Ingrese el cargo', 'autocomplete' => 'off']) !!}
            {{-- <input type="text" id="cargo" class="form-control mayuscula" value="" placeholder="Ingrese el cargo"> --}}
        </div>
        <div class="col-lg-4">
            {!! Form::text('institucion', null, ['class' => 'form-control', 'id' => 'institucion', 'placeholder' =>
            'Ingrese institución', 'autocomplete' => 'off']) !!}
            {{-- <input type="text" id="cargo" class="form-control mayuscula" value="" placeholder="Ingrese el cargo"> --}}
        </div>
        <div class="col-lg-1">
            <button class="btn btn-info" type="button" onclick="agregarPara()">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-4">
            <ul id="para_lista">
            </ul>
        </div>
    </div>
    {!! Form::hidden('delegados_para', $tabla->delegados_para, ['id' => 'delegados_para']) !!}

    <div class="form-group">
        <label class="col-lg-3 control-label">Con copia:</label>
        <div class="col-lg-2">
            {!! Form::select('titulo', $tabla->titulos, null, ['id'=>'titulo_copia','class' => 'form-control',
            'required']) !!}
        </div>
        <div class="col-lg-4">
            {!! Form::text('nombres', null, ['class' => 'form-control ', 'id' => 'nombres_copia', 'placeholder'
            => 'Ingrese el nombre', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-2">
            {!! Form::text('cargo', null, ['class' => 'form-control ', 'id' => 'cargo_copia', 'placeholder' =>
            'Ingrese el cargo', 'autocomplete' => 'off']) !!}
            {{-- <input type="text" id="cargo" class="form-control mayuscula" value="" placeholder="Ingrese el cargo"> --}}
        </div>
        <div class="col-lg-4">
            {!! Form::text('institucion', null, ['class' => 'form-control mayuscula', 'id' => 'institucion_copia', 'placeholder' =>
            'Ingrese institución', 'autocomplete' => 'off']) !!}
            {{-- <input type="text" id="cargo" class="form-control " value="" placeholder="Ingrese el cargo"> --}}
        </div>
        <div class="col-lg-1">
            <button class="btn btn-info" type="button" onclick="agregarCopia()">
                <i class="fa fa-plus"></i>
            </button>
        </div>
    </div>
    <div class="form-group">
        <label class="col-lg-3 control-label"></label>
        <div class="col-lg-4">
            <ul id="copia_lista">
            </ul>
        </div>
    </div>
    {!! Form::hidden('delegados_copia', $tabla->delegados_copia, ['id' => 'delegados_copia']) !!}
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">Asunto:</label>
    <div class="col-lg-7">
        <input type="text" class="form-control mayuscula" id="asunto" value="{{ old('asunto', $tabla->asunto) }}" name="asunto" required autocomplete="off" {{ isset($configuraciongeneral[6]) ? $configuraciongeneral[6] : ''}}>
    </div>
</div>

@if ($configuraciongeneral[3] == 'editar')
<div class="form-group">
    <label class="col-lg-3 control-label">Archivo adjunto:</label>
    <div class="col-lg-3">
        @if($tabla->documento)
        <a href="{{ asset('archivos_firmados/'.$tabla->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i>
        </a>
        @else
        <b>Sin documento</b>
        @endif

    </div>
</div>
@endif

@if (isset($tabla->contestados) && count($tabla->contestados) > 0)
@foreach ($tabla->contestados as $item)
<div class="form-group">
    <label class="col-lg-3 control-label">{{ $item->usuario->name }}:</label>
    <div class="col-lg-3">
        <a href="{{ asset('archivos_firmados/'.$item->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i>
        </a>
    </div>
</div>
@endforeach
@endif

@if (isset($respuesta) && $respuesta->estado == 'CONTESTADO')
<div class="form-group">
    <label class="col-lg-3 control-label">Archivo de respuesta:</label>
    <div class="col-lg-3">
        <a href="{{ asset('archivos_firmados/'.$respuesta->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i>
        </a>
    </div>
</div>
@else
@if($configuraciongeneral[6] != 'copia')
<div class="form-group">
    <label class="col-lg-3 control-label">
        {{ $configuraciongeneral[6] == 'readonly' ? 'Respuesta:' : 'Petición:' }}
    </label>
    <div class="col-lg-7">
        @if($configuraciongeneral[6] == 'readonly')
        <textarea name="peticion" id="peticion" class="summernote" rows="15">{{ old('peticion') }}</textarea>
        @else
        @if (!isset($ocultarPeticion))
        <textarea name="peticion" id="peticion" class="summernote" rows="15">{{ old('peticion', $tabla->peticion) }}</textarea>
        @endif
        @endif
    </div>
</div>
@endif
@endif

@if (isset($tabla->desvinculados) && count($tabla->desvinculados) > 0)
<hr class="hr-line-solid">
<div class="form-group">
    <label class="col-lg-3 control-label">Desvinculados:</label>
    <div class="col-lg-7">
        <div class="form-group table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Observación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tabla->desvinculados as $item)
                    <tr>
                        <th>{{ $item->usuario->name }}</th>
                        <th>{{ $item->observacion }}</th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif

@if($configuraciongeneral[3] == 'editar' && isset($anexos) && count($anexos) > 0 && $tabla->tipo_envio=='EMVIAR')
@include('vistas.archivos', ['anexos' => $anexos])
@endif

@if($configuraciongeneral[6] != 'copia')
<hr>
@if(isset($anexos))
@include('vistas.anexos', ['anexos' => $anexos])
@else
@include('vistas.anexos')
@endif
<hr>
@endif
@if ($configuraciongeneral[6] != 'copia' || (isset($respuesta->estado) && $respuesta->estado == 'ASIGNADO') ||
!isset($ocultarPeticion))

<div class="form-group">
    <label class="col-lg-3 control-label">Tipo de firma:</label>
    <div class="col-lg-5">
        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="customRadioInline1" name="tipo_firma" {{((isset($tabla->tipo_firma) && $tabla->tipo_firma=="TOKEN")?'checked':'')}} {{(($tabla->tipo_firma==null)?'checked':'')}} value="TOKEN" class="custom-control-input">
            <label class="custom-control-label" for="customRadioInline1">Token / Certificado (Archivo)</label>
        </div>
        <div class="col-lg-3">
            <select name="tipo_autenticacion" class="form-control" id="tipo_autenticacion">
                <option value="1">TOKEN</option>
                <option value="2">ARCHIVO</option>
            </select>
        </div>
        <div><b>Para poder firmar con el token se debe tener instalado en su máquina el aplicativo FirmaEC_SistemasIC, en caso de no tenerlo instalado solicitar a Tecnología la instalación del mismo.</b> </div>

        <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="customRadioInline2" name="tipo_firma" {{((isset($tabla->tipo_firma) && $tabla->tipo_firma=="CORREO")?'checked':'')}} value="CORREO" class="custom-control-input">
            <label class="custom-control-label" for="customRadioInline2">Generar QR</label>
        </div>
        <!-- @if(isset($no_director)) -->
        <!-- @endif -->
    </div>
</div>
<div class="form-group">
    <label class="col-lg-3 control-label">Firma electrónica:</label>
    <div class="col-lg-5">
        <button type="button" id="btn_firmar" class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i>
            Firmar electrónicamente
        </button>
        <button type="button" id="btn_firmarcorreo" hidden class="btn btn-success"><i class="fa fa-pencil" aria-hidden="true"></i>
            Generar código de autorización
        </button>
        <!-- @if(isset($no_director))
        @endif -->


        <button class="btn btn-primary" {{((isset($tabla->documento))?'':'disabled')}} type="button" id="btn_validar_documento"><i class="fa fa-check" aria-hidden="true"></i>
            Validar documento firmado para poder guardar 🙂
        </button>
        <button class="btn btn-info" type="button" id="btn_ver_borrador"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>
            Ver borrador
        </button>
        <input type="text" class="form-control" name="documento" value="{{ old('documento', $tabla->documento) }}" required readonly id="documento">
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">Acción:</label>
    @if(isset($configuraciongeneral[10]))
        <div class="col-lg-5">
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" checked id="customRadioInline3" name="tipo_envio"
                    {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="ENVIAR")?'checked':'')}} value="ENVIAR"
                    class="custom-control-input">
                <label class="custom-control-label" for="customRadioInline3">Enviar</label>
            </div>
            @if(isset($esSecretaria) && $esSecretaria)
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline01" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR_DIRECTOR")?'checked':'')}}
                        value="BORRADOR_DIRECTOR" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline01">Enviar a director</label>
                </div>
            @endif
            @if(isset($enviarSubdirector) && $enviarSubdirector)
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline001" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR_DIRECTOR")?'checked':'')}}
                        value="BORRADOR_SUBDIRECTOR" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline001">Enviar a sub director</label>
                </div>
            @endif
        </div>
    @else
        <div class="col-lg-5">
            @if (isset($respuestaSG) && isset($numtramiteExterno))
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="tipo_envio" checked value="ENVIAR"
                        class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline2">Enviar</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline3" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR")?'checked':'')}}
                        {{(($tabla->tipo_envio==null)?'checked':'')}} value="BORRADOR" class="custom-control-input">

                    <label class="custom-control-label" for="customRadioInline3">Guardar / borrador</label>
                </div>
                @if(isset($esSecretaria) && $esSecretaria)
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="tipo_envio"
                            {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR_DIRECTOR")?'checked':'')}}
                            value="BORRADOR_DIRECTOR" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Enviar a director</label>
                    </div>
                @endif
                @if(isset($enviarSubdirector) && $enviarSubdirector)
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="tipo_envio"
                            {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR_DIRECTOR")?'checked':'')}}
                            value="BORRADOR_SUBDIRECTOR" class="custom-control-input">
                        <label class="custom-control-label" for="customRadioInline1">Enviar a sub director</label>
                    </div>
                @endif
            @else
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline2" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && ($tabla->tipo_envio=="ENVIAR" || $tabla->tipo_envio=='BORRADOR_DIRECTOR'))?'checked':'')}} value="ENVIAR"
                        class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline2">Enviar</label>
                </div>
            @if(isset($esSecretaria) && $esSecretaria && !isset($tabla->elaborado_por))
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR_DIRECTOR")?'checked':'')}}
                        value="BORRADOR_DIRECTOR" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline1">Enviar a director</label>
                </div>
            @endif
            @if(isset($enviarSubdirector) && $enviarSubdirector)
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline1" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio == "BORRADOR_DIRECTOR") ? 'checked' : '')}}
                        value="BORRADOR_SUBDIRECTOR" class="custom-control-input">
                    <label class="custom-control-label" for="customRadioInline1">Enviar a sub director</label>
                </div>
            @endif
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" id="customRadioInline3" name="tipo_envio"
                        {{((isset($tabla->tipo_envio) && $tabla->tipo_envio=="BORRADOR")?'checked':'')}}
                        {{(($tabla->tipo_envio==null)?'checked':'')}} value="BORRADOR" class="custom-control-input">

                    <label class="custom-control-label" for="customRadioInline3">Guardar / borrador</label>
                </div>
            @endif
        </div>
    @endif
</div>
@endif



<div class="form-group">
    {!! $configuraciongeneral[5] !!}
</div>
