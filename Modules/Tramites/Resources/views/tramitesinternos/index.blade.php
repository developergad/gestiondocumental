@extends(request()->has("menu") ? 'layouts.layout_basic_no_head': 'layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')

<style>
    .swal2-textarea {
        font-size: 2em;
    }
    #swal2-content {
        font-size: 1.5em;
    }
</style>

<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                    </div>
                </div>
                <div class="ibox-content">
                    @include('tramites::tramitesinternos.errors')
                    @if(isset($crear_editar)  && $crear_editar == 'SI')
                        <div>
                            <a href="{{ route('tramitesinternos.index') }}" class="btn btn-primary ">Todos</a>
                            <a href="{{ route('tramitesinternos.create') }}" class="btn btn-default ">Nuevo</a>
                        </div>
                    @endif
                    {{-- {{dd($estado)}} --}}
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>N° de trámite</th>
                                    <th>Adjunto</th>
                                    <th>Anexos</th>
                                    <th>Remitente</th>
                                    <th>Asunto</th>
                                    <th>Tipo</th>
                                    <th>Fecha de registro</th>
                                    <th>Para</th>
                                    <th>Con copia</th>
                                    @if($estado == 'ANALISTA' || $estado == 'ASIGNADO')
                                        <th>Asignado a</th>
                                    @endif
                                    @if($tipo_envio == 'BORRADOR')
                                        <th>elaborado por</th>
                                    @endif
                                    @if ($estado == 'ASIGNADO' || $estado == 'INICIAL' || $estado == 'ARCHIVADO' || $estado == 'ANALISTA')
                                        <th>Acciones</th>
                                    @endif
                                </tr>
                            </thead>
                        </table>
                        @if(isset($analistas) && count($analistas) > 0)
                            @include('vistas.modal_analistas', ['analistas' => $analistas])
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/tramitesinternos.js').'?v='.rand(1,1000) }}"></script>
    <script>
        var tablaTramitesIntenos;

        $(function() {
            $('.selective-tags').selectize({
                plugins: ['remove_button'],
                persist: false,
                create: true,
                render: {
                    item: function(data, escape) {
                        return '<div>"' + escape(data.text) + '"</div>';
                    }
                },
                onDelete: function(values) {
                    return confirm('Esta seguro de borrar la selección?');
                }
            });

            tablaTramitesIntenos = $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[ 0, 'DESC' ]],
                ajax: '{!! route('tramites.ajax') !!}?estado={{$estado}}&tipo_envio={{$tipo_envio}}',
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                // dom: 'Blfrtip',
                // buttons: [
                //     {
                //         extend: 'pdfHtml5',
                //         title: 'Trámites internos',
                //         text: '<i class="fa fa-file-pdf"></i> PDF'
                //     },
                //     {
                //         extend: 'excelHtml5',
                //         title: 'Trámites internos',
                //         footer: true
                //     },
                // ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'memo', name: 'memo' },
                    { data: 'anexos', name: 'anexos' },
                    { data: 'remitente.name', name: 'remitente.name' },
                    { data: 'asunto', name: 'asunto' },
                    { data: 'tipo_tramite', name: 'tipo_tramite' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'asignados', name: 'asignados' },
                    { data: 'copias', name: 'copias' },
                    @if($estado == 'ANALISTA' || $estado == 'ASIGNADO')
                        { data: 'analistas', name: 'analistas' },
                    @endif
                    @if($tipo_envio == 'BORRADOR')
                        { data: 'elaborado_por', name: 'elaborado_por' },
                    @endif
                    @if ($estado == 'ASIGNADO' || $estado == 'INICIAL' || $estado == 'ARCHIVADO' || $estado == 'ANALISTA')
                        { data: 'action', name: 'action', orderable: false, searchable: false }
                    @endif
                ]
            });
        });
    </script>



    <script>
        $(document).ready(function() {
            let queryParam = getParameterByName('ti')
            if(queryParam != '')
            {
                console.log(queryParam)
                $('#tabla-tramites').on('draw.dt', function () {
                    console.log('draw')
                    $(`#${queryParam}`).trigger('click')
                });
            }
        })
        function getParameterByName(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    </script>

@endsection
