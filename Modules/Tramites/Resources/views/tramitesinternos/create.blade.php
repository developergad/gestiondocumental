@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

<style>
    #swal2-content {
        font-size: 1.5em;
    }
</style>

@section('contenido')
<div class="row animated fadeInRight">
    <div class="col-lg-12">
        <div class="imgback">
            <h1>{{ $configuraciongeneral[4] }}</h1>

            <div class="ibox float-e-margins">
                {{-- <div class="ibox-title">
                    <div>
                        <a href="{{ route('tramitesinternos.index') }}" class="btn btn-primary ">Todos</a>
                        <a href="{{ route('tramitesinternos.create') }}" class="btn btn-default ">Nuevo</a>
                    </div>
                </div> --}}

                <div class="ibox-content">
                    @include('tramites::tramitesinternos.errors')
                    @if ($configuraciongeneral[3] == 'crear')
                        {!! Form::open(['route' => $configuraciongeneral[1], 'method' => 'POST', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
                            @include('tramites::'.$configuraciongeneral[2], ['edicion' => false])
                    @elseif($configuraciongeneral[3] == 'editar')
                        {!! Form::model($tabla, ['route' => [$configuraciongeneral[1], Crypt::encrypt($tabla->id)], 'method' => 'PUT', 'id' => "form", 'class' => 'form-horizontal','files' => true]) !!}
                            @include('tramites::'.$configuraciongeneral[2], ['edicion' => true])
                    @endif
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include("vistas.includes.mainjs")

@if(isset($timelineTramiteInternos))
    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <h1>Historial</h1>
            <div hidden>{{ $contador=0 }}</div>
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">
                    @foreach ($timelineTramiteInternos as $item)
                        @php
                            $arraycolores = ["#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080"];
                            $color_aleatoreo = $arraycolores[array_rand($arraycolores)];
                        @endphp
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            </div>
                            <div class="vertical-timeline-content">
                                @if ($contador==0)
                                    <h2><i class="fa fa-save"></i>  Inicial</h2>
                                @elseif($contador>=1)
                                    <h2><i class="fa fa-pencil"></i> Actualización</h2>
                                @endif
                                <div hidden> {{$contador+=1}} </div>
                                <p>
                                    <pre style="text-align: left; white-space: pre-line;">
                                        <strong>Fecha de registro: </strong>{{ $item->created_at }}
                                        <strong>Numero de trámite: </strong>{{ $item->numtramite }}
                                        <strong>Remitente: </strong>  {{ $item->remitente }}
                                        <strong>Asunto: </strong>  {{ $item->asunto }}
                                        <strong>Tipo de trámite: </strong>  {{ $item->tipo_tramite }}
                                        <strong>Estado: </strong>  {{ mb_strtoupper($item->estado) }}
                                        <br>
                                        <strong>Modificado Por: </strong>{{ $item->usuario }}
                                    </pre>
                                    @if (isset($timelineTramiteResp))
                                        <strong><i class="fa fa-users"></i> Delegados</strong>
                                        <pre style="text-align: left; white-space: pre-line;">
                                            @foreach ($timelineTramiteResp as  $i)
                                                <strong>Nombre: </strong>{{$i->name}}
                                            @endforeach
                                        </pre>
                                    @endif
                                </p>
                                <span class="vertical-date">
                                    {{fechas(2,$item->updated_at)}} <br/>
                                    <small>{{fechas(1000,$item->updated_at)}} </small>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endif
@stop

@section('scripts')
    <script src="{{ asset('js/tramitesinternos.js').'?v='.rand(1,1000) }}"></script>
@endsection
