<div class="form-group">
    <label class="col-lg-3 control-label">Tipo de trámite:</label>
    <div class="col-lg-3">
        <input type="text" class="form-control" id="tipo_tramite" name="tipo_tramite" value="{{ old('tipo_tramite', $tabla->tipo_tramite) }}" readonly>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">Número de trámite:</label>
    @if ($configuraciongeneral[3] == 'crear')
    <div class="col-lg-3">
        <input type="text" class="form-control" id="numtramite" name="numtramite" value="{{ old('numtramite', $numtramite) }}" readonly>
    </div>
    <div class="col-lg-2">
        <button class="btn btn-info" type="button" onclick="actualizarNumTramite()">
            <i class="fa fa-history"></i>
        </button>
    </div>
    @else
    <div class="col-lg-3">
        <input type="text" class="form-control" id="numtramites" name="numtramite" value="{{ $tabla->numtramite }}" readonly>
    </div>
    @endif
</div>

<div class="form-group"  >
    <label class="col-lg-3 control-label">De:</label>
    <div class="col-lg-7">
        <input type="text" class="form-control" value="{{ $tabla->remitente->name }}" readonly>
    </div>
</div>

<div class="form-group">
    <label class="col-lg-3 control-label">Archivo adjunto:</label>
    <div class="col-lg-3">
        @if($tabla->documento)
        <a href="{{ asset('archivos_firmados/'.$tabla->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i>
        </a>
        @else
        <b>Sin documento</b>
        @endif

    </div>
</div>

@if(isset($anexos) && count($anexos) > 0)
    <div class="form-group">
        <label class="col-lg-3 control-label">Anexos:</label>
        <div class="col-lg-3">
            @foreach ($anexos as $item)
                <a href="{{ asset('archivos_sistema/'.$item->ruta) }}" class="btn btn-info dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
                    <i class="fa fa-file-pdf-o"></i>
                </a>
            @endforeach
        </div>
    </div>
@endif

@if(isset($analistasAsignados) && count($analistasAsignados) > 0)
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                <h3>Informe de analista(s)</h3>
                <hr>
                <div class="form-group">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Analista(s)</th>
                                <th>Estado</th>
                                <th>Adjunto</th>
                                <th>Anexos</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($analistasAsignados as $res)
                                <tr>
                                    <th>{{ $res->name }}</th>
                                    <th class="text-justify">{{ $res->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO' }}</th>
                                    @if ($res->estado == 'ANALISTA-CONTESTADO')
                                        <th class="text-justify">
                                            @if (isset($res->respuestaTramite->documento))
                                                <a href="{{ asset('archivos_firmados/'.$res->respuestaTramite->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
                                                    <i class="fa fa-file-pdf-o"></i>
                                                </a>
                                            @else
                                                <a href="{{ asset('archivos_firmados/'.$res->documento) }}" class="btn btn-success dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
                                                    <i class="fa fa-file-pdf-o"></i>
                                                </a>
                                            @endif
                                        </th>
                                        <th class="text-justify">
                                            @if(isset($res->respuestaTramite->anexos))
                                                @foreach ($res->respuestaTramite->anexos as $idx => $anexo)
                                                    <a href="{{ asset('archivos_sistema/'.$anexo->ruta) }}" class="btn btn-info btn-sm dropdown-toggle divpopup" target='_blank' onclick="popup(this)">
                                                        <i class="fa fa-file-pdf-o"></i>
                                                    </a>
                                                @endforeach
                                            @endif
                                        </th>
                                    @else
                                        <th class="text-justify"></th>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endif

@if ($btnAsignar == 0)
    <div class="form-group">
        @include('vistas.modal_analistas', ['analistas' => $analistas])
        <button type="button" onclick="obtenerAnalistas()" id="btn_asignar_analista" class="btn btn-success" style="float:right; margin-right: 20px;">
            Asignar analista
        </button>
    </div>
@endif