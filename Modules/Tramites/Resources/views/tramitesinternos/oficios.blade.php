@extends(Input::has("menu") ? 'layout_basic_no_head':'layout' )

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        {{-- <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a> --}}
                        {{-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a> --}}
                        {{-- <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Listar todos</a>
                            </li>
                            <li><a href="#">Nuevo</a>
                            </li>
                        </ul> --}}
                        {{-- <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a> --}}
                    </div>
                </div>

                <div class="ibox-content">
                    @include('tramites::tramitesinternos.errors')
                    @if(isset($crear_editar)  && $crear_editar == 'SI')
                        <div>
                            <a href="{{ route('tramitesinternos.index') }}" class="btn btn-primary ">Todos</a>
                            <a href="{{ route('tramitesinternos.create') }}" class="btn btn-default ">Nuevo</a>
                        </div>
                    @endif
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>N° de trámite</th>
                                    <th>Memo</th>
                                    <th>Anexos</th>
                                    <th>Remitente</th>
                                    <th>Asunto</th>
                                    <th>Fecha de registro</th>
                                    <th>Para</th>
                                    <th>Con copia</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/tramitesinternos.js').'?v='.rand(1,1000) }}"></script>

    <script>
        $(function() {
            $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[ 0, 'DESC' ]],
                ajax: '{!! route('oficios.ajax') !!}',
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                // dom: 'Blfrtip',
                // buttons: [
                //     {
                //         extend: 'pdfHtml5',
                //         title: 'Trámites internos',
                //         text: '<i class="fa fa-file-pdf"></i> PDF'
                //     },
                //     {
                //         extend: 'excelHtml5',
                //         title: 'Trámites internos',
                //         footer: true
                //     },
                // ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'oficio', name: 'oficio' },
                    { data: 'anexos', name: 'anexos' },
                    { data: 'remitente.name', name: 'remitente.name' },
                    { data: 'asunto', name: 'asunto' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'asignados', name: 'asignados' },
                    { data: 'copias', name: 'copias' },
                ]
            });
        });
    </script>
@endsection
