@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['method' => 'POST', 'id' => 'search-form', 'role'=>'form', 'class'=>'form-row', 'style' => 'margin: 0 0 0 0;']) !!}
                            <div class="form-group">
                                <div class="row">
                                    {{-- <div class="col-sm-1"></div> --}}
                                    @if (Auth::user()->id_perfil != 50)
                                        <div class="col-sm-4">
                                            <select type="text" class="form-control" id="analista" name="analista">
                                                <option hidden selected value="">ANALISTAS</option>
                                                @foreach ($analistas as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-3">
                                            <select type="text" class="form-control" id="estado" name="estado">
                                                <option hidden selected value="">ESTADO</option>
                                                <option value="ANALISTA">PENDIENTE</option>
                                                <option value="ANALISTA-CONTESTADO">CONTESTADO</option>
                                            </select>
                                        </div>
                                    @endif

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="numtramite" name="numtramite"
                                            value="{{ old('numtramite') }}" placeholder="Número de trámite" autocomplete="off">
                                    </div>

                                    <div class="col-sm-2">
                                        <button type="submit" class="btn btn-success">Buscar</button>
                                        <button type="button" class="btn btn-info" id="limpiar">Limpiar</button>
                                    </div>
                                    {{-- <div class="col-sm-1"></div> --}}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <hr>
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de trámite</th>
                                    <th>Remitente</th>
                                    <th>Dirección a atender</th>
                                    <th>Con copia</th>
                                    <th>Prioridad</th>
                                    <th>Analistas asignados</th>
                                    <th>Fecha estimada de finalización</th>
                                    <th>Adjunto</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            var table = $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                // order: [[ 7, 'asc' ]],
                // orderFixed: {
                //     "pre": [ 2, 'desc' ],
                //     "post": [ 7, 'desc' ]
                // },
                // columnDefs: [
                //     { responsivePriority: 1, targets: 0 },
                //     { responsivePriority: 2, targets: -1 }
                // ],
                ajax: {
                    url: '{!! route('tramites.revisar.ajax') !!}?estado={{$estado}}',
                    data: function(d) {
                        d.analista = $('#analista').val();
                        d.estado = $('#estado').val();
                        d.numtramite = $('input[name=numtramite]').val();
                    }
                },
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        title: 'Permisos Municipales Solicitados',
                        text: '<i class="fa fa-file-pdf"></i> PDF'
                    },
                    {
                        extend: 'excelHtml5',
                        title: 'Permisos Municipales Solicitados',
                        footer: true
                    },
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                    { data: 'tipo_tramite', name: 'tipo_tramite' },
                    { data: 'remitente', name: 'remitente' },
                    { data: 'atender', name: 'atender' },
                    { data: 'copia', name: 'copia' },
                    { data: 'prioridad', name: 'prioridad' },
                    { data: 'analistas', name: 'analistas' },
                    { data: 'fecha_fin', name: 'fecha_fin' },
                    { data: 'adjunto', name: 'adjunto' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

            $('#limpiar').on('click', function(e) {
                $('#estado').val('');
                $('#analista').val('');
                $('#numtramite').val('');
                table.draw();
                e.preventDefault();
            });
        });
    </script>
@endsection
