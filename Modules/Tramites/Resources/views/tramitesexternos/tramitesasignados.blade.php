@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    @include('tramites::tramitesinternos.errors')
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de trámite</th>
                                    <th>Remitente</th>
                                    <th>Asunto</th>
                                    <th>Dirección a atender</th>
                                    <th>Estado</th>
                                    <th>Fecha estimada de finalización</th>
                                    @if ($estado == 'ANALISTA-CONTESTADO|SUB-ANALISTA-CONTESTADO|INFORME-RESPONDIDO|ANALISTA-INFORME-RESPONDIDO|INFORME-DESVINCULADO|ANALISTA-INFORME-DESVINCULADO')
                                        <th>Archivo</th>
                                    @endif
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[ 0, 'DESC' ]],
                ajax: '{!! route('asignados.ajax') !!}?estado={{$estado}}',
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        title: 'Trámites internos',
                        text: '<i class="fa fa-file-pdf"></i> PDF'
                    },
                    {
                        extend: 'excelHtml5',
                        title: 'Trámites internos',
                        footer: true
                    },
                ],
                columns: [
                    { data: 'cabecera.id', name: 'cabecera.id' },
                    { data: 'cabecera.numtramite', name: 'cabecera.numtramite' },
                    { data: 'cabecera.fecha_ingreso', name: 'cabecera.fecha_ingreso' },
                    { data: 'tipo_tramite', name: 'tipo_tramite', orderable: false, searchable: false },
                    { data: 'cabecera.remitente', name: 'cabecera.remitente' },
                    { data: 'cabecera.asunto', name: 'cabecera.asunto' },
                    { data: 'atender', name: 'atender' },
                    { data: 'estado', name: 'estado' },
                    { data: 'fecha_fin', name: 'fecha_fin', orderable: false, searchable: false },
                    @if ($estado == 'ANALISTA-CONTESTADO|SUB-ANALISTA-CONTESTADO|INFORME-RESPONDIDO|ANALISTA-INFORME-RESPONDIDO|INFORME-DESVINCULADO|ANALISTA-INFORME-DESVINCULADO')
                        { data: 'archivo', name: 'archivo', orderable: false, searchable: false },
                    @endif
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@endsection
