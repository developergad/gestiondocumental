<?php
if(!isset($nobloqueo))
    // Autorizar(Request::path());
?>
@extends(request()->has("menu") ? 'layouts.layout_basic_no_head':'layouts.layout' )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop

<style type="text/css">
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 90%;
        z-index: 9999;
        background: url({{ asset('img/loading35.gif') }}) 50% 50% no-repeat rgb(249,249,249);
        opacity: .8;
    }

    .disabled {
        pointer-events: none;
        background: #AAA;
        color: #F5F5F5;
        overflow:hidden;
    }

    .anchoinput {
        width: 7em;
    }

    table {
        width: 100%;
    }

    th {
        height: 10px;
    }

    #label_archivo_final {
        display: none;
    }

    #swal2-content{
        font-size: 1.5em;
    }
</style>

@section ('scripts')
  {!! HTML::script('js/jquery.blockUI.js') !!}
  {!! HTML::script('gridedit/mindmup-editabletable.js') !!}
  {!! HTML::script('gridedit/numeric-input-example.js') !!}

    @include("vistas.includes.mainjs")
    <link rel="stylesheet" href="{{ asset('css/tramites.css') }}">

    <script type="text/javascript">
        function popup(obj) {
            var row= $(obj).parent().parent();
            row.css("background-color","#cdf7c8");
            $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
        }
    </script>

    @if (isset($scriptjs))
        <script src="{{ asset('js/tramites.js').'?v='.rand(1,1000) }}"></script>
        {{-- <script src="{{ asset('js/tramitesadmin.js').'?v='.rand(1,1000) }}"></script> --}}
    @endif

    @include("vistas.includes.jsfunciones")
@stop

@section ('contenido')
    <h1>
        @if($configuraciongeneral[2]=="crear")
            {!! $configuraciongeneral[0] !!}
        @else
            {!! $configuraciongeneral[0] !!} <i class="fa fa-edit"></i>
        @endif
    </h1>

<div class="ibox float-e-margins">
    @if (Session::has('warning'))
        <script>
            $(document).ready(function() {
                toastr["warning"]("{{ Session::get('warning') }}");
            });
        </script>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-warning text-center">⚠ Advertencia ⚠ <br>{{ Session::get('warning') }}</div>
            </div>
        </div>
    @endif

    @if(Session::has('error'))
        <script>
            $(document).ready(function() {
                toastr["error"]("{{ Session::get('error') }}");
            });
        </script>
        <div class="col-md-6 col-md-offset-3">
            <div class="alert alert-danger text-center">{{ Session::get('error') }}</div>
        </div>
    @endif

    <div class="ibox-title">
        @if($errors->all())
            <script>
                $(document).ready(function() {
                    toastr["error"]($("#diverror").html());
                });
            </script>
            <div class="alert alert-danger" style="text-align: left;" id='diverror'>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <h4>Error!</h4>
                <div id="diverror">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif
        @if(Session::has('message'))
            <script>
                $(document).ready(function() {
                    toastr["success"]("{{ Session::get('message') }}");
                });
            </script>
            <div class="col-md-6 col-md-offset-3">
                <div class="alert alert-info text-center">{{ Session::get('message') }}</div>
            </div>
        @endif
    </div>

    <div class="ibox-content">
        @php $menupo=""; @endphp
        @if(request()->has("menu"))
            @php $menupo="?menu=no"; @endphp
        @endif
        @if($configuraciongeneral[2]=="crear")
            {!! Form::open(array('url' => $configuraciongeneral[1].$menupo,'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal',
            'files' => true,'style'=>"padding-bottom: 10%;")) !!}
        @else
            {!! Form::model($tabla, array('url' => $configuraciongeneral[1].'/'. $tabla->id.$menupo, 'method' =>
            'PUT','class'=>'form-horizontal', 'id'=>'form1', 'files' => true )) !!}
        @endif

        @if (isset($labels_tramite))
        <div class="loader" id="position" style="diplay: none;"></div>
        @endif

        @foreach($objetos as $i => $campos)
            <div class="form-group " id="div_{{$campos->Nombre}}">
                @if($campos->Tipo!="divresul")
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("id"=>"label_".$campos->Nombre,"class"=> "col-lg-3  control-label class_".$campos->Nombre)) !!}
                @endif
                <div class="col-lg-{{(isset($campos->Adicional))?'6':'8'}}" id="div-{{$campos->Nombre}}">
                    @if($campos->Tipo=="funcionjs")
                        {!! $campos->Valor !!}
                    @elseif($campos->Tipo=="date")
                        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control
                        datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
                    @elseif($campos->Tipo=="datetext")
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            {!! Form::text($campos->Nombre, (($campos->Valor != 'Null')?$campos->Valor:null), array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha '.$campos->Clase,
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            'readonly'=>'readonly'
                            )) !!}
                        </div>
                    @elseif($campos->Tipo=="datetimetext")
                        <div id="date_{{$campos->Nombre}}" class="input-group datetime">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                            {!! Form::text($campos->Nombre, (($campos->Valor != 'Null')?$campos->Valor:null) , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha '.$campos->Clase,
                            'placeholder'=>str_replace("(*)","",$campos->Descripcion),
                            $campos->Nombre,
                            )) !!}
                        </div>
                    @elseif($campos->Tipo=="select")
                        {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) :
                        ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
                        "form-control ".$campos->Clase)) !!}
                    @elseif($campos->Tipo=="select-actualizar-tipo-tramite")
                        <div class="row">
                            <div class="col-sm-8">
                                {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) :
                                ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
                                "form-control ".$campos->Clase)) !!}
                            </div>
                            <div class="col-sm-4">
                                <input type="hidden" id="id_tramite_externo" value="{{ $tabla->id }}">
                                <button type="button" id="btn_actualizar_tramite"
                                    class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                                    Actualizar tipo de trámite
                                </button>
                            </div>
                        </div>
                    @elseif($campos->Tipo=="telefonos")
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
                                'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion)) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text($campos->Nombre.'_2',old($campos->Valor), array('class' =>
                                'form-control solonumeros ' . $campos->Clase, 'placeholder'=>'Ingrese '. $campos->Descripcion . ' 2')) !!}
                            </div>
                        </div>
                    @elseif($campos->Tipo=="numtramite")
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::text($campos->Valor, $campos->ValorAnterior, array('class' =>
                                'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion, 'id' => $campos->Nombre, 'name' => $campos->Nombre, 'readonly' => true)) !!}
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-info" type="button" onclick="actualizarNumTramiteExterno()">
                                    <i class="fa fa-history"></i>
                                </button>
                            </div>
                        </div>
                    @elseif($campos->Tipo=="direccion")
                        <div class="row">
                            <div class="col-md-6">
                                {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
                                'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion)) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::text('referencia',old($campos->Valor), array('class' =>
                                'form-control '. $campos->Clase, 'placeholder'=>'Ingrese una referencia de su dirección*')) !!}
                            </div>
                        </div>
                    @elseif($campos->Tipo=="select2")
                        {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control
                        ".$campos->Clase)) !!}
                    @elseif($campos->Tipo=="select-multiple")
                        {!! Form::select($campos->Nombre."[]",$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre)
                        : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
                        "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
                    @elseif($campos->Tipo=="file")
                        {!! Form::file($campos->Nombre, array('class' => 'form-control '.$campos->Clase)) !!}
                        @if(isset($tabla))
                            <br>
                            @php
                                $id_c= $campos->Nombre;
                                if (strpos($tabla->$id_c, 'pdf') === false) {
                                    $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a onclick=\"mostrarImagen(\''.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").'\')\" class=\"btn btn-info dropdown-toggle\"><i class=\"fa fa-picture-o\"></i> Ver imagen</a></div>';";
                                } else {
                                    $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a></div>';";
                                }
                                eval($cadena);
                            @endphp
                        @endif
                    @elseif($campos->Tipo=="textdisabled")
                        {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
                        'form-control','placeholder'=>'Ingrese'. ((isset($camposcaption[$i]))?$camposcaption[$i]:"") ,'readonly')) !!}
                    @elseif($campos->Tipo=="textdisabled3")
                        {!! Form::text($campos->Nombre,$campos->Valor, array('class' =>
                        'form-control','placeholder'=>'Ingrese'. ((isset($camposcaption[$i]))?$camposcaption[$i]:"") ,'readonly')) !!}
                    @elseif($campos->Tipo=="textarea")
                        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
                        $campos->Clase ,'placeholder'=>'Ingrese '.str_replace("(*)","",$campos->Descripcion))) !!}
                    @elseif($campos->Tipo=="textarea2")
                        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
                        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}
                    @elseif($campos->Tipo=="FormHidden")
                        {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}
                    @elseif($campos->Tipo=="htmlplantilla")
                        <div class="form-group col-md-12 col-lg-12" id="obj-{{$campos->Nombre}}">
                            <div class="col-lg-12 " id="{{$campos->Nombre}}">
                                <?php echo $campos->Valor; ?>
                            </div>
                        </div>
                    @elseif($campos->Tipo=="textmayus")
                        {!! Form::text($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase
                        ,'placeholder'=>'Ingrese '.$campos->Descripcion,'onkeyup'=>'aMays(event, this)')) !!}
                    @elseif($campos->Tipo=="textdisabled2")
                        {!! Form::text($campos->Nombre,old($campos->Valor), array('class' => 'form-control
                        '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                    @elseif($campos->Tipo=="selectdisabled")
                        {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) :
                        ($campos->ValorAnterior !="Null") )? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
                        "form-control selectdisabled".$campos->Clase, 'readonly')) !!}
                    @elseif($campos->Tipo=="select-multiple-disabled")
                        {!! Form::select($campos->Nombre."[]",$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre)
                        : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
                        "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}
                    @elseif($campos->Tipo=="textarea-disabled")
                        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
                        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
                    @elseif($campos->Tipo=="textarea-2")
                        {!! Form::textarea($campos->Nombre, $campos->Valor , array('class' => 'form-control ' .
                        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion,"rows"=>3)) !!}
                    @elseif($campos->Tipo=="textdisabled")
                        {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior :
                        old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.str_replace("(*)","",$campos->Descripcion),  'readonly' => true)) !!}
                    @else
                        {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior :
                        old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.str_replace("(*)","",$campos->Descripcion))) !!}
                    @endif
                </div>
                @php echo ((isset($campos->Adicional))?$campos->Adicional:'') @endphp
            </div>
        @endforeach

        @if(isset($res_dir) && $res_dir != null && count($res_dir) > 0)
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Respuestas</h3>
                        <hr>
                        <div class="form-group">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Dirección</th>
                                <th>Respuesta</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($res_dir as $res)
                                <tr>
                                    <td>{{ $res->direccion}}</td>
                                    <td>{{ $res->observacion }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($informeOtrasAreasAnalista) && count($informeOtrasAreasAnalista) > 0)
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Informe de otras áreas</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Respuesta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($informeOtrasAreasAnalista as $res)
                                        <tr>
                                            <td>{{ $res->direccion }}</td>
                                            <td>{{ $res->estado == 'ANALISTA-INFORME' ? 'PENDIENTE' : ($res->estado == 'ANALISTA-INFORME-RESPONDIDO' ? 'CONTESTADO' : ($res->estado == 'ANALISTA-INFORME-DESVINCULADO' ? 'DESVINCULADO' : 'ARCHIVADO')) }}</td>
                                            <td class="text-justify">{{ isset($res->observacion) ? $res->observacion : 'NINGUNA' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($desvinculados) && count($desvinculados) > 0)
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Dirección</th>
                                        <th>Observación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($desvinculados as $item)
                                        <tr>
                                            <td>{{ $item->alias }}</td>
                                            <td>{{ $item->observacion }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($respuestasInforme) && $respuestasInforme != null && count($respuestasInforme) > 0)
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Informe de otras áreas</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Respuesta</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($respuestasInforme as $res)
                                        <tr>
                                            <td>{{ $res->direccion }}</td>
                                            <td>{{ $res->estado == 'INFORME' ? 'PENDIENTE' : ($res->estado == 'INFORME-RESPONDIDO' ? 'CONTESTADO' : ($res->estado == 'INFORME-DESVINCULADO' ? 'DESVINCULADO' : 'ARCHIVADO')) }}</td>
                                            <td class="text-justify">{{ isset($res->observacion) ? $res->observacion : 'NINGUNA' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($analistas_asignados) && count($analistas_asignados) > 0)
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Informe de analista(s)</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Analista(s)</th>
                                        <th>Fecha Asignación</th>
                                        <th>Estado</th>
                                        <th>Observación</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($analistas_asignados as $res)
                                        <tr>
                                            <td>{{ $res->name }}</td>
                                            <td>{{ $res->created_at }}</td>
                                            @if (isset($res->respuesta) && $res->respuesta == 2 && ($res->estado == 'ANALISTA-CONTESTADO' || $res->estado == 'SUBANALISTA-CONTESTADO'))
                                                <td class="text-justify">RESPUESTA ENVIADA AL CIUDADANO</td>
                                            @else
                                                <td class="text-justify">{{ $res->estado == 'ANALISTA' || $res->estado == 'SUB-ANALISTA' ? 'PENDIENTE' : 'CONTESTADO' }}</td>
                                            @endif
                                            <td class="text-justify">{{ isset($res->observacion) ? $res->observacion : 'NINGUNA' }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if(isset($direccionesAsignadas) && count($direccionesAsignadas) > 0)
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Direcciones Asignadas</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Fecha Asignación</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($direccionesAsignadas as $res)
                                        <tr>
                                            @if($res['id_direccion'] == 1470)
                                                <td>{{ $res['direccion'] }} (EP - OTROS)</td>
                                            @else
                                                <td>{{ $res['direccion'] }}</td>
                                            @endif
                                            <td>
                                                @if ($res['estado'] == 'INFORME-RESPONDIDO' || $res['estado'] == 'ANALISTA-INFORME-RESPONDIDO')
                                                    INFORME-CONTESTADO
                                                @else
                                                    {{ $res['estado'] }}
                                                @endif
                                            </td>
                                            <td>{{ $res['created_at'] }}</td>
                                            <td class="text-justify">
                                                <i onclick="borrarAsignacion({{$res['id'] }})"
                                                    class="fa fa-times-circle"
                                                    style="font-size: x-large;color: red; cursor: pointer;"
                                                    aria-hidden="true">
                                                </i>
                                                @if($tabla->tipo != 'TRAMITES MUNICIPALES'
                                                    && $res['estado'] != 'PENDIENTE'
                                                    && $res['estado'] != 'CON COPIA'
                                                    && $res['estado'] != 'ANALISTA-INFORME'
                                                    || ($res['id_direccion'] != 1470 && $res['estado'] == 'ASIGNADO'))
                                                    <i onclick="devolverEstadoAnterior({{ $res['id'] }})"
                                                        class="fa fa-history"
                                                        style="font-size: x-large;color: green; cursor: pointer;"
                                                        aria-hidden="true">
                                                    </i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(isset($solicitudesInforme) && count($solicitudesInforme) > 0)
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Solicitudes de informe</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Solicitante</th>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Fecha Asignación</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($solicitudesInforme as $solicitud)
                                        <tr>
                                            <td>{{ $solicitud['solicitante'] }}</td>
                                            @if($solicitud['id_direccion'] == 1470)
                                                <td>{{ $solicitud['direccion'] }} (EP - OTROS)</td>
                                            @else
                                                <td>{{ $solicitud['direccion'] }}</td>
                                            @endif
                                            <td>
                                                @if ($solicitud['estado'] == 'INFORME-RESPONDIDO' || $solicitud['estado'] == 'ANALISTA-INFORME-RESPONDIDO')
                                                    INFORME-CONTESTADO
                                                @else
                                                    {{ $solicitud['estado'] }}
                                                @endif
                                            </td>
                                            <td>{{ $solicitud['created_at'] }}</td>
                                            <td class="text-justify">
                                                <i onclick="borrarAsignacion({{$solicitud['id'] }})"
                                                    class="fa fa-times-circle"
                                                    style="font-size: x-large;color: red; cursor: pointer;"
                                                    aria-hidden="true">
                                                </i>
                                                @if($tabla->tipo != 'TRAMITES MUNICIPALES'
                                                    && $solicitud['estado'] != 'ANALISTA-INFORME'
                                                    && $solicitud['estado'] != 'INFORME'
                                                    || ($solicitud['id_direccion'] != 1470 && $solicitud['estado'] == 'ASIGNADO'))
                                                    <i onclick="devolverEstadoAnterior({{ $solicitud['id'] }})"
                                                        class="fa fa-history"
                                                        style="font-size: x-large;color: green; cursor: pointer;"
                                                        aria-hidden="true">
                                                    </i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(isset($analistasAsignadas) && count($analistasAsignadas) > 0)
            <hr>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
                        <h3>Analistas Asignados</h3>
                        <hr>
                        <div class="form-group">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>Estado</th>
                                        <th>Fecha Asignación</th>
                                        <th>Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($analistasAsignadas as $res)
                                        <tr>
                                            <td>{{ $res->name }}</td>
                                            <td>{{ $res->direccion }}</td>
                                            <td>{{ $res->estado }}</td>
                                            <td>{{ $res->created_at }}</td>
                                            <td class="text-justify">
                                                @if($res->estado == 'SUB-ANALISTA' || $res->estado == 'ANALISTA' || $res->estado == 'ANALISTA-INFORME')
                                                    <i onclick="borrarAsignacionAnalista({{$res->id }})"
                                                        class="fa fa-times-circle"
                                                        style="font-size: x-large;color: red;"
                                                        aria-hidden="true">
                                                    </i>
                                                @elseif($res->estado == 'ANALISTA-CONTESTADO' || $res->estado == 'SUB-ANALISTA-CONTESTADO')
                                                    <i onclick="devolverEstadoAnterior({{ $res->id }})"
                                                        class="fa fa-history"
                                                        style="font-size: x-large;color: green; cursor: pointer;"
                                                        aria-hidden="true">
                                                    </i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if(isset($archivos) && isset($archivos_total))
            <hr>
            @include('vistas.archivos', [
                'archivos' => $archivos,
                'archivos_total' => $archivos_total,
                'referencia' => $tabla->id,
                'btnElaborarRespuesta' => isset($btnElaborarRespuesta) ? $btnElaborarRespuesta : null
            ])
        @endif

        @if(isset($configuraciongeneral[10]))
            {!!Form::text('ruta', $configuraciongeneral[10], array('class' => 'form-control hidden')) !!}
        @endif
        @if(isset($popup))
            <input type="hidden" value="popup" name="txtpopup" />
        @endif
        @if(Auth::user()->id_perfil == 1 && isset($tabla))
            <input type="hidden" id="id_tramite_externo" value="{{ $tabla->id }}">
            <button type="button" id="btn_actualizar_tramite"
                class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                Actualizar
            </button>

            @if ($tabla->disposicion == 'FINALIZADO')
                @if(isset($tabla->correo_electronico))
                    <button type="button" onclick="reenviarRespuesta({{ $tabla->id }})"
                    id="btn_archivar_tramite" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                        Reenviar respuesta
                    </button>
                @endif
                @if($tabla->tipo == 'TRAMITES MUNICIPALES')
                    <button type="button" onclick="devolverEstadoAnterior({{ $tabla->id }})"
                    id="btn_devolver_anterior_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                        Devolver a estado anterior
                    </button>
                @endif
            @endif
        @endif
        @if(isset($permisos))
            @if($permisos->guardar=="SI")
                {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @endif
        @else
            @if(!isset($btnguardar))
                @if(Auth::user()->id_perfil == 55 && isset($btnEp))
                    {!! Form::submit("Enviar a secretaría general", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
                    <button type="button" onclick="desvincularTramite({{ $tabla->id }})"
                        id="btn_devolver_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                        ¿No corresponde a la dirección?
                    </button>

                    <button type="button" onclick="archivarTramite({{ $tabla->id }})"
                    id="btn_archivar_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                        Archivar trámite
                    </button>
                @elseif(Auth::user()->esDirector() && isset($asignacion) && ($asignacion->estado == 'ASIGNADO' || $asignacion->estado == 'DEVUELTO-COORDINADOR') && isset($tabla->obtener_respuesta) && $tabla->obtener_respuesta == 'NO')
                    <input type="hidden" value="{{ $tabla->id }}" id="tramite_id">
                    <button type="button" id="finalizarImcompleto"
                        class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                        Enviar respuesta a ciudadano
                    </button>
                @else
                    @if(Auth::user()->id_direccion == 21 && isset($tabla) && isset($btnFinalizar) && $tabla->correo_electronico == null)
                        <button type="button" onclick="finalizarTramiteDirector({{ $tabla->id }}, '{{ $btnFinalizar }}', 'NO')"
                            id="btn_finalizar_tramite" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                            Finalizar
                        </button>
                    @else
                        {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
                    @if(isset($btnDevolverVentanilla))
                        <button type="button" onclick="devolverVentanilla({{ $tabla->id }})"
                            id="btn_devolver_ventanilla" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                            Devolver
                        </button>
                    @endif
                @endif
            @endif
            @if(isset($btnDevolver))
                @include('vistas.modal_direcciones', ['direcciones' => $direcciones])
                <button type="button" onclick="obtenerDirecciones()"
                    id="btn_solicitar_informe" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                    ¿Necesita informe de otras áreas?
                </button>
                <button type="button" onclick="desvincularTramite({{ $tabla->id }})"
                    id="btn_devolver_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                    ¿No corresponde a la dirección?
                </button>

                @if(isset($btnDonacion))
                    <button type="button" onclick="donacion({{ $tabla->id }})"
                        id="btn_donacion" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                        Archivar como donación
                    </button>
                @endif

                @if(count($analistas) > 0)
                    @include('vistas.modal_analistas', ['analistas' => $analistas])
                    <button type="button" onclick="obtenerAnalistas()"
                        id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                        Asignar analista
                    </button>
                @endif
            @endif
        @elseif(isset($btnFinalizar))
            @if($asignacion && $tabla->obtener_respuesta == 'SI')
                {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @elseif($btnFinalizar == 'DIRECTOR')
                <button type="button" onclick="finalizarTramiteDirector({{ $tabla->id }}, '{{ $btnFinalizar }}')"
                    id="btn_finalizar_tramite" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                    Finalizar
                </button>
            @endif
        @endif
        @if(isset($btnResponder) && $btnResponder == 'SI' && Auth::user()->id_perfil != 50 && Auth::user()->id_perfil != 57)
            <button type="button" id="btn_finalizar_pago" onclick="finalizarConPago({{ $tabla->id }})"
                class="btn btn-default flotar" style="float:right; margin-right: 7px;">
                Finalizar pendiente de pago
            </button>

            <button type="button" id="btn_finalizar_director" onclick="enviarCiudadano({{ $tabla->id }})"
                class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Finalizar
            </button>

            <button type="button" id="btn_devolver" onclick="enviarCiudadano({{ $tabla->id }}, 'true')"
                class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                Devolver
            </button>

            @if(count($analistas) > 0)
                @include('vistas.modal_analistas', ['analistas' => $analistas])
                <button type="button" onclick="obtenerAnalistas()"
                    id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Asignar analista
                </button>
            @endif
        @elseif(isset($btnAsignarAnalistas) && $btnAsignarAnalistas == 'SI' && count($analistas) > 0)
            @include('vistas.modal_analistas', ['analistas' => $analistas])
            <button type="button" onclick="obtenerAnalistas()"
                id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                Asignar analista
            </button>
        @endif
        @if(isset($btnAnalista))
            @if(isset($eSubdirector) && $eSubdirector)
                @if($tabla->obtener_respuesta == 'NO' && $tabla->correo_electronico != null)
                    <button type="button" id="btn_finalizar_director" onclick="finalizarSubdirector({{ $tabla->id }})"
                        class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                        Finalizar trámite
                    </button>
                @elseif($tabla->obtener_respuesta == 'SI')
                    <button type="button" id="btn_finalizar_director" onclick="enviarSecretaria({{ $tabla->id }})"
                        class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                        Enviar a secretaría general
                    </button>
                @endif
            @endif
            <button type="button" onclick="guardaInformeAnalista({{ $tabla->id }})"
                id="btn_informe_analista" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                {{ isset($eSubdirector) && $eSubdirector ? 'Enviar informe a director' : 'Enviar informe' }}
            </button>
            @if($tabla->tipo != 'TRAMITES MUNICIPALES')
                @include('vistas.modal_direcciones', ['direcciones' => $direcciones])
                <button type="button" onclick="obtenerDirecciones()"
                    id="btn_solicitar_informe" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                    ¿Necesita informe de otras áreas?
                </button>
            @endif
            @if(App\User::analistaArchivar($tabla->tipo_tramite) || App\User::analistaArchivarTH($tabla->tipo_tramite))
                <button type="button" onclick="archivarTramiteAnalista({{ $tabla->id }})"
                    id="btn_archivar_informe" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                    Archivar
                </button>
            @endif
            @if(isset($btnAsignarAnalistas2) && count($analistas) > 0)
                @include('vistas.modal_analistas', ['analistas' => $analistas])
                <button type="button" onclick="obtenerAnalistas()"
                    id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Asignar analista
                </button>
            @endif
        @elseif(isset($btnCoordinador))
            <button type="button" onclick="archivarTramite({{ $tabla->id }})"
                id="btn_archivar_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                Archivar trámite
            </button>

            @if(isset($btnDonacion))
                <button type="button" onclick="donacion({{ $tabla->id }})"
                    id="btn_donacion" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                    Archivar como donación
                </button>
            @endif

            @if(count($analistas) > 0)
                @include('vistas.modal_analistas', ['analistas' => $analistas])
                <button type="button" onclick="obtenerAnalistas()"
                    id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Asignar analista
                </button>
            @endif
        @elseif(isset($btnGuardarInforme))
            <button type="button" onclick="guardarInforme({{ $tabla->id }})"
                id="btn_guardar_informe" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Guardar
            </button>
            <button type="button" onclick="archivarTramiteInforme({{ $tabla->id }})"
                id="btn_archivar_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                Archivar trámite
            </button>
            @if(count($analistas) > 0)
                @include('vistas.modal_analistas', ['analistas' => $analistas])
                <button type="button" onclick="obtenerAnalistas()"
                    id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Asignar analista
                </button>
            @endif
            <button type="button" onclick="devolverTramiteInforme({{ $tabla->id }})"
                id="btn_devolver_informe_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                Desvincularse
            </button>
        @elseif(isset($informeUnidad))
            <button type="button" onclick="guardarInforme({{ $tabla->id }}, 'asignados')"
                id="btn_guardar_informe" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Enviar informe
            </button>
            <button type="button" onclick="devolverTramiteInforme({{ $tabla->id }}, 'asignados')"
                id="btn_devolver_informe_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                Desvincularse
            </button>
        @elseif(isset($btnFinalizarTramite))
            @if($contestados > 0 && $contestados == $dirAsignados && isset($tabla->correo_electronico))
                {!! Form::submit("Finalizar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @elseif($contestados > 0 && $tabla->obtener_respuesta == 'NO' && isset($tabla->correo_electronico))
                <input type="hidden" value="{{ $tabla->id }}" id="tramite_id">
                <button type="button" id="finalizarImcompleto"
                    class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                    Enviar respuesta a ciudadano
                </button>
            @elseif(!isset($tabla->correo_electronico))
                <button type="button" id="btn_guardar" onclick="finalizarTramiteDirector({{$tabla->id}}, 'GENERAL', 'NO')"
                    class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                    Guardar como notificado
                </button>
            @else
                {!! Form::submit("Finalizar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @endif
            @elseif(isset($btnReservarTramite))
                {{-- {!! Form::submit("Reservar Trámite", array('id'=>'btn_reservar','class' => 'btn btn-warning', 'style' => 'float:right;margin-right: 7px;' )) !!} --}}
                <a id="btn_acuerdo" onclick="generarAcuerdo(this)"
                    class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Acuerdo de responsabilidad
                </a>
            @endif
        @endif
        {!! Form::close() !!}
        @if (isset($timelineTramites))
            <hr>
            <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div>
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">
                    @foreach ($timelineTramites as $item)
                        @php
                            $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                            $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
                        @endphp
                        <div class="vertical-timeline-block">
                            <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            </div>
                            <div class="vertical-timeline-content">
                                @if ($contador==0)
                                    <h2><i class="fa fa-save"></i> Inicial</h2>
                                @elseif($contador>=1)
                                    <h2><i class="fa fa-pencil"></i> Actualización</h2>
                                @endif
                                <div hidden> {{$contador+=1}} </div>
                                <p>
                                    <pre style="text-align: left; white-space: pre-line;">
                                        <strong>Asunto: </strong>{{$item->asunto}}
                                        @if(isset($item->direccion))
                                            <strong>Dirección a cargo: </strong>  {{$item->direccion}}
                                        @endif
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_ingreso}}
                                            @if(isset($item->fecha_fin))
                                                <strong>Fecha estimada de finalización: </strong>  {{$item->fecha_fin}}
                                            @endif
                                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                                        <strong>Estado de trámite: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->disposicion)  }};">{{$item->disposicion}}</span>
                                        @if(isset($item->observacion))
                                            <strong>Observación: </strong> {{$item->observacion}}
                                        @endif
                                        <br>
                                        <strong>Modificado por: </strong>  {{ $item->name == 'ADMINISTRADOR' ? 'CIUDADANO' : $item->name }}
                                    </pre>
                                </p>
                                <span class="vertical-date">
                                    {{fechas(2,$item->updated_at)}} <br />
                                    <small>{{fechas(1000,$item->updated_at)}} </small>
                                </span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
@stop
