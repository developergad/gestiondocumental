<html class="translated-ltr"><head>
    <meta charset="UTF-8">
    <title>Acuerdo de Responsabilidad - {{ $data['remitente'] }}</title>
    <script src="https://pp-panel.botscrew.com/file/js/widget-privacy-policy-bot.js"></script>
    <style>
        body{
            margin: 0px auto;
            max-width: 800px;
            font-family: "Helvetica Neue",Helvetica,Arial,FreeSans,sans-serif;
            padding: 25px 30px;
            font-size: 85%;
        }
       /* .tab {
            padding-bottom: 21px;
            border-bottom: 1px dotted #dfdfdf;
        }*/
    </style>
<link type="text/css" rel="stylesheet" charset="UTF-8" href="https://translate.googleapis.com/translate_static/css/translateelement.css"></head>
    <body>

                <div class="tab text-center" style="    text-align: center; ">
                    <img src="img/mantafirmes.png" class ="img-responsive text-center    " width="35%" >
                </div>
                <div class="tab">

                    <h1  style="text-align: center;  font-size: 1.5em; margin-top:20px" ><font style="text-align: center;"><font style="text-align: center;">ACUERDO DE RESPONSABILIDAD Y USO DE MEDIOS ELECTRÓNICOS</font></font></h1>

                    <hr>

                    <p style="text-align: justify;">
                        <font style="text-align: justify;"><font style=" text-align: justify;">
                            El Gobierno Autónomo Descentralizado Municipal del Cantón Manta con la finalidad de convertir a nuestra ciudad Manta en una Ciudad Digital,
                            incorpora para los ciudadanos, el Acuerdo de Responsabilidad y Uso de Medios Electrónicos. Con este antecedente <b>{{ $data['remitente'] }}</b>
                            en adelante “Ciudadano” con documento de identidad No.<b>{{ $data['cedula_remitente'] }}</b>, acepta recibir notificación electrónica de las comunicaciones
                            del GADMC Manta a través de su dirección de correo electrónico <b>sistemasic@manta.gob.ec</b> y a su vez en el portal web <strong>https://portalciudadano.manta.gob.ec</strong>
                            o cualquier otro acceso a servicios que el Gobierno Autónomo Descentralizado Municipal del Cantón Manta ponga a su disposición a través de Internet.
                            Dicho mensaje de datos se lo entiende como toda información creada, generada, procesada, enviada, recibida, comunicada o archivada por medios electrónicos.
                            Los documentos desmaterializados en mensajes de datos, de conformidad con lo establecido en la Ley de Comercio Electrónico, Firmas Electrónicas y Mensajes de Datos,
                            tienen el mismo valor jurídico que los documentos escritos, por lo cual el acceso a los mismos será entendido como el acceso al documento original.
                            El Ciudadano asume la responsabilidad total de la dirección electrónica proporcionada como titular de esta, debiendo cumplir con las obligaciones derivadas de tal titularidad.
                            La responsabilidad derivada de la falta de cuidado, de la indebida reserva, del mal uso o del uso por terceros autorizados o no, mediante mandato del titular, ocasionándose o no perjuicios,
                            será exclusivamente del Ciudadano titular de dicho correo electrónico o en su defecto de su respectivo representante legal.
                        </font></font>
                    </p>

                    <p style="text-align: justify;">
                        <font style="text-align: justify;"><font style=" text-align: justify;">
                            El Gobierno Autónomo Descentralizado Municipal del Cantón Manta a través de su portal web <strong>https://portalciudadano.manta.gob.ec</strong>
                            pone a disposición del Ciudadano un sistema de consulta que permita revisar las notificaciones enviadas a través de la Internet.
                            El Gobierno Autónomo Descentralizado Municipal del Cantón Manta, verificará por medio de sus herramientas informáticas, el día y hora exactos
                            en el que se produjo dicha recepción y sentará, por medio del funcionario competente, la constancia de notificación pertinente, como prueba
                            de haberse esta realizado. El Ciudadano se compromete a ingresar periódicamente al portal web, así como también revisar el correo electrónico
                            que señale en este Acuerdo, a fin de revisar las notificaciones que por dicho medio le sean periódicamente realizadas, la omisión en el cumplimiento
                            de esta obligación no afectará la validez jurídica de la notificación realizada, sin perjuicio de las responsabilidades y sanciones a que haya lugar.
                        </font></font>
                    </p>

                    <p style="text-align: justify;">
                        <font style="text-align: justify;"><font style=" text-align: justify;">
                            La suscripción del acuerdo implicará la aceptación de todas y cada una de las disposiciones establecidas en el mismo. El Ciudadano suscribe este acuerdo
                            por su propia iniciativa y se somete voluntariamente a lo aquí estipulado. El Gobierno Autónomo Descentralizado Municipal del Cantón Manta se reserva el
                            derecho de modificar las condiciones del servicio, el periodo y los procedimientos técnicos de identificación y validación de los datos transmitidos,
                            de acuerdo con sus necesidades. Cualquier información relativa, así como los envíos de notificaciones electrónicas, el Ciudadano las recibirá en la
                            siguiente dirección de correo electrónico <b>{{ $data['correo_electronico'] }}</b>.
                        </font></font>
                    </p>

                    <p style="text-align: justify;"><font style="text-align: justify;"><font style=" text-align: justify;">Manta, {{ fechas(600) }} </font></font></p>

                    <p style="text-align: center;"><font style="text-align: center;"><font style=" text-align: center;">
                       <br><br> __________________________
                        <br>{{ $data['remitente'] }}
                        <br>CI: {{ $data['cedula_remitente'] }}
                        <br>Tel: {{ $data['telefono'] }}
                        {{-- <br>Trámite N°: {{ $data['numtramite'] }} --}}
                    </font></font></p>
                </div>

<div class="goog-te-spinner-pos"><div class="goog-te-spinner-animation"><svg xmlns="http://www.w3.org/2000/svg" class="goog-te-spinner" width="96px" height="96px" viewBox="0 0 66 66"><circle class="goog-te-spinner-path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle></svg></div></div></body></html>
