@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    @include('tramitesalcaldia::tramitesinternos.errors')
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Fecha de inicio</th>
                                    <th>Tipo de trámite</th>
                                    <th>Asunto</th>
                                    <th>Remitente</th>
                                    <th>Dirección a atender</th>
                                    <th>Con copia</th>
                                    <th>Prioridad</th>
                                    <th>Fecha estimada de finalización</th>
                                    <th>Analistas asignados</th>
                                    <th>Archivo</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[ 0, 'DESC' ]],
                ajax: '{!! route('concejales.ajax') !!}',
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'pdfHtml5',
                        title: 'Trámites internos',
                        text: '<i class="fa fa-file-pdf"></i> PDF'
                    },
                    {
                        extend: 'excelHtml5',
                        title: 'Trámites internos',
                        footer: true
                    },
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'tipo_tramite', name: 'tipo_tramite', orderable: false, searchable: false },
                    { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                    { data: 'remitente', name: 'remitente' },
                    { data: 'asunto', name: 'asunto' },
                    { data: 'atender', name: 'atender' },
                    { data: 'copia', name: 'copia' },
                    { data: 'prioridad', name: 'prioridad' },
                    { data: 'fecha_fin', name: 'fecha_fin', orderable: false, searchable: false },
                    { data: 'analistas', name: 'analistas', orderable: false, searchable: false },
                    { data: 'adjunto', name: 'adjunto', orderable: false, searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@endsection
