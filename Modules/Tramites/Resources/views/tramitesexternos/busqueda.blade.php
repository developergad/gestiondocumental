@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        {!! Form::open(['method' => 'POST', 'id' => 'search-form', 'role'=>'form', 'class'=>'form-row', 'style' => 'margin: 0 0 0 0;']) !!}
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-3">
                                        <select type="text" class="form-control" id="disposicion" name="disposicion">
                                            <option hidden selected value="">ESTADOS</option>
                                            <option value="APROBADA">APROBADA</option>
                                            <option value="EN PROCESO">EN PROCESO</option>
                                            <option value="FINALIZADO">FINALIZADO</option>
                                            <option value="REVISION">REVISION</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-4">
                                        <select type="text" class="form-control" id="tipo" name="tipo">
                                            <option hidden selected value="">TIPOS</option>
                                            <option value="PERMISOS MUNICIPALES">PERMISOS MUNICIPALES</option>
                                            <option value="SECRETARIA GENERAL">SECRETARÍA GENERAL</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="numtramite" name="numtramite"
                                            value="{{ old('numtramite') }}" placeholder="Número de trámite" autocomplete="off">
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-4">
                                        <select type="text" class="form-control" id="id_direccion" name="id_direccion">
                                            <option hidden selected value="">DIRECCIÓN ASIGNADA</option>
                                            @foreach ($direcciones as $idx => $item)
                                                <option value="{{ $idx }}">{{ $item }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="col-sm-4">
                                        <select type="text" class="form-control" id="analistas" name="analistas" disabled>
                                            <option hidden selected value="">ANALISTAS ASIGNADOS</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <input type="text" class="form-control solonumeros" id="cedula" name="cedula"
                                            value="{{ old('cedula') }}" placeholder="Cédula / RUC" autocomplete="off" maxlength="13">
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-sm-3"></div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control mayuscula" id="remitente" name="remitente"
                                            value="{{ old('remitente') }}" placeholder="Remitente" autocomplete="off">
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="text" class="form-control mayuscula" id="asunto" name="asunto"
                                            value="{{ old('asunto') }}" placeholder="Asunto" autocomplete="off">
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <br>
                                <div class="row">
                                    <div class="col-sm-4 col-sm-offset-4">
                                        <button type="submit" class="btn btn-success">Buscar</button>
                                        <button type="button" class="btn btn-info" id="limpiar">Limpiar</button>
                                    </div>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <hr>
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Origen</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de trámite</th>
                                    <th>Remitente</th>
                                    <th>Asunto</th>
                                    <th>Dirección a atender</th>
                                    <th>Con copia</th>
                                    <th>Analista asignado</th>
                                    <th>Disposición</th>
                                    @if ($verArchivos)
                                        <th>Archivos</th>
                                    @endif
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var table;
        $(function() {
            $('#id_direccion').change(function() {
                let id = $(this);
                let analistas = $('#analistas');
                if(id.val() != '')
                {
                    $.ajax({
                        type: 'GET',
                        url: `/tramitesalcaldia/obteneranalistaspordireccion/${id.val()}`,
                    }).done((data) => {
                        analistas.find('option').remove();
                        analistas.append('<option hidden selected value="">ANALISTAS ASIGNADOS</option>');
                        if(data.ok) {
                            analistas.attr('disabled', false)
                            $(data.analistas).each(function(i, v) {
                                analistas.append('<option value="' + v.id + '">' + v.name + '</option>');
                                analistas.trigger('chosen:updated');
                            })
                        }
                        else analistas.attr('disabled', true)
                    })
                }
            })

            $('.solonumeros').on('input', function () {
                this.value = this.value.replace(/[^0-9.]/g,'');
            });

            table = $('#tabla-tramites').DataTable({
                // dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
                // "<'row'<'col-xs-12't>>"+
                // "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
                processing: true,
                serverSide: true,
                responsive: true,
                searching: false,
                order: [[ 0, 'DESC' ]],
                ajax: {
                    url: '{!! route('tramites.busqueda.ajax') !!}',
                    data: function(d) {
                        d.disposicion = $('#disposicion').val();
                        d.id_direccion = $('#id_direccion').val();
                        d.analistas = $('#analistas').val();
                        d.remitente = $('#remitente').val();
                        d.asunto = $('#asunto').val();
                        d.numtramite = $('input[name=numtramite]').val();
                        d.cedula = $('input[name=cedula]').val();
                        d.tipo = $('#tipo').val();
                    }
                },
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                "dom": 'Bfrtlip',
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>' //text: '<i class="fa fa-print"></i> Imprimir'
                    },
                    {
                        extend: 'copy',
                        text: '<i class="fa fa-copy"></i>' //text: '<i class="fa fa-copy"></i> Copiar'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>' //text: '<i class="fa fa-file-excel-o"></i> Excel'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>', //text: '<i class="fa fa-file-pdf-o"></i> PDF',
                        orientation: 'landscape',
                            title: '{{$configuraciongeneral[0]}}'
                    },
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye-slash"></i>' //text: '<i class="fa fa-eye-slash"></i> Ocultar Columnas'
                    }
                ],
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'numtramite', name: 'numtramite' },
                    { data: 'origen', name: 'origen' },
                    { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                    { data: 'tipo_tramite', name: 'tipo_tramite' },
                    { data: 'remitente', name: 'remitente' },
                    { data: 'asunto', name: 'asunto' },
                    { data: 'atender', name: 'atender' },
                    { data: 'copia', name: 'copia' },
                    { data: 'analista', name: 'analista' },
                    { data: 'disposicion', name: 'disposicion' },
                    @if($verArchivos)
                        { data: 'archivos', name: 'archivos', orderable: false, searchable: false },
                    @endif
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });

            $('#search-form').on('submit', function(e) {
                table.draw();
                e.preventDefault();
            });

            $('#limpiar').on('click', function(e) {
                $('#cedula').val('');
                $('#numtramite').val('');
                $('#remitente').val('');
                $('#asunto').val('');
                $('#id_direccion').val('');
                $('#analistas').val('');
                $('#tipo').val('');
                $('#disposicion').val('');
                table.draw();
                e.preventDefault();
            });
        });

        function inactivarTramite(tramite) {
            Swal.fire({
                title: '¿Seguro de continuar?',
                allowOutsideClick: false,
                width: '500px',
                allowEscapeKey: false,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Continuar',
                cancelButtonText: 'Cancelar',
                reverseButtons: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return new Promise(function(resolve) {
                        $.ajax({
                          type: 'POST',
                          url: `/tramitesalcaldia/inactivartramite/${tramite}`,
                          headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
                        }).done((data) => {
                          if(data.ok)
                          {
                            table.draw();
                            Swal.fire({
                              title: data.message,
                              allowOutsideClick: false,
                              width: '500px',
                              allowEscapeKey: false
                            })
                          }
                          else
                          {
                            Swal.fire({
                              title: data.message,
                              text: data.exception,
                              allowOutsideClick: false,
                              width: '500px',
                              allowEscapeKey: false
                            })
                          }
                        })
                    })
                }
            })
        }
    </script>
@endsection
