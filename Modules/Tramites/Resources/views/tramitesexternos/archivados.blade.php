@extends('layouts.layout')

@section ('titulo') {{ $configuraciongeneral[0] }} @stop

@section('contenido')
<div class="row wrapper border-bottom">
    <div class="col-lg-10">
        <h1 class="font-bold text-primary">{{ $configuraciongeneral[4] }}</h1>
    </div>
</div>
<div class="animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    @if (Session::has('message'))
                        <script>
                            $(document).ready(function() {
                                toastr.options = {
                                    "positionClass": "toast-bottom-right"
                                };
                                toastr["success"]("{{ Session::get('message') }}");
                            });
                        </script>
                        <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <table class="table table-striped table-bordered table-hover display" id="tabla-tramites" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Número de trámite</th>
                                    <th>Fecha de ingreso</th>
                                    <th>Tipo de trámite</th>
                                    <th>Remitente</th>
                                    <th>Dirección a atender</th>
                                    <th>Archivado por</th>
                                    <th>Adjunto</th>
                                    <th>Archivos</th>
                                    <th>Acción</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#tabla-tramites').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order: [[ 0, 'DESC' ]],
                ajax: '{!! route('tramites.archivados.ajax') !!}',
                language: { url: '../../language/es.json' },
                lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'TODOS']],
                // dom: 'Blfrtip',
                // buttons: [
                //     {
                //         extend: 'pdfHtml5',
                //         title: 'Trámites internos',
                //         text: '<i class="fa fa-file-pdf"></i> PDF'
                //     },
                //     {
                //         extend: 'excelHtml5',
                //         title: 'Trámites internos',
                //         footer: true
                //     },
                // ],
                columns: [
                    @if(Auth::user()->id_perfil == 10 || Auth::user()->id_perfil == 68 || Auth::user()->id_perfil == 20 || Auth::user()->id_perfil == 54 || Auth::user()->id_perfil == 50 || Auth::user()->id_perfil == 57)
                        { data: 'cabecera.id', name: 'cabecera.id' },
                        { data: 'cabecera.numtramite', name: 'cabecera.numtramite' },
                        { data: 'cabecera.fecha_ingreso', name: 'cabecera.fecha_ingreso' },
                        { data: 'tipoTramite', name: 'tipoTramite' },
                        { data: 'cabecera.remitente', name: 'cabecera.remitente' },
                    @else
                    { data: 'id', name: 'id' },
                        { data: 'numtramite', name: 'numtramite' },
                        { data: 'fecha_ingreso', name: 'fecha_ingreso' },
                        { data: 'tipoTramite', name: 'tipoTramite' },
                        { data: 'remitente', name: 'remitente' },
                    @endif
                    { data: 'direccion_atender', name: 'direccion_atender' },
                    { data: 'archivado_por', name: 'archivado_por' },
                    { data: 'archivo', name: 'archivo' },
                    { data: 'archivos_director', name: 'archivos_director' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ]
            });
        });
    </script>
@endsection
