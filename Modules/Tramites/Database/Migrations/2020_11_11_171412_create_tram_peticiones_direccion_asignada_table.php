<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramPeticionesDireccionAsignadaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_peticiones_direccion_asignada', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_cab')->constrained('tram_peticiones_cab');
            $table->integer('id_direccion');
            $table->integer('direccion_solicitante')->nullable();
            $table->integer('respondido')->default(0);
            $table->text('observacion');
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_peticiones_direccion_asignada');
    }
}
