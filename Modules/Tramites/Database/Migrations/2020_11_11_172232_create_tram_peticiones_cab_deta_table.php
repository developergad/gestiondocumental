<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramPeticionesCabDetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_peticiones_cab_deta', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_usuario')->constrained('users');
            $table->foreignId('id_tram_cab')->constrained('tram_peticiones_cab');
            $table->string('numtramite');
            $table->string('remintente');
            $table->string('asunto');
            $table->text('peticion');
            $table->string('prioridad');
            $table->text('recomendacion')->nullable();
            $table->string('disposicion');
            $table->text('observacion')->nullable();
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->string('ip');
            $table->string('pc');
            $table->date('fecha_ingreso');
            $table->date('fecha_respuesta')->nullable();
            $table->integer('estado_proceso');
            $table->date('fecha_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_peticiones_cab_deta');
    }
}
