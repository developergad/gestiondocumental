<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramPeticionesCabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_peticiones_cab', function (Blueprint $table) {
            $table->id();
            $table->foreignId('creado_por')->constrained('users');
            $table->foreignId('id_usuario')->constrained('users');
            $table->foreignId('id_area')->nullable()->constrained('tram_peticiones_areas');
            $table->foreignId('tipo_tramite')->nullable()->constrained('tram_peticiones_tipo_tramite');
            $table->integer('id_usuario_externo')->nullable();
            $table->string('numtramite');
            $table->string('remintente');
            $table->string('asunto');
            $table->text('peticion');
            $table->string('prioridad');
            $table->text('recomendacion')->nullable();
            $table->string('disposicion')->default('PENDIENTE');
            $table->integer('id_prioridad');
            $table->text('observacion')->nullable();
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->enum('tipo', ['VENTANILLA', 'SECRETARIA GENERAL', 'TRAMITES MUNICIPALES'])->default('VENTANILLA');
            $table->string('ip');
            $table->string('pc');
            $table->date('fecha_ingreso');
            $table->date('fecha_respuesta')->nullable();
            $table->integer('estado_proceso')->default(0);
            $table->string('direccion_atender')->nullable();
            $table->string('direccion_informar')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->string('cedula_remitente', 15);
            $table->string('correo_electronico')->nullable();
            $table->string('telefono', 15);
            $table->string('telefono_2', 15)->nullable();
            $table->string('direccion');
            $table->string('referencia')->nullable();
            $table->integer('id_parroquia');
            $table->integer('id_barrio');
            $table->string('finalizar_por', 20)->default('GENERAL');
            $table->integer('asignado_a');
            $table->enum('obtener_respuesta', ['SI', 'NO']);
            $table->text('observacion_2')->nullable();
            $table->text('observacion_general')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_peticiones_cab');
    }
}
