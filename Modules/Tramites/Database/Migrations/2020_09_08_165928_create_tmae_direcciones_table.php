<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmaeDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmae_direcciones', function (Blueprint $table) {
            $table->id();
            $table->string('direccion');
            $table->string('alias');
            $table->string('mta')->nullable();
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreignId('id_direccion')->nullable()->constrained('tmae_direcciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmae_direcciones');
    }
}
