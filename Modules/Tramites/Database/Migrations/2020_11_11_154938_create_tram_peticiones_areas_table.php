<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramPeticionesAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_peticiones_areas', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_direccion')->constrained('tmae_direcciones');
            $table->string('area');
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_peticiones_areas');
    }
}
