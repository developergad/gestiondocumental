<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmaTramAutorizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tma_tram_autorizacion', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_usuario')->constrained('users');
            $table->string('cedula');
            $table->string('nombre');
            $table->string('adjunto')->nullable();
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->enum('estado_autorizacion', ['PENDIENTE', 'APROBADO'])->default('PENDIENTE');
            $table->integer('id_direccion');
            $table->string('cargo')->nullable();
            $table->string('numero');
            $table->string('correo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tma_tram_autorizacion');
    }
}
