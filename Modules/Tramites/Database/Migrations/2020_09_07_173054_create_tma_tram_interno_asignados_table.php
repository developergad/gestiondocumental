<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmaTramInternoAsignadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tma_tram_interno_asignados', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_tramite')->constrained('tma_tram_interno');
            $table->foreignId('id_usuario_asignado')->constrained('users');
            $table->integer('solicitante')->nullable();
            $table->integer('respuesta')->nullable();
            $table->text('observacion')->nullable();
            $table->string('estado');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tma_tram_interno_asignados');
    }
}
