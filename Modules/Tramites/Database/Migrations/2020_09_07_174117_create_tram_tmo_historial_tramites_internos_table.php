<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramTmoHistorialTramitesInternosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_tmo_historial_tramites_internos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tramite_id')->constrained('tma_tram_interno');
            $table->foreignId('id_remitente')->constrained('users');
            $table->foreignId('id_usuario')->constrained('users');
            $table->string('numtramite');
            $table->string('estado');
            $table->enum('tipo_tramite', ['MEMO', 'OFICIO', 'INFORME']);
            $table->string('asunto');
            $table->text('peticion');
            $table->text('delegados_para')->nullable();
            $table->text('delegados_copia')->nullable();
            $table->string('tipo_firma')->nullable();
            $table->string('tipo_envio')->nullable();
            $table->string('modificio_fecha', 2)->default('NO');
            $table->dateTime('fecha')->nullable();
            $table->string('numtramite_externo', 45)->nullable();
            $table->string('nombre_informe', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_tmo_historial_tramites_internos');
    }
}
