<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTmaTramInternoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tma_tram_interno', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_remitente')->constrained('users');
            $table->foreignId('elaborado_por')->nullable()->constrained('users');
            $table->string('numtramite');
            $table->enum('tipo_tramite', ['MEMO', 'OFICIO', 'INFORME']);
            $table->string('asunto');
            $table->longText('peticion');
            $table->string('observacion')->nullable();
            $table->string('documento')->nullable();
            $table->text('delegados_para')->nullable();
            $table->text('delegados_copia')->nullable();
            $table->string('tipo_firma')->nullable();
            $table->string('tipo_envio')->nullable();
            $table->string('modifico_fecha', 2)->default('NO');
            $table->dateTime('fecha');
            $table->string('numtramite_externo', 45)->nullable();
            $table->string('nombre_informe')->nullable();
            $table->string('texto_adicional')->nullable();
            $table->string('cargo_adicional')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tma_tram_interno');
    }
}
