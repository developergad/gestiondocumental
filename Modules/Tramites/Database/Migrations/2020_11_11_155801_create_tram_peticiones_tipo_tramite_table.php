<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTramPeticionesTipoTramiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tram_peticiones_tipo_tramite', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_area')->constrained('tram_peticiones_areas');
            $table->string('valor');
            $table->integer('dias_despachar');
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tram_peticiones_tipo_tramite');
    }
}
