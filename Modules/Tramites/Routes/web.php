<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('tramites')->group(function() {
    Route::get('/', 'TramitesController@index');

    /**ÁREAS Y TIPOS DE TRÁMITES EXTERNOS */
    Route::resource('areas', 'AreasController', ['except' => ['show', 'destroy']]);
	Route::get('areasajax', 'AreasController@areasajax');
    Route::resource('tipostramites', 'TipoTramiteController', ['except' => ['show', 'destroy']]);
    Route::get('tipostramitesajax', 'TipoTramiteController@tipostramitesajax');

    /**TRÁMITES EXTERNOS */
    Route::resource('externos', 'TramAlcaldiaCabController');
	Route::resource('tipostramites', 'TipoTramiteController', ['except' => ['show', 'destroy']]);
    Route::get('ContratoElectronico', 'TramAlcaldiaCabController@PolicyUsoDeMediosElectronicos')->name('contrato');
    Route::get('tramitesdespacho', 'TramAlcaldiaCabController@index3');
	Route::get('tramitesdespachoatendidosnegados', 'TramAlcaldiaCabController@indexatendidos');
	Route::get('tramitesdespachoatendidosaprobados', 'TramAlcaldiaCabController@indexatendidos');
	Route::get('consultar_cedula/{cedula}', 'TramAlcaldiaCabController@consultar_cedula');
	Route::get('obtener_ubicacion/{direccion}', 'TramAlcaldiaCabController@obtener_ubicacion');
    Route::get('obtenerbarrios/{parroquia}', 'TramAlcaldiaCabController@obtenerBarrios');
    Route::get('tramitesrespondidos', 'TramAlcaldiaCabController@index2')->name('tramitesrespondidos');
	Route::get('tramitesfinalizados', 'TramAlcaldiaCabController@indexFinalizados');
	Route::get('tramitesrevision', 'TramAlcaldiaCabController@indexRevision');
	Route::get('tramitesrevisionatender', 'TramAlcaldiaCabController@indexRevisionAtender');
	Route::get('tramitesenrevision', 'TramAlcaldiaCabController@revisionAnalista');
	Route::get('tramitesrevisar', 'TramAlcaldiaCabController@tramitesRevisar')->name('tramites.revisar');
	Route::get('tramitesrevisarajax', 'TramAlcaldiaCabController@tramitesRevisarAjax')->name('tramites.revisar.ajax');
	Route::get('tramitesdevueltos', 'TramAlcaldiaCabController@indexDevueltos');
    Route::get('calcular_fecha_despachar/{tramite}/{fecha_inicial}', 'TramAlcaldiaCabController@calcular_fecha_despachar');
    Route::get('despachartramite', 'TramAlcaldiaCabController@editdespacho');
	Route::get('finalizartramite', 'TramAlcaldiaCabController@editFinalizar')->name('tramite.editfinalizar');
    Route::post('devolver/{id}/{timeline}', 'TramAlcaldiaCabController@devolver');
    Route::get('asignados', 'AsignarAnalistasController@asignados');
	Route::get('asignadoscontestados', 'AsignarAnalistasController@asignadosContestados');
	Route::get('asignadosajax', 'AsignarAnalistasController@asignadosAjax')->name('asignados.ajax');
	Route::post('asignaranalista/{id}', 'AsignarAnalistasController@asignarAnalista');
	Route::post('guardarinformeanalista/{id}', 'AsignarAnalistasController@guardarInformeAnalista');
	Route::post('enviarsecretaria/{id}', 'AsignarAnalistasController@enviarSecretaria');
    Route::post('archivartramiteanalista/{id}', 'AsignarAnalistasController@archivarTramiteAnalista');
    Route::get('obtenerusuario/{id}', 'TramAlcaldiaCabController@obtenerUsuario');

    Route::get('tramitesexternos', 'TramAlcaldiaCabController@tramites');
	Route::get('tramitesexternosajax', 'TramAlcaldiaCabController@tramitesAjax')->name('tramites.externos.ajax');
	Route::get('tramitesexternosportal', 'TramAlcaldiaCabController@tramitesPortal');
	Route::get('tramitesbusqueda', 'TramAlcaldiaCabController@tramitesBusqueda');
    Route::get('tramitesingresadosajax', 'TramAlcaldiaCabController@tramitesBusquedaAjax')->name('tramites.busqueda.ajax');

    Route::get('reservados', 'TramAlcaldiaProcesosController@tramitesReservados');
	Route::get('tramitesreservadosajax', 'TramAlcaldiaProcesosController@tramitesReservadosAjax');
	Route::get('concopia', 'TramAlcaldiaProcesosController@conCopia');
	Route::get('tramitesconcopiasajax', 'TramAlcaldiaProcesosController@tramitesConCopiasAjax');
	Route::post('desvinculartramite/{id}', 'TramAlcaldiaProcesosController@desvincularTramite');
	Route::post('solicitarinforme/{id}', 'TramAlcaldiaProcesosController@solicitarInforme')->name('solicitarinforme');
	Route::post('notificarcoordinador/{id}/{direccion}', 'TramAlcaldiaProcesosController@notificarCoordinador');
	Route::post('enviaraprobacion/{id}', 'TramAlcaldiaProcesosController@enviarAprobacion');
	Route::get('solicitudesinforme', 'TramAlcaldiaProcesosController@solicitudesInforme');
	Route::post('guardarinforme/{id}', 'TramAlcaldiaProcesosController@guardarInforme');
	Route::post('devolverdirector/{id}/{id_direccion}', 'TramAlcaldiaProcesosController@devolverDirector');
	Route::post('reservartramite', 'TramAlcaldiaProcesosController@reservarTramite');
	Route::post('finalizartramitedirector/{id}', 'TramAlcaldiaProcesosController@finalizarTramiteDirector');
	Route::post('finalizarsubdirector/{id}', 'TramAlcaldiaProcesosController@finalizarTramiteSubdirector');
	Route::post('finalizarimcompleto/{id}', 'TramAlcaldiaProcesosController@finalizarImcompleto');
	Route::get('obtenertipostramite/{area}', 'TramAlcaldiaProcesosController@obtenerTiposTramite');
	Route::post('eliminararchivotramite', 'TramAlcaldiaProcesosController@eliminarArchivo');
	Route::post('subirarchivotramite', 'TramAlcaldiaProcesosController@subirArchivo')->name('subirarchivo.tramite');
	Route::post('subiralcancetramite', 'TramAlcaldiaProcesosController@subirArchivoAlcance')->name('subiralcance.tramite');
	Route::post('subiracuerdo', 'TramAlcaldiaProcesosController@subirArchivoAcuerdo')->name('subiracuerdo.tramite');
	Route::post('eliminaracuerdo', 'TramAlcaldiaProcesosController@eliminarArchivoAcuerdo');
	Route::get('cargaracuerdo/{cedula}', 'TramAlcaldiaProcesosController@cargarArchivoAcuerdo');
	Route::post('eliminararchivoalcance', 'TramAlcaldiaProcesosController@eliminarArchivoAlcance');
	Route::post('enviarrespuestaciudadano/{id}', 'TramAlcaldiaProcesosController@enviarRespuestaCiudadano');
	Route::post('finalizarconpago/{id}', 'TramAlcaldiaProcesosController@finalizarConPago');
	Route::post('enviardonacion/{id}', 'TramAlcaldiaProcesosController@enviarDonacion');
    Route::post('devolverventanilla/{id}', 'TramAlcaldiaProcesosController@devolverVentanilla');

    Route::get('tramitesingresados', 'TramAlcaldiaProcesosController@tramitesIngresadoPortal');
	Route::get('tramitesingresadoajax', 'TramAlcaldiaProcesosController@tramitesIngresadoPortalAjax')->name('ingresados.portal.ajax');
	Route::get('tramitesingresadosfinalizados', 'TramAlcaldiaProcesosController@tramitesFinalizadosPortal');
	Route::get('tramitesingresadosfinalizadosajax', 'TramAlcaldiaProcesosController@tramitesFinalizadosPortalAjax')->name('finalizados.portal.ajax');
	Route::post('archivartramite/{id}', 'TramAlcaldiaProcesosController@archivarTramite');
	Route::post('archivartramiteinforme/{id}', 'TramAlcaldiaProcesosController@archivarTramiteInforme');
	Route::get('tramitesarchivados', 'TramAlcaldiaProcesosController@tramitesArchivados');
	Route::get('tramitesarchivadosajax', 'TramAlcaldiaProcesosController@tramitesArchivadosAjax')->name('tramites.archivados.ajax');
	Route::get('tramitesnotificar', 'TramAlcaldiaProcesosController@tramitesNotificar');
	Route::get('tramitesnotificarajax', 'TramAlcaldiaProcesosController@tramitesNotificarAjax');
	Route::get('obteneranalistaspordireccion/{id}', 'TramAlcaldiaCabController@obtenerAnalistasPorDireccion');
    Route::post('devolvertramiteinforme/{id}', 'TramAlcaldiaProcesosController@devolverTramiteInforme');

    Route::get('rechazadostramitesAlcaldiajax', 'TramAlcaldiaCabController@rechazadostramitesAlcaldiajax');
	Route::get('tramitesAlcaldiaDevueltosajax', 'TramAlcaldiaCabController@tramitesAlcaldiaDevueltosajax');
	Route::get('tramitesAlcaldiajax', 'TramAlcaldiaCabController@tramitesAlcaldiajax');
	Route::get('tramitesAlcaldiaPenidentesajax', 'TramAlcaldiaCabController@tramitesAlcaldiaPenidentesajax');
	Route::get('tramitesAlcaldiaDespachadosajax', 'TramAlcaldiaCabController@tramitesAlcaldiaDespachadosajax');
	Route::get('tramitesAlcaldiaEjecutadosajax', 'TramAlcaldiaCabController@tramitesAlcaldiaEjecutadosajax');
	Route::get('tramitesAlcaldiaFinalizadosajax', 'TramAlcaldiaCabController@tramitesAlcaldiaFinalizadosajax');
	Route::get('recibo/{id}', 'TramAlcaldiaCabController@recibo');

    /**AUTORIZACIONES PARA FIRMA QR (TRÁMITES INTERNOS)*/
    Route::resource('autorizaciones', 'TramInternoAutorizacionController');
	Route::get('tramitesAutorizacionesjax', 'TramInternoAutorizacionController@tramitesAutorizacionesjax');
	Route::get('descargarFormatoAutorizacion/{cedula}', 'TramInternoAutorizacionController@descargarFormatoAutorizacion');

    /**TRÁMITES INTERNOS */
    Route::get('tramitesinternos/recibidos', 'TramitesInternosController@recibidos')->name('tramites.recibidos');
	Route::get('tramitesinternos/copia', 'TramitesInternosController@conCopia')->name('tramites.copia');
	Route::get('tramitesinternos/contestados', 'TramitesInternosController@contestados')->name('tramites.contestados');
	Route::get('tramitesinternos/archivados', 'TramitesInternosController@archivados')->name('tramites.archivados');
	Route::get('tramitesinternos/borradores', 'TramitesInternosController@borradores')->name('tramites.borradores');
	Route::get('tramitesinternos/oficios', 'TramitesInternosController@oficios')->name('oficios.index');
	Route::get('tramitesinternos/oficiosajax', 'TramitesInternosController@oficiosAjax')->name('oficios.ajax');
	Route::get('tramitesinternoshistorial/{id}', 'TramitesInternosController@show')->name('internos.show');
	Route::get('agregarhistorial', 'TramitesInternosController@agregarHistorial');
	Route::post('asignaranalistainternos/{id}', 'TramitesInternosController@asignarAnalistaInternos');
	Route::get('tramitesinternos/asignados', 'TramitesInternosController@asignados');
	Route::get('cambiarremitente', 'TramitesInternosController@cambiarRemitente');
	Route::post('eliminarBorrador', 'TramitesInternosController@eliminarBorrador');

	Route::resource('tramitesinternos', 'TramitesInternosController');
	Route::get('indexajax', 'TramitesInternosController@indexajax')->name('tramites.ajax');
	Route::get('copiaedit/{id}', 'TramitesInternosController@copiaEdit')->name('copia.edit');
	Route::get('revisionedit/{id}', 'TramitesInternosController@revisionEdit')->name('revision.edit');
	Route::get('asignaranalistas/{id}', 'TramitesInternosController@asignarAnalistas')->name('asignar.analista');
	Route::put('guardarrevision/{id}', 'TramitesInternosController@guardarRevision')->name('revision.store');
	Route::post('desvinculartramiteinterno/{id}', 'TramitesInternosController@desvincularTramiteInterno');
	Route::post('archivartramiteinterno/{id}', 'TramitesInternosController@archivarTramite');
    Route::get('consultardireccion', 'TramitesInternosController@consultardireccion');
    Route::get("getDate", "TramitesInternosController@getDate");

    Route::resource('direcciones', 'DireccionesController');
	Route::get("getusuariosdireccion","DireccionesController@getusuariosdireccion");
	Route::get("getcordinaciondireccion","DireccionesController@getcordinaciondireccion");
	Route::get("direccionesajax", "DireccionesController@direccionesajax");

	////firma electronica
	Route::post('generarDocumento', 'TramitesInternosController@generarDocumento')->name('generarDocumento');
	Route::get('generarDocumento', 'TramitesInternosController@generarDocumento')->name('generarDocumento');
	Route::get('validarDocumento', 'TramitesInternosController@validarDocumento')->name('validarDocumento');
	Route::post('generarCodigo', 'TramitesInternosController@generarCodigo')->name('generarCodigo');
});
