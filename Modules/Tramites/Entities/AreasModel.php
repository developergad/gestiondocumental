<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class AreasModel extends Model
{
    protected $table = 'tram_peticiones_areas';
    protected $fillable = ['id_direccion', 'area', 'estado'];
}
