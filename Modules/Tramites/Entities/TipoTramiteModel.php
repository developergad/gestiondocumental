<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class TipoTramiteModel extends Model
{
    protected $table = 'tram_peticiones_tipo_tramite';
    protected $fillable = ['id_area', 'valor', 'dias_despachar', 'estado'];
}
