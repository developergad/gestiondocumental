<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class HistorialInternosModel extends Model
{
    protected $table = 'tram_tmo_historial_tramites_internos';
    protected $guarded = ['id'];
}
