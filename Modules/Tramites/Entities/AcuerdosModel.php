<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class AcuerdosModel extends Model
{
    protected $table = 'tmov_tram_acuerdos';
    protected $fillable = ['id_usuario', 'cedula', 'ruta', 'nombre', 'tipo', 'estado'];
}
