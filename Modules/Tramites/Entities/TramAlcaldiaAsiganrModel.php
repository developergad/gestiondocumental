<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class TramAlcaldiaAsiganrModel extends Model
{
    protected $table = 'tram_peticiones_direccion_asignada';
    protected $fillable = ['id_cab', 'id_direccion', 'estado', 'direccion_solicitante', 'respondido', 'observacion'];

    public function cabecera()
    {
        return $this->belongsTo(TramAlcaldiaCabModel::class, 'id_cab');
    }
}
