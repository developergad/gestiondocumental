<?php

namespace Modules\Tramites\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;

class TramiteInternoModel extends Model
{
    protected $table = 'tma_tram_interno';

    protected $guarded = ['id'];

    public function remitente()
    {
        return $this->belongsTo(User::class, 'id_remitente');
    }

    public function asignados()
    {
        return $this->hasMany(AsignadoTramiteInternoModel::class, 'id_tramite');
    }

    public function anexos()
    {
        return $this->hasMany(AnexosModel::class, 'id_referencia');
    }
}
