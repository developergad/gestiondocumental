<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class TramInternoAutorizacionesModel extends Model
{
    protected $table = 'tma_tram_autorizacion';
    public static function rules($id = 0, $merge = [])
    {
        return array_merge(
            [
                // 'adjunto' => 'required'
            ],
            $merge
        );
    }
    public static function rules_2($id = 0, $merge = [])
    {
        return array_merge(
            [],
            $merge
        );
    }
}
