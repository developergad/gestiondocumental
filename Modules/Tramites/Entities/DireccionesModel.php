<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class direccionesModel extends Model
{
    protected $table = 'tmae_direcciones';
    protected $fillable = [];

    public static function rules($id=0, $merge=[])
    {
		return array_merge([
            'direccion' => 'required|unique:tmae_direcciones'. ($id ? ",id,$id" : ''),
            'alias' => 'required'
		], $merge);
    }
}
