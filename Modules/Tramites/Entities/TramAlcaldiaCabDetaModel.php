<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class TramAlcaldiaCabDetaModel extends Model
{
    protected $table="tram_peticiones_cab_deta";
    protected $guarded = ['id'];
}
