<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class AnexosModel extends Model
{
    protected $table = 'tmov_tram_anexos';
    protected $fillable = ['usuario_id', 'id_referencia', 'ruta', 'nombre', 'hojas', 'descripcion', 'tipo'];

    public function usuario()
    {
        return $this->belongsTo(User::class, 'usuario_id');
    }
}
