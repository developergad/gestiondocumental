<?php

namespace Modules\Tramites\Entities;

use Illuminate\Database\Eloquent\Model;

class TramInternoCodigosModel extends Model
{
    protected $table = 'tma_codigos_tramites_internos';
}
