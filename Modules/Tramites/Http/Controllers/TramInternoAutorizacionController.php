<?php

namespace Modules\Tramites\Http\Controllers;

use stdClass;
use App\User;
use App\PerfilModel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NotificacionesController;
use Modules\Tramites\Entities\direccionesModel;
use Modules\Tramites\Entities\TramInternoAutorizacionesModel;

class TramInternoAutorizacionController extends Controller
{

    public $configuraciongeneral = array("Autorización Trámites internos", "tramites/autorizaciones", "index", 6 => "tramites/tramitesAutorizacionesjax", 7 => "autorizaciones");
    public $escoja = array(null => "Escoja opción...");
    public $objetos = '[
		{"Tipo":"textdisabled3","Descripcion":"Número","Nombre":"numero","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"cedula usuario_sistema","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textdisabled","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Correo","Nombre":"correo","Clase":"email","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Cargo","Nombre":"cargo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"html2","Descripcion":"Formato","Nombre":"formato","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"file","Descripcion":"Adjunto","Nombre":"adjunto","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Estado","Nombre":"estado_autorizacion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Perfil","Nombre":"id_perfil","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" }
    ]';

    var $estados = [
        'PENDIENTE' => 'PENDIENTE',
        'APROBADO' => 'APROBADO'
    ];

    var $cargo = [
        'ANALISTA' => 'ANALISTA',
        'CAGO X' => 'CAGO X'
    ];

    var $filtros = '[
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Cedula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
    ]';

    protected $notificacion;

    var $validarjs = array(
        "adjunto" => "adjunto: {
            required: true
        }"
    );

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
        $this->archivos_path = public_path('archivos_sistema');
    }
    public function verpermisos($objetos = array(), $tipo = "index")
    {
        $tabla =  TramInternoAutorizacionesModel::from('tma_tram_autorizacion as au')->select('au.*')
            ->join("users as u", "u.id", "=", "au.id_usuario");
        return array($objetos, $tabla);
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $filtros = json_decode($this->filtros);
        unset($objetos[6]);
        $filtros[0]->Valor = $this->estados;
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $eliminar = 'no';
        if ($id_tipo_pefil->tipo == 1) {
            $eliminar = 'si';
            $create = 'si';
        } else {
            $eliminar = 'si';

            $create = 'si';
        }

        // $filtros = null;
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            'filtros' => $filtros,
            "configuraciongeneral" => $this->configuraciongeneral,
            'create' => $create,
            'delete' => $eliminar
        ]);
    }
    public function tramitesAutorizacionesjax(Request $request)
    {
        $objetos = json_decode($this->objetos);
        $estado = request()->estado;
        $tabla  = TramInternoAutorizacionesModel::from('tma_tram_autorizacion as au')->select('au.*', 'd.direccion as id_direccion')
            ->join("users as u", "u.id", "=", "au.id_usuario")
            ->join("tmae_direcciones as d", "d.id", "=", "au.id_direccion")
            ->where(["au.estado" => "ACT", 'au.estado_autorizacion' => $estado]);
        $columns = array();
        $columns[] = 'id';
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';
        $cedula = request()->cedula;
        // dd($cedula);
        if ($cedula != null) {
            $tabla  = $tabla->where(['au.cedula' => $cedula]);
        }

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit == '-1') {
                $posts = $tabla
                    ->orderBy($order, $dir)
                    ->get();
            } else {
                $posts = $tabla->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir)
                    ->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit == '-1') {
                $posts = $tabla->where(function ($query) use ($search) {
                    $query->where('au.cedula', 'LIKE', "%{$search}%")
                        ->orWhere('au.nombre', 'LIKE', "%{$search}%");
                })
                    ->orderBy($order, $dir);
                $totalFiltered = $posts->count();
                $posts = $posts->get();
            } else {
                $posts = $tabla->where(function ($query) use ($search) {
                    $query->where('au.cedula', 'LIKE', "%{$search}%")
                        ->orWhere('au.nombre', 'LIKE', "%{$search}%");
                })
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy($order, $dir);
                $totalFiltered = $posts->count();
                $posts = $posts->get();
            }
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $acciones = '';
                // dd(Auth::user());
                if (Auth::user()->id == 1182  || Auth::user()->id_perfil == 1) {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash  btn btn-danger"></i></a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="' . csrf_token() . '">
                    <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                    $acciones .= link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-info'));
                }
                if ($post->adjunto != null) {
                    if (strpos($post->adjunto, 'pdf') === false) {
                        $adjunto = "<a onclick='mostrarImagen(\"" . URL::to('/archivos_sistema/' . $post->adjunto) . "\")' class='btn btn-info dropdown-toggle '><i class='fa fa-picture-o'></i></a>";
                    } else {
                        $adjunto = ($post->adjunto != null) ? "<a href='" . URL::to('/archivos_sistema/' . $post->adjunto) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>" : "";
                    }
                } else {
                    $adjunto = '';
                }

                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }
                // $nestedData['id_direccion'] =$post->id_direccion;
                $nestedData['adjunto'] = $adjunto;
                $perfil = User::join('ad_perfil as p', 'p.id', 'users.id_perfil')->where('cedula', $post->cedula)->first();
                if ($perfil) {
                    $nestedData['id_perfil'] = $perfil->nombre_perfil;
                } else {
                    $nestedData['id_perfil'] = '';
                }
                $nestedData['acciones'] = $acciones;

                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    public function guardar($id)
    {
        $input = request()->all();
        // show($input);
        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new TramInternoAutorizacionesModel();
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
            $existe = TramInternoAutorizacionesModel::where(['cedula' => request()->cedula, 'estado' => 'ACT'])->first();
            if ($existe) {
                return Redirect::to("$ruta")
                    ->withErrors(['Usuario con cédula ' . $existe . ', ya esta registrado con estado ' . $existe->estado_autorizacion])
                    ->withInput();
            }
        } else {
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = TramInternoAutorizacionesModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = request()->all();
        if ($id == 0) {
            $validator = Validator::make($input, TramInternoAutorizacionesModel::rules_2($id));
        } else {
            $validator = Validator::make($input, TramInternoAutorizacionesModel::rules($id));
        }

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            DB::beginTransaction();
            try {
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "id_perfil") {
                        $guardar->$key = $value;
                    }
                }
                if ($id == 0) {
                    $guardar->id_usuario = Auth::user()->id;
                }
                if (request()->hasFile("adjunto")) {
                    $dir = public_path() . '/archivos_sistema/';
                    $docs = request()->adjunto;
                    if ($docs) {
                        $fileName = "Autorizacion-" . $guardar->id_usuario . "-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }
                        $oldfile = $dir .  $fileName;
                        if (is_file($oldfile)) {
                            unlink($oldfile);
                        }
                        $guardar->adjunto = $fileName;
                        $docs->move($dir, $fileName);
                    }
                }
                $guardar->save();
                if ($guardar->estado_autorizacion == 'APROBADO') {
                    $user = User::where('cedula', $guardar->cedula)->first();
                    if ($user) {
                        $registro_usuario = User::find($user->id);
                    } else {
                        $registro_usuario = new User;
                        $registro_usuario->password = bcrypt($guardar->cedula);
                        $registro_usuario->telefono = '0000000000';
                        $registro_usuario->cedula = $guardar->cedula;
                        $registro_usuario->id_perfil = request()->id_perfil;
                        $registro_usuario->id_direccion = $guardar->id_direccion;
                        $registro_usuario->nuevo = 1;
                        $registro_usuario->fecha_nacimiento = date('Y-m-d');
                    }
                    $registro_usuario->email = $guardar->correo;
                    $registro_usuario->name = $guardar->nombre;
                    $registro_usuario->cargo = $guardar->cargo;
                    $registro_usuario->estado = 'ACT';
                    $registro_usuario->save();
                    // dd($registro_usuario);
                    $this->notificacion->notificacionesweb('Su solicitud para el uso de código QR fue ' . $guardar->estado_autorizacion,  '', $registro_usuario->id, '2c438f');
                    $this->notificacion->EnviarEmail($registro_usuario->email, 'SOLICITUD DE AUTORIZACIÓN TRÁMITES INTERNOS', 'Su solicitud para el uso de código QR fue ' . $guardar->estado_autorizacion, 'SOLICITUD DE AUTORIZACIÓN TRÁMITES INTERNOS', '',  "vistas.emails.email");
                }
                Auditoria($msgauditoria . " - ID: " . $id . "- solicitud de autorización");
                DB::commit();
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function formularioscrear($id)
    {
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $objetos = $this->verpermisos($objetos, "crear");

        $objetos[0][6]->Valor = "<div>
            <button type='button' id='descargarFormatoAutorizacion'
            class='btn btn-primary flotar' style='float:left; margin-right: 7px;'>
                Descargar formato de autorización
            </button>
        </div>";

        $objetos[0][5]->Valor = direccionesModel::where('estado', 'ACT')->pluck('direccion', 'id')->all();
        $objetos[0][8]->Valor = $this->estados;

        $perfil = PerfilModel::pluck("nombre_perfil", "id")->all();
        $objetos[0][9]->Valor = $perfil;

        unset($objetos[0][6]);
        unset($objetos[0][7]);

        if ($id == "")
        {
            $objetos[0][0]->Valor = date('YmdHi') . '-' . (TramInternoAutorizacionesModel::where('estado', 'ACT')->count() + 1);
            $this->configuraciongeneral[2] = "crear";
            $datos = [
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "autoLocalizar" => "SI"
            ];
        }
        else
        {
            $this->configuraciongeneral[2] = "editar";
            $id = intval(Crypt::decrypt($id));
            $tabla = $objetos[1]->where("au.id", $id)->first();
            $objetos[0][0]->Valor = $tabla->numero;
            $perfil = User::join('ad_perfil as p', 'p.id', 'users.id_perfil')->where('cedula', $tabla->cedula)->first();
            if ($perfil) {
                $objetos[0][9]->ValorAnterior = $perfil->id_perfil;
            } else {
                // $objetos[0][9]->ValorAnterior = $perfil->id_perfil;
            }
            $datos = [
                "tabla" => $tabla,
                "objetos" => $objetos[0],
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "autoLocalizar" => "SI"
            ];
        }
        return view('vistas.create', $datos);
    }

    public function create()
    {
        return $this->formularioscrear("");
    }

    public function descargarFormatoAutorizacion($cedula)
    {
        if ($cedula != 0)
        {
            $datos = TramInternoAutorizacionesModel::where('cedula', $cedula)->first();
            if (!$datos)
            {
                $pdf = \App::make('dompdf.wrapper');
                $context = stream_context_create([
                    'ssl' => [
                        'verify_peer' => FALSE,
                        'verify_peer_name' => FALSE,
                        'allow_self_signed' => TRUE
                    ]
                ]);
                $pdf->getDompdf()->setHttpContext($context);
                $pdf->loadHtml('<div><h1>Documento no valido</h1></div>')->setPaper('a4', 'portrait');
                return $pdf->stream('No valido.pdf');
            }
        }
        else
        {
            $datos = new stdClass;
            $datos->nombre = request()->nombre;
            $datos->cedula = request()->cedula;
            $datos->direccion = request()->direccion;
            $datos->cargo = request()->cargo;
            $datos->correo = request()->correo;
            $datos->numero = request()->numero;
            // date('YmdHi') . '-' . $datos->id;
        }


        ini_set('max_execution_time', '300');
        $pdf = \App::make('dompdf.wrapper');
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);
        $pdf->getDompdf()->setHttpContext($context);
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $canvas->page_text(75, 800, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
        $pdf->loadView('tramites::firmaelectronica.autorizacion', [
            'datos' => $datos
        ])->setPaper('a4', 'portrait');


        return $pdf->stream('Acuerdo de responsabilidad.pdf');
        return $pdf->download('Autorizacion trámites internos.pdf');
    }


    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar(0);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return '';
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tabla = TramInternoAutorizacionesModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
