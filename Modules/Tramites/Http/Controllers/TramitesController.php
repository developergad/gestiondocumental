<?php

namespace Modules\Tramites\Http\Controllers;

use App\ModuloModel;
use App\MenuModuloModel;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class TramitesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		$rucu = Route::currentRouteName();
        $vermodulo = explode(".",$rucu);
        $mod = ModuloModel::where("ruta",$vermodulo[0])->first();
        $usuario = Auth::user();
        $tbnivel = MenuModuloModel::join("ad_menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as b","b.id_menu_modulo","=","ad_menu_modulo.id")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("b.id_perfil", Auth::user()->id_perfil);
        if($mod)
            $tbnivel = $tbnivel->where("ad_menu_modulo.id_modulo",$mod->id);
        $tbnivel = $tbnivel->orderby("ad_menu_modulo.orden")->get();

        return view('tramites::index',[
            "modulo" => $mod,
            "usuario" => $usuario,
            "iconos" => $tbnivel,
            "delete" => "si",
            "create" => "si"
        ]);
	}
}
