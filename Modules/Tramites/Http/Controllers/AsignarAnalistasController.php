<?php

namespace Modules\Tramites\Http\Controllers;

use App\ArchivosModel;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\NotificacionesController;
use Modules\Tramites\Entities\AnexosModel;
use Modules\Tramites\Entities\direccionesModel;
use Modules\Tramites\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramites\Entities\TramAlcaldiaCabDetaModel;
use Modules\Tramites\Entities\TramAlcaldiaCabModel;

class AsignarAnalistasController extends Controller
{
    public $configuraciongeneral = ['Gestión de trámites externos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];

    protected $Notificacion;

    public function __construct(NotificacionesController $Notificacion)
    {
        $this->middleware('auth');
        $this->Notificacion = $Notificacion;
    }

    public function asignados()
    {
        $this->configuraciongeneral[4] = 'Trámites asignados';

        return view('tramites::tramitesexternos.tramitesasignados', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'estado' => 'ANALISTA|SUB-ANALISTA|INFORME|ANALISTA-INFORME'
        ]);
    }

    public function asignadosContestados()
    {
        $this->configuraciongeneral[4] = 'Trámites contestados';
        return view('tramites::tramitesexternos.tramitesasignados', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'estado' => 'ANALISTA-CONTESTADO|SUB-ANALISTA-CONTESTADO|INFORME-RESPONDIDO|ANALISTA-INFORME-RESPONDIDO|INFORME-DESVINCULADO|ANALISTA-INFORME-DESVINCULADO'
        ]);
    }

    public function asignadosAjax(Request $request)
    {
        $estado = explode('|', $request->estado);
        $id = [1, 35];
        $unidad = 0;

        // $analistaSubarea = $this->obtenerAnalistaSubarea(Auth::user()->id_direccion);
        // $analistaSubarea = isset($analistaSubarea) ? $analistaSubarea->id : 0;
        $solicitantes = [Auth::user()->id_direccion];
        $solicitantes = array_merge($solicitantes, $id);

        $collect = collect(User::USUARIOS_UNIDADES);
        $unidades = collect(User::UNIDADES);
        $usuarioUnidad = $collect->contains(Auth::user()->id);
        if($usuarioUnidad)
        {
            $index = $collect->search(Auth::user()->id);

            $unidad = $unidades[$index];
            $informes = [
                'INFORME', 'ANALISTA-INFORME',
                'INFORME-RESPONDIDO', 'ANALISTA-INFORME-RESPONDIDO',
                'INFORME-DESVINCULADO', 'ANALISTA-INFORME-DESVINCULADO'
            ];

            $solicitudesInforme = TramAlcaldiaAsiganrModel::where(['id_direccion' => $unidad])
            ->whereIn('estado', $informes)->pluck('direccion_solicitante');

            $solicitantes = array_merge($solicitantes, $solicitudesInforme->toArray());
        }

        $tramites = TramAlcaldiaAsiganrModel::whereHas('cabecera', function($q) {
            $q->where(['estado' => 'ACT']);
        })->with('cabecera.tipoTramite')
        ->where(function($q) use($unidad) {
            $q->where(['id_direccion' => Auth::user()->id])
            ->orWhere(['id_direccion' => $unidad]);
        })->whereIn('tram_peticiones_direccion_asignada.estado', $estado)
        ->whereIn('direccion_solicitante', $solicitantes);

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
            return isset($tramite->cabecera->tipo_tramite) ? $tramite->cabecera->tipoTramite->valor : '';
        })->addColumn('atender', function($tramite) {
            $direcciones = explode('|', $tramite->direccion_atender);
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->cabecera->id])
            ->whereIn('asig.id_direccion', $direcciones)->get();

            return $atender->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('archivo', function($tramite) {
            $archivosTramite = ArchivosModel::where(['id_referencia' => $tramite->id, 'id_usuario' => Auth::user()->id])
            ->whereIn('tipo', [10, 11])->get(['ruta']);
            $archivos = '';
            if(count($archivosTramite) == 0)
            {
                return '';
            }

            foreach($archivosTramite as $archivo)
            {
                $path = Str::contains($archivo->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                $archivos .= '<a href="' . URL::to($path . $archivo->ruta) . '" class="btn btn-success btn-xs dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            return $archivos;

        })->addColumn('estado', function($tramite) {
            $unidadInforme = collect(User::UNIDADES)->contains(Auth::user()->id_direccion);
            $idDireccion = $unidadInforme ? [Auth::user()->id_direccion, Auth::user()->id] : [Auth::user()->id];

            $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite->id])
            ->whereIn('id_direccion', $idDireccion)->first();
            // ->whereIn('id_direccion', [Auth::user()->id_direccion, Auth::user()->id])->first();

            $informes = collect([
                'INFORME', 'ANALISTA-INFORME',
                'INFORME-RESPONDIDO', 'ANALISTA-INFORME-RESPONDIDO',
                'INFORME-DESVINCULADO', 'ANALISTA-INFORME-DESVINCULADO'
            ])->contains($asignado->estado);

            $estado = $informes ? 'SOLICITUD DE INFORME' : 'ASIGNADO';
            $html = '<span class="badge badge-success">'.$estado.'</span>';
            return $html;
        })->addColumn('action', function($tramite) use($estado) {
            $id = $tramite->cabecera->id;
            if($estado[0] == 'ANALISTA' || $estado[1] == 'SUB-ANALISTA')
            {
                return '<a href="' . route('tramites.show', ['id' => $id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a>
                <a href="' . route('tramite.editfinalizar') . "?id=$id " . '">
                <i class="fa fa-pencil-square-o"></i></a>';
            }
            else
            {
                return '<a href="' . route('tramites.show', ['id' => $id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a>';
            }
        })->rawColumns(['action', 'archivo', 'estado'])->make(true);
    }

    public function guardarInformeAnalista(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);
            $subAnalista = collect(User::SUB_ANALISTAS)->contains(Auth::user()->id_perfil);

            // if($subAnalista && isset($area->id_direccion) && $area->id_direccion == 21)
            // {
            //     $subdirector = User::obtenerSubdirectores();
            //     TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $id, 'id_direccion' => $subdirector[0]->id, 'direccion_solicitante' => $area->id_direccion], [
            //         'estado' => 'ANALISTA'
            //     ]);
            // }

            $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id
            ])->whereIn('estado', ['ANALISTA', 'SUB-ANALISTA'])->first();

            if(!$analistaAsignado)
            {
                $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion,
                    'estado' => 'ANALISTA'
                ])->first();
            }

            // $respuesta = $analistaAsignado->estado == 'SUB-ANALISTA' || ($subAnalista && isset($area->id_direccion) && $area->id_direccion == 21) ? 'SUB-ANALISTA-CONTESTADO' : 'ANALISTA-CONTESTADO';
            $respuesta = $analistaAsignado->estado == 'SUB-ANALISTA' ? 'SUB-ANALISTA-CONTESTADO' : 'ANALISTA-CONTESTADO';

            $archivo = ArchivosModel::where([
                'tipo' => 10,
                'id_referencia' => $tramite->id,
                'id_usuario' => Auth::user()->id
            ])->count();
            // dd($archivo);

            // if($archivo == 0 && isset(request()->observacion))
            // {
                $analistaAsignado->observacion = request()->observacion;
                $analistaAsignado->estado = $respuesta;
                $analistaAsignado->respondido = 1;

                // if($subAnalista && isset($area->id_direccion) && $area->id_direccion == 21)
                // {
                //     $subdirector = User::obtenerSubdirectores();
                //     $analistaAsignado->direccion_solicitante = $subdirector[0]->id;
                // }

                $analistaAsignado->save();

                $direccion = direccionesModel::find(Auth::user()->id_direccion);
                $this->guardarTimeline($id, $tramite, 'Respuesta del analista asignado: ' . $direccion->direccion);

                // $subareas = SubDireccionModel::whereIn('id_direccion', [6, 21])->pluck('area');
                // $analistaUnidad = collect($subareas)->contains(Auth::user()->id_direccion);

                if(Auth::user()->id_direccion == 6)//|| $analistaUnidad
                {
                    if($tramite->tipo == 'TRAMITES MUNICIPALES')
                    {
                        if($respuesta == 'SUB-ANALISTA-CONTESTADO')
                        {
                            $director = User::where(['id' => $analistaAsignado->direccion_solicitante])
                            ->get(['id', 'name', 'email']);
                        }
                        else
                        {
                            $perfiles = array_merge(User::ANALISTA_DIRECTOR, User::DIRECTORES);
                            $perfilesAD = collect($perfiles);
                            $director = User::where([
                                'id_direccion' => Auth::user()->id_direccion
                            ])->select('id', 'name', 'email')
                            ->whereIn('id_perfil', $perfilesAD)->get();
                        }
                    }
                    else
                    {
                        if($respuesta == 'SUB-ANALISTA-CONTESTADO')
                        {
                            $director = User::where(['id' => $analistaAsignado->direccion_solicitante])
                            ->get(['id', 'name', 'email']);
                        }
                        else
                        {
                            // $subarea = SubDireccionModel::where(['area' => Auth::user()->id_direccion])->first(['id_direccion']);
                            // $id_direccion = isset($subarea->id_direccion) ? $subarea->id_direccion : 0;
                            $director = User::where(function ($q) {
                                $q->where(['id_direccion' => Auth::user()->id_direccion]);
                                // ->orWhere(['id_direccion' => $id_direccion]);
                            })->select('id', 'name', 'email')
                            ->whereIn('id_perfil', User::ANALISTA_DIRECTOR)->get();
                        }
                    }
                }
                else
                {
                    if($respuesta == 'SUB-ANALISTA-CONTESTADO')
                    {
                        $director = User::where(['id' => $analistaAsignado->direccion_solicitante])
                        ->get(['id', 'name', 'email']);
                    }
                    else
                    {
                        $director = User::directores()
                        ->where(['users.id_direccion' => $analistaAsignado->direccion_solicitante])->get();
                    }

                    if(count($director) == 0)
                    {
                        $director = User::where(['id_perfil' => 54,'id_direccion' => $analistaAsignado->direccion_solicitante])->get();
                    }
                }

                $ruta = 'tramites/finalizartramite?id=' . $tramite->id;
                $mensaje = Auth::user()->name .', ha respondido al trámite asignado N° ' . $tramite->numtramite;

                foreach($director as $dir)
                {
                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                    $this->Notificacion->EnviarEmail($dir->email, 'Respuesta del analista asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                }

                $perfiles = collect(User::DIRECTORES);
                $esDirector = $perfiles->contains(Auth::user()->id_perfil);
                $redirect = $esDirector ? 'tramitesrespondidos' : 'asignados';

                DB::commit();
                return response()->json([
                    'ok' => true,
                    // 'message' => $request->has('archivar') && $request->archivar == 'SI' ? 'El trámite ha sido archivado' : 'Su informe ha sido enviado correctamente',
                    'message' => 'Su informe ha sido enviado correctamente',
                    'redirect' => $redirect
                ]);
            // }
            // else if(!isset(request()->observacion))
            // {
            //     return response()->json([
            //         'ok' => false,
            //         'message' => 'No se puede enviar hasta que suba su informe'
            //     ]);
            // }
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar, inténtelo de nuevo.',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function asignarAnalista(Request $request, $id)
    {
        if($request->ajax())
        {
            DB::beginTransaction();
            try
            {
                $tramite = TramAlcaldiaCabModel::findOrFail($id);
                $tramite->observacion = $request->observacion;
                $tramite->save();

                // $esCoodinacion = collect(User::COORDINACIONES)->contains(Auth::user()->id_direccion);
                $analistasInformar = $request->analistas;

                $direccionAsignada = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion
                ])->first();

                if(!$direccionAsignada)
                {
                    $direccionAsignada = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id,
                        'id_direccion' => Auth::user()->id
                    ])->first();
                }

                $direccionAsignada->observacion = $request->observacion;
                $direccionAsignada->save();

                $ruta = 'tramites/finalizartramite?id=' . $tramite->id;
                $mensaje = Auth::user()->name . ' le ha asignado el trámite N° ' . $tramite->numtramite;

                $esAnalistaSubarea = User::esAnalistaSubarea();
                $estado = $esAnalistaSubarea ? 'SUB-ANALISTA' : 'ANALISTA';
                $solicitante = $esAnalistaSubarea ? Auth::user()->id : Auth::user()->id_direccion;

                foreach($analistasInformar as $informar)
                {
                    // if($esCoodinacion)
                    // {
                    //     TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $id, 'id_direccion' => $informar], [
                    //         'direccion_solicitante' => $solicitante,
                    //         'respondido' => 0,
                    //         'estado' => $estado
                    //     ]);

                    //     $director = User::directores()->where('users.id_direccion', $informar)->get();
                    //     foreach($director as $dir)
                    //     {
                    //         $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                    //         $this->Notificacion->EnviarEmail($dir->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                    //     }
                    // }
                    // else
                    // {
                    // }
                    $analista = User::findOrFail($informar);

                    TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $id, 'id_direccion' => $analista->id], [
                        'direccion_solicitante' => $solicitante,
                        'estado' => $estado
                    ]);

                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $analista->id, '2c438f');
                    $this->Notificacion->EnviarEmail($analista->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');

                }

                $direccion = direccionesModel::find(Auth::user()->id_direccion);
                $this->guardarTimeline($id, $tramite, 'Asignar trámite a analista: ' . $direccion->direccion);

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha notificado correctamente a los analistas seleccionados.'
                ]);
            }
            catch(\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => 'No se pudo notificar a las direcciones seleccionada, inténtelo de nuevo.',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        }
        else abort(401);
    }

    public function enviarSecretaria(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $usuario = User::find(Auth::user()->id, ['id']);
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])
                ->where('id_usuario', $usuario->id)->get(['id', 'id_usuario', 'ruta']);

            $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
            ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario->id])->get();

            $archivo = array_merge($archivo->toArray(), $archivos_->toArray());

            if(count($archivo) == 0)
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'No se puede guardar el trámite hasta que suba su respuesta'
                ]);
            }

            $director = User::obtenerDirector();
            $asignadoDirector = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => $director['id_direccion']
            ])->whereIn('estado', ['PENDIENTE', 'ASIGNADO'])->first();

            $asignadoDirector->respondido = 1;
            $asignadoDirector->estado = 'CONTESTADO';
            $asignadoDirector->save();

            $asignadoSubdirector = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => $usuario->id,
                'estado' => 'ANALISTA'
            ])->first();

            $asignadoSubdirector->respondido = 1;
            $asignadoSubdirector->estado = 'ANALISTA-CONTESTADO';
            $asignadoSubdirector->observacion = $request->observacion;
            $asignadoSubdirector->save();

            $tramite->disposicion = 'APROBADA';
            $tramite->estado_proceso = 1;
            $tramite->save();

            $idTimeline = $this->guardarTimeline($id, $tramite, 'Trámite en proceso de gestión');
            $timeline = TramAlcaldiaCabDetaModel::find($idTimeline);
            $timeline->disposicion = 'RESPONDIDO';
            $timeline->fecha_respuesta = now();
            $timeline->save();

            $notifica = TramAlcaldiaAsiganrModel::Select(DB::raw("COUNT(*) as total"))
                ->where(['id_cab' => $tramite->id, 'respondido' => 0])
                ->whereIn('estado', ['PENDIENTE', 'ASIGNADO'])->first();

            if ($notifica->total == 0)
            {
                $secretario = User::where(['id_perfil' => User::SECRETARIO_GENERAL, 'id_direccion' => 58])->first();

                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' se encuentra disponible para su revisión', 'tramites/finalizartramite?id=' . $tramite->id . '&revision=true', $secretario->id, '2c438f');

                if ($tramite->correo_electronico)
                {
                    $mensaje = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' se encuentra en proceso de gestión.';
                    $this->Notificacion->EnviarEmailTram($tramite->correo_electronico, 'Trámite en Proceso de Gestión', '', $mensaje);
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'trámite enviado a Secretaría General correctamente'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar el trámite',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function archivarTramiteAnalista(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);
            $esImpugnacion = collect(User::IMPUGNACIONES)->contains($tramite->tipo_tramite);
            $otroTipo = collect(User::OTRO_TIPO)->contains($tramite->tipo_tramite);
            // $direccion = Auth::user()->id_direccion;

            $analista = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id, 'estado' => 'ANALISTA',
                'id_direccion' => Auth::user()->id
            ])->first();

            $director = User::obtenerDirector();
            $direccion = $director['id_direccion'];

            $analista->estado = 'ANALISTA-CONTESTADO';
            $analista->respondido = 1;
            $analista->observacion = $request->observacion;
            $analista->save();

            $asignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => $direccion
            ])->whereIn('estado', ['PENDIENTE', 'ASIGNADO'])->first();

            $asignado->estado = 'ARCHIVADO';
            $asignado->respondido = 1;
            $asignado->observacion = $request->observacion;
            $asignado->save();

            $direccionesAtender = explode('|', $tramite->direccion_atender);
            $asignados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'respondido' => 1])
            ->whereIn('id_direccion', $direccionesAtender)->count();

            if(count($direccionesAtender) == $asignados)
            {
                $tramite->disposicion = 'FINALIZADO';
                $tramite->finalizar_por = 'DIRECTOR';
                $tramite->fecha_respuesta = now();
                $tramite->estado_proceso = 2;
                $tramite->save();
            }

            $this->guardarTimeline($id, $tramite, 'Trámite archivado', 0, 'ARCHIVADO');

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            $nombre = $direccion->direccion;
            $mensaje = 'El trámite N° ' . $tramite->numtramite . ' ha sido archivado por parte de ' . $nombre;
            $ruta = 'tramites/tramitesarchivados';

            $director = User::obtenerDirector();
            $mensajeDirector = 'El trámite N° ' . $tramite->numtramite . ' ha sido archivado por parte de ' . Auth::user()->name;
            $this->Notificacion->notificacionesweb($mensajeDirector, $ruta, $director['id'], '2c438f');

            if(isset($tramite->creado_por))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->creado_por, '2c438f');
            }
            if(isset($tramite->asignado_a))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->asignado_a, '2c438f');
            }

            if($tramite->correo_electronico != null)
            {
                $correo = config('app.env') == 'local' ? 'darquiroz@gmail.com' : $tramite->correo_electronico;
                if($esImpugnacion)
                {
                    $mensaje = 'Estimado(a) ciudadano(a) su trámite '. $tramite->numtramite .' ha sido atendido, en los próximos días podrá consultar en la página https://manta.gob.ec/multas-de-transito/';
                    $this->Notificacion->EnviarEmailTram($correo, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                }
                else if($otroTipo)
                {
                    $mensaje = 'Estimado(a) ciudadano(a) su trámite '. $tramite->numtramite .' ha sido atendido, usted puede retirar en horarios laborables la resolución en nuestra ventanilla en la dirección municipal de Tránsito para que continúe con su proceso.';
                    $this->Notificacion->EnviarEmailTram($correo, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'se ha archivado el trámite correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo archivar el trámite, inténtelo de nuevo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function guardarTimeline($id, $data, $observacion = null, $estado = 0, $disposicion = null):int
    {
        $timeline = new TramAlcaldiaCabDetaModel;
        $timeline->id_usuario = Auth::user()->id;
        $timeline->id_tram_cab = $id;
        $timeline->numtramite = $data->numtramite;
        $timeline->remitente = $data->remitente;
        $timeline->asunto = $data->asunto;
        $timeline->peticion = $data->peticion;
        $timeline->prioridad = $data->prioridad;
        $timeline->disposicion = $disposicion == null ? $data->disposicion : $disposicion;
        $timeline->observacion = $observacion;
        $timeline->ip = request()->ip();
        $timeline->pc = request()->getHttpHost();
        $timeline->fecha_ingreso = $data->fecha_ingreso;
        if($observacion == 'Trámite en proceso de gestión')
        {
            $timeline->fecha_respuesta = now();
        }
        $timeline->estado_proceso = $estado;
        $timeline->fecha_fin = $data->fecha_fin;
        $timeline->save();

        return $timeline->id;
    }

    // public function obtenerAnalistaSubarea($direccion)
    // {
    //     $direccionSubArea = SubDireccionModel::where(['id_direccion' => $direccion])->pluck('area');
    //     $direccionSubArea = array_merge($direccionSubArea->toArray(), [$direccion]);

    //     $analista = User::with('direccion')->whereIn('id_perfil', User::ANALISTA_SUBAREA)
    //     ->whereIn('id_direccion', $direccionSubArea)
    //     // ->where(['id_direccion' => $direccion])
    //     ->first();

    //     return $analista;
    // }
}
