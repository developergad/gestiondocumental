<?php

namespace Modules\Tramites\Http\Controllers;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Modules\Tramites\Entities\AnexosModel;
use Modules\Tramites\Entities\TramiteInternoModel;
use App\Http\Controllers\NotificacionesController;
use App\Http\Controllers\FirmaElectronicaController;
use Modules\Tramites\Entities\HistorialInternosModel;
use Modules\Tramites\Entities\TramInternoCodigosModel;
use Modules\Tramites\Entities\AsignadoTramiteInternoModel;
use Modules\Tramites\Entities\direccionesModel;
use Modules\Tramites\Entities\TramInternoAutorizacionesModel;
use stdClass;

class TramitesInternosController extends Controller
{
    public $configuraciongeneral = ['Gestión de trámites internos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];

    protected $notificacion;
    private $archivos_path;

    var $prioridad = [
        'ALTA' => 'ALTA',
        'MEDIA' => 'MEDIA',
        'BAJA' => 'BAJA'
    ];

    var $tipo_tramite = [
        'MEM' => 'MEMO',
        'OFI' => 'OFICIO',
        'INF' => 'INFORME'
    ];

    var $tipo_tramite_analistas = [
        'MEM' => 'MEMO',
        'INF' => 'INFORME'
    ];


    const DIRECTORES = [
        2, 3, 4, 5, 6
    ];

    const SECRETARIA_DIRECTOR = [72];

    const ANALISTA = [
        3
        // 56, 63, 53, 51, 48, 47, 50
    ];

    const DIRECCIONES_INA = [
        23, 32, 33, 41
    ];

    var $titulos = [
        'Sr.' => 'Sr.',
        'Sr. Abogado' => 'Sr. Abogado',
        'Sr. Analista' => 'Sr. Analista',
        'Sr. Arquitecto' => 'Sr. Arquitecto',
        'Sr. Capitán' => 'Sr. Capitán',
        'Sr. Contador' => 'Sr. Contador',
        'Sr. Coronel' => 'Sr. Coronel',
        'Sr. Doctor' => 'Sr. Doctor',
        'Sr. Economista' => 'Sr. Economista',
        'Sr. Ingeniero' => 'Sr. Ingeniero',
        'Sr. Licenciado' => 'Sr. Licenciado',
        'Sr. Magister' => 'Sr. Magister',
        'Sr. Odontólogo' => 'Sr. Odontólogo',
        'Sr. Psicólogo' => 'Sr. Psicólogo',
        'Sr. Tecnólogo' => 'Sr. Tecnólogo',
        'Sra.' => 'Sra.',
        'Sra. Abogada' => 'Sra. Abogada',
        'Sra. Analista' => 'Sra. Analista',
        'Sra. Arquitecta' => 'Sra. Arquitecta',
        'Sra. Capitán' => 'Sra. Capitán',
        'Sra. Contadora' => 'Sra. Contadora',
        'Sra. Coronel' => 'Sra. Coronel',
        'Sra. Doctora' => 'Sra. Doctora',
        'Sra. Economista' => 'Sra. Economista',
        'Sra. Ingeniera' => 'Sra. Ingeniera',
        'Sra. Licenciada' => 'Sra. Licenciada',
        'Sra. Magister' => 'Sra. Magister',
        'Sra. Odontóloga' => 'Sra. Odontóloga',
        'Sra. Psicóloga' => 'Sra. Psicóloga',
        'Sra. Tecnóloga' => 'Sra. Tecnóloga',
        'Srta.' => 'Srta.',
        'Srta. Abogada' => 'Srta. Abogada',
        'Srta. Analista' => 'Srta. Analista',
        'Srta. Arquitecta' => 'Srta. Arquitecta',
        'Srta. Capitán' => 'Srta. Capitán',
        'Srta. Contadora' => 'Srta. Contadora',
        'Srta. Coronel' => 'Srta. Coronel',
        'Srta. Doctora' => 'Srta. Doctora',
        'Srta. Economista' => 'Srta. Economista',
        'Srta. Ingeniera' => 'Srta. Ingeniera',
        'Srta. Licenciada' => 'Srta. Licenciada',
        'Srta. Magister' => 'Srta. Magister',
        'Srta. Odontóloga' => 'Srta. Odontóloga',
        'Srta. Psicóloga' => 'Srta. Psicóloga',
        'Srta. Tecnóloga' => 'Srta. Tecnóloga',
        // 'Ing.' => 'Ing.',
        // 'Lic.' => 'Lic.',
        // 'Eco.' => 'Eco.',
        // 'Mgs.' => 'Mgs.',
    ];

    public function __construct(
        NotificacionesController $notificacion,
        FirmaElectronicaController $firmaElectronicaController
    ) {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
        $this->firmaElectronicaController = $firmaElectronicaController;
        $this->archivos_path = public_path('archivos_sistema');
    }

    /**TRÁMITES ELABORADOS */
    public function index()
    {
        $this->configuraciongeneral[4] = 'Creación de trámite';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'SI',
            'ajax' => 'tramites.ajax',
            'estado' => 'INICIAL',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    public function indexAjax(Request $request)
    {
        $estado = $request->estado;
        $tipo_envio = $request->tipo_envio;
        $perfil = Auth::user()->id_perfil;
        $analistaEspecial = collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
        $analista = collect(User::ANALISTAS)->contains($perfil);

        $query = TramiteInternoModel::with('remitente', 'asignados.usuario');

        if ($request->tipo_envio) {
            if ($tipo_envio == 'BORRADOR') {
                $query = $query->whereIn('tipo_envio', [$tipo_envio, 'BORRADOR_DIRECTOR']);
            } else {
                $query = $query->where(['tipo_envio' => $tipo_envio]);
            }
        }

        if ($estado != 'INICIAL') {
            if ($estado == 'ANALISTA') {
                $esDirector = collect(User::DIRECTORES)->contains(Auth::user()->id_perfil);
                if($esDirector)
                {
                    $tramites = $query->whereHas('asignados', function ($q) use ($estado)
                    {
                        $q->whereIn('solicitante', [1643, 1676, 1646, 1645])
                        ->whereIn('id_usuario_asignado', [Auth::user()->id_direccion, Auth::user()->id])
                        ->where(['estado' => $estado]);
                    });
                }
                else
                {
                    $tramites = $query->whereHas('asignados', function ($q) use ($estado) {
                        $q->whereIn('id_usuario_asignado', [Auth::user()->id_direccion, Auth::user()->id])
                        ->where(['estado' => $estado]);
                    });
                }
            }
            else
            {
                if($analistaEspecial)
                {
                    $director = $this->obtenerDirector();

                    $tramites = $query->whereHas('asignados', function ($q) use ($estado, $director) {
                        $q->whereIn('id_usuario_asignado', [Auth::user()->id, $director['id']])
                        ->where(['estado' => $estado]);
                    });
                }
                else
                {
                    if ($estado == 'ASIGNADO')
                    {
                        $tramites = $query->whereHas('asignados', function ($q) use ($estado) {
                            $q->where(['id_usuario_asignado' => Auth::user()->id])
                                ->whereIn('estado', [$estado, 'ASIGNADO-RESPUESTA']);
                        });
                    }
                    else
                    {
                        $tramites = $query->whereHas('asignados', function ($q) use ($estado) {
                            $q->where(['id_usuario_asignado' => Auth::user()->id, 'estado' => $estado]);
                        });
                    }
                }
            }
        }
        else
        {
            if (Auth::user()->id_perfil == 1)
            {
                $tramites = $query;
            }
            else if ($analista)
            {
                if ($tipo_envio == 'BORRADOR')
                {
                    $tramites = $query->where(['elaborado_por' => Auth::user()->id])
                        ->orWhere(['id_remitente' => Auth::user()->id])
                        ->whereIn('tipo_envio', [$tipo_envio, 'BORRADOR_DIRECTOR', 'BORRADOR_SUBDIRECTOR']);
                }
                else
                {
                    $tramites = $query->where(['elaborado_por' => Auth::user()->id])
                        ->orWhere(['id_remitente' => Auth::user()->id])
                        ->where(['tipo_envio' => $tipo_envio]);
                }
            }
            else
            {
                $tramites = $query->where(['id_remitente' => Auth::user()->id]);
            }
        }

        return DataTables::of($tramites)->addColumn('asignados', function ($tramite) {
            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                return $tramite->asignados->whereIn('estado', ['ASIGNADO', 'ASIGNADO-RESPUESTA', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ');
            } else {
                $para = json_decode($tramite->delegados_para);
                $para = collect($para);
                return $tramite->asignados->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ') . ', ' . $para->map(function ($asignado) {
                    return isset($asignado->titulo) && isset($asignado->nombre) ? "$asignado->titulo $asignado->nombre - $asignado->cargo" : "";
                })->implode(', ');
            }
        })->addColumn('copias', function ($tramite) {
            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                return $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ');
            } else {
                $copia = json_decode($tramite->delegados_copia);
                $copia = collect($copia);
                return $tramite->asignados->where('estado', '=', 'COPIA')->map(function ($asignado) {
                    return $asignado->usuario->name;
                })->implode(', ') . ', ' . $copia->map(function ($asignado) {
                    return isset($asignado->titulo) ? "$asignado->titulo $asignado->nombre - $asignado->cargo" : "$asignado->nombre - $asignado->cargo";
                })->implode(', ');
            }
        })->addColumn('analistas', function ($tramite) {
            $analistas = AsignadoTramiteInternoModel::from('tma_tram_interno_asignados as asig')
                ->join('users as u', 'asig.id_usuario_asignado', 'u.id')
                ->join('users as soli', 'asig.solicitante', 'soli.id')
                ->select('u.name', 'asig.estado')
                ->where(['asig.id_tramite' => $tramite->id])
                // ->whereNotIn('soli.id_direccion', User::COORDINACIONES)
                ->whereIn('asig.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();

            return $analistas->map(function ($analista) {
                return $analista['name'];
            })->implode(' - ');
        })->addColumn('elaborado_por', function($tramite) {
            if(isset($tramite->elaborado_por))
            {
                $user = User::find($tramite->elaborado_por, ['name']);
                return $user->name;
            }
            else return '';

        })->addColumn('action', function ($tramite) use ($estado, $analistaEspecial) {
            $show = '<a href="' . route('internos.show', ['id' => $tramite->id]) . '" class="divpopup" data-toggle="tooltip" data-placement="left" title="" data-original-title="Tooltip on left" target="_blank" onclick="popup(this)">
            <i class="fa fa-newspaper-o"></i></i></a>';

            $esDirector = collect(User::DIRECTORES)->contains(Auth::user()->id_perfil) || Auth::user()->id == 1643 || $analistaEspecial;
            // $esCoordinador = User::esCoordinador();

            if ($tramite->tipo_tramite == 'MEMO' || $tramite->tipo_tramite == 'INFORME') {
                if ($estado == 'INICIAL') {
                    $acciones = '<a href="' . route('tramitesinternos.edit', Crypt::encrypt($tramite->id)) . '" class="">
                        <i class="fa fa-edit"></i></i></a> ';
                    if ($tramite->tipo_envio == 'ENVIAR') {
                        if (Auth::user()->id != 1182) {
                            $acciones = $show;
                        } else {
                            $acciones .= $show;
                        }
                    }
                } elseif ($estado == 'ASIGNADO') {
                    $analistas = $this->obtenerAnalistas(Auth::user()->id_direccion, $tramite->id_remitente);
                    $acciones = $show . ' <a href="' . route('tramitesinternos.create') . '?tramite=' . Crypt::encrypt($tramite->id) . '">
                    <i class="fa fa-history"></i></a> </br>
                    <a href="#" class="" onclick="archivarTramiteInterno(' . $tramite->id . ')"><i class="fa fa-archive"></i></a>';
                    if(count($analistas) > 0 && ($esDirector)) //|| $esCoordinador
                    {
                        $acciones .= ' <a href="'. route('asignar.analista', ['id' => Crypt::encrypt($tramite->id)]) .'" id="btn_asignar_analista">
                        <i class="fa fa-users"></i></a>';
                    }

                } elseif ($estado == 'COPIA') {
                    // $acciones = '<a href="' . route('copia.edit', Crypt::encrypt($tramite->id)) . '" class="btn btn-xs btn-primary">
                    // <i class="fa fa-newspaper-o"></i></i></a>';
                    $acciones = '';
                } elseif ($estado == 'ARCHIVADO') {
                    $acciones = $show;
                } elseif ($estado == 'CONTESTADO') {
                    $acciones = '<a href="' . route('revision.edit', Crypt::encrypt($tramite->id)) . '" class="btn btn-xs btn-primary">
                    <i class="fa fa-newspaper-o"></i></i></a>';
                } elseif ($estado == 'ANALISTA') {
                    $acciones = $show . ' <a href="' . route('revision.edit', Crypt::encrypt($tramite->id)) . '">
                    <i class="fa fa-edit"></i></i></a>';
                } else {
                    if (Auth::user()->id != 1182) {
                    }
                    $acciones = '';
                }
                // dd($tramite->tipo_envio);
                if ($tramite->tipo_envio == 'BORRADOR' || $tramite->tipo_envio == 'BORRADOR_DIRECTOR' || Auth::user()->id_perfil == 1) {
                    $acciones .= $show . ' <a class="" onclick="eliminarBorrador(\'' . Crypt::encrypt($tramite->id) . '\')"><i class="fa fa-trash-o"></i></i></a>';
                }
            } elseif ($tramite->tipo_tramite = 'OFICIO') {
                $analistas = $this->obtenerAnalistas(Auth::user()->id_direccion, $tramite->id_remitente);

                $acciones = '';
                $acciones = '<a href="' . route('tramitesinternos.edit', Crypt::encrypt($tramite->id)) . '">
                <i class="fa fa-edit"></i></i></a> ' . $show .
                ' <a href="#" class="" onclick="archivarTramiteInterno(' . $tramite->id . ')"><i class="fa fa-archive"></i></a>';

                if ($tramite->tipo_envio == 'ENVIAR') {
                    if (Auth::user()->id != 1182) {
                        if ($estado == 'ASIGNADO') {
                            $acciones = $show . ' <a href="#" class="" onclick="archivarTramiteInterno(' . $tramite->id . ')"><i class="fa fa-archive"></i></a>';
                            if(count($analistas) > 0 && ($esDirector)) //|| $esCoordinador
                            {
                                $acciones .= ' <a href="'. route('asignar.analista', ['id' => Crypt::encrypt($tramite->id)]) .'" id="btn_asignar_analista">
                                <i class="fa fa-users"></i></a>';
                            }
                        }
                        elseif ($estado == 'ARCHIVADO') {
                            $acciones = $show;
                        }
                        // $acciones = $show;
                    } else {
                        // $acciones .= $show;
                    }
                }
                if ($tramite->tipo_envio == 'BORRADOR' || $tramite->tipo_envio == 'BORRADOR_DIRECTOR' || Auth::user()->id_perfil == 1) {
                    $acciones .= '<a onclick="eliminarBorrador(\'' . Crypt::encrypt($tramite->id) . '\')"><i class="fa fa-trash-o"></i></i></a>';
                }
            } else $acciones = '';

            return $acciones;
        })->addColumn('memo', function ($tramite) {
            if ($tramite->documento) {
                $numtramite = str_replace('-', '', $tramite->numtramite);
                $memo = '<a href="' . asset('archivos_firmados/' . $tramite->documento) . '?v='.rand(1,1000).'" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)" id="' . $numtramite . '">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            } else {
                $memo = '';
            }
            return $memo;
        })->addColumn('anexos', function ($tramite) {
            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $tramite->id, 'tipo' => 11])->get();
            $archivos = '';

            foreach ($anexos as $anexo) {
                $archivos .= '<a href="' . asset('archivos_sistema/' . $anexo->ruta) . '?v='.rand(1,1000).'" class="btn btn-sm btn-info dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>&nbsp;&nbsp;';
            }

            return $archivos;
        })->addColumn('created_at', function ($tramite) {
            return (isset($tramite->fecha)) ? $tramite->fecha : $tramite->created_at;
        })->rawColumns(['memo', 'action', 'anexos'])->make(true);
    }

    public function oficios()
    {
        $this->configuraciongeneral[4] = 'Oficios creados';
        return view('tramites::tramitesinternos.oficios', [
            'configuraciongeneral' => $this->configuraciongeneral,
        ]);
    }

    public function oficiosAjax()
    {
        $oficios = TramiteInternoModel::with('remitente')
            ->where(['tipo_tramite' => 'OFICIO', 'tipo_envio' => 'ENVIAR', 'id_remitente' => Auth::user()->id]);

        return DataTables::of($oficios)->addColumn('asignados', function ($oficios) {
            return '';
        })->addColumn('copias', function ($oficios) {
            return '';
        })->addColumn('oficio', function ($oficios) {
            $oficion = '<a href="' . asset('archivos_firmados/' . $oficios->documento) . '" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
            <i class="fa fa-file-pdf-o"></i></i></a>';
            return $oficion;
        })->addColumn('anexos', function ($oficios) {
            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $oficios->id, 'tipo' => 11])->get();
            $archivos = '';

            foreach ($anexos as $anexo) {
                $archivos .= '<a href="' . asset('archivos_sistema/' . $anexo->ruta) . '" class="btn btn-sm btn-info dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>&nbsp;&nbsp;';
            }

            return $archivos;
        })->rawColumns(['memo', 'action', 'anexos'])->make(true);
    }

    public function create()
    {
        $tramite = null;
        try
        {
            if (request()->tramite)
            {
                $id = Crypt::decrypt(request()->tramite);
                $tramite = TramiteInternoModel::findOrFail($id);
            }
        }
        catch (\Exception $ex)
        {
            return back();
        }

        $esSecretaria = collect(User::ANALISTAS)->contains(Auth::user()->id_perfil);
        $tabla = new TramiteInternoModel;
        if(isset($tramite))
        {
            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->where('users.id', $tramite->id_remitente)->orderBy('users.name')
                ->pluck('users.name', 'users.id');

            $tabla['con_copia'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->where('users.id', '<>', Auth::user()->id)
                ->where('users.id', '<>', $tramite->id_remintente)
                ->whereIn('ap.id', self::DIRECTORES)->orderBy('users.name')->pluck('users.name', 'users.id');
        }
        else
        {
            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->where('users.id', '<>', Auth::user()->id)
                ->whereIn('ap.id', self::DIRECTORES)->orderBy('users.name')->pluck('users.name', 'users.id');
        }

        $numtramite = '';
        $analista = collect(User::ANALISTAS)->contains(Auth::user()->id_perfil);
        // $analistas = array_merge($analista, User::ANALISTA_DIRECTOR);
        // $esSecretaria = collect($analistas)->contains(Auth::user()->id_perfil);

        $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;
        // $tabla['tipo_tramite'] = $analista ? $this->tipo_tramite_analistas : $this->tipo_tramite;
        $tabla['tipo_tramite'] = $this->tipo_tramite;
        $tabla['titulos'] = [null => 'Elija una opción'] + $this->titulos;

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                Guardar</button>';

        if (in_array(Auth::user()->id_perfil, self::ANALISTA)) {
            $no_director = true;
        } else {
            $no_director = null;
        }
        // dd(in_array(Auth::user()->id_perfil, self::ANALISTA));

        return view('tramites::tramitesinternos.create', [
            'tabla' => $tabla,
            'configuraciongeneral' => $configuraciongeneral,
            'numtramite' => $numtramite,
            'esSecretaria' => $esSecretaria,
            // 'esAnalista' => $esAnalista,
            'no_director' => $no_director,
            'responder' => $tramite
        ]);
    }

    public function consultardireccion()
    {
        return direccionesModel::where('id', Auth::user()->id_direccion)->first();
    }

    public function store(Request $request)
    {
        if ($request->tipo_tramite == 'MEM' || $request->tipo_tramite == 'INF') {
            $validaciones = [
                'numtramite' => 'required|unique:tma_tram_interno,numtramite',
                'asignados' => 'required',
                'asunto' => 'required',
                'peticion' => 'required',
            ];
            if ($request->tipo_envio == 'ENVIAR') {
                $validaciones['documento'] = 'required';
            }
            $validations = Validator::make($request->all(), $validaciones);
        } else {
            $validaciones = [
                'numtramite' => 'required|unique:tma_tram_interno,numtramite',
                // 'delegados_para' => 'required',
                'asunto' => 'required',
                'peticion' => 'required',
            ];
            if (!$request->asignados) {
                // dd($request->asignados);
                $array_validaciones['delegados_para'] = 'required';
            }
            if ($request->tipo_envio == 'ENVIAR') {
                $validaciones['documento'] = 'required';
            }
            $validations = Validator::make($request->all(), $validaciones);
        }

        if ($validations->fails()) {
            return back()->withErrors($validations)->withInput();
        }
        return $this->guardar($request);
    }

    public function edit($id)
    {
        try {
            $ocultarPeticion = null;
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);

            $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])
                ->pluck('id_usuario_asignado');
            // dd($asignados);
            // dd($tabla);
            if ($tabla->tipo_tramite == 'OFICIO') {
                $tabla->tipo_tramite_a = 'OFI';
            }
            if ($tabla->tipo_tramite == 'INFORME') {
                $tabla->tipo_tramite_a = 'INF';
            }

            $asignados_ = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'ASIGNADO'])->count();

            $copias = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'COPIA'])->pluck('id_usuario_asignado');

            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                // ->whereIn('ap.id', [3, 5, 9, 21, 22])->pluck('users.name', 'users.id');
                ->whereIn('ap.id', self::DIRECTORES)->orderBy('users.name')->pluck('users.name', 'users.id');

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;
            $tabla['titulos'] = $this->titulos;
            $tabla['tipo_tramite'] = $this->tipo_tramite;

            $tabla['desvinculados'] = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'DESVINCULADO'])->get();
            // dd(count($tabla['desvinculados']));
            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'tramitesinternos.update';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Editar trámite';

            if (count($asignados) == $asignados_) {
                $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                    Guardar</button>';
            } else {
                $ocultarPeticion = 'SI';
                $configuraciongeneral[6] = 'copia';

                $tabla['contestados'] = AsignadoTramiteInternoModel::with('usuario')
                    ->where(['id_tramite' => $id, 'estado' => 'CONTESTADO'])->get();

                $configuraciongeneral[5] = '';
            }

            if (in_array(Auth::user()->id_perfil, self::DIRECTORES)) {
                $no_director = null;
            } else {
                $no_director = true;
            }

            return view('tramites::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'copias' => $copias,
                'anexos' => $anexos,
                'no_director' => $no_director,
                'ocultarPeticion' => $ocultarPeticion
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $id = Crypt::decrypt($id);
            // dd($request->all());
            if ($request->tipo_tramite == 'OFI' || $request->tipo_tramite == 'INF') {
                $array_validaciones = [
                    'numtramite' => [
                        'required',
                        Rule::unique('tma_tram_interno')->ignore($id)
                    ],
                    'asunto' => 'required',
                    'peticion' => 'required'
                ];
                if ($request->tipo_envio == 'ENVIAR') {
                    $array_validaciones['documento'] = 'required';
                }
                if (!$request->asignados) {
                    // dd($request->asignados);
                    $array_validaciones['delegados_para'] = 'required';
                }
            } else {
                $array_validaciones = [
                    'numtramite' => [
                        'required',
                        Rule::unique('tma_tram_interno')->ignore($id)
                    ],
                    'asignados' => 'required',
                    'asunto' => 'required',
                    'peticion' => 'required'
                ];
                if ($request->tipo_envio == 'ENVIAR') {
                    $array_validaciones['documento'] = 'required';
                }
            }


            $validations = Validator::make($request->all(), $array_validaciones);
            if ($validations->fails()) {
                return back()->withErrors($validations)->withInput();
            }

            return $this->guardar($request, $id);
        } catch (\Exception $ex) {
            return back()->withErrors(['' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function guardar($request, $id = null)
    {
        DB::beginTransaction();
        try {
            $message = $id == null ? 'Trámite creado correctamente' : 'Trámite actualizado correctamente';
            // $documento = $id == null ? $request->documento : TramiteInternoModel::find($id)->documento;
            if ($request->tipo_tramite == 'MEM' || $request->tipo_tramite == 'INF') {
                $tipo_tramite = $request->tipo_tramite == 'MEM' ? 'MEMO' : 'INFORME';
                // dd($tipo_tramite);
            } else {
                $tipo_tramite = 'OFICIO';
            }
            // dd($request->all());
            $remitente = null;
            $analistas = array_merge(self::SECRETARIA_DIRECTOR, User::ANALISTAS);
            $esSecretaria = collect($analistas)->contains(Auth::user()->id_perfil);
            if ($request->tipo_envio == 'BORRADOR_DIRECTOR' && $esSecretaria) {
                $director = $request->tipo_envio == 'BORRADOR_SUBDIRECTOR' ? 'SD' : 'SI';
                $request->request->add(['director' => $director]);
                $remitente = $this->cambiarRemitente($request);
                $remitente = json_decode($remitente->content());
                $remitente = $remitente->id;
                $mensaje = Auth::user()->name . ' ha elaborado un ' . $tipo_tramite . ' para su correspondiente firma';
                $ruta = 'tramites/tramitesinternos/borradores';
                $this->notificacion->notificacionesweb($mensaje . '', $ruta, $remitente, '2c438f');
            } else $remitente = Auth::user()->id;

            $elaboradoPor = $remitente === Auth::user()->id ? null : Auth::user()->id;

            $tramite = TramiteInternoModel::updateOrCreate(['id' => $id], [
                'id_remitente' => $remitente,
                'numtramite' => $request->numtramite,
                'asunto' => $request->asunto,
                'peticion' => $request->peticion,
                'documento' => $request->documento,
                'tipo_envio' => $request->tipo_envio,
                'fecha' => $request->fecha,
                'tipo_firma' => $request->tipo_firma,
                'tipo_tramite' => $tipo_tramite,
                // 'modifico_fecha' => $request->modifico_fecha,
                'numtramite_externo' => $request->numtramite_externo,
                'nombre_informe' => $request->nombre_informe,
                'texto_adicional' => $request->texto_adicional,
                'cargo_adicional' => $request->cargo_adicional
            ]);

            if ($id == null)
            {
                $tramite->elaborado_por = $elaboradoPor;
                $tramite->save();
            }

            if ($request->has('analista_responde')) {
                $id_responder = Crypt::decrypt($request->analista_responde);

                $analista = AsignadoTramiteInternoModel::where(['id_tramite' => $id_responder])->where(function ($q) {
                    $q->where(['id_usuario_asignado' => Auth::user()->id])
                        ->orWhere(['id_usuario_asignado' => Auth::user()->id_direccion]);
                })->first();

                $analista->estado = 'ANALISTA-CONTESTADO';
                $analista->respuesta = $tramite->id;
                $analista->save();

                if ($request->tipo_envio == "ENVIAR") {
                    $idTramite = Crypt::encrypt($id_responder);
                    $tramiteResponder = TramiteInternoModel::findOrFail($id_responder);
                    $director = User::findOrFail($analista->solicitante, ['id', 'email']);
                    $ruta = "tramites/asignaranalistas/{$idTramite}";
                    $mensaje = Auth::user()->name . " ha contestado el trámite N° {$tramiteResponder->numtramite}";

                    AsignadoTramiteInternoModel::updateOrCreate([
                        'id_tramite' => $tramite->id,
                        'id_usuario_asignado' => $analista->solicitante
                    ], [
                        'estado' => 'ASIGNADO-RESPUESTA'
                    ]);

                    $this->guardarHistorial($tramite, "{$tipo_tramite} creado correctamente");
                    $this->guardarHistorial($tramiteResponder, "Respuesta de analista asignado: " . Auth::user()->name);

                    $analistaEspecial = $this->analistaEspecialDirector($analista->solicitante);
                    if (isset($analistaEspecial->id)) {
                        $this->notificacion->notificacionesweb($mensaje . '', $ruta, $analistaEspecial->id, '2c438f');
                    }

                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                    $this->notificacion->EnviarEmail($director->email, 'Trámite asignado', '', $mensaje, $ruta);
                }

                if ($request->has('anexos_json')) {
                    $this->guardarAnexos($request->archivo_anexo, $request->anexos_json, $tramite->id);
                }
            }
            else
            {
                if ($id == null)
                {
                    if ($request->tipo_envio == 'BORRADOR' || $request->tipo_envio == 'BORRADOR_DIRECTOR')
                    {
                        $this->guardarHistorial($tramite, "{$tipo_tramite} creado correctamente, pendiente de envío");
                    }
                    else
                    {
                        $this->guardarHistorial($tramite, "{$tipo_tramite} creado correctamente");
                    }
                }
                else
                {
                    $this->guardarHistorial($tramite, "{$tipo_tramite} enviado correctamente");
                }

                if ($tipo_tramite == 'OFICIO')
                {
                    $delegados_para = json_decode($request->delegados_para);
                    foreach($delegados_para as $key => $value)
                    {
                        if($value == null || $value == 'null')
                        {
                            unset($delegados_para[$key]);
                        }
                    }

                    sort($delegados_para);
                    $delegados_para = json_encode($delegados_para);
                    $tramite->delegados_para = $delegados_para;

                    if(isset($request->delegados_copia))
                    {
                        $delegados_copia = json_decode($request->delegados_copia);
                        foreach($delegados_copia as $key => $value)
                        {
                            if($value == null || $value == 'null')
                            {
                                unset($delegados_copia[$key]);
                            }
                        }

                        sort($delegados_copia);
                        $delegados_copia = json_encode($delegados_copia);
                        $tramite->delegados_copia = $delegados_copia;
                    }

                    $tramite->save();
                }

                if ($request->has('anexos_json')) {
                    $this->guardarAnexos($request->archivo_anexo, $request->anexos_json, $tramite->id);
                }

                $numtramite = str_replace('-', '', $tramite->numtramite);
                $ruta = 'tramites/tramitesinternos/recibidos?ti=' . $numtramite;
                $mensaje = Auth::user()->name . " le ha asignado el trámite N° $tramite->numtramite";
                if ($request->asignados) {

                    foreach ($request->asignados as $asignado) {
                        $usuarioAsignado = User::findOrFail($asignado);
                        AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $tramite->id, 'id_usuario_asignado' => $asignado], [
                            'estado' => 'ASIGNADO'
                        ]);
                        if ($request->tipo_envio == "ENVIAR") {
                            $this->notificacion->notificacionesweb($mensaje . '', $ruta, $asignado, '2c438f');
                            $this->notificacion->EnviarEmail($usuarioAsignado->email, 'Trámite asignado', '', $mensaje, $ruta);
                        }
                    }
                }

                // $ruta_ = 'tramites/copiaedit/' . Crypt::encrypt($tramite->id);
                $numtramite = str_replace('-', '', $tramite->numtramite);
                $ruta_ = 'tramites/tramitesinternos/copia?ti='.$numtramite;
                $mensaje_ = Auth::user()->name . " le ha enviado con copia el trámite N° $tramite->numtramite";
                if ($request->copia) {
                    foreach ($request->copia as $copia) {
                        AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $tramite->id, 'id_usuario_asignado' => $copia], [
                            'estado' => 'COPIA'
                        ]);

                        $usuarioCopia = User::findOrFail($copia);

                        if ($request->tipo_envio == "ENVIAR") {
                            $this->notificacion->notificacionesweb($mensaje_ . '', $ruta_, $copia, '2c438f');
                            $this->notificacion->EnviarEmail($usuarioCopia->email, 'Trámite asignado', '', $mensaje, $ruta_);
                        }
                    }
                }
                // }
            }



            if ($request->tipo_envio == "BORRADOR" && Auth::user()->id == 1643) {
                $usuario = User::where('id', Auth::user()->id)->first();
                if ($usuario) {
                    $players = [];
                    $telefonos_personas = DB::table('tma_playerid')
                        ->join('users as u', 'u.id', '=', 'tma_playerid.users_id')
                        ->where('u.id', $usuario->id)->get();
                    foreach ($telefonos_personas as $key => $v) {
                        array_push($players, $v->id_player);
                    }

                    $this->notificacion->notificacion($players, $tramite->id, 'Nuevo ' . $tramite->tipo_tramite . ' N° ' . $tramite->numtramite . ' borrador para revisar', 3, ['Nulo']);
                }
            }

            DB::commit();
            return redirect()->route('tramitesinternos.index')->with('message', $message);
        } catch (\Exception $ex) {
            DB::rollback();
            return back()->withErrors(['No se pudo guardar el trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    /**TRÁMITES RECIBIDOS */
    public function recibidos()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites recibidos';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'ASIGNADO',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    public function revisionEdit($id)
    {
        try {
            $ocultarPeticion = null;

            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::findOrFail($id, ['asunto', 'observacion', 'numtramite']);
            $tabla->asunto = "RE: {$tabla->asunto}";

            $query = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->where(function ($q) {
                    $q->where(['id_usuario_asignado' => Auth::user()->id])
                        ->orWhere(['id_usuario_asignado' => Auth::user()->id_direccion]);
                })->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO']);

            $estado = $query->first(['estado']);

            if ($estado->estado == 'ANALISTA-CONTESTADO') {
                return back()->with('message', "El trámite N° {$tabla->numtramite} ya fue contestado");
            }

            $asignados = $query->pluck('solicitante');

            if ($tabla->tipo_tramite == 'OFICIO') {
                $tabla->tipo_tramite_a = 'OFI';
            }
            if ($tabla->tipo_tramite == 'INFORME') {
                $tabla->tipo_tramite_a = 'INF';
            }

            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->where('users.id', $asignados[0])->orderBy('users.name')->pluck('users.name', 'users.id');

            // $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            //     ->join('users as u', 'ax.usuario_id', 'u.id')
            //     ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $user = User::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
                ->where('users.id', Auth::user()->id)->first();

            // $numtramite = 'MTA-' . $user->mta . '-' . 'MEM' . '-' . date('dmYHi');
            $numtramite = '';
            $analista = collect(User::ANALISTAS)->contains(Auth::user()->id_perfil);

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;
            $tabla['titulos'] = $this->titulos;
            $tabla['tipo_tramite'] = $analista ? $this->tipo_tramite_analistas : $this->tipo_tramite;

            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'tramitesinternos.store';
            $configuraciongeneral[3] = 'crear';
            $configuraciongeneral[4] = 'Contestar trámite asignado';
            $configuraciongeneral[10] = Crypt::encrypt($id);

            $configuraciongeneral[5] = '<button class="btn btn-primary" type="submit" style="float:right; margin-right: 30px;" id="btn_guardar">
                Guardar</button>';

            $no_director = in_array(Auth::user()->id_perfil, self::DIRECTORES) ? null : true;

            return view('tramites::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'numtramite' => $numtramite,
                'no_director' => $no_director,
                'ocultarPeticion' => $ocultarPeticion
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    public function guardarRevision(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $id = Crypt::decrypt($id);
            $tramite = TramiteInternoModel::findOrFail($id);

            $asignado = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'ASIGNADO',
                'id_usuario_asignado' => Auth::user()->id,
            ])->first();

            $asignado->respuesta = $request->peticion;
            $asignado->documento = $request->documento;
            $asignado->tipo_envio = $request->tipo_envio;
            $asignado->tipo_firma = $request->tipo_firma;
            $asignado->estado = 'CONTESTADO';
            $asignado->save();

            if ($request->has('archivo_anexo')) {
                $this->guardarAnexos($request->archivo_anexo, $request->anexos_json, $tramite->id);
            }

            $ruta = 'tramites/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
            $mensaje = Auth::user()->name . ' ha respondido al trámite N° ' . $tramite->numtramite;


            if ($request->tipo_envio == "ENVIAR") {
                $this->notificacion->notificacionesweb($mensaje . '', $ruta, $tramite->id_remitente, '2c438f');
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Trámite respondido', '', $mensaje, $ruta);
            }

            $copias = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'COPIA'
            ])->get();

            $rutaCopia = 'tramites/copiaedit/' . Crypt::encrypt($tramite->id);
            foreach ($copias as $copia) {

                if ($request->tipo_envio == "ENVIAR") {
                    $this->notificacion->EnviarEmail($copia->usuario->email, 'Trámite respondido', '', $mensaje, $rutaCopia);
                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $copia->id_usuario_asignado, '2c438f');
                }
            }

            DB::commit();
            return redirect()->route('tramites.recibidos')->with('message', 'Se ha respondido al trámite correctamente');
        } catch (\Exception $ex) {
            DB::rollback();
            return back()->withErrors(['No se pudo guardar la respuesta del trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function archivarTramite(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            // $id = Crypt::decrypt($id);
            $tramite = TramiteInternoModel::findOrFail($id);
            $asignado = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id,
                'id_usuario_asignado' => Auth::user()->id
            ])->whereIn('estado', ['ASIGNADO', 'ASIGNADO-RESPUESTA'])->first();

            // $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
            // ->where(function($q) {
            //     $q->where(['estado' => 'ASIGNADO'])
            //     ->orWhere(['estado' => 'ARCHIVADO']);
            // })->count();

            $this->guardarHistorial($tramite, 'Trámite archivado');

            $asignado->observacion = $request->observacion;
            $asignado->estado = 'ARCHIVADO';
            $asignado->save();

            // $ruta = 'tramites/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
            $ruta = 'tramites/tramitesinternos';
            $mensaje = Auth::user()->name . ' ha archivado el trámite N° ' . $tramite->numtramite;

            if ($tramite->tipo_envio == "ENVIAR") {
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Trámite archivado', '', $mensaje, $ruta);
                $this->notificacion->notificacionesweb($mensaje . '', $ruta, $tramite->id_remitente, '2c438f');
            }

            $copias = AsignadoTramiteInternoModel::where([
                'id_tramite' => $id, 'estado' => 'COPIA'
            ])->get();

            $rutaCopia = 'tramites/copiaedit/' . Crypt::encrypt($tramite->id);
            foreach ($copias as $copia) {
                if ($request->tipo_envio == "ENVIAR") {
                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $copia->id_usuario_asignado, '2c438f');
                    $this->notificacion->EnviarEmail($copia->usuario->email, 'Trámite respondido', '', $mensaje, $rutaCopia);
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha archivado el trámite correctamente'
            ]);
            // return redirect()->route('tramites.revision')->with('message', 'Se ha archivado el trámite correctamente');
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo archivar el trámite',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
            // return back()->withErrors(['No se pudo guardar la respuesta del trámite. ' . $ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }

    public function desvincularTramiteInterno(Request $request, $id)
    {
        if (request()->ajax()) {
            DB::beginTransaction();
            try {
                $tramite = TramiteInternoModel::findOrFail($id);
                $usuario = Auth::user();

                AsignadoTramiteInternoModel::where([
                    'id_tramite' => $id,
                    'id_usuario_asignado' => $usuario->id,
                    'estado' => 'ASIGNADO'
                ])->update([
                    'estado' => 'DESVINCULADO',
                    'observacion' => $request->observacion
                ]);

                $ruta = 'tramites/tramitesinternos/' . Crypt::encrypt($tramite->id) . '/edit';
                $mensaje = Auth::user()->name . ' se ha desvinculado del trámite N° ' . $tramite->numtramite . 'por el siguiente motivo: ' . $request->observacion;
                $mensajeWeb = Auth::user()->name . ' se ha desvinculado del trámite N° ' . $tramite->numtramite;

                $this->notificacion->notificacionesweb($mensajeWeb . '', $ruta, $tramite->id_remitente, '2c438f');
                $this->notificacion->EnviarEmail($tramite->remitente->email, 'Usuario desvinculado de trámite', '', $mensaje, $ruta);

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha desvinculado del trámite N° ' . $tramite->numtramite
                ]);
            } catch (\Exception $ex) {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        } else abort(401);
    }

    /**TRÁMITES RECIBIDOS CON COPIA */
    public function conCopia()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites recibidos con copia';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'COPIA',
            'tipo_envio' => 'ENVIAR',
        ]);
    }

    public function copiaEdit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);
            $asignados = AsignadoTramiteInternoModel::where(['id_tramite' => $id])
                ->whereIn('estado', ['ASIGNADO', 'CONTESTADO', 'ARCHIVADO'])->pluck('id_usuario_asignado');
            $copias = AsignadoTramiteInternoModel::where(['id_tramite' => $id, 'estado' => 'COPIA'])->pluck('id_usuario_asignado');
            $tabla['destinatarios'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->whereIn('users.id', $asignados)->pluck('users.name', 'users.id');

            $tabla['copias'] = User::join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
                ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
                ->whereIn('users.id', $copias)->pluck('users.name', 'users.id');

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $tabla['prioridades'] = [null => 'Elija una opción'] + $this->prioridad;

            $tabla['contestados'] = AsignadoTramiteInternoModel::with('usuario')
                ->where(['id_tramite' => $id, 'estado' => 'CONTESTADO'])->get();

            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'revision.store';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Revisión de trámites asignados';
            $configuraciongeneral[5] = '';
            $configuraciongeneral[6] = 'copia';
            // dd($configuraciongeneral);

            if (in_array(Auth::user()->id_perfil,  self::DIRECTORES)) {
                $no_director = null;
            } else {
                $no_director = true;
            }
            return view('tramites::tramitesinternos.create', [
                'tabla' => $tabla,
                'configuraciongeneral' => $configuraciongeneral,
                'asignados' => $asignados,
                'copias' => $copias,
                'anexos' => $anexos,
                'no_director' => $no_director,
                'editcopia' => 'SI'
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors([' ' .  $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    /**TRÁMITES CONTESTADOS */
    public function contestados()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites contestados';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'CONTESTADO',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    /**TRÁMITES ARCHIVADOS */
    public function archivados()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites archivados';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'ARCHIVADO',
            'tipo_envio' => 'ENVIAR'
        ]);
    }
    /**TRÁMITES BORRADORES */
    public function borradores()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites por enviar';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'INICIAL',
            'tipo_envio' => 'BORRADOR'
        ]);
    }

    public function guardarAnexos($archivos, $info, $id_tramite)
    {
        $info = json_decode($info);
        $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')->where(['id_referencia' => $id_tramite, 'tipo' => 11])->get();
        // dd($info);
        foreach ($anexos as $key => $value) {
            $bandera = true;
            foreach ($info as $kk => $vv) {
                # code...
                if ($vv->id_anexo == $value->id) {
                    $bandera = false;
                }
            }
            if ($bandera) {
                $upload = AnexosModel::find($value->id);
                $ruta = $this->archivos_path . '/' . $upload->ruta;
                if (is_file($ruta)) {
                    unlink($ruta);
                }
                $upload->delete();
            }
        }

        // dd($archivos);
        if (is_array($archivos)) {
            foreach ($archivos as $key => $value) {
                $archivo = $value;
                $name = 'Archivo_Ref_' . $id_tramite . '_tipo_11_' . sha1(date('YmdHis') . Str::random(10));
                $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

                $archivo->move($this->archivos_path, $resize_name);

                $upload = new AnexosModel;
                $upload->usuario_id = Auth::user()->id;
                $upload->id_referencia = $id_tramite;
                $upload->ruta = $resize_name;
                $upload->nombre = basename($archivo->getClientOriginalName());
                $upload->hojas = $info[$key]->hojas;
                $upload->descripcion = $info[$key]->descripcion;
                $upload->tipo = 11;
                $upload->save();
            }
        }
    }

    public function generarDocumento()
    {
        try
        {
            $input = request()->all();
            $tipo_x = request()->tipo;

            request()->request->add(['director' => $input['enviar_director']]);

            $cambio = $this->cambiarRemitente(request());
            $cambio = json_decode($cambio->content());

            if (isset($input['enviar_director']))
            {
                $id = $input['enviar_director'] == 'NO' ? Auth::user()->id : $cambio->id;
            }
            else
            {
                $id = Auth::user()->id;
            }

            $nombre_documento = 'Tramite_interno' . $input['numtramite'] . '_' . $id . '.pdf';
            $datos = new stdClass;
            $datos->nombre_documento = $nombre_documento;
            $userpara = explode(',', request()->para);
            $para = User::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $userpara)->get();
            $usercopia = explode(',', request()->copia);
            $copia = User::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')->select('name', 'cargo', 'direccion')->whereIn('users.id', $usercopia)->get();

            $datos->asunto = request()->asunto;

            $anexos_json = json_decode(request()->anexos_json);
            if (is_array($anexos_json) && isset($anexos_json[0]))
            {
                $datos->anexos = $anexos_json;
            }
            else
            {
                $datos->anexos = null;
            }

            $datos->tipo_firma = $tipo_x;
            if ($tipo_x == 2)
            {
                $datos->fecha_firma = date('Y-m-d H:i:s');
                $datos->codigo = request()->codigo;
            }
            else
            {
                $datos->codigo = '';
                $datos->fecha_firma = '';
            }

            $tipo = request()->tipo_tramite_interno;
            $datos->tipo = 'MEMORANDO';
            if ($tipo == 'OFI') {
                $datos->tipo = 'OFICIO';
                $delegados_para = json_decode(request()->delegados_para);

                if (is_array($delegados_para) && isset($delegados_para[0])) {
                    // $para = [];
                    foreach ($delegados_para as $key => $value) {
                        $para_datos = new stdClass;
                        $para_datos->titulo = $value->titulo;
                        $para_datos->name = $value->nombre;
                        $para_datos->cargo = $value->cargo;
                        $para_datos->institucion = $value->institucion;
                        $para[] = $para_datos;
                    }
                } else {
                    // $para = [];
                }

                $delegados_copia = json_decode(request()->delegados_copia);

                if (is_array($delegados_copia) && isset($delegados_copia[0]))
                {
                    foreach ($delegados_copia as $key => $value) {
                        $copia_datos = new stdClass;
                        $copia_datos->name = $value->titulo . '<br>' . $value->nombre;
                        $copia_datos->cargo = $value->cargo;
                        $copia_datos->institucion = $value->institucion;
                        $copia[] = $copia_datos;
                    }
                }
                else
                {
                    // $copia = [];
                }
                //  dd($copia);
            }
            if ($tipo == 'INF')
            {
                $datos->tipo = 'INFORME';
                $datos->numtramite_externo = request()->numtramite_externo;
                $datos->nombre_informe = request()->nombre_informe;
            }

            if (request()->fecha)
            {
                $datos->fecha = request()->fecha;
            }

            $datos->para = $para;
            $datos->copia = $copia;
            $datos->peticion = request()->peticion;
            $datos->borrador = $tipo_x;
            // $datos->prioridad = Input::get('prioridad');
            $datos->numtramite = request()->numtramite;
            $user = User::join('tmae_direcciones as d', 'd.id', '=', 'users.id_direccion')
                ->where('users.id', $id)->first();
            $datos->user = $user;

            $elaboradoPor = request()->elaborado_por;
            $elaboradoPor = User::find($elaboradoPor, ['name']);
            $esDirector = collect(User::DIRECTORES)->contains(Auth::user()->id_perfil);
            $datos->elaborado_por = (isset($elaboradoPor) && (isset($input['enviar_director']) && ($input['enviar_director'] == 'SI'))) ? $elaboradoPor->name : null;

            $pdf = \App::make('dompdf.wrapper');

            $context = stream_context_create([
                'ssl' => [
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                    'allow_self_signed' => TRUE
                ]
            ]);
            // $pdf->setOptions(['isPhpEnabled' => true]);
            // $pdf->setOptions(['dpi' => 150, 'defaultFont' => 'Arial']);
            $pdf->getDompdf()->setHttpContext($context);
            $pdf->loadView('tramites::firmaelectronica.memo', [
                "datos" => $datos,
                "configuraciongeneral" => $this->configuraciongeneral
            ])->setPaper('a4', 'portrait'); //])->setPaper('a4','landscape');
            // return $pdf->stream($nombre_documento);
            $dom_pdf = $pdf->getDomPDF();
            $canvas = $dom_pdf->get_canvas();


            if ($tipo == 'INF') {
                $canvas->page_text(430, 95, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            } else {
                $canvas->page_text(10, 800, "Pag. {PAGE_NUM} de {PAGE_COUNT}", null, 10, array(0, 0, 0));
            }

            $pdf->download($nombre_documento);


            $documento = $pdf->output();
            $carpeta  = public_path() . '/archivos_temporales';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            $cedula = Auth::user()->cedula;
            // $cedula = '1309029708';
            $rutafinal = $carpeta . '/' . $nombre_documento;
            file_put_contents($rutafinal, $documento);

            if ($tipo_x == 2) {
                $codigo = TramInternoCodigosModel::where(['estado' => 'ACT', 'numtramite' => request()->numtramite, 'codigo' => request()->codigo])->first();
                // $codigo = TramInternoCodigosModel::where(['estado' => 'ACT'])->first();
                if ($codigo) {
                    $codigo = TramInternoCodigosModel::find($codigo->id);
                    $codigo->fecha_firma = date('Y-m-d H:i:s');
                    $codigo->save();
                    $carpeta_2  = public_path() . '/archivos_firmados';
                    $rutafinal_2 = $carpeta_2 . '/' . $nombre_documento;
                    if (is_file($rutafinal_2)) {
                        unlink($rutafinal_2);
                    }
                    file_put_contents($rutafinal_2, $documento);
                    return ['estado' => true, 'documento' => $nombre_documento];
                } else {
                    return ['estado' => false, 'documento' => null,  'msg' => 'Código inválido'];
                }
            }

            if ($tipo_x == 3) {
                return ['estado' => true, 'msg' => url('/') . '/archivos_temporales/' . $nombre_documento];
            }
            ////para pruebas se generar localmente para dejarlo pasar sin firmar
            if (config('app.env') == 'local') {
                $carpeta_2  = public_path() . '/archivos_firmados';
                $rutafinal_2 = $carpeta_2 . '/' . $nombre_documento;
                if (is_file($rutafinal_2)) {
                    unlink($rutafinal_2);
                }
                file_put_contents($rutafinal_2, $documento);
            }
            $tipo_firma = request()->tipo_firma;
            // $tipo_firma = 2;
            return $this->firmaElectronicaController->firmaToken($nombre_documento, $rutafinal, $cedula, $tipo_firma);
        } catch (\Throwable $th) {
            // dd($th);
            Log::info($th);
            return [
                'estado' => false,
                'documento' => null,
                'msg' => 'Si el texto fue copiado de un documento de Word por favor dar click derecho, luego seleccionar pegar como texto sin formato, luego darle el formato correspondiente con el editor web de esta página.',
                'exception' => $th->getMessage() . ' - ' . $th->getLine()
            ];
        }
    }

    public function validarDocumento()
    {
        $input = request()->all();
        $carpeta  = public_path() . '/archivos_firmados/' . $input['documento'];
        if (file_exists($carpeta)) {
            $ruta = 'https://sistemasic.manta.gob.ec/archivos_firmados/' . $input['documento'];
            $ruta = url('/') . '/archivos_firmados/' . $input['documento'];
            return ['estado' => true, 'msg' => 'Documento encontrado', 'ruta' => $ruta, 'documento' => $input['documento']];
        } else {
            return ['estado' => false, 'msg' => 'No se encuentra el documento firmado en el servidor'];
        }
    }
    public function generarCodigo()
    {
        try
        {
            $datos = TramInternoAutorizacionesModel::where([
                'cedula' => Auth::user()->cedula,
                'estado_autorizacion' => 'APROBADO'
            ])->first();

            if (!$datos)
            {
                return ['estado' => false, 'msg' => 'No tiene autorización, por favor acercarse a R.R.H.H para que sea habilitado.'];
            }

            TramInternoCodigosModel::where('numtramite', request()->numtramite)->update(['estado' => 'INA']);
            $codigo = new TramInternoCodigosModel;
            $codigo->codigo = Auth::user()->id . rand(1000, 5000);
            $codigo->numtramite = request()->numtramite;
            $codigo->id_usuario = Auth::user()->id;
            $codigo->save();
            $this->notificacion->EnviarEmail(Auth::user()->email, 'CÓDIGO DE AUTORIZACIÓN', $codigo->codigo, 'CÓDIGO DE AUTORIZACIÓN', '',  "vistas.emails.email", "NO");
            return ['estado' => true, 'msg' => 'Código enviado al correo ' . Auth::user()->email];
        } catch (\Throwable $th) {
            // return $th->getMessage();
            return ['estado' => false, 'msg' => 'No se pudo enviar el código de validación'];
        }
    }

    public function eliminarBorrador()
    {
        try
        {
            $id = request()->id;
            $id = Crypt::decrypt($id);
            HistorialInternosModel::where(['tramite_id' => $id])->delete();
            AsignadoTramiteInternoModel::where(['id_tramite' => $id])->delete();
            $tramite = TramiteInternoModel::find($id);
            $carpeta  = public_path() . '/archivos_firmados/' . $tramite->documento;
            if (is_file($carpeta)) {
                unlink($carpeta);
            }
            $tramite->delete();
            return ['estado' => true, 'msg' => 'Borrador eliminado correctamente'];
        } catch (\Throwable $th) {
            return ['estado' => false, 'msg' => 'No se pudo eliminar el borrador', 'ex' => $th->getMessage()];
        }
    }

    public function show($id)
    {
        $tabla = TramiteInternoModel::find($id);

        $archivados = AsignadoTramiteInternoModel::from('tma_tram_interno_asignados as asig')
            ->join('users as u', 'u.id', 'asig.id_usuario_asignado')
            ->select('u.name as usuario', 'asig.observacion', 'asig.updated_at as fecha')
            ->where(['asig.id_tramite' => $id, 'asig.estado' => 'ARCHIVADO'])->get();
        $historial = $this->obtenerHistorial($id);

        $analistasAsignados = AsignadoTramiteInternoModel::from('tma_tram_interno_asignados as asig')
            ->join('users as u', 'asig.id_usuario_asignado', 'u.id')
            ->select('u.name', 'asig.estado', 'asig.observacion')
            ->where(['asig.id_tramite' => $id, 'solicitante' => Auth::user()->id])
            ->whereIn('asig.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();

        return view('vistas.historialtramitesinternos', [
            'timelineTramiteInternos' => $historial,
            'objetos' => null,
            'tabla' => $tabla,
            'archivados' => $archivados,
            // 'analistasAsignados' => $analistasAsignados,
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    public function asignados()
    {
        $this->configuraciongeneral[4] = 'Revisión de trámites asignados';
        return view('tramites::tramitesinternos.index', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'crear_editar' => 'NO',
            'estado' => 'ANALISTA',
            'tipo_envio' => 'ENVIAR'
        ]);
    }

    public function asignarAnalistas($id)
    {
        try
        {
            $id = Crypt::decrypt($id);
            $tabla = TramiteInternoModel::with('remitente')->findOrFail($id);
            $analistas = $this->obtenerAnalistas(Auth::user()->id_direccion, $tabla->id_remitente);
            $historial = $this->obtenerHistorial($id);

            // if (User::esCoordinador())
            // {
            //     $query = AsignadoTramiteInternoModel::with('respuestaTramite.anexos')->from('tma_tram_interno_asignados as asig')
            //         ->join('tmae_direcciones as dir', 'asig.id_usuario_asignado', 'dir.id')
            //         ->select('dir.direccion as name', 'asig.estado', 'asig.respuesta')
            //         ->where(['asig.id_tramite' => $id, 'asig.solicitante' => Auth::user()->id]);

            //     $analistasAsignados = $query->whereIn('asig.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();
            //     $btnAsignar = $query->where(['asig.estado' => 'ANALISTA'])->count();
            // }
            // else
            // {
                $analistaEspecial = collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
                $director = $this->obtenerDirector();
                $id_director = $analistaEspecial ? $director['id'] : Auth::user()->id;

                $query = AsignadoTramiteInternoModel::with('respuestaTramite.anexos')->from('tma_tram_interno_asignados as asig')
                    ->join('users as u', 'asig.id_usuario_asignado', 'u.id')
                    ->select('u.name', 'asig.estado', 'asig.observacion', 'asig.respuesta')
                    ->where(['asig.id_tramite' => $id, 'asig.solicitante' => $id_director]);

                $analistasAsignados = $query->whereIn('asig.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();
                $btnAsignar = $query->where(['asig.estado' => 'ANALISTA'])->count();
            // }

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 11])->get();

            $configuraciongeneral = $this->configuraciongeneral;
            $configuraciongeneral[1] = 'revision.store';
            $configuraciongeneral[2] = 'tramitesinternos.formasignar';
            $configuraciongeneral[3] = 'editar';
            $configuraciongeneral[4] = 'Asignación de analistas';

            return view('tramites::tramitesinternos.create', [
                'tabla' => $tabla,
                'timelineTramiteInternos' => $historial,
                'configuraciongeneral' => $configuraciongeneral,
                'analistasAsignados' => $analistasAsignados,
                'anexos' => $anexos,
                'analistas' => $analistas,
                'btnAsignar' => $btnAsignar
            ]);
        } catch (\Exception $ex) {
            return back()->withErrors(['No se pudo ' . $ex->getMessage() . ' - ' . $ex->getLine()]);
        }
    }

    public function asignarAnalistaInternos(Request $request, $id)
    {
        if ($request->ajax())
        {
            DB::beginTransaction();
            try
            {
                $tramite = TramiteInternoModel::findOrFail($id);
                $tramite->observacion = $request->observacion;
                $tramite->save();

                // $esCoodinacion = collect(User::COORDINACIONES)->contains(Auth::user()->id_direccion);
                $analistasInformar = $request->analistas;

                $ruta = 'tramites/revisionedit/' . Crypt::encrypt($tramite->id);
                $mensaje = Auth::user()->name . ' le ha asignado el trámite N° ' . $tramite->numtramite;
                $analistaEspecial = collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
                $director = null;

                if($analistaEspecial)
                {
                    $director = $this->obtenerDirector();
                }

                $solicitante = isset($director) ? $director['id'] : Auth::user()->id;

                foreach ($analistasInformar as $informar)
                {
                    // if ($esCoodinacion)
                    // {
                    //     AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $id, 'id_usuario_asignado' => $informar], [
                    //         'solicitante' => $solicitante,
                    //         'respondido' => 0,
                    //         'estado' => 'ANALISTA'
                    //     ]);

                    //     $director = User::directores()->where('users.id_direccion', $informar)->get();
                    //     foreach ($director as $dir) {
                    //         $this->notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                    //         $this->notificacion->EnviarEmail($dir->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                    //     }

                    //     $analistas = direccionesModel::whereIn('id', $analistasInformar)->get(['direccion']);
                    //     $analistas = $analistas->map(function ($analista) {
                    //         return $analista->direccion;
                    //     })->implode(', ');

                    //     $this->guardarHistorial($tramite, 'Asignar trámite a analista: ' . $analistas);
                    // }
                    // else
                    // {
                    // }
                    $analista = User::findOrFail($informar);

                    AsignadoTramiteInternoModel::updateOrCreate(['id_tramite' => $id, 'id_usuario_asignado' => $analista->id], [
                        'solicitante' => $solicitante,
                        'estado' => 'ANALISTA'
                    ]);

                    $this->notificacion->notificacionesweb($mensaje . '', $ruta, $analista->id, '2c438f');
                    $this->notificacion->EnviarEmail($analista->email, 'Trámite asignado', '', $mensaje, $ruta, 'vistas.emails.email');

                    $analistas = User::whereIn('id', $analistasInformar)->get();
                    $analistas = $analistas->map(function ($analista) {
                        return $analista->name;
                    })->implode(', ');

                    $this->guardarHistorial($tramite, 'Asignar trámite a analista: ' . $analistas);
                }

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha notificado correctamente a los analistas seleccionados.'
                ]);
            } catch (\Exception $ex) {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => 'No se pudo notificar a las direcciones seleccionada, inténtelo de nuevo.',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        } else abort(401);
    }

    public function obtenerAnalistas($direccion, $remitente = null)
    {
        // $esCoodinacion = collect(User::COORDINACIONES)->contains($direccion);
        // $id = Auth::user()->id;

        // if($esCoodinacion)
        // {
        //     $ina = [Auth::user()->id_direccion] + User::DIRECCIONES_INA;
        //     $analistas = CoordinacionDetaDireccionModel::from('coor_tmov_coordinacion_direccion_deta as deta')
        //         ->join('coor_tmae_coordinacion_main as coor', 'deta.id_coor_cab', 'coor.id')
        //         ->join('tmae_direcciones as dir', 'dir.id', 'deta.id_direccion')
        //         ->join('users as u', 'coor.id_responsable', 'u.id')
        //         ->join('users as ud', 'deta.id_direccion', 'ud.id_direccion')
        //         ->select('dir.id', 'dir.direccion as name', 'ud.cargo', 'dir.direccion')
        //         ->where(['coor.id_responsable' => Auth::user()->id, 'dir.estado' => 'ACT'])
        //         ->whereNotIn('dir.id', $ina)
        //         ->whereIn('ud.id_perfil', User::SOLO_DIRECTORES)
        //         ->groupBy('dir.id')->orderBy('dir.direccion')->get();
        // }
        // else if($id == 1643) //ALCALDE
        // {
        //     $asignar = array_merge(User::SOLO_DIRECTORES, User::ID_COORDINADOR);

        //     $analistas = User::where(['estado' => 'ACT'])
        //         ->whereIn('id_perfil', $asignar)
        //         ->where('id', '<>', $remitente)
        //         ->orderBy('name')->get(['cargo', 'name', 'id']);
        // }
        // else
        // {
        //     $perfiles = collect(User::ANALISTAS);
        //     $unidades = SubDireccionModel::where(['id_direccion' => $direccion])->pluck('area');
        //     $analistas = User::where(function ($q) use ($direccion, $unidades) {
        //         $q->whereIn('id_direccion', $unidades)
        //             ->orWhere(['id_direccion' => $direccion]);
        //     })->whereIn('id_perfil', $perfiles)->select('cargo', 'name', 'id')->orderBy('name')->get();
        // }
        $perfiles = User::ANALISTAS;
        $analistas = User::where(function ($q) use ($direccion) {
                    $q->where(['id_direccion' => $direccion]);
                })->whereIn('id_perfil', $perfiles)->select('cargo', 'name', 'id')->orderBy('name')->get();

        return $analistas;
    }

    private function guardarHistorial($data, $estado)
    {
        $historial = HistorialInternosModel::create([
            'tramite_id' => $data->id,
            'id_remitente' => $data->id_remitente,
            'id_usuario' => Auth::user()->id,
            'numtramite' => $data->numtramite,
            'tipo_tramite' => $data->tipo_tramite,
            'asunto' => $data->asunto,
            'peticion' => $data->peticion,
            'estado' => $estado,
            'delegados_para' => $data->delegados_para,
            'delegados_copia' => $data->delegados_copia,
            'tipo_firma' => $data->tipo_firma,
            'tipo_envio' => $data->tipo_envio,
            'numtramite_externo' => $data->numtramite_externo,
            'nombre_informe' => $data->nombre_informe
        ]);

        return $historial->id;
    }

    private function obtenerHistorial($id)
    {
        $tabla = HistorialInternosModel::from('tram_tmo_historial_tramites_internos as histo')
            ->join('users as remitente', 'histo.id_remitente', 'remitente.id')
            ->join('users as u', 'histo.id_usuario', 'u.id')
            ->select('histo.*', 'remitente.name as remitente', 'u.name as usuario')
            ->where('histo.tramite_id', $id)->orderBy('histo.updated_at', 'ASC')->get();
        return $tabla;
    }

    public function cambiarRemitente(Request $request)
    {
        $director = $request->has('director') ? $request->director : 'SI';

        $remitente = [];
        if (Auth::user()->id_perfil == 1)
        {
            return response()->json([
                'ok' => false,
                'remitente' => '',
                'id' => '',
                // 'remitentes' => $remitentes
            ]);
        }

        if ($director == 'SI')
        {
            $analista = collect(User::ANALISTAS)->contains(Auth::user()->id_perfil);

            $id_direccion = Auth::user()->id_direccion;

            $usuarios = User::where(['id_direccion' => $id_direccion])
                ->whereIn('id_perfil', User::DIRECTORES)->get(['id', 'id_perfil', 'name', 'cargo']);

            foreach ($usuarios as $idx => $director)
            {
                $esDirector = Str::contains($director->cargo, 'DIRECTOR')
                    || Str::contains($director->cargo, 'SECRETARIO')
                    || Str::contains($director->cargo, 'PROCURADOR');

                if (!$esDirector)
                {
                    unset($usuarios[$idx]);
                }
                else
                {
                    $remitente = $director;
                    break;
                }
            }
        }
        elseif ($director == 'SD')
        {
            $remitente = User::where(['id_direccion' => Auth::user()->id_direccion])
                ->whereIn('id_perfil', User::SUBDIRECTORES)->first(['id', 'id_perfil', 'name', 'cargo']);

            if (!$remitente)
            {
                $remitente = User::where(['id_direccion' => Auth::user()->id_direccion])
                    ->whereIn('id_perfil', User::ANALISTA_SUBAREA)
                    ->first(['id', 'id_perfil', 'name', 'cargo']);
            }
        }
        else
        {
            $remitente = Auth::user();
        }

        return response()->json([
            'ok' => true,
            'remitente' => isset($remitente->name) ? $remitente->name : Auth::user()->name,
            'id' => isset($remitente->id) ? $remitente->id : Auth::user()->id,
        ]);
    }

    public function obtenerDirector()
    {
        // $subarea = SubDireccionModel::where(['area' => Auth::user()->id_direccion])->first(['id_direccion']);
        // $id_direccion = isset($subarea->id_direccion) ? $subarea->id_direccion : Auth::user()->id_direccion;
        $id_direccion = Auth::user()->id_direccion;
        $perfiles = array_merge(User::DIRECTORES, [12]);

        $usuarios = User::where(['id_direccion' => $id_direccion])
                ->whereIn('id_perfil', $perfiles)->get(['id', 'id_perfil', 'name', 'cargo']);

        $remitente = null;

        foreach ($usuarios as $idx => $director)
        {
            $esDirector = Str::contains($director->cargo, 'DIRECTOR') || Str::contains($director->cargo, 'SECRETARIO') || Str::contains($director->cargo, 'PROCURADOR') || Str::contains($director->cargo, 'ALCALDE');
            if (!$esDirector)
            {
                unset($usuarios[$idx]);
            }
            else
            {
                $remitente = $director;
                break;
            }
        }

        return [
            'ok' => true,
            'director' => $remitente->name,
            'id' => $remitente->id
        ];
    }

    public function analistaEspecialDirector($id_director)
    {
        $director = User::findOrFail($id_director, ['id_direccion']);
        $analistaEspecial = User::where(['estado' => 'ACT', 'id_direccion' => $director->id_direccion])
        ->whereIn('id_perfil', User::ANALISTA_DIRECTOR)->first(['id', 'name', 'email']);

        return $analistaEspecial ?? null;
    }

    public function getDate()
    {
        return date('Y-m-d H:i');
    }
}
