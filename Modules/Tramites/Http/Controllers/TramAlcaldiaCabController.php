<?php

namespace Modules\Tramites\Http\Controllers;

use App\User;
use DateTimeZone;
use Carbon\Carbon;
use App\BarrioModel;
use App\CantonModel;
use App\ArchivosModel;
use App\parroquiaModel;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Redirect;
use Modules\Tramites\Entities\AreasModel;
use Illuminate\Support\Facades\Validator;
use GuzzleHttp\Client as GuzzleHttpClient;
use Modules\Tramites\Entities\AnexosModel;
use Modules\Tramites\Entities\direccionesModel;
use Modules\Tramites\Entities\TipoTramiteModel;
use App\Http\Controllers\NotificacionesController;
use Modules\Tramites\Entities\TramAlcaldiaCabModel;
use Modules\Tramites\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramites\Entities\TramAlcaldiaCabDetaModel;

class TramAlcaldiaCabController extends Controller
{
    const ID_PERFIL_SECRETARIA = [20, 80, 82];
    const ID_PERFIL_ADMINISTRADOR = 1;

    const ID_PERFIL_DIRECTOR = 3;
    const ID_PERFIL_DIRECTOR_OOPP = 5;
    const ID_PERFIL_DIRECTOR_TI = 9;
    const ID_PERFIL_SECRETARIA_ALCALDIA = 16;
    const ID_PERFIL_DIRECTOR_DONACIONES = 42;
    const ID_PERFIL_DIRECTOR_PLAN = 22;

    const ID_DESPACHO_ALCALDIA = 8;

    private $archivos_path;

    public $configuraciongeneral = array("Gestión de trámites", "tramites/externos", "index", 6 => "tramites/tramitesAlcaldiaPenidentesajax", 7 => "tramites");
    public $escoja = array(null => "Escoja opción...");
    public $objetos = '[
		{"Tipo":"textdisabled","Descripcion":"Número de Trámite","Nombre":"numtramite","Clase":"numeros-letras-guion mayuscula disabled","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de inicio*","Nombre":"fecha_ingreso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Área*","Nombre":"id_area","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"select","Descripcion":"Tipo de Trámite*","Nombre":"tipo_tramite","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Asunto*","Nombre":"asunto","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea2","Descripcion":"Petición*","Nombre":"peticion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Tipo de documento*","Nombre":"tipo_documento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Número de documento*","Nombre":"cedula_remitente","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Remitente*","Nombre":"remitente","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"telefonos","Descripcion":"Número Telefónico","Nombre":"telefono","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"email","Descripcion":"Correo Electrónico","Nombre":"correo_electronico","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia*","Nombre":"id_parroquia","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Barrio*","Nombre":"id_barrio","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"direccion","Descripcion":"Dirección*","Nombre":"direccion","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Dirección a atender*","Nombre":"direccion_atender","Clase":"selective-tags","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select-multiple","Descripcion":"Con copia a","Nombre":"direccion_informar","Clase":"selective-tags","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Prioridad*","Nombre":"prioridad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"textarea","Descripcion":"Observaciones generales","Nombre":"observacion_general","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Fecha estimada de finalización","Nombre":"fecha_fin","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea2","Descripcion":"Recomendación*","Nombre":"recomendacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"¿Desea que la respuesta llegue a secretaría?","Nombre":"obtener_respuesta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"textarea2","Descripcion":"Observación*","Nombre":"observacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de Ingreso*","Nombre":"created_at","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
		{"Tipo":"text","Descripcion":"Estado","Nombre":"disposicion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha de Finalización","Nombre":"fecha_respuesta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto*","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"file","Descripcion":"Adjunto de Finalización","Nombre":"archivo_finalizacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Finalizar","Nombre":"estado_proceso","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';

    public $validarjs = array(
        "fecha_ingreso" => "fecha_ingreso: { required: true }",
        "numtramite" => "numtramite: { required: true }",
        "remitente" => "remitente: { required: true }",
        "asunto" => "asunto: { required: true }",
        "peticion" => "peticion: { required: true }",
        "prioridad" => "prioridad: { required: true }",
        "direccion_atender" => "direccion_atender: { required: true }",
        "fecha_fin" => "fecha_fin: { required: true }",
        "recomendacion" => "recomendacion: { required: false }",
    );

    var $tercera_edad = [
        'SI' => 'SI',
        'NO' => 'NO'
    ];

    var $tipo_documento = [
        'CEDULA' => 'CÉDULA',
        'RUC' => 'RUC'
    ];

    var $obtener_respuesta = [
        'SI' => 'SI',
        'NO' => 'NO'
    ];

    var $sin_correo = '<script>
    $(document).ready(function () {
        let data = "NO SE PROPORCIONÓ";
        $("#tabla_message").append(\'<tr id="fila_1\'+\'" ><td>\'+data+\'</td>\');
    });
    </script>
    <table class="table table-striped" id="tabla_message">
    <tbody>
    </tbody>
    </table>';

    var $respuesta_si = '<div><p class="bg-primary text-center">LA RESPUESTA VA A SECRETARÍA GENERAL</p></div>';
    var $respuesta_no = '<div><p class="bg-primary text-center">SECRETARÍA GENERAL INDICA QUE LA RESPUESTA VA A SER DIRECTAMENTE AL CIUDADANO</p></div>';

    protected $Notificacion;

    public function __construct(NotificacionesController $Notificacion)
    {
        $this->middleware('auth', ['except' => ['tramitesCiudadano', 'timeline', 'crearSolicitud','recibo']]);
        // $this->middleware('creartramite', ['only' => ['index', 'create']]);
        $this->Notificacion = $Notificacion;
        $this->archivos_path = public_path('archivos_sistema');
    }

    // public function tramitesCiudadano($cedula)
    // {
    //     try
    //     {
    //         $ids = array_merge(User::DIRECTORES, User::ANALISTA_DIRECTOR);
    //         $ids = array_merge($ids, User::SUBDIRECTORES);
    //         $ids = array_merge($ids, [User::SECRETARIO_GENERAL]);

    //         $tramites = TramAlcaldiaCabModel::where([
    //             'cedula_remitente' => $cedula,
    //             'estado' => 'ACT'
    //         ])->get();

    //         foreach($tramites as $te)
    //         {
    //             if($te->obtener_respuesta == 'SI' && $te->tipo != 'TRAMITES MUNICIPALES')
    //             {
    //                 $ids = [User::SECRETARIO_GENERAL];
    //             }
    //             else
    //             {
    //                 $ids = array_merge(User::DIRECTORES, User::ANALISTA_DIRECTOR);
    //                 $ids = array_merge($ids, User::SUBDIRECTORES);
    //                 $ids = array_merge($ids, [User::SECRETARIO_GENERAL]);
    //             }

    //             $archivos = ArchivosModel::from('tmov_archivos as ar')
    //                 ->join('users as u', 'u.id', 'ar.id_usuario')
    //                 ->select('ar.ruta')
    //                 ->where(['ar.tipo' => 10, 'id_referencia' => $te->id])
    //                 ->whereIn('u.id_perfil', $ids)->get();

    //             $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
    //                 ->join('users as u', 'ax.usuario_id', 'u.id')
    //                 ->select('ax.ruta')
    //                 ->where(['tipo' => 11, 'id_tramite_externo' => $te->id])
    //                 ->whereIn('u.id_perfil', $ids)->get();

    //             $archivos = array_merge($archivos->toArray(), $archivos_->toArray());
    //             $te->archivos = $archivos;
    //         }

    //         return response()->json([
    //             'tramites' => $tramites
    //         ]);
    //     } catch (\Exception $ex)
    //     {
    //         return response()->json('Algo salió mal: ' . $ex->getMessage() . ' - ' . $ex->getLine());
    //     }
    // }

    // public function timeline($id)
    // {
    //     $tramite = TramAlcaldiaCabModel::find($id, ['tipo']);
    //     $estado = ['EN PROCESO','REVISION', 'FINALIZADO'];

    //     if($tramite->tipo == 'TRAMITES MUNICIPALES')
    //     {
    //         $estado = ['PENDIENTE', 'EN PROCESO','REVISION', 'FINALIZADO'];
    //     }
    //     $timeline = TramAlcaldiaCabDetaModel::select('id_tram_cab', 'estado_proceso as valor_estado', 'observacion as estado', 'recomendacion', 'disposicion', 'created_at')
    //         ->where('id_tram_cab', $id)->where(function ($q) use($estado) {
    //             $q->whereNull('disposicion')
    //             ->orWhereIn('disposicion', $estado);
    //         })->whereNotNull('observacion')->get();

    //     return response()->json($timeline);
    // }

    public function index()
    {
        $objetos = json_decode($this->objetos);

        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[18]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[26]);
        unset($objetos[27]);

        $objetos[17]->Tipo = 'text';
        $objetos[17]->Nombre = 'asignado_a';
        $objetos[17]->Descripcion = 'Asignado a';

        $objetos = array_values($objetos);
        $tabla = [];
        $edit = null;
        $verid = null;
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil) || Auth::user()->id_perfil == 1;
        $crear = $ingresaTramites ? 'si' : 'no';

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => 'si',
            "edit" => $edit,
            "verid" => $verid,
            "create" => $crear,
        ]);
    }

    public function tramitesAlcaldiaPenidentesajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'cedula',
            2 => 'telefono',
            3 => 'fecha_ingreso',
            4 => 'numtramite',
            5 => 'remitente',
            6 => 'asunto',
            7 => 'peticion',
            8 => 'direccion_atender',
            9 => 'fecha_fin',
            10 => 'recomendacion',
            11 => 'archivo',
            12 => 'acciones',
        );

        // $query = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
        //     ->join('users as u', 'cab.asignado_a', 'u.id')
        //     ->select('cab.*', 'u.name as asignado_a')->where([
        //         ["cab.estado", "=", "ACT"],
        //         ["cab.disposicion", "<>", "FINALIZADO"],
        //         // ["cab.disposicion", "<>", "RESERVADO"],
        //         // ["cab.estado_proceso", "<>", 3]
        //     // ])->whereNull('tipo'); // ["disposicion", "=", "PENDIENTE"], ["prioridad", "<>", null]
        //     ])->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
        //     ->where(function($q) {
        //         $q->where(["cab.creado_por" => Auth::user()->id])
        //         ->orWhereNull('cab.creado_por');
        //     });

        $query = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
            ->select('cab.*')->where([
                ["cab.estado", "=", "ACT"],
                ["cab.disposicion", "<>", "FINALIZADO"]
            ])->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
            ->where(function($q) {
                $q->where(["cab.creado_por" => Auth::user()->id])
                ->orWhereNull('cab.creado_por');
            });

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit == '-1')
            {
                $posts = $query->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit == '-1')
            {
                $posts = $query->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->where(function ($query) use ($search) {
                        $query->where('numtramite', 'LIKE', "%{$search}%")
                            ->orWhere('remitente', 'LIKE', "%{$search}%")
                            ->orWhere('asunto', 'LIKE', "%{$search}%");
                    })->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
            $totalFiltered = $query
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);

                $asignado = isset($post->asignado_a) ? User::find($post->asignado_a) : null;

                if (isset($tipo_tramite->id) && (intval($tipo_tramite->id) == 8 || intval($tipo_tramite->id) == 9)) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                        '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')"><i class="fa fa-trash"></i></a>
                     <div style="display: none;">
                     <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                         <input name="_token" type="hidden" value="' . csrf_token() . '">
                         <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                     </form>
                     </div>';
                } else if ($post->disposicion == 'PENDIENTE' || $post->disposicion == 'EN PROCESO') {//&& $post->estado_proceso == 0) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                        link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array($post->id), array('class' => 'fa fa-pencil-square-o')) . '&nbsp;';
                } else {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;&nbsp;' .
                        '&nbsp;&nbsp;';
                }

                $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->first();
                // dd($archivos);
                if ($archivo) {
                    $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                } else $adjunto = '';
                // $adjunto = '';
                // foreach ($archivos as $idx => $archivo)
                // {
                //     $adjunto .= "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                //     if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                // }

                $nestedData['id'] = $post->id;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor ?? '';
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $nestedData['asignado_a'] = isset($asignado) ? $asignado->name : '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['archivo'] = $adjunto;
                $nestedData['acciones'] = $aciones . "<a href='" . URL::to('') . "/tramites/recibo/".$post->id."' class='fa fa-print divpopup' target='_blank'></a>";
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function tramitesAlcaldiajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            14 => 'acciones',
        );

        $query = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'PENDIENTE', 'estado_proceso' => 0]);

        $tipo = [10, 11, 12, 13, 14];

        $sg = collect(User::ID_SECRETARIA_GENERAL)->contains(Auth::user()->id_perfil);
        if ($sg) {
            $query = TramAlcaldiaCabModel::where([
                'estado' => 'ACT',
                'disposicion' => 'PENDIENTE',
                'estado_proceso' => 0,
                'asignado_a' => Auth::user()->id
            ]);
            // $totalData = TramAlcaldiaCabModel::where([["estado", "ACT"], ["disposicion", "=", "PENDIENTE"], ["estado_proceso", "=", -1]])
            //     ->count();
            $totalData = TramAlcaldiaCabModel::where([
                'estado' => 'ACT',
                'disposicion' => 'PENDIENTE',
                'estado_proceso' => 0,
                'asignado_a' => Auth::user()->id
            ])->count();
            // $query->whereNotIn('tipo_tramite', $tipo);
        } else {
            $totalData = TramAlcaldiaCabModel::where([["estado", "ACT"], ["disposicion", "=", "PENDIENTE"], ["estado_proceso", "=", 0]])->count();
        }

        $usu = $this->getTipoUsuario();
        $id_perfil = $this->getTipoUsuarioByPerfil();
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);

        // if ($usu['tipo'] == self::ID_PERFIL_DIRECTOR && $id_perfil != self::ID_DESPACHO_ALCALDIA && $id_perfil != self::ID_PERFIL_ADMINISTRADOR)
        if ($esDirector)
        {
            $query->where("tram_peticiones_direccion_asignada.id_direccion", Auth::user()->id_direccion);
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $query->groupby('id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('remitente', 'LIKE', "%{$search}%")
                    ->orWhere('asunto', 'LIKE', "%{$search}%");
            })
                ->groupby('id')
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["disposicion", "=", "PENDIENTE"], ["estado_proceso", "=", 0]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();
        if (!empty($posts)) {
            if ($request->posicion == 'tramitesdespacho')
            {
                $estado = 1;
            }

            foreach ($posts as $post) {
                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);

                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "
                <a href='" . URL::to('tramites/despachartramite?id=' . $post->id . '&estado=' . $estado) . "'><i class='fa fa-pencil-square-o'> Verificar trámite</i></a>";

                $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->get();
                $adjunto = '';
                foreach ($archivos as $idx => $archivo) {
                    $adjunto .= "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                    if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                }

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor ?? '';
                $nestedData['telefono'] = $post->telefono;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function rechazadostramitesAlcaldiajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            14 => 'acciones',
        );

        if ($request->estado == 'negado') $estado = "NEGADA";
        else $estado = "APROBADA";

        $estado_proceso = $estado == "NEGADA" ? 4 : 0;
        if ($estado == 'APROBADA') {
            $query = TramAlcaldiaAsiganrModel::join("tram_peticiones_cab as pt", "pt.id", "=", "tram_peticiones_direccion_asignada.id_cab")
                ->select("pt.*")
                ->where([["pt.estado", "ACT"], ["pt.disposicion", "=", $estado], ["pt.estado_proceso", "=", $estado_proceso]]);
        } else {
            $query = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => $estado, 'estado_proceso' => $estado_proceso]);
        }

        $usu = $this->getTipoUsuario();
        $id_perfil = $this->getTipoUsuarioByPerfil();

        if ($usu['tipo'] == self::ID_PERFIL_DIRECTOR && $id_perfil != self::ID_DESPACHO_ALCALDIA && $id_perfil != self::ID_PERFIL_ADMINISTRADOR) {
            $query->where("tram_peticiones_direccion_asignada.id_direccion", Auth::user()->id_direccion);
        }

        $totalData = TramAlcaldiaCabModel::where([["estado", "ACT"], ["disposicion", "<>", "PENDIENTE"], ["estado_proceso", "=", 0]])->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $query->groupby('id')->offset($start)
                ->limit($limit)->orderBy($order, $dir)->get();
        } else {
            $search = $request->input('search.value');

            $posts = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('remitente', 'LIKE', "%{$search}%")
                    ->orWhere('asunto', 'LIKE', "%{$search}%");
            })->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["disposicion", "=", "NEGADO"], ["estado_proceso", "=", 0]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);
                // if ($id_perfil != self::ID_PERFIL_SECRETARIA) {
                if (!$ingresaTramites) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "
                    <a href='" . URL::to('tramites/despachartramite?id=' . $post->id . '&estado=' . $estado) . "'><i class='fa fa-pencil-square-o'></i></a>";
                } else {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "
                    <a href='" . URL::to('tramites/tramites/' . $post->id . '/edit') . "'><i class='fa fa-pencil-square-o'></i></a>";
                }

                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                if ($post->direccion_atender) {
                    $direc = str_replace('|', ',', $post->direccion_atender);
                    $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                    $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                } else $nestedData['direccion_atender'] = '';
                if ($post->direccion_informar = !null) {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                } else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function tramitesAlcaldiaDespachadosajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            6 => 'direccion_atender',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            14 => 'acciones',
        );

        $estado = $request->estado;
        $query = TramAlcaldiaCabModel::where(['estado' => 'ACT'])
            ->where(function ($q) {
                $q->where('estado_proceso', 1)
                    ->orWhere('estado_proceso', 0);
            })->whereNotNull(['asignado_a']);

        $usu = $this->getTipoUsuario();
        $perfil = Auth::user()->id_perfil;
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        $sg = collect(User::ID_SECRETARIA_GENERAL)->contains($perfil);
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);

        // if ($esDirector || Auth::user()->id_perfil == self::ANALISTA_DIRECTOR) {
        if ($esDirector || $this->analistaDirector())
        {
            if ($estado == 'ASIGNADO')
            {
                $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                    ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                    ->where(function ($q) {
                        $q->where('tram_peticiones_cab.estado_proceso', 1)
                            ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                    })->where(['da.id_direccion' => Auth::user()->id_direccion, 'tram_peticiones_cab.estado' => 'ACT', 'concejales' => 0])
                    ->where(function($q) use($estado) {
                        $q->whereIn('da.estado', [$estado, 'REVISION-COORDINADOR', 'DEVUELTO-COORDINADOR', 'DEVUELTO'])
                        ->orWhere(function($qr) {
                            $qr->where(['da.estado' => 'ANALISTA', 'da.id_direccion' => Auth::user()->id_direccion]);
                            // ->whereIn('da.direccion_solicitante', User::COORDINACIONES);
                        });
                    })->where('tipo', '<>', 'TRAMITES MUNICIPALES')
                    ->orderBy('tram_peticiones_cab.id_prioridad', 'ASC');

                if(Auth::user()->id_direccion == 21)
                {
                    $query = $query->whereNotNull('tram_peticiones_cab.correo_electronico');
                }

                $totalData = $query->count();
            }
            else
            {
                if(Auth::user()->id_direccion == 21 && $this->analistaDirector())
                {
                    $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                        ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                        ->where('tram_peticiones_cab.estado', 'ACT')
                        ->where(function ($q) {
                            $q->where('tram_peticiones_cab.estado_proceso', 1)
                                ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                        })->where(['da.id_direccion' => Auth::user()->id_direccion])
                        ->where(function ($q) use ($estado) {
                            $q->where('da.estado', $estado);
                        })
                        ->whereDoesntHave('analistas', function($q) {
                            $q->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO']);
                        })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])->orderBy('id_prioridad', 'ASC');

                    $totalData = $query->count();
                }
                else if(Auth::user()->id_direccion == 21 && $esDirector)
                {
                    if($estado == 'INFORME')
                    {
                        $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                        ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                        ->where('tram_peticiones_cab.estado', 'ACT')
                        ->where(function ($q) {
                            $q->where('tram_peticiones_cab.estado_proceso', 1)
                                ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                        })->where(['da.id_direccion' => Auth::user()->id_direccion])
                        // ->where(function ($q) use ($estado) {
                        //     $q->where('da.estado', $estado);
                        // })
                        ->whereIn('da.estado', [$estado, 'ANALISTA-INFORME', 'INFORME-RESPONDIDO', 'ANALISTA-INFORME-RESPONDIDO'])
                        ->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])->orderBy('id_prioridad', 'ASC');
                    }
                    else
                    {
                        $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                            ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                            ->where('tram_peticiones_cab.estado', 'ACT')
                            ->where(function ($q) {
                                $q->where('tram_peticiones_cab.estado_proceso', 1)
                                    ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                            })->where(['da.id_direccion' => Auth::user()->id_direccion])
                            ->where(function ($q) use ($estado) {
                                $q->where('da.estado', $estado);
                            })->whereHas('analistas', function($q) {
                                $q->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO']);
                            })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])->orderBy('id_prioridad', 'ASC');
                    }

                    $totalData = $query->count();
                }
                // else if(Auth::user()->id_direccion == 6)
                // {
                //     $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                //         ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                //         ->where('tram_peticiones_cab.estado', 'ACT')
                //         ->where(function ($q) {
                //             $q->where('tram_peticiones_cab.estado_proceso', 1)
                //                 ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                //         })->where(['da.id_direccion' => Auth::user()->id_direccion])
                //         ->where(function ($q) use ($estado) {
                //             $q->where('da.estado', $estado);
                //         })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
                //         ->orderBy('id_prioridad', 'ASC')->orderBy('created_at', 'DESC');

                //         $totalData = $query->count();
                // }
                else
                {
                    if($estado == 'INFORME')
                    {
                        $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                            ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                            ->where(['tram_peticiones_cab.estado' => 'ACT'])
                            ->whereIn('da.estado', [$estado, 'ANALISTA-INFORME', 'INFORME-RESPONDIDO', 'ANALISTA-INFORME-RESPONDIDO'])
                            ->whereIn('tram_peticiones_cab.estado_proceso', [0, 1])
                            ->whereIn('tram_peticiones_cab.tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
                            ->where(['da.id_direccion' => Auth::user()->id_direccion])
                            ->orderBy('id_prioridad', 'ASC');
                    }
                    else
                    {
                        $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                        ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                        ->where('tram_peticiones_cab.estado', 'ACT')
                        ->whereIn('tram_peticiones_cab.estado_proceso', [0, 1])
                        ->whereIn('tram_peticiones_cab.tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
                        ->whereIn('da.estado', [$estado, 'ANALISTA-CONTESTADO'])
                        ->where(['da.id_direccion' => Auth::user()->id_direccion])
                        ->where(function($q) {
                            // $q->whereIn('da.direccion_solicitante', User::COORDINACIONES)
                            $q->whereNull('da.direccion_solicitante');
                        })->orderBy('id_prioridad', 'ASC');
                    }

                    $totalData = $query->count();
                }

                $totalData = $query->count();
            }
        }
        else if (Auth::user()->id_perfil == 55)
        {
            if($estado == 'INFORME')
            {
                $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                    ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                    ->where('tram_peticiones_cab.estado', 'ACT')
                    ->whereIn('tram_peticiones_cab.estado_proceso', [0, 1])
                    ->whereIn('da.id_direccion', [76, 77])
                    ->where(['da.estado' => 'INFORME'])->orderBy('id_prioridad');
            }
            else
            {
                $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                    ->select('tram_peticiones_cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
                    ->where('tram_peticiones_cab.estado', 'ACT')
                    ->where(function ($q) {
                        $q->where('tram_peticiones_cab.estado_proceso', 1)
                            ->orWhere('tram_peticiones_cab.estado_proceso', 0);
                    })->where(['da.id_direccion' => Auth::user()->id, 'da.estado' => 'ASIGNADO'])->orderBy('id_prioridad');
            }

            $totalData = $query->count();
        }
        else if ($perfil == self::ID_DESPACHO_ALCALDIA || $sg || Auth::user()->id_perfil == 1)
        {
            $query->whereIn('disposicion', ['EN PROCESO', 'APROBADA'])->get();
            $totalData = $query->whereIn('disposicion', ['EN PROCESO', 'APROBADA'])->count();
        }
        // else if (User::esCoordinador())
        // {
        //     $query = TramAlcaldiaCabModel::join('tram_coordinador_asignado as coor', 'tram_peticiones_cab.id', 'coor.id_cab')
        //         ->join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
        //         ->select('tram_peticiones_cab.*', 'coor.id_direccion', 'da.respondido', 'da.estado as estado_asignacion')
        //         ->where(['coor.id_coordinador' => Auth::user()->id, 'tram_peticiones_cab.estado' => 'ACT'])
        //         ->where(function ($q) {
        //             $q->where(['tram_peticiones_cab.disposicion' => 'EN PROCESO'])
        //                 ->orWhere(['tram_peticiones_cab.disposicion' => 'APROBADA']);
        //         })->orderBy('id_prioridad');
        // }
        // else if ($perfil == self::ID_PERFIL_SECRETARIA)
        else if ($ingresaTramites)
        {
            $query = TramAlcaldiaCabModel::where(['disposicion' => 'REVISION', 'estado' => 'ACT']);
            $totalData = TramAlcaldiaCabModel::where(['disposicion' => 'REVISION', 'estado' => 'ACT'])->count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')))
        {
            if ($limit == '-1')
            {
                $posts = $query->limit($limit)->orderBy($order, $dir)->groupBy('tram_peticiones_cab.id')->get();
            }
            else
            {
                $posts = $query->offset($start)->limit($limit)->orderBy($order, $dir)->groupBy('tram_peticiones_cab.id')->get();
            }
        }
        else
        {
            $search = $request->input('search.value');

            if ($limit == '-1')
            {
                $posts = $query->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }

            $totalFiltered = TramAlcaldiaCabModel::where(["estado" => "ACT", "estado_proceso" => 0])
                ->where("disposicion", "<>", "PENDIENTE")
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();
        if (!empty($posts))
        {
            foreach ($posts as $post)
            {
                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);

                if(User::esCoordinador())
                {
                    $analistas_ = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
                    ->join('tmae_direcciones as dir', 'dira.id_direccion', 'dir.id')
                    ->where(['dira.id_cab' => $post->id, 'dira.direccion_solicitante' => Auth::user()->id_direccion])
                    ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
                    ->select('dir.direccion as name', 'dira.estado')->get();
                }
                else
                {
                    $analistas_ = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
                    ->join('users as u', 'dira.id_direccion', 'u.id')
                    ->where(['dira.id_cab' => $post->id, 'dira.direccion_solicitante' => Auth::user()->id_direccion])
                    ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
                    ->select('u.name', 'dira.estado')->get();
                }


                $analistas = $analistas_->map(function($item, $key) {
                    // $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                    // return "{$item->name} ({$estado})";
                    return "{$item->name}";
                })->implode(', ');

                $estado_analista = $analistas_->map(function($item, $key) {
                    $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                    return "{$estado}";
                })->implode(', ');

                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . '&nbsp;';

                $user = User::find(Auth::user()->id);
                if (!$sg || $user->id_perfil != self::ID_DESPACHO_ALCALDIA)
                {
                    $perfiles = collect(User::DIRECTORES);
                    $esDirector = $perfiles->contains(Auth::user()->id_perfil);
                    if ($esDirector || $this->analistaDirector())
                    {
                        $estado_asignacion = collect(['REVISION-COORDINADOR', 'DEVUELTO-COORDINADOR', 'ASIGNADO', 'PENDIENTE', 'INFORME', 'ANALISTA-INFORME'])
                            ->contains($post->estado_asignacion);
                        if ($estado_asignacion && ($post->id_direccion == Auth::user()->id_direccion))
                        {
                            $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "'><i class='fa fa-pencil-square-o'></i></a>"; // dd($aciones);
                        }
                        else if($post->estado_asignacion == 'ANALISTA')
                        {
                            $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . $post->id . '&estado='.Crypt::encrypt('ANALISTA')) . "'><i class='fa fa-pencil-square-o'></i></a>"; // dd($aciones);
                        }
                        else if (User::esCoordinador() && ($post->disposicion == 'EN PROCESO' || $post->disposicion == 'APROBADA'))
                        {
                            $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . Crypt::encrypt($post->id . '-' . $post->id_direccion)) . "'><i class='fa fa-pencil-square-o'></i></a>";
                        }
                    }
                    else if(User::esCoordinador() && ($post->disposicion == 'EN PROCESO' || $post->disposicion == 'APROBADA'))
                    {
                        $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . Crypt::encrypt($post->id . '-' . $post->id_direccion)) . "'><i class='fa fa-pencil-square-o'></i></a>";
                    // } else if ($perfil == self::ID_PERFIL_SECRETARIA) {
                    }
                    else if($ingresaTramites)
                    {
                        $aciones .= "<a href='" . URL::to('tramites/tramites/' . $post->id . '/edit') . "'><i class='fa fa-pencil-square-o'></i></a>";
                    }
                    else if(Auth::user()->id_perfil == 55 && $post->estado_asignacion == 'ASIGNADO')
                    {
                        $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "'><i class='fa fa-pencil-square-o'></i></a>"; // dd($aciones);
                    }
                    else if(Auth::user()->id_perfil == 55)
                    {
                        $aciones .= "<a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "'><i class='fa fa-pencil-square-o'></i></a>";
                    }

                    $deta = TramAlcaldiaCabDetaModel::where(['disposicion' => 'RESPONDIDO', 'id_tram_cab' => $post->id])->pluck('fecha_respuesta');
                    $fecha_res = $post->fecha_respuesta ? '' : $deta;
                }

                $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->first();
                $adjuntos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id, 'id_usuario' => Auth::user()->id])->get();

                // if($esDirector && Auth::user()->id_direccion == 21)
                // {
                //     $subdirectores = User::obtenerSubdirectores()->pluck('id');
                //     array_merge($subdirectores->toArray(), [Auth::user()->id]);

                //     $adjuntos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])
                //         ->whereIn('id_usuario', $subdirectores)->get();
                // }

                $respuesta = '';
                if ($archivo)
                {
                    $path = Str::contains($archivo->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                    $adjunto = "<a href='" . URL::to($path . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' type='button' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                }
                else $adjunto = '';

                if(count($adjuntos) > 0)
                {
                    foreach($adjuntos as $idx => $archivo)
                    {
                        $path = Str::contains($archivo->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                        $respuesta .= "<a href='" . URL::to($path . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                        if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                    }
                }

                $color = $post->prioridad == 'ALTA' ? 'danger' : ($post->prioridad == 'MEDIA' ? 'warning' : 'default');

                $nestedData['id'] = $post->id;
                $nestedData['cedula_remitente'] = $post->cedula_remitente;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor ?? '';
                $nestedData['telefono'] = $post->telefono;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = '<span class="label label-'.$color.'">'.$post->prioridad.'</span>';
                if (isset($post->direccion_atender) && $post->direccion_atender != '')
                {
                    $direc = str_replace('|', ',', $post->direccion_atender);
                    $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                    $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                }
                else $nestedData['direccion_atender'] = '';
                if (isset($post->direccion_informar) && $post->direccion_informar != '')
                {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    if(!empty($direc_inf))
                    {
                        $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                        $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                    }
                    else $nestedData['direccion_informar'] = '';
                }
                else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $fecha_res;
                $nestedData['archivo'] = $adjunto;
                $nestedData['archivo_finalizacion'] = $respuesta;
                $nestedData['analistas'] = $analistas;
                $nestedData['estado_analista'] = $estado_analista;
                if($estado == 'INFORME')
                {
                    $nestedData['estado'] = $post->estado_asignacion == 'ANALISTA-INFORME' || $post->estado_asignacion == 'INFORME' ? 'PENDIENTE' : 'CONTESTADO';
                }
                else $nestedData['estado'] = '';
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function getTipoUsuario()
    {
        $user = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo", "ap.id")->where("users.id", Auth::user()->id)->first();

        return $user;
    }

    public function getModulo()
    {
        $rucu = Route::getCurrentRoute()->getPrefix();
        $rucu = Route::getCurrentRoute()->uri;
        $vermodulo = explode("/", $rucu);

        return $vermodulo;
    }

    public function getDireccionByIDUser()
    {
        $user = User::join("tmae_direcciones as d", "d.id", "=", "users.id_direccion")
            ->select("d.id")->where("users.id", Auth::user()->id)->first();

        return $user;
    }


    public function getTipoUsuarioByPerfil()
    {
        $user = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.id")->where("users.id", Auth::user()->id)->first();

        return $user->id;
    }

    public function tramitesAlcaldiaDevueltosajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            6 => 'direccion_atender',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            14 => 'acciones',
        );

        $query = TramAlcaldiaAsiganrModel::join("tram_peticiones_cab as pt", "pt.id", "=", "tram_peticiones_direccion_asignada.id_cab")
            ->select("pt.*")->where([["pt.estado", "ACT"]]);

        $usu = $this->getTipoUsuario();
        $id_perfil = $this->getTipoUsuarioByPerfil();
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);

        if ($ingresaTramites)
        {
            $query->where("pt.estado_proceso", "=", 2);
        }
        else if ($esDirector && $id_perfil != self::ID_DESPACHO_ALCALDIA && $id_perfil != self::ID_PERFIL_ADMINISTRADOR) {
            $query->where([["tram_peticiones_direccion_asignada.id_direccion", Auth::user()->id_direccion], ['tram_peticiones_direccion_asignada.respondido', '=', 0], ['tram_peticiones_direccion_asignada.estado', '=', 'DEVUELTO']]);
        } else {
            $query->where('tram_peticiones_direccion_asignada.estado', '=', 'DEVUELTO');
        }

        $totalData = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 1]])->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $query->whereOr()->offset($start)->limit($limit)->orderBy($order, $dir)->get();
        } else {
            $search = $request->input('search.value');
            $posts = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 1]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 1]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();
        if (!empty($posts)) {
            //show($posts);
            foreach ($posts as $post) {
                $id_perfil = $this->getTipoUsuarioByPerfil();

                if ($id_perfil == self::ID_DESPACHO_ALCALDIA) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp
                    <a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "&revision=true'><i class='fa fa-pencil-square-o'></i></a>";
                } else if ($id_perfil == self::ID_PERFIL_DIRECTOR || $id_perfil == self::ID_PERFIL_DIRECTOR_TI) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp
                    <a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "'><i class='fa fa-pencil-square-o'></i></a>";
                    $this->configuraciongeneral[10] = 'tramites/tramitesdevueltos';
                } else {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp";
                }
                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $direc = str_replace('|', ',', $post->direccion_atender);
                $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                if ($post->direccion_informar = !null) {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                } else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['archivo_finalizacion'] = $archivo_finalizacion;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function tramitesAlcaldiaFinalizadosajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            6 => 'direccion_atender',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            13 => 'fecha_respuesta',
            14 => 'acciones',
        );

        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);
        $filtra = TramAlcaldiaAsiganrModel::select('id', DB::raw("COUNT(1) as total"))->where('respondido', '=', 1)->groupBy('id_cab')->get();
        $id = array();
        foreach ($filtra as $key => $value) {
            array_push($id, $value->id);
        }
        $query = TramAlcaldiaAsiganrModel::join("tram_peticiones_cab as pt", "pt.id", "=", "tram_peticiones_direccion_asignada.id_cab")
            ->select('pt.*')
            ->where(['pt.estado' => 'ACT'])
            ->whereIn('pt.tipo', ['VENTANILLA', 'SECRETARIA GENERAL']);

        $usu = $this->getTipoUsuario();
        $id_perfil = $this->getTipoUsuarioByPerfil();
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);

        if ($id_perfil == User::SECRETARIO_GENERAL || $ingresaTramites)
        {
            $query->where("pt.estado_proceso", "=", 2);
            $totalData = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO', 'estado_proceso' => 2])->count();
        }
        else if ($esDirector || $this->analistaDirector())
        {
            $query->where([
                'tram_peticiones_direccion_asignada.id_direccion' => Auth::user()->id_direccion,
                'pt.estado_proceso' => 2
            ]);
            $totalData = $query->count();
        }
        else
        {
            $query->where("pt.estado_proceso", "=", 2);
            $totalData = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'FINALIZADO', 'estado_proceso' => 2])->count();
        }

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            if ($limit == '-1')
            {
                $posts = $query->groupBy('pt.id')->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->groupBy('pt.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
        } else {
            $search = $request->input('search.value');

            if ($limit == '-1')
            {
                $posts = TramAlcaldiaCabModel::where([["estado", "=", "ACT"], ["estado_proceso", "=", 2]])
                    ->where(function ($query) use ($search) {
                        $query->where('numtramite', 'LIKE', "%{$search}%")
                            ->orWhere('remitente', 'LIKE', "%{$search}%")
                            ->orWhere('asunto', 'LIKE', "%{$search}%")
                            ->orWhere('id', 'LIKE', "%{$search}%");
                    })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
                    ->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = TramAlcaldiaCabModel::where([["estado", "=", "ACT"], ["estado_proceso", "=", 2]])
                    ->where(function ($query) use ($search) {
                        $query->where('numtramite', 'LIKE', "%{$search}%")
                            ->orWhere('remitente', 'LIKE', "%{$search}%")
                            ->orWhere('asunto', 'LIKE', "%{$search}%");
                    })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
                    ->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            }

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 2]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])->count();
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $id_perfil = $this->getTipoUsuarioByPerfil();
                $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp";
                if($ingresaTramites)
                {
                    // $aciones .= link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.edit', '', array($post->id), array('class' => 'fa fa-pencil-square-o')) . '&nbsp;&nbsp';
                }

                $archivoPeticion = ArchivosModel::from('tmov_archivos as ar')
                ->join('users as u', 'u.id', 'ar.id_usuario')
                ->where(['ar.tipo' => 10, 'ar.id_referencia' => $post->id])
                ->whereIn('u.id_perfil', self::ID_PERFIL_SECRETARIA)->first();

                if (isset($archivoPeticion->ruta))
                {
                    $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $archivoPeticion->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                }
                else $adjunto = '';

                if ($post->finalizar_por == 'GENERAL' || $post->finalizar_por == 'DESPACHO') {
                    $secretario = User::where(['id_perfil' => User::SECRETARIO_GENERAL, 'id_direccion' => 58])->first();
                    if($post->archivo_finalizacion)
                    {
                        $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                    }
                    else
                    {
                        $archivo_finalizacion = '';
                        $secretario = User::where(['id_perfil' => User::SECRETARIO_GENERAL, 'id_direccion' => 58])->first();
                        $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id, 'id_usuario' => $secretario->id])->get();

                        if(count($archivos) == 1)
                        {
                            $path = Str::contains($archivos[0]->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                            $archivo_finalizacion .= "<a href='" . URL::to($path . $archivos[0]->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                        }
                        else
                        {
                            foreach($archivos as $archivo)
                            {
                                $path = Str::contains($archivo->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                                $archivo_finalizacion .= "<a href='" . URL::to($path . $archivo->ruta) . "' class='btn btn-success btn-xs dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                            }
                        }
                    }
                } else {
                    // if($esDirector || Auth::user()->id_perfil == self::ANALISTA_DIRECTOR)
                    if($esDirector || $this->analistaDirector())
                    {
                        $archivo_finalizacion = '';

                        if(Auth::user()->id_perfil == User::SECRETARIO_GENERAL)
                        {
                            $archivos = ArchivosModel::from('tmov_archivos as ar')
                                ->join('users as u', 'ar.id_usuario', 'u.id')
                                ->select('ar.ruta')
                                ->where(['ar.tipo' => 10, 'ar.id_referencia' => $post->id])
                                ->whereNotIn('u.id_perfil', self::ID_PERFIL_SECRETARIA)->get();
                        }
                        else
                        {
                            if($this->analistaDirector())
                            {
                                $archivos = ArchivosModel::from('tmov_archivos as ar')
                                ->join('users as u', 'ar.id_usuario', 'u.id')->select('ar.ruta')
                                ->where(['ar.tipo' => 10, 'ar.id_referencia' => $post->id])
                                ->where(function($q) {
                                    // $q->WhereIn('u.id_perfil', self::ANALISTAS)
                                    $q->WhereIn('u.id_perfil', User::ANALISTAS)
                                    ->orWhereIn('u.id_perfil', User::DIRECTORES);
                                })->get();
                            }
                            else
                            {
                                $perfiles = array_merge(User::ANALISTAS, User::SOLO_DIRECTORES);
                                $archivos = ArchivosModel::from('tmov_archivos as ar')
                                ->join('users as u', 'ar.id_usuario', 'u.id')->select('ar.ruta')
                                ->where(['ar.tipo' => 10, 'ar.id_referencia' => $post->id])
                                ->where(function($q) {
                                    $q->WhereIn('u.id_perfil', User::ANALISTAS)
                                    ->orWhere(['ar.id_usuario' => Auth::user()->id]);
                                })->get(['ar.ruta']);

                                $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                                    ->join('users as u', 'ax.usuario_id', 'u.id')
                                    ->where(['tipo' => 11, 'id_tramite_externo' => $post->id])->get(['ax.ruta']);

                                if(count($archivos_) > 0)
                                {
                                    $archivos = array_merge($archivos->toArray(), $archivos_->toArray());
                                }
                            }
                        }

                        if(count($archivos) == 1)
                        {
                            $path = Str::contains($archivos[0]['ruta'], 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                            $archivo_finalizacion .= "<a href='" . URL::to($path . $archivos[0]['ruta']) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                        }
                        else
                        {
                            foreach($archivos as $archivo)
                            {
                                $path = Str::contains($archivo['ruta'], 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                                $archivo_finalizacion .= "<a href='" . URL::to($path . $archivo['ruta']) . "' class='btn btn-success btn-xs dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                            }
                        }
                    }
                    else
                    {
                        $archivo_finalizacion = '';
                        $archivos = ArchivosModel::from('tmov_archivos as ar')
                        ->join('users as u', 'ar.id_usuario', 'u.id')
                        ->select('ar.ruta')
                        ->where(['ar.tipo' => 10, 'ar.id_referencia' => $post->id])
                        // ->where('u.id_perfil', '<>', self::ID_PERFIL_SECRETARIA)->get();
                        ->whereNotIn('u.id_perfil', self::ID_PERFIL_SECRETARIA)->get();

                        if(count($archivos) == 1)
                        {
                            $path = Str::contains($archivos[0]->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                            $archivo_finalizacion .= "<a href='" . URL::to($path . $archivos[0]->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                        }
                        else
                        {
                            foreach($archivos as $archivo)
                            {
                                $path = Str::contains($archivo->ruta, 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                                $archivo_finalizacion .= "<a href='" . URL::to($path . $archivo->ruta) . "' class='btn btn-success btn-xs dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                            }
                        }
                        // $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $archivo->last()->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                    }
                }

                // foreach ($archivos as $idx => $archivo) {
                //     $adjunto .= "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                //     if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                // }

                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['tipo_tramite'] = isset($tipo_tramite->valor) ? $tipo_tramite->valor : '';
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $direc = str_replace('|', ',', $post->direccion_atender);
                if ($post->direccion_atender = !null && $post->direccion_atender = '') {
                    $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                    $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                } else $nestedData['direccion_atender'] = '';
                if ($post->direccion_informar = !null) {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                } else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['archivo_finalizacion'] = $archivo_finalizacion;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }


    public function tramitesAlcaldiaEjecutadosajax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            6 => 'direccion_atender',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            13 => 'fecha_respuesta',
            14 => 'acciones',
        );

        $query = TramAlcaldiaAsiganrModel::join("tram_peticiones_cab as pt", "pt.id", "=", "tram_peticiones_direccion_asignada.id_cab")->select("pt.*")
            ->where(['pt.estado' => 'ACT', 'tram_peticiones_direccion_asignada.estado' => 'CONTESTADO'])
            ->where(function ($q) {
                $q->where(['tram_peticiones_direccion_asignada.respondido' => 1])
                    ->orWhere(['tram_peticiones_direccion_asignada.respondido' => 2]);
            });

        $usu = $this->getTipoUsuario();
        $id_perfil = $this->getTipoUsuarioByPerfil();
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        $estado = request()->estado;
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);
        // if ($id_perfil == self::ID_PERFIL_SECRETARIA)
        if ($ingresaTramites)
        {
            $query->where("pt.estado_proceso", "=", 2);
            $totalData = $query->count();
            $totalFiltered = $totalData;
        }

        else if (($esDirector || $this->analistaDirector()) && $estado != 'SECRETARIO')
        {
            $query->where([["tram_peticiones_direccion_asignada.id_direccion", Auth::user()->id_direccion], ["pt.estado_proceso", "=", 1]]);
            $totalData = $query->count();
            $totalFiltered = $totalData;
        }
        else
        {
            if ($id_perfil == self::ID_DESPACHO_ALCALDIA)
            {
                $query->where(['pt.estado_proceso' => 1, 'finalizar_por' => 'DESPACHO']);
                $totalData = $query->count();
                $totalFiltered = $totalData;
            }
            elseif ($id_perfil == User::SECRETARIO_GENERAL || Auth::user()->id_perfil == 50 || Auth::user()->id_perfil)
            {
                $query = $query->where(['pt.estado_proceso' => 1, 'pt.obtener_respuesta' => 'SI'])
                ->whereNotNull('pt.correo_electronico')
                ->orWhere(function($q) {
                    $q->where(['tram_peticiones_direccion_asignada.id_direccion' => 1470])
                    ->whereNotNull('pt.correo_electronico');
                });
                $totalData = $query->count();
                $totalFiltered = $totalData;
            }
            else if(Auth::user()->id_perfil == 55)
            {
                $query->where([["tram_peticiones_direccion_asignada.id_direccion", Auth::user()->id], ["pt.estado_proceso", "=", 1]]);
                $totalData = $query->count();
                $totalFiltered = $totalData;
            }
            // dd($query->get());
        }

        // $totalData = TramAlcaldiaCabModel::where(["estado" => "ACT", "estado_proceso" => 1])->count();

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')))
        {
            if ($limit == '-1')
            {
                $posts = $query->groupBy('pt.id')->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->groupBy('pt.id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
        }
        else
        {
            $search = $request->input('search.value');

            if(Auth::user()->id_direccion == 21)
            {
                $posts = TramAlcaldiaCabModel::where(["estado" => "ACT", "estado_proceso" => 1])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->whereNotNull('correo_electronico')->groupBy('id')->offset($start)
                ->limit($limit)->orderBy($order, $dir)->get();
            }
            else if($estado == 'SECRETARIO')
            {
                $posts = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'estado_proceso' => 1, 'obtener_respuesta' => 'SI'])
                ->whereNotNull('correo_electronico')
                ->where(function ($q) use ($search) {
                    $q->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->groupBy('id')->offset($start)
                ->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = TramAlcaldiaCabModel::where(["estado" => "ACT", "estado_proceso" => 1])
                    ->where(function ($query) use ($search) {
                        $query->where('numtramite', 'LIKE', "%{$search}%")
                            ->orWhere('remitente', 'LIKE', "%{$search}%")
                            ->orWhere('asunto', 'LIKE', "%{$search}%");
                    })->groupBy('id')->offset($start)
                    ->limit($limit)->orderBy($order, $dir)->get();
            }

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 1]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->count();
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {
                $id_perfil = $this->getTipoUsuarioByPerfil();
                // if ($id_perfil == self::ID_DESPACHO_ALCALDIA || $id_perfil == self::ID_SECRETARIA_GENERAL) {
                if ($id_perfil == self::ID_DESPACHO_ALCALDIA || ($id_perfil == User::SECRETARIO_GENERAL && $estado != null)) {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp
                    <a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "&revision=true'><i class='fa fa-pencil-square-o'></i></a>";
                } else {
                    $aciones = link_to_route(str_replace("/", ".", $this->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp";
                }

                $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->first();
                // $adjunto = '';
                if(!$archivo) $adjunto = '';
                else $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";

                // foreach ($archivos as $idx => $archivo) {
                //     $adjunto .= "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                //     if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                // }
                $finalizacion = '';
                if($esDirector)
                {
                    $archivosContestacion = ArchivosModel::from('tmov_archivos as ar')
                    ->join('users as u', 'ar.id_usuario', 'u.id')
                    ->select('ar.ruta')->where([
                        'ar.tipo' => 10,
                        'ar.id_referencia' => $post->id,
                        // 'ar.id_usuario' => Auth::user()->id
                        'u.id_direccion' => Auth::user()->id_direccion
                    ])->get();

                    if(count($archivosContestacion) == 1)
                    {
                        $finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $archivosContestacion[0]->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                    }
                    else
                    {
                        foreach($archivosContestacion as $ar)
                        {
                            $finalizacion .= "<a href='" . URL::to('/archivos_sistema/' . $ar->ruta) . "' class='btn btn-success btn-xs dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";
                        }
                    }

                }

                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $direc = str_replace('|', ',', $post->direccion_atender);
                $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                if ($post->direccion_informar) {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                } else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['archivo_finalizacion'] = $finalizacion;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    // Parte de coordinador despacho alcaldia
    public function index3()
    {
        $objetos = json_decode($this->objetos);
        // dd($objetos);
        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[17]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        // unset($objetos[25]);
        unset($objetos[27]);
        unset($objetos[26]);
        // dd($objetos);

        $editfinalizartramite = "si";

        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) $editfinalizartramite = null;

        $configuraciongeneral = $this->configuraciongeneral;

        $user = User::find(Auth::user()->id);

        $configuraciongeneral[0] =  collect(User::ID_SECRETARIA_GENERAL)->contains($user->id_perfil) ? "Trámites Ingresados" : "Despacho Alcaldía";
        $configuraciongeneral[6] = "tramites/tramitesAlcaldiajax?posicion=tramitesdespacho";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => "no",
            "edit" => 'no',
            "verid" => null,
            "create" => "no",
            "editfinalizartramite" => $editfinalizartramite,
            "imprimir" => $imprimir,
        ]);
    }

    public function indexatendidos()
    {
        $objetos = json_decode($this->objetos);
        $tabla = [];
        $edit = 'no';
        $verid = null;
        $editfinalizartramite = "si";

        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[19]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);

        $rucu = Route::getCurrentRoute()->getPrefix();
        $rucu = Route::getCurrentRoute()->uri;
        $vermodulo = explode("/", $rucu);

        $configuraciongeneral = $this->configuraciongeneral;
        if ($vermodulo[1] === "tramitesdespachoatendidosnegados") {
            $configuraciongeneral[0] = "Trámites rechazados";
            $configuraciongeneral[6] = "tramites/rechazadostramitesAlcaldiajax?estado=negado";
        } else if ($vermodulo[1] === "tramitesdespachoatendidosaprobados") {
            $configuraciongeneral[0] = "Trámites aprobados";
            $configuraciongeneral[6] = "tramites/rechazadostramitesAlcaldiajax?estado=aprobada";
        }

        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => "no",
            "edit" => $edit,
            "verid" => $verid,
            "create" => "no",
            "editfinalizartramite" => $editfinalizartramite,
            "imprimir" => $imprimir,
        ]);
    }

    public function index2()
    {
        $objetos = json_decode($this->objetos);
        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        // unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[26]);
        unset($objetos[27]);
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);

        // if (self::ID_PERFIL_SECRETARIA == Auth::user()->id_perfil)
        if ($ingresaTramites)
        {
            unset($objetos[3]);
            unset($objetos[14]);
            unset($objetos[15]);
            unset($objetos[18]);
        }
        else
        {
            $analistas = '[{"Tipo":"text","Descripcion":"Analistas asignados","Nombre":"analistas","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
            $revision = json_decode($analistas);
            $objetos = array_merge($objetos, $revision);

            $estado = '[{"Tipo":"text","Descripcion":"Estado","Nombre":"estado_analista","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
            $revision = json_decode($estado);
            $objetos = array_merge($objetos, $revision);
        }

        $tabla = [];
        $edit = 'no';
        $verid = null;
        $editfinalizartramite = 'si';

        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $user = User::find(Auth::user()->id);
        $configuraciongeneral = $this->configuraciongeneral;
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        $configuraciongeneral[0] = collect(User::ID_SECRETARIA_GENERAL)->contains($user->id_perfil) || $user->id_perfil == self::ID_DESPACHO_ALCALDIA ? "Trámites Despachados" : "Trámites Asignados";
        // $configuraciongeneral[6] = $esDirector || Auth::user()->id_perfil == self::ANALISTA_DIRECTOR ? "tramites/tramitesDespachadosajax?estado=ASIGNADO" : "tramites/tramitesDespachadosajax?estado=COORDINADOR";
        $configuraciongeneral[6] = $esDirector || $this->analistaDirector() ? "tramites/tramitesAlcaldiaDespachadosajax?estado=ASIGNADO" : "tramites/tramitesAlcaldiaDespachadosajax?estado=COORDINADOR";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => "no",
            "edit" => $edit,
            "verid" => $verid,
            "create" => "no",
            "editfinalizartramite" => $editfinalizartramite,
            "imprimir" => $imprimir,
        ]);
    }

    public function indexDevueltos()
    {
        $objetos = json_decode($this->objetos);
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[10]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[20]);
        unset($objetos[21]);

        $tabla = [];
        $edit = 'no';
        $verid = null;

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0] = "Trámites devueltos";
        $configuraciongeneral[6] = "tramites/tramitesAlcaldiaDevueltosajax";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => $edit,
            "verid" => $verid,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function indexFinalizados()
    {
        $objetos = json_decode($this->objetos);
        $configuraciongeneral = $this->configuraciongeneral;
        $tabla = [];
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral[0] = "Trámites Finalizados";

        $edit = 'no';
        $verid = null;

        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);

        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[27]);

        // $objetos[27]->Descripcion = 'Adjunto analista';
        // $objetos[27]->Tipo = 'file';
        // $objetos[27]->Nombre = 'adjunto_analista';

        $configuraciongeneral[6] = "tramites/tramitesAlcaldiaFinalizadosajax";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => $edit,
            "verid" => $verid,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function indexRevision()
    {
        $objetos = json_decode($this->objetos);
        // dd($objetos);
        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);

        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);

        // unset($objetos[26]);
        unset($objetos[27]);
        $perfiles = collect(User::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        if($esDirector)
        {
            $objetos[26]->Descripcion = 'Contestación';
        }
        $tabla = [];
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0] = "Revisión de trámites en ejecución";
        $secretariaGeneral = collect(User::ID_SECRETARIA_GENERAL)->contains(Auth::user()->id_perfil);
        $configuraciongeneral[6] = Auth::user()->id_perfil == self::ID_DESPACHO_ALCALDIA || $secretariaGeneral ? "tramites/tramitesAlcaldiaEjecutadosajax" : "tramites/tramitesAlcaldiaDespachadosajax?estado=CONTESTADO";
        $imprimir = "si";
        // dd($objetos);

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function indexRevisionAtender()
    {
        $objetos = json_decode($this->objetos);
        // dd($objetos);
        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);

        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);

        unset($objetos[26]);
        unset($objetos[27]);

        $tabla = [];
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0] = "Trámites por responder";
        $configuraciongeneral[6] = "tramites/tramitesAlcaldiaEjecutadosajax?estado=SECRETARIO";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function revisionAnalista()
    {
        $esDirector = collect(User::SOLO_DIRECTORES)->contains(Auth::user()->id_perfil);
        // $analistaDirector = collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
        if($esDirector || Auth::user()->id_perfil == 67)
        {
            return redirect()->route('tramites.revisar');
        }

        $objetos = json_decode($this->objetos);
        // dd($objetos);
        unset($objetos[2]);
        unset($objetos[4]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        // unset($objetos[16]);
        unset($objetos[17]);

        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);

        unset($objetos[26]);
        unset($objetos[27]);

        $analistas = '[{"Tipo":"text","Descripcion":"Analistas asignados","Nombre":"analistas","Clase":"numeros-letras-guion mayuscula disabled","Valor":"Null","ValorAnterior" :"Null" }]';
        $revision = json_decode($analistas);
        $objetos = array_merge($objetos, $revision);

        $estado = '[{"Tipo":"text","Descripcion":"Estado","Nombre":"estado","Clase":"numeros-letras-guion mayuscula disabled","Valor":"Null","ValorAnterior" :"Null" }]';
        $revision = json_decode($estado);
        $objetos = array_merge($objetos, $revision);

        $tabla = [];
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[0] = "Trámites por revisar";
        $configuraciongeneral[6] = "tramites/tramitesAlcaldiaDespachadosajax?estado=PENDIENTE";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function tramitesRevisar()
    {
        $analistas = $this->obtenerAnalistas(Auth::user()->id_direccion);
        $this->configuraciongeneral[4] = 'Trámites por revisar';

        return view('tramites::tramitesexternos.revision', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'analistas' => $analistas,
            'estado' => 'PENDIENTE'
        ]);
    }

    public function tramitesRevisarAjax(Request $request)
    {
        $tramites = TramAlcaldiaCabModel::whereHas('asignados', function($q) {
            $q->where(['estado' => 'PENDIENTE', 'id_direccion' => Auth::user()->id_direccion]);
        })->where(['estado' => 'ACT'])->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
        ->with('area', 'tipoTramite')->orderBy('id_prioridad', 'ASC');

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
            return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
        })->editColumn('prioridad', function($tramite) {
            $color = $tramite->prioridad == 'ALTA' ? 'danger' : ($tramite->prioridad == 'MEDIA' ? 'warning' : 'default');
            $label = '<span class="label label-'.$color.'">'.$tramite->prioridad.'</span>';
            return $label;
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id])
            ->where('asig.estado', '<>', 'CON COPIA')->get();

            return $atender->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('copia', function($tramite) {
            $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();

            return $copia->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('analistas', function($tramite) {
            $direcciones = explode('|', $tramite->direccion_atender);
            $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['dira.id_cab' => $tramite->id])
            ->whereIn('dira.direccion_solicitante', $direcciones)
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado')->get();

            return $analistas->map(function($item, $key) {
                $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                return "{$item->name} ({$estado})";
            })->implode(', ');

        })->addColumn('adjunto', function($tramite) {
            $adjunto = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->first(['ruta']);

            if(isset($adjunto->ruta))
            {
                $archivos = '<a href="' . asset('archivos_sistema/' . $adjunto->ruta) . '" class="btn btn-success btn-sm dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            else $archivos = '';

            return $archivos;
        })->addColumn('action', function($tramite) {
            return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>
            <a href="' . route('tramite.editfinalizar') . "?id=$tramite->id" . '">
            <i class="fa fa-pencil-square-o"></i></a>';
        })->filter(function($query) use($request) {
            if($request->has('analista') && $request->analista != '' && $request->has('estado') && $request->estado != '')
            {
                $query->whereHas('asignados', function($q) use($request) {
                    $q->where(['id_direccion' => $request->analista])
                    ->where(['estado' => $request->estado]);
                });
            }
            else if($request->has('analista') && $request->analista != '' && $request->has('estado') && $request->estado != '' && $request->has('numtramite') && $request->numtramite != '')
            {
                $query->whereHas('asignados', function($q) use($request) {
                    $q->where(['id_direccion' => $request->analista])
                    ->where(['estado' => $request->estado]);
                })->where('numtramite', 'LIKE', "%$request->numtramite%");
            }
            else
            {
                if($request->has('analista') && $request->analista != '')
                {
                    $query->whereHas('asignados', function($q) use($request) {
                        $q->where(['id_direccion' => $request->analista]);
                    });
                }
                if($request->has('estado') && $request->estado != '')
                {
                    $query->whereHas('asignados', function($q) use($request) {
                        $q->where(['estado' => $request->estado]);
                    });
                }
                if ($request->has('numtramite') && $request->numtramite != '')
                {
                    $query->where('numtramite', 'LIKE', "%$request->numtramite%");
                }
            }
        })->rawColumns(['action', 'adjunto', 'prioridad'])->make(true);
    }

    // public function reporte($id)
    // {
    //     $tabla = TramAlcaldiaCabModel::where([["estado", "ACT"], ["id", $id]])->get();
    //     $tabladel = TramAlcaldiaDelModel::join("users as us", "us.id", "=", "tram_peticion_del.id_usuario")
    //         ->where([["tram_peticion_del.estado", "ACT"], ["id_tram_cab", $id]])->get();

    //     if (count($tabladel) == 0) {
    //         $tabladel = null;
    //     }

    //     return view('vistas.reporteshtml.reporte', [
    //         "tabla" => $tabla,
    //         "tabladel" => $tabladel,
    //     ]);
    // }

    public function obtener_ubicacion($direccion)
    {
        if (request()->ajax()) {
            $direccion .= ',Ecuador';
            $key = 'AIzaSyDy1ViaIr_ziZYThMeiNuvrZK7pUb7X_SI';
            $key = 'AIzaSyDaLTBGlDiHpfZi91deulcQRJDOyDS7HV8';
            $client = new GuzzleHttpClient();
            $consulta = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $direccion . '&key=' . $key);
            $respuesta = json_decode($consulta->getBody()->getContents());
            return response()->json($respuesta);
        } else abort(404);
    }

    public function consultar_cedula($cedula)
    {
        if (request()->ajax()) {
            try {

                $consulta = TramAlcaldiaCabModel::where(['cedula_remitente' => $cedula])->first();

                if($consulta)
                {
                    $respuesta = [
                        'id' => $consulta->id,
                        'cedula' => $consulta->cedula_remitente,
                        'name' => $consulta->remitente,
                        'email' => $consulta->correo_electronico,
                        'telefono' => $consulta->telefono,
                        'direccion' => $consulta->direccion,
                        'referencia' => $consulta->referencia,
                        'parroquia' => $consulta->id_parroquia,
                        'barrio' => $consulta->id_barrio
                    ];

                    return response()->json([
                        'ok' => true,
                        'message' => $respuesta
                    ]);
                }

                $paquete = strlen($cedula) == 10 ? '/1015' : '/1021';
                $client_1 = new GuzzleHttpClient(['verify' => false]);
                $consulta_1 = $client_1->request('GET', 'https://portalciudadano.manta.gob.ec/existeusuario/' . $cedula);
                // $consulta_1 = $client_1->request('GET', 'http://localhost/mantaentusmanos/public/existeusuario/' . $cedula);
                $respuesta = json_decode($consulta_1->getBody()->getContents());

                if ($respuesta->ok) {
                    return response()->json([
                        'ok' => true,
                        'message' => $respuesta->usuario,
                    ]);
                } else {
                    $client_2 = new GuzzleHttpClient(['verify' => false]);
                    $consulta_2 = $client_2->request('GET', 'webserver.manta.gob.ec:82/api/validacion/getFichaGeneral/' . $cedula . $paquete);
                    $respuesta = json_decode($consulta_2->getBody()->getContents());
                    if ($respuesta[0] == "01:LA FUENTE NO DEVOLVIÓ DATOS" || $respuesta[0] == "04:CEDULA INVALIDA") {
                        return response()->json([
                            'ok' => false,
                            'message' => 'Número de cédula no válida.',
                        ]);
                    }

                    return response()->json([
                        'ok' => true,
                        'message' => $respuesta[0],
                    ]);
                }
            } catch (\Exception $ex) {
                return response()->json([
                    'ok' => false,
                    'message' => 'Servicio no disponible por el momento, intente de nuevo más tarde. ' . $ex->getMessage() . ' - ' . $ex->getLine(),
                ]);
            }
        } else abort(404);
    }

    public function calcular_fecha_despachar($id_tipo_tramite, $fecha_inicial)
    {
        // if (request()->ajax())
        // {
            $ingreso = $fecha_inicial;
            $fecha_inicial = strtotime(date($fecha_inicial));
            $fechaFinal = null;

            if($id_tipo_tramite == 'OTROS')
            {
                $tipo_tramite = '';
                return response()->json([
                    'ok' => true,
                    'fecha_finalizacion' => date("Y-m-d"),
                    'fecha_ingreso' => date('Y-m-d', strtotime($ingreso)),
                    // 'despachar_en' => $n_dias . ' días',
                    'disabled' => 'NO'
                ]);
            }
            else if($id_tipo_tramite == 'EP')
            {
                $tipo_tramite = '';
            }
            else
            {
                $tipo_tramite = TipoTramiteModel::findOrFail($id_tipo_tramite);
            }
            // if($tipo_tramite->dias_despachar > 0)
            // {
                // $tipo_tramite = TipoTramiteModel::findOrFail($id_tipo_tramite);
                $n_dias = $tipo_tramite->dias_despachar > 0 ? $tipo_tramite->dias_despachar : 3;
                $segundos = 0;

                for ($i = 0; $i < $n_dias; $i++) {
                    $segundos += 86400;
                    $caduca = date('D', $fecha_inicial + $segundos);

                    if ($caduca == 'Sat' || $caduca == 'Sun') $i--;
                    else $fechaFinal = date("Y-m-d", $fecha_inicial + $segundos);
                }

                return response()->json([
                    'ok' => true,
                    'fecha_finalizacion' => $fechaFinal,
                    'fecha_ingreso' => date('Y-m-d', strtotime($ingreso)),
                    'despachar_en' => $n_dias . ' días',
                    'disabled' => $tipo_tramite->dias_despachar > 0 ? 'SI' : 'NO'
                ]);
            // }
            // else
            // {
            //     return response()->json([
            //         'ok' => false
            //     ]);
            // }
        // }
        // else abort(404);
    }

    public function create()
    {
        $this->configuraciongeneral[2] = "crear";
        $objetos = json_decode($this->objetos);
        $objetos[1]->Valor = date("Y-m-d");
        $objetos[18]->Valor = date("Y-m-d");
        $objetos[18]->Tipo = 'textdisabled2';
        $objetos[6]->Valor = $this->escoja + $this->tipo_documento;

        $objetos[0]->Tipo = 'numtramite';
        $objetos[0]->ValorAnterior = 'TE' . date('dmYHi');

        $prioridad = explodewords(ConfigSystem("prioridadtramites"), "|");
        $objetos[16]->Valor = $this->escoja + $prioridad;

        $parroquias = parroquiaModel::where('estado', 'ACT')
            ->whereNotIn('id', [8, 9])->orderBy('parroquia')->pluck('parroquia', 'id')->all() + [10 => 'Parroquia sin especificar'];
        $objetos[11]->Valor = $this->escoja + $parroquias;
        $objetos[12]->Valor = $this->escoja;

        $objetos[2]->Valor = $this->escoja + AreasModel::where('estado', 'ACT')
            ->orderBy('area')->pluck('area', 'id')->all();
        $objetos[3]->Valor = $this->escoja;

        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[18]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);
        unset($objetos[27]);

        $adjunto = '{"Tipo":"file","Descripcion":"Adjunto*","Nombre":"archivo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $adjunto = json_decode($adjunto);
        $objetos = array_merge($objetos, [$adjunto]);

        $js = '{"Tipo":"htmlplantilla","Descripcion":"Anexos","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }';
        $js = json_decode($js);
        $js->Valor = view('vistas.anexos');

        $objetos = array_merge($objetos, [$js]);

        return view('tramites::tramitesexternos.create', [
            "objetos" => $objetos,
            'labels_tramite' => true,
            'btnReservarTramite' => 'SI',
            "edit" => null,
            "scriptjs" => true,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
        ]);
    }

    public function store(Request $request)
    {
        return $this->guardar(0);
    }

    public function numTramite($tipo)
    {
        $hoy = date('dmYHi');
        $numtramite = $tipo . $hoy;
        $existe = TramAlcaldiaCabModel::where(['numtramite' => $numtramite])->first();

        if($existe)
        {
            $future = strtotime('now + 1 minute');
            $hoy = date('dmYHi', $future);
            $numtramite = $tipo . $hoy;
        }

        return $numtramite;
    }

    public function guardar($id)
    {
        $input = request()->all();
        $validator = Validator::make($input, TramAlcaldiaCabModel::rules($id));
        // dd(request()->all());
        $ruta = $this->configuraciongeneral[1];
        DB::beginTransaction();

        try {
            $input = request()->all();
            $arrapas = array();

            if ($id == 0)
            {
                $ruta .= "/create";
                $guardar = new TramAlcaldiaCabModel;
                $msg = "Registro Creado Exitosamente...!";
                $msgauditoria = "Registro de Comunicación";
                $existe = TramAlcaldiaCabModel::where([["estado", 'ACT'], ["numtramite", $input['numtramite']]])->first();
                $numero = TramAlcaldiaCabModel::orderBy("id", "desc")->first();
                // $sinletras = preg_replace("/[a-zA-Z-]-/", "", $numero['numtramite']);
                $msg2 = "Registro Creado Exitosamente...!";

                if ($validator->fails()) {
                    return Redirect::to("$ruta")->withErrors($validator)->withInput();
                } else {
                    if (isset($existe->id)) {
                        return Redirect::to("$ruta")->withErrors('El número de tramite ya existe')->withInput();
                    } else {
                        foreach ($input as $key => $value) {
                            if (
                                $key != "_method" && $key != "_token" && $key != "idusuario" && $key != "archivo" && $key != "anexos_json" && $key != 'id_usuario_externo'
                                && $key != 'ruta' && $key != 'archivos' && $key != 'ubicacion' && $key != 'direccion_evento' && $key != "archivo_anexo" && $key != 'creado_por'
                                && $key != 'fecha_inicio_evento' && $key != 'fecha_fin_evento' && $key != 'n_beneficiarios_evento' && $key != 'tipo_documento' && $key != 'tipo'
                                && $key != 'id_prioridad' && $key != 'concejales'
                            ) {
                                if ($key == 'numtramite') {
                                    $guardar->numtramite = $this->numTramite('TE');
                                    // if (empty($sinletras)) {
                                    //     $sinletras = 1;
                                    //     $guardar->numtramite = 'T' . date("mY") . '-000' . $sinletras;
                                    // } else {
                                    //     $sinletras = ($numero->id) + 1;
                                    //     $guardar->numtramite = 'T' . date("mY") . '-000' . $sinletras;
                                    // }
                                }
                                if ($key == 'direccion_atender') {
                                    $guardar->$key = implode('|', $value);
                                } else {
                                    $guardar->$key = $value;
                                }
                            }
                        }

                        $idAsignado = null;
                        $guardar->id_usuario = Auth::user()->id;
                        $guardar->creado_por = Auth::user()->id;
                        $guardar->id_prioridad = request()->prioridad == 'ALTA' ? 1 : (request()->prioridad == 'MEDIA' ? 2 : 3);
                        $guardar->ip = request()->ip();
                        $guardar->pc = request()->getHttpHost();

                        $idAsignado = $this->asignarUsuario();
                        $guardar->asignado_a = $idAsignado;
                        $guardar->save();
                        $this->guardarTimeline($guardar->id, $guardar, 'Solicitud ingresada satisfactoriamente');

                        $idcab = $guardar->id;

                        if (request()->has("direccion_atender")) {
                            $direcciones = request()->direccion_atender;
                            // TramAlcaldiaAsiganrModel::where("id_cab", $idcab)->delete();
                            foreach ($direcciones as $keydir => $valuedir) {
                                $guardares = TramAlcaldiaAsiganrModel::updateOrCreate(['id_cab' => $idcab], [
                                    'id_direccion' => $valuedir
                                ]);
                            }
                        }
                        /*ARCHIVO*/
                        if (request()->hasFile("archivo")) {
                            $dir = public_path() . '/archivos_sistema/';
                            $archivo = request()->file("archivo");
                            // dd($archivo);
                            if ($archivo) {
                                // $fileName = "tramite-$idcab-" . date("YmdHis") . "." . $archivo->getClientOriginalExtension(); //$archivo->getClientOriginalName();
                                $ext = $archivo->getClientOriginalExtension();
                                // $perfiles = explode("|", ConfigSystem("archivospermitidos"));
                                // if (!in_array($ext, $perfiles)) {
                                //     DB::rollback();
                                //     $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                                //     return redirect()->back()->withErrors([$mensaje])->withInput();
                                // }

                                $name = 'Archivo_Ref_' . $idcab . '_tipo_10_' . sha1(date('YmdHis') . Str::random(10));
                                $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();
                                $tipo_archivo = $archivo->getClientOriginalExtension();

                                if ($tipo_archivo == 'png' || $tipo_archivo == 'jpg') {
                                    Image::make($archivo)->resize(1124, null, function ($constraints) {
                                        $constraints->aspectRatio();
                                    })->save($this->archivos_path . '/' . $resize_name);
                                } else {
                                    $archivo->move($this->archivos_path, $resize_name);
                                }

                                $upload = new ArchivosModel();
                                $upload->tipo = 10;
                                $upload->id_usuario = Auth::user()->id;
                                $upload->id_referencia = $idcab;
                                $upload->nombre = basename($archivo->getClientOriginalName());
                                $upload->tipo_archivo = $archivo->getClientOriginalExtension();
                                $upload->ruta = $resize_name;
                                $upload->save();
                            }
                        }
                        if (request()->hasFile("archivo_anexo")) {
                            $archivos = $input['archivo_anexo'];
                            // dd($archivos);
                            $info = json_decode($input['anexos_json']);
                            foreach ($archivos as $key => $value) {
                                $archivo = $value;
                                $name = 'Archivo_Ref_' . $idcab . '_tipo_10_' . sha1(date('YmdHis') . Str::random(10));
                                $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();
                                $tipo_archivo = $archivo->getClientOriginalExtension();

                                if ($tipo_archivo == 'png' || $tipo_archivo == 'jpg') {
                                    Image::make($archivo)->resize(1124, null, function ($constraints) {
                                        $constraints->aspectRatio();
                                    })->save($this->archivos_path . '/' . $resize_name);
                                } else {
                                    $archivo->move($this->archivos_path, $resize_name);
                                }

                                $upload = new AnexosModel;
                                $upload->usuario_id = Auth::user()->id;
                                $upload->id_referencia = $idcab;
                                $upload->ruta = $resize_name;
                                $upload->nombre = basename($archivo->getClientOriginalName());
                                $upload->hojas = $info[$key]->hojas;
                                $upload->descripcion = $info[$key]->descripcion;
                                $upload->tipo = 10;
                                $upload->save();
                            }
                        }
                        // else return back()->withErrors(['Debe adjuntar un archivo.'])->withInput();
                        // $this->guardarTimeline($idcab, $guardar, 'Solicitud ingresada satisfactoriamente');

                        // $tipo_tramite = TipoTramiteModel::findOrFail($guardar->tipo_tramite);
                        if ($guardar->correo_electronico != null) {
                            $mensaje = 'Sr.(a) ' . $guardar->remitente . ' Su petición ha sido ingresada de manera satisfactoria, su número de trámite es: ' . $guardar->numtramite . '; será despachado a la brevedad posible.';
                            $this->Notificacion->EnviarEmailTram($guardar->correo_electronico, 'Trámite Ingresado', '', $mensaje);
                        }

                        if(isset($idAsignado))
                        {
                            $this->Notificacion->notificacionesweb('Nuevo trámite ingresado N° ' . $guardar->numtramite, 'tramites/despachartramite?id=' . $guardar->id . '&estado=1', $idAsignado, '2c438f');
                        }
                    }
                }
            }
            else if ($id > 0)
            {
                $ruta .= "/$id/edit";
                $guardar = TramAlcaldiaCabModel::find($id);
                $secretariaGeneral = collect(User::ID_SECRETARIA_GENERAL)->contains(Auth::user()->id_perfil);
                if ((Auth::user()->id_perfil == self::ID_DESPACHO_ALCALDIA || $secretariaGeneral) && request()->disposicion == 'EN PROCESO' && (!request()->direccion_atender && !request()->direccion_informar)) {
                    // return back()->withWarning('Debe ingresar las direciones a atender para este trámite.')->withinput();
                    return back()->withWarning('Debe ingresar las direciones a atender o con copia para este trámite.')->withinput();
                }
                // if(Auth::user()->id_perfil == self::ID_DESPACHO_ALCALDIA && $guardar->estado_proceso == 1 && $guardar->disposicion == 'EN PROCESO')
                if (($secretariaGeneral || Auth::user()->id_perfil == self::ID_DESPACHO_ALCALDIA) && $guardar->estado_proceso == 1 && $guardar->disposicion == 'APROBADA') {
                    if (!request()->archivo_finalizacion) {
                        return back()->withWarning('Debe subir el archivo de finalización para poder guardar el trámite.')->withinput();
                    }
                }
                $msg = "Registro Actualizado Exitosamente...!";
                $msg2 = "Registro Actualizado Exitosamente...!";
                $msgauditoria = "Edición Comunicación";

                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "idusuario" && $key != "archivo" && $key != 'ruta' && $key != 'id_prioridad'
                        && $key != 'tipo_documento' && $key != 'id_usuario_externo' && $key != 'creado_por' && $key != 'tipo' && $key != 'concejales')
                    {
                        if ($key == 'direccion_atender') $guardar->$key = implode('|', $value);
                        else if ($key == 'direccion_informar') $guardar->$key = implode('|', $value);
                        else $guardar->$key = $value;
                    }
                }

                if ($guardar->asignado_a == null) {
                    $idAsignado = $this->asignarUsuario();
                    $guardar->asignado_a = $idAsignado;
                }

                $guardar->id_usuario = Auth::user()->id;
                $guardar->ip = request()->ip();
                $guardar->pc = request()->getHttpHost();

                if(request()->has('prioridad'))
                {
                    $guardar->id_prioridad = request()->prioridad == 'ALTA' ? 1 : (request()->prioridad == 'MEDIA' ? 2 : 3);
                }

                $guardar->save();
                if ($guardar->disposicion == 'RESERVADO') $guardar->disposicion == 'EN PROCESO';
                $idcab = $guardar->id;

                /*ARCHIVO*/
                if (request()->hasFile("archivo")) {
                    $docs = request()->file("archivo");
                    $dir = public_path() . '/archivos_sistema/';

                    if ($docs) {
                        $fileName = "tramite-$idcab-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();

                        $file = TramAlcaldiaCabModel::find($idcab);
                        /*Tratamiento de Archivo Anterior*/
                        $oldfile = $dir . $file->archivo;
                        File::delete($oldfile);
                        $file->archivo = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                    }
                } else if (request()->hasFile("archivo_finalizacion")) {
                    $docs = request()->file("archivo_finalizacion");
                    $dir = public_path() . '/archivos_sistema/';

                    if ($docs) {
                        $fileName = "Finalizacion_tramite-$idcab-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
                        $ext = $docs->getClientOriginalExtension();
                        $perfiles = explode("|", ConfigSystem("archivospermitidos"));

                        if (!in_array($ext, $perfiles)) {
                            DB::rollback();
                            $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
                            return redirect()->back()->withErrors([$mensaje])->withInput();
                        }

                        $file = TramAlcaldiaCabModel::find($idcab);
                        /*Tratamiento de Archivo Anterior*/
                        $oldfile = $dir . $file->archivo_finalizacion;
                        File::delete($oldfile);
                        /**/
                        $file->archivo_finalizacion = $fileName;
                        $file->save();
                        $docs->move($dir, $fileName);
                    }
                }

                $direccion_atender = User::join('tmae_direcciones as dir', 'dir.id', 'users.id_direccion')
                    ->select('dir.id')->where('users.id', Auth::user()->id)->first();

                $idtl = $this->guardarTimeline($idcab, $guardar);
                if ($guardar->disposicion == 'EN PROCESO' && $guardar->estado_proceso == 0) //$guardar->estado_proceso != 2 && $guardar->estado_proceso != 1)
                { //se delega a director (es)
                    if (request()->has("direccion_atender"))
                    {
                        $direcciones = request()->direccion_atender;
                        $collect = collect($direcciones);
                        $ep_otros = $collect->contains(76) || $collect->contains(77);

                        if($ep_otros)
                        {
                            $usuario = User::where(['id_perfil' => 55])->first();
                            $guardares = new TramAlcaldiaAsiganrModel;
                            $guardares->id_cab = $idcab;
                            $guardares->id_direccion = $usuario->id;
                            $guardares->estado = 'ASIGNADO';
                            $guardares->save();

                            $cab = TramAlcaldiaCabModel::find($idcab);
                            $cab->direccion_atender = implode('|', $direcciones);
                            $cab->save();

                            $ruta = 'tramites/finalizartramite?id=' . $guardar->id;
                            $mensaje = 'Se le ha asignado el trámite N° ' . $guardar->numtramite;
                            $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $usuario->id, '2c438f');
                            $this->Notificacion->EnviarEmail($usuario->email, 'Trámite Asignado', '', $mensaje, $ruta,'vistas.emails.email');

                            foreach ($direcciones as $valuedir)
                            {
                                if($valuedir != 76 && $valuedir != 77)
                                {
                                    $guardares = new TramAlcaldiaAsiganrModel;
                                    $guardares->id_cab = $idcab;
                                    $guardares->id_direccion = $valuedir;
                                    $guardares->save();
                                }
                            }

                            $direcciones = array();
                            $direcciones = explode('|', $guardar->direccion_atender);
                            foreach ($direcciones as $key => $value)
                            {
                                $user = User::directores()->where('users.id_direccion', $value)->get();

                                foreach ($user as $key => $value)
                                {
                                    $ruta = 'tramites/finalizartramite?id=' . $guardar->id;
                                    $mensaje = 'Se le ha asignado el trámite N° ' . $guardar->numtramite;
                                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $value->id, '2c438f');
                                    $this->Notificacion->EnviarEmail($value->email, 'Trámite Asignado', '', $mensaje, $ruta,'vistas.emails.email');
                                }
                            }
                        }
                        else
                        {
                            $cab = TramAlcaldiaCabModel::find($idcab);
                            $cab->direccion_atender = implode('|', $direcciones);
                            $cab->save();

                            TramAlcaldiaAsiganrModel::where(["id_cab" => $idcab])
                            ->whereIn('estado', ['PENDIENTE', 'ASIGNADO'])->delete();

                            foreach ($direcciones as $keydir => $valuedir)
                            {
                                $guardares = TramAlcaldiaAsiganrModel::updateOrCreate([
                                    'id_cab' => $idcab,
                                    'id_direccion' => $valuedir,
                                    'estado' => 'PENDIENTE'
                                ]);
                            }

                            $direcciones = array();
                            $direcciones = explode('|', $guardar->direccion_atender);
                            foreach ($direcciones as $key => $value)
                            {
                                if($value == 6 || $value == 21)
                                {
                                    // $user = User::where(['id_perfil' => self::ANALISTA_DIRECTOR, 'id_direccion' => $value])->get();
                                    $perfilesAD = collect(User::ANALISTA_DIRECTOR);
                                    $user = User::where(['id_direccion' => $value])->whereIn('id_perfil', $perfilesAD)->get();
                                }
                                else
                                {
                                    $user = User::directores()->where('users.id_direccion', $value)->get();
                                }
                                // $user = User::directores()->where('users.id_direccion', $value)->get();

                                foreach ($user as $key => $value)
                                {
                                    $ruta = 'tramites/finalizartramite?id=' . $guardar->id;
                                    $mensaje = 'Se le ha asignado el trámite N° ' . $guardar->numtramite;
                                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $value->id, '2c438f');
                                    $this->Notificacion->EnviarEmail($value->email, 'Trámite Asignado', '', $mensaje, $ruta,'vistas.emails.email');
                                }
                            }
                        }
                    }
                    if (request()->has("direccion_informar"))
                    {
                        $informar = request()->direccion_informar;
                        $cab = TramAlcaldiaCabModel::find($idcab);
                        $cab->direccion_informar = implode('|', $informar);
                        $cab->save();

                        $direcciones_informar = array();
                        $direcciones_informar = explode('|', $guardar->direccion_informar);
                        TramAlcaldiaAsiganrModel::where(["id_cab" => $idcab, 'estado' => 'CON COPIA'])->delete();
                        foreach ($direcciones_informar as $di)
                        {
                            $copia = TramAlcaldiaAsiganrModel::updateOrCreate([
                                'id_cab' => $idcab,
                                'id_direccion' => $di,
                                'estado' => 'CON COPIA'
                            ]);

                            $directores = User::directores()->where('users.id_direccion', $di)->get();

                            foreach ($directores as $key => $director)
                            {
                                $ruta = 'tramites/finalizartramite?id=' . $guardar->id;
                                $mensaje = 'Para su información se le ha asignado el trámite N° ' . $guardar->numtramite;
                                $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                                $this->Notificacion->EnviarEmail($director->email, 'Trámite Asignado', '', $mensaje, $ruta,'vistas.emails.email');
                            }
                        }
                    }

                    $timeline = TramAlcaldiaCabDetaModel::find($idtl);
                    $nombreDirecciones = '';

                    if(isset($guardar->direccion_atender))
                    {
                        $direc = str_replace('|', ',', $guardar->direccion_atender);
                        $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR " - ") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                        $timeline->observacion = 'Trámite asignado a dirección respectiva. ' . $direciones_text[0]->direccion_atender;
                        $nombreDirecciones = $direciones_text[0]->direccion_atender;
                    }
                    else
                    {
                        $direc = str_replace('|', ',', $guardar->direccion_informar);
                        $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR " - ") as direccion_informar from tmae_direcciones where id in (' . $direc . ') ');
                        $timeline->observacion = 'Trámite asignado a dirección respectiva. CC: ' . $direciones_text[0]->direccion_informar;
                        $nombreDirecciones = $direciones_text[0]->direccion_informar;
                    }

                    $tipo_tramite = TipoTramiteModel::findOrFail($guardar->tipo_tramite);
                    // $timeline->recomendacion = 'Trámite asignado a dirección respectiva';
                    $timeline->save();
                    $txt = '';
                    $direcciones = array();
                    $direcciones = explode('|', $guardar->direccion_atender);

                    if(count($direcciones) > 0)
                    {
                        $txt = count($direcciones) > 1 ? 'las siguentes direcciones: ' : 'la siguiente dirección: ';
                    }
                    else
                    {
                        $txt = count($direcciones_informar) > 1 ? 'las siguentes direcciones: ' : 'la siguiente dirección: ';
                    }

                    if ($guardar->correo_electronico) {
                        $dias = $tipo_tramite->dias_despachar > 1 ? ' días laborables.' : ' día laborable.';
                        $mensaje = 'Sr.(a) ' . $guardar->remitente . ' su trámite N° ' . $guardar->numtramite . ' ha sido asignado a  ' . $txt . $nombreDirecciones
                            . '. La fecha máxima de atención es de ' . $tipo_tramite->dias_despachar . $dias;
                        $this->Notificacion->EnviarEmailTram($guardar->correo_electronico, 'Trámite Asignado', '', $mensaje);
                    }
                }
                else if ($guardar->disposicion == 'PENDIENTE' && $guardar->estado_proceso == 4)
                {
                    TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                        ->update(['disposicion' => 'PENDIENTE', 'estado_proceso' => 0]);

                    $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("users.id");
                    $tipo_tra = intval($guardar->tipo_tramite);

                    if ($tipo_tra === 10 || $tipo_tra === 11 || $tipo_tra === 12 || $tipo_tra === 13 || $tipo_tra === 14) {
                        $usuariosByperfil = $usuariosByperfil->where("ap.id", self::ID_DESPACHO_ALCALDIA)->get();
                    } else {
                        $usuariosByperfil = $usuariosByperfil->whereIn("ap.id", User::ID_SECRETARIA_GENERAL)->get();
                    }

                    foreach ($usuariosByperfil as $key => $value) {
                        $this->Notificacion->notificacionesweb('Se ha corregido el trámite N° ' . $guardar->numtramite, 'tramites/despachartramite?id=' . $guardar->id . '&estado=1', $value->id, '2c438f');
                    }
                } else if ($guardar->disposicion == 'NEGADA') //se devuelve el trámite a quien lo creó
                {
                    TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                        ->update(['estado_proceso' => 4]);
                    $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("users.id")
                        ->whereIn("ap.id", self::ID_PERFIL_SECRETARIA)->get();

                    foreach ($usuariosByperfil as $key => $value) {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido negado', 'tramites/tramites/' . $guardar->id . '/edit', $value->id, '2c438f');
                    }
                    // $timeline->recomendacion = 'Trámite negado';
                } else if ($guardar->estado_proceso == 1) // El director responde a secretaria general
                {
                    $delegados = TramAlcaldiaAsiganrModel::where('id_cab', $guardar->id)->get();

                    $id_direccion = $this->getDireccionByIDUser();
                    $direccionActual = TramAlcaldiaAsiganrModel::where(["id_cab" => $guardar->id, 'id_direccion' => $id_direccion->id])->first();
                    if(!$direccionActual) $direccionActual = TramAlcaldiaAsiganrModel::where(["id_cab" => $guardar->id, 'id_direccion' => Auth::user()->id])->first();

                    if ($direccionActual->estado == 'PENDIENTE')
                    {
                        $direccionActual->estado = 'ASIGNADO';
                        $direccionActual->observacion = $guardar->observacion;
                        $direccionActual->save();

                        // if((Auth::user()->id_direccion == 6 || Auth::user()->id_direccion == 21) && Auth::user()->id_perfil == self::ANALISTA_DIRECTOR)
                        if((Auth::user()->id_direccion == 6 || Auth::user()->id_direccion == 21) && $this->analistaDirector())
                        {
                            $director = User::where(['id_direccion' => Auth::user()->id_direccion])->whereIn('id_perfil', User::DIRECTORES)->get();

                            foreach($director as $dir)
                            {
                                $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' está disponible para su revisión', 'tramites/finalizartramite?id=' . $guardar->id, $dir->id, '2c438f');
                            }
                        }
                    }
                    else
                    {
                        $usuario = Auth::user()->id;
                        $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $guardar->id])
                            ->where('id_usuario', $usuario)->get(['id', 'id_usuario', 'ruta']);

                        $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                        ->join('users as u', 'ax.usuario_id', 'u.id')
                        ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                        ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario])->get();

                        $archivo = array_merge($archivo->toArray(), $archivos_->toArray());
                        if(count($archivo) == 0)
                        {
                            return back()->withErrors(['No se puede guardar el trámite hasta que suba su respuesta'])->withInput();
                        }

                        TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                            ->update(['disposicion' => 'APROBADA']);

                        $direccionActual->respondido = 1;
                        $direccionActual->estado = 'CONTESTADO';
                        $direccionActual->observacion = $guardar->observacion;
                        $direccionActual->save();

                        $notifica = TramAlcaldiaAsiganrModel::Select(DB::raw("COUNT(*) as total"))
                            ->where([["id_cab", $guardar->id], ['respondido', '=', 0]])->first();

                        if ($notifica->total == 0)
                        {
                            $secretario = User::where(['id_perfil' => User::SECRETARIO_GENERAL, 'id_direccion' => 58])->first();

                            $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' se encuentra disponible para su revisión', 'tramites/finalizartramite?id=' . $guardar->id . '&revision=true', $secretario->id, '2c438f');

                            // $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("users.id")->where("ap.id", self::ID_SECRETARIA_GENERAL)->get();
                            // foreach ($usuariosByperfil as $key => $value) {
                            //     $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' se encuentra disponible para su revisión', 'tramites/finalizartramite?id=' . $guardar->id . '&revision=true', $value->id, '2c438f');
                            // }

                            if ($guardar->correo_electronico) {
                                $mensaje = 'Sr.(a) ' . $guardar->remitente . ' su trámite N° ' . $guardar->numtramite . ' se encuentra en proceso de gestión.';
                                $this->Notificacion->EnviarEmailTram($guardar->correo_electronico, 'Trámite en Proceso de Gestión', '', $mensaje);
                            }
                        }

                        $timeline = TramAlcaldiaCabDetaModel::find($idtl);
                        $timeline->recomendacion = 'Trámite en proceso de gestión';
                        $timeline->observacion = 'Trámite en proceso de gestión';
                        $timeline->disposicion = 'RESPONDIDO';
                        $timeline->fecha_respuesta = now();
                        $timeline->save();

                        $direcciones_id = explode('|', $guardar->direccion_atender);

                        $directores = User::whereIn('id_direccion', $direcciones_id)->where(function ($q) {
                            $q->where('id_perfil', self::ID_PERFIL_DIRECTOR)
                                ->orWhere('id_perfil', self::ID_PERFIL_DIRECTOR_TI)
                                ->orWhere('id_perfil', self::ID_PERFIL_DIRECTOR_OOPP)
                                ->orWhere('id_perfil', self::ID_PERFIL_DIRECTOR_PLAN);
                        })->get();
                    }
                    // ->update(['respondido' => 1, 'observacion' => $guardar->observacion])
                } else if ($guardar->estado_proceso == 2) //secretaria general finaliza el trámite
                {
                    $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $guardar->id, 'id_usuario' => Auth::user()->id])->get();
                    $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                        ->join('users as u', 'ax.usuario_id', 'u.id')
                        ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                        ->where(['tipo' => 11, 'id_tramite_externo' => $guardar->id, 'usuario_id' => Auth::user()->id])->get();
                    $archivo = array_merge($archivos->toArray(), $archivos_->toArray());

                    if(count($archivos) == 0)
                    {
                        return back()->withErrors(['Debe subir al menos un archivo para poder finalizar el trámite'])->withInput();
                    }

                    TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                        ->update(['disposicion' => 'FINALIZADO', 'fecha_respuesta' => now()]);

                    $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                        ->select("users.id")->whereIn("ap.id", User::ID_SECRETARIA_GENERAL)->get();

                    $ventanilla = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                        ->select("users.id")->where("ap.id", self::ID_PERFIL_SECRETARIA)->get();

                    foreach ($ventanilla as $key => $venta) {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
                    }
                    foreach ($usuariosByperfil as $key => $value) {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
                    }

                    $timeline = TramAlcaldiaCabDetaModel::find($idtl);
                    $timeline->recomendacion = 'Trámite finalizado';
                    $timeline->observacion = 'Trámite finalizado';
                    $timeline->disposicion = 'FINALIZADO';
                    $timeline->save();

                    $tram = TramAlcaldiaCabModel::find($id);
                    $mensajeCiudadano = 'Sr.(a) ' . $guardar->remitente . ' su trámite N° ' . $tram->numtramite . ' ha sido finalizado, gracias por la espera';
                    $pdf = public_path() . '/archivos_sistema/' . $tram->archivo_finalizacion;

                    $direcciones_id = explode('|', $tram->direccion_atender);
                    $direcciones_informar = $tram->direccion_informar ? explode('|', $tram->direccion_informar) : null;
                    $direcciones_informe = TramAlcaldiaAsiganrModel::where(['id_cab' => $tram->id, 'estado' => 'INFORME-RESPONDIDO'])->get();

                    if ($direcciones_informar != null) {
                        $direcciones_id = array_merge($direcciones_id, $direcciones_informar);
                    }
                    if (count($direcciones_informe) >= 1) {
                        $array_dirIn = array();
                        foreach ($direcciones_informe as $dirIn) {
                            $direcciones_id = array_merge($direcciones_id, [$dirIn->id_direccion]);
                            // $array_dirIn = array_push($array_dirIn, $dirIn);
                        }
                    }

                    $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
                    $mensaje = 'El trámite número ' . $tram->numtramite . ' ha finalizado.';
                    $ruta = 'tramites/tramitesfinalizados';

                    foreach ($directores as $key => $value) {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', $ruta, $value->id, '2c438f');
                        $this->Notificacion->EnviarEmail($value->email, 'Trámite Finalizado', '', $mensaje, $ruta,'vistas.emails.email');
                    }

                    $correoGestor = ConfigSystem('correo_gestor');
                    $this->Notificacion->EnviarEmailTramiteFinArray($correoGestor, 'Trámite Finalizado', '', $mensajeCiudadano, $archivos);
                    if ($guardar->correo_electronico != null) {
                        // $this->Notificacion->EnviarEmailTramiteFin($guardar->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $pdf);
                        $this->Notificacion->EnviarEmailTramiteFinArray($guardar->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $archivos);
                    }
                } else if ($guardar->estado_proceso == 3) // despacho devuelve a director
                {
                    TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                        ->update(['disposicion' => 'DEVUELTO']);

                    $direcciones = array();
                    $direcciones = explode('|', $guardar->direccion_atender);

                    foreach ($direcciones as $key => $value) {
                        TramAlcaldiaAsiganrModel::where([["id_cab", $guardar->id], ['id_direccion', $value]])
                            ->update(['respondido' => 0]);

                        $user = User::directores()->where('users.id_direccion', $value)->get();

                        foreach ($user as $key => $value) {
                            $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido devuelto', 'tramites/tramitesdevueltos', $value->id, '2c438f');
                        }
                    }
                }
            }
            DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            $mensaje = $e->getMessage() . ' - ' . $e->getLine();
            return redirect()->back()->withErrors([$mensaje])->withInput();
        }

        if ($msg != $msg2) Session::flash('message', $msg2);
        else Session::flash('message', $msg);

        if ($guardar->disposicion == 'PENDIENTE' || $guardar->disposicion == 'NEGADA' || $guardar->disposicion == 'APROBADA') {
            return Redirect::to('tramites/tramitesrespondidos');
        } else if ($guardar->estado_proceso == 1) {
            return Redirect::to('tramites/tramitesrespondidos');
        } else if ($guardar->estado_proceso == 2 || $guardar->estado_proceso == 3 || $guardar->disposicion == 'EN PROCESO') {
            return Redirect::to($input['ruta']);
        } else {
            if ($id == 0) {
                return Redirect::to($this->configuraciongeneral[1] . '?id=' . $idcab);
            } else {
                return Redirect::to($this->configuraciongeneral[1]);
            }
        }
    }

    public function asignarUsuario()
    {
        $horaHoy = date('H:i');
        $hoy = date('Y-m-d');

        $diaInicio = date('Y-m-d', strtotime('2020-11-09'));
        $horaInicio = date('H:i', strtotime('16:00'));
        $horaFin = date('H:i', strtotime('20:00'));

        if($hoy == $diaInicio && $horaHoy >= $horaInicio && $horaHoy <= $horaFin)
        {
            //MARIA MENDOZA 1473
            //PATRICIA GONZALEZ 1469
            return 1473;
        }

        $tramitesAsignados = TramAlcaldiaCabModel::where(['estado' => 'ACT'])->whereNotNull('asignado_a')->count();
        $totalUsuarios = User::whereIn('id_perfil', User::ID_SECRETARIA_GENERAL)->count();

        if ($tramitesAsignados > $totalUsuarios) {
            $contador = $tramitesAsignados / $totalUsuarios;
        } else $contador = 1;

        $contador = round($contador, PHP_ROUND_HALF_DOWN);

        $usuariosAsignar = User::whereIn('id_perfil', User::ID_SECRETARIA_GENERAL)
        ->select('id', 'name')->orderBy('id')->get();
        $count = count($usuariosAsignar);

        $tramite = TramAlcaldiaCabModel::where(['estado' => 'ACT'])->whereNotNull('asignado_a')->get();
        $ultimo = $tramite->last();

        $asignado = null;

        foreach ($usuariosAsignar as $key => $usuario) {
            if($usuario->id == $ultimo->asignado_a)
            {
                if($key + 1 < $count)
                {
                    $asignado = $usuariosAsignar[$key + 1]->id;
                }
                else
                {
                    $asignado = $usuariosAsignar[0]->id;
                }
            }
            // $tramites = TramAlcaldiaCabModel::where(['asignado_a' => $usuario->id])->count();
            // if ($tramites < $contador) {
            //     $asignado = $usuario->id;
            //     break;
            // }
        }
        if($asignado == null) $asignado = $usuariosAsignar[0]->id;

        // return $asignado == null ? $usuariosAsignar[0]->id : $asignado;
        // return $asignado ?? $usuariosAsignar[0]->id;
        return $asignado;
    }

    public function show($id)
    {
        $objetos = json_decode($this->objetos);

        $tabla = TipoTramiteModel::join('tram_peticiones_cab as cab', 'cab.tipo_tramite', 'tram_peticiones_tipo_tramite.id')
            ->join('parroquia as pa', 'pa.id', 'cab.id_parroquia')
            ->join('barrio as ba', 'ba.id', 'cab.id_barrio')
            ->join('tram_peticiones_areas as area', 'cab.id_area', 'area.id')
            ->select('cab.numtramite', 'cab.id_usuario_externo', 'cab.fecha_ingreso', 'area.area as id_area',
                'tram_peticiones_tipo_tramite.valor as tipo_tramite', 'cab.asunto', 'cab.id_area as area',
                'cab.peticion', 'cab.cedula_remitente', 'cab.remitente', 'cab.telefono', 'cab.telefono_2',
                'cab.tipo', 'cab.correo_electronico', 'cab.direccion', 'cab.referencia', 'cab.direccion_informar',
                'cab.direccion_atender', 'cab.prioridad', 'cab.fecha_fin as fecha_fin', 'cab.disposicion',
                'cab.obtener_respuesta', 'cab.id', 'ba.barrio as id_barrio', 'pa.parroquia as id_parroquia'
            )->where(['cab.estado' => 'ACT', 'cab.id' => $id])->first();

        if ($tabla == null)
        {
            $tabla = TipoTramiteModel::join('tram_peticiones_cab as cab', 'cab.tipo_tramite', 'tram_peticiones_tipo_tramite.id')
                ->join('tmo_canton as can', 'can.id', 'cab.id_barrio')
                ->join('tram_peticiones_areas as area', 'cab.id_area', 'area.id')
                ->select('cab.numtramite', 'cab.fecha_ingreso', 'area.area as id_area', 'cab.id_usuario_externo',
                    'tram_peticiones_tipo_tramite.valor as tipo_tramite', 'cab.asunto',
                    'cab.obtener_respuesta', 'cab.peticion', 'cab.remitente','cab.cedula_remitente', 'cab.telefono',
                    'cab.tipo', 'cab.telefono_2', 'cab.correo_electronico', 'cab.direccion', 'cab.referencia',
                    'cab.direccion_informar','cab.direccion_atender','cab.prioridad', 'cab.id_area as area',
                    'cab.fecha_fin as fecha_fin','cab.disposicion','cab.id','can.nombre_canton as id_barrio'
                )->where(['cab.estado' => 'ACT', 'cab.id' => $id])->first();

            if($tabla)
            {
                $tabla->id_parroquia = 'Sin especificar';
            }
            else $tabla = TramAlcaldiaCabModel::find($id);
            $objetos[12]->Descripcion = 'Ciudad*';
        }

        unset($objetos[6]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);
        unset($objetos[27]);

        if($tabla->tipo != 'TRAMITES MUNICIPALES')
        {
            $notificado = '[{"Tipo":"text","Descripcion":"Notificación","Nombre":"notificado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
            $notificado = json_decode($notificado);
            $objetos = array_merge($objetos, $notificado);

            $respuesta = $tabla->obtener_respuesta == 'NO' ? 'LA RESPUESTA VA DIRECTAMENTE AL CIUDADANO - ' : 'LA RESPUESTA VA A SECRETARÍA GENERAL - ';
            // if (count($tabla2) == 0) $tabla2 = null;
            $tabla->notificado = isset($tabla->correo_electronico) ? $respuesta . 'NOTIFICADO MEDIANTE CORREO ELECTRÓNICO'
            : (isset($tabla->area) && $tabla->area == 32 ? $respuesta . 'NOTIFICADO DE FORMA FÍSICA' : $respuesta . 'NOTIFICADO MEDIANTE SECRETARÍA GENERAL');
        }

        $objetos[7]->Descripcion = strlen($tabla->cedula_remitente) == 10 ? 'Cédula*' : 'RUC*';
        $objetos[8]->Descripcion = strlen($tabla->cedula_remitente) == 10 ? 'Remitente*' : 'razón Social*';

        $referencia = '[{"Tipo":"text","Descripcion":"Referencia","Nombre":"referencia","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" }]';
        $referencia = json_decode($referencia);
        $objetos = array_merge($objetos, $referencia);

        $telefono_2 = '[{"Tipo":"text","Descripcion":"Número telefónico 2","Nombre":"telefono_2","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" }]';
        $telefono_2 = json_decode($telefono_2);

        if ($tabla->telefono_2 != null) {
            $objetos = array_merge($objetos, $telefono_2);
        }

        $solicitudesInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
        ->join('tmae_direcciones as dir', 'diras.id_direccion', 'dir.id')
        ->join('tmae_direcciones as dirsol', 'diras.direccion_solicitante', 'dirsol.id')
        ->select('dir.direccion as direccion_asignada', 'dirsol.direccion as solicitante', 'diras.estado')
        ->where(['diras.id_cab' => $tabla->id])
        ->whereIn('diras.estado', ['INFORME', 'INFORME-RESPONDIDO', 'INFORME-DESVINCULADO'])->get();

        $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
        ->join('users as u', 'u.id', 'diras.id_direccion')
        ->join('tmae_direcciones as dir', 'dir.id', 'diras.direccion_solicitante')
        ->select('u.name as analista', 'dir.direccion', 'diras.observacion', 'diras.direccion_solicitante')
        ->where(['id_cab' => $id])
        ->whereIn('diras.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();

        // foreach($analistas as $index => $analista)
        // {
        //     if(collect(User::COORDINACIONES)->contains($analista->direccion_solicitante))
        //     {
        //         unset($analistas[$index]);
        //     }
        // }

        if(count($analistas) == 0)
        {
            $analistasCoordinador = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
            ->join('tmae_direcciones as u', 'u.id', 'diras.id_direccion')
            ->join('tmae_direcciones as dir', 'dir.id', 'diras.direccion_solicitante')
            ->select('u.direccion as analista', 'dir.direccion', 'diras.observacion', 'diras.direccion_solicitante')
            ->where(['id_cab' => $id])
            ->whereIn('diras.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->get();
            $analistas = array_merge($analistas->toArray(), $analistasCoordinador->toArray());
        }

        $archivados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
        ->join('tmae_direcciones as dir', 'dir.id', 'diras.id_direccion')
        ->select('dir.direccion', 'diras.observacion')
        ->where(['diras.id_cab' => $id, 'diras.estado' => 'ARCHIVADO'])->get();

        $archivados_ep = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
        ->join('users as u', 'u.id', 'diras.id_direccion')
        // ->join('tmae_direcciones as dir', 'dir.id', 'diras.id_direccion')
        ->select('u.name as direccion', 'diras.observacion')
        ->where(['diras.id_cab' => $id, 'diras.estado' => 'ARCHIVADO', 'u.id_perfil' => 55])->get();

        $archivados = array_merge($archivados->toArray(), $archivados_ep->toArray());

        if ($tabla->disposicion == 'FINALIZADO') {
            $tablaDirectores = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                ->select('dir.direccion', 'asig.updated_at as respuesta', 'asig.observacion')
                ->where(['asig.id_cab' => $tabla->id, 'asig.estado' => 'CONTESTADO'])->get();
        } else {
            $tablaDirectores = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                ->select('dir.direccion', 'asig.updated_at as respuesta', 'asig.observacion')
                ->where(['asig.id_cab' => $tabla->id])
                ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])->get();

            $tablaDirectores_2 = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('users as u', 'u.id', 'asig.id_direccion')
            ->select('u.name as direccion', 'asig.updated_at as respuesta', 'asig.observacion')
            ->where(['asig.id_cab' => $id])
            ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])
            ->where(['u.id_perfil' => 55])
            ->get();

            if(count($tablaDirectores_2) > 0)
            {
                $collect = collect($tablaDirectores_2);
                $tablaDirectores = $collect->merge($tablaDirectores);
            }

            $direc = str_replace('|', ',', $tabla->direccion_atender);

            if ($direc != '') {
                $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR " - ") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                $tabla->direccion_atender = $direciones_text[0]->direccion_atender;
            } else {
                unset($objetos[12]);
                unset($objetos[13]);
            }
        }

        $direc_inf = str_replace('|', ',', $tabla->direccion_informar);
        if ($direc_inf != '') {
            $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR " - ") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
            $tabla->direccion_informar = $direciones_text_inf[0]->direccion_informar;
        } else unset($objetos[13]);
        $tabla->correo_electronico = $tabla->correo_electronico == null ? 'NO SE PROPORCIONÓ' : $tabla->correo_electronico;

        $timelineTramite = $this->getTimeLineCab($id, 1);
        $timelineTramiteResp = $this->getTimeLineCab($id, 2);

        $ingresadoPor = null;
        if(isset($tabla->id_usuario_externo))
        {
            $usuario = $this->obtenerUsuario($tabla->id_usuario_externo);
            $usuario = json_decode($usuario->content());
            $ingresadoPor = $usuario->data->name;
        }

        return view('vistas.show', [
            "timelineTramite" => $timelineTramite,
            "timelineTramiteResp" => $timelineTramiteResp,
            "objetos" => $objetos,
            "tabla" => $tabla,
            "tablaDirectores" => $tablaDirectores,
            "solicitudesInforme" => $solicitudesInforme,
            "analistas" => $analistas,
            "archivados" => $archivados,
            "ingresadoPor" => $ingresadoPor,
            "tabla2" => null,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    public function update(Request $request, $id)
    {
        return $this->guardar($id);
    }

    public function edit($id)
    {
        $variableControl = null;
        $tabla = TramAlcaldiaCabModel::findOrFail($id);
        $btnguardar = $tabla->disposicion == 'EN PROCESO' ? 'OCULTAR' : null;
        $btnDevolverVentanilla = $tabla->tipo == 'SECRETARIA GENERAL' || $tabla->tipo == 'TRAMITES MUNICIPALES' ? 'SI' : null;
        // $btnDevolverVentanilla = null;

        $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 10])
            ->where('hojas', '>', 0)->get();

        // $nHojas = 10 + intval($id);
        $alcance = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')
            ->where(['id_referencia' => $id, 'hojas' => -$id, 'tipo' => 10, 'u.id_perfil' => 20])->get();

        $this->configuraciongeneral[10] = 'tramites/tramites';
        $this->configuraciongeneral[2] = "editar";
        $objetos = json_decode($this->objetos);

        $objetos[6]->Valor = $this->escoja + $this->tipo_documento;

        $prioridades = explodewords(ConfigSystem("prioridadtramites"), "|");
        $objetos[16]->Valor = $this->escoja + $prioridades;

        $objetos[23]->Tipo = "textdisabled2";
        $objetos[23]->Valor = 'PENDIENTE';

        $objetos[2]->Valor = $this->escoja + AreasModel::where(['estado' => 'ACT'])
            ->orderBy('area')->pluck('area', 'id')->all();
        $objetos[3]->Valor = $this->escoja + TipoTramiteModel::where(['estado' => 'ACT', 'id_area' => $tabla->id_area])
            ->orderBy('valor', 'ASC')->pluck('valor', 'id')->all();;

        // $parroquias = ParroquiaModel::where('estado', 'ACT')->pluck('parroquia', 'id')->all() + [10 => 'Parroquia sin especificar'];
        $parroquias = parroquiaModel::where('estado', 'ACT')
            ->whereNotIn('id', [8, 9])->orderBy('parroquia')->pluck('parroquia', 'id')->all() + [10 => 'Parroquia sin especificar'];

        $objetos[11]->Valor = $this->escoja + $parroquias;
        $cantones = CantonModel::whereNotIn('id', [146])->orderBy('nombre_canton')->pluck('nombre_canton', 'id')->all();
        $barrios = $tabla->id_parroquia == 10 ? $cantones : BarrioModel::where('estado', 'ACT')->orderBy('barrio')->pluck('barrio', 'id')->all();
        $objetos[12]->Valor = $this->escoja + $barrios;

        $this->configuraciongeneral[0] = "Editar trámite";

        if ($tabla->disposicion != 'NEGADA') {
            // unset($objetos[14]);
        }

        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[6]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        // unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);
        unset($objetos[27]);
        // dd($objetos);
        $tipo_doc = strlen($tabla->cedula_remitente) == 10 ? 'CEDULA' : 'RUC';
        // $objetos[6]->ValorAnterior = $this->tipo_documento[$tipo_doc];
        if($tabla->disposicion != 'FINALIZADO')
        {
            $tabla->disposicion = 'PENDIENTE';
        }

        $edit = null;

        $variableControl = null;
        if (request()->has("list")) {
            $btnguardar = 1;
        }

        $this->configuraciongeneral[3] = "10";
        $this->configuraciongeneral[4] = null;

        // $tipoArchivo = ['pdf', 'docx', 'doc'];
        $archivos = ArchivosModel::leftJoin('users as u', 'u.id', 'tmov_archivos.id_usuario')
            ->select('tmov_archivos.*', 'u.name')
            ->where(['id_referencia' => $id, 'tipo' => '10'])->get();
            // ->whereIn('tipo_archivo', $tipoArchivo)

        $archivos_total = ArchivosModel::where(['tipo' => '10', 'id_referencia' => $tabla->id])->count();

        // return view('vistas.create', [
        return view('tramites::tramitesexternos.create', [
            'archivos' => $archivos,
            'anexos' => $anexos,
            'archivos_total' => $archivos_total,
            "objetos" => $objetos,
            "variableControl" => $variableControl,
            "tabla" => $tabla,
            "btnDevolverVentanilla" => $btnDevolverVentanilla,
            "edit" => $edit,
            "scriptjs" => true,
            "alcance" => $alcance,
            "labels_tramite" => true,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "btnguardar" => $btnguardar,
        ]);
    }

    public function editdespacho()
    {
        $estado = request()->estado;
        $id = request()->id;
        $variableControl = null;
        $tabla = TramAlcaldiaCabModel::find($id);
        $usuario = $this->getTipoUsuarioByPerfil();
        $desvinculados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
            ->join('tmae_direcciones as dir', 'da.id_direccion', 'dir.id')
            ->select('dir.direccion', 'dir.alias', 'da.estado', 'da.observacion')
            ->where(['da.id_cab' => $id, 'da.estado' => 'DESVINCULADO'])->get();

        if(count($desvinculados) == 0)
        {
            $desvinculados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
            ->join('users as dir', 'da.id_direccion', 'dir.id')
            ->select('dir.name as direccion', 'dir.name as alias', 'da.estado', 'da.observacion')
            ->where(['da.id_cab' => $id, 'da.estado' => 'DESVINCULADO'])->get();
        }
        if (!$tabla) return back()->with('message', 'No se encontró el trámite al que quiere acceder.');

        if ($tabla->disposicion == 'APROBADA' && $tabla->estado_proceso == 0) {
            return back()->with('message', 'El trámite N° ' . $tabla->numtramite .  ' ya ha sido asignado a la direcciones correspondientes.');
        } else if ($tabla->disposicion == 'FINALIZADO') {
            return back()->with('message', 'El trámite N° ' . $tabla->numtramite .  ' ya ha finalizado.');
        }

        if ($estado == 1) {
            $this->configuraciongeneral[10] = "tramites/tramitesdespacho";
        } else if ($estado == 'NEGADA') {
            $this->configuraciongeneral[10] = "tramites/tramitesdespachoatendidosnegados";
        } else if ($estado == 'APROBADA') {
            $this->configuraciongeneral[10] = "tramites/tramitesdespachoatendidosaprobados";
        }

        $this->configuraciongeneral[2] = "editar";
        $this->configuraciongeneral[0] = "Despachar Trámite";

        $objetos = json_decode($this->objetos);
        // dd($objetos);
        $objetos[1]->Tipo = "textdisabled2";
        $objetos[7]->Clase = "disabled";
        $objetos[8]->Clase = "disabled";
        $objetos[9]->Clase = "disabled";
        $objetos[10]->Clase = "disabled";
        $objetos[13]->Clase = "disabled";
        $objetos[17]->Clase = "disabled";
        $objetos[18]->Tipo = 'textdisabled2';

        $parroquia = ParroquiaModel::find($tabla->id_parroquia);
        $objetos[11]->Valor = $tabla->id_parroquia == 10 ? [10 => 'Parroquia sin especificar'] : [$tabla->id_parroquia => $parroquia->parroquia];

        $barrio = $tabla->id_parroquia == 10 ? CantonModel::find($tabla->id_barrio) : BarrioModel::find($tabla->id_barrio);
        $objetos[12]->Valor = [$tabla->id_barrio => $tabla->id_parroquia == 10 ? $barrio->nombre_canton : $barrio->barrio];
        $objetos[12]->Descripcion = $tabla->id_parroquia == 10 ? 'Ciudad*' : 'Barrio*';

        $otros = ['EP' => 'EP', 'OTROS' => 'OTROS'];
        $objetos[2]->Valor = $this->escoja + AreasModel::where(['estado' => 'ACT'])
            ->orderBy('area')->pluck('area', 'id')->all();
        $objetos[3]->Valor = $this->escoja + TipoTramiteModel::where(['estado' => 'ACT', 'id_area' => $tabla->id_area])->pluck('valor', 'id')->all();
        // dd($objetos[3]);

        $direcciones = direccionesModel::where('tmae_direcciones.estado', '=', 'ACT')
            ->orderby("direccion", "asc")->pluck('direccion', 'id')->all();
        $objetos[14]->Valor = $direcciones;
        $objetos[15]->Valor = $direcciones;

        foreach ($direcciones as $value) {
            $direcciones_v[$value] = $value;
        }

        $objetos[14]->ValorAnterior = $direcciones_v;
        $objetos[15]->ValorAnterior = $direcciones_v;

        $direcciones = array();
        $direcciones = explode('|', $tabla->direccion_atender);
        $direcciones_v = array();
        foreach ($direcciones as $key => $value) {
            $direcciones_v[$value] = $value;
        }
        $objetos[14]->ValorAnterior = $direcciones_v;

        $informar = array();
        $informar = explode('|', $tabla->direccion_informar);
        $informar_v = array();
        foreach ($informar as $key => $value) {
            $informar_v[$value] = $value;
        }
        $objetos[15]->ValorAnterior = $informar_v;

        $prioridades = explodewords(ConfigSystem("prioridadtramites"), "|");
        $objetos[16]->Valor = $this->escoja + $prioridades;

        if ($tabla->correo_electronico == null) {
            $objetos[10]->Tipo = 'htmlplantilla';
            $objetos[10]->Valor = $this->sin_correo;

            $objetos[20]->Valor = ['SI' => 'SI'];
        } else $objetos[20]->Valor = $this->obtener_respuesta;

        $objetos[23]->Tipo = "select";
        $objetos[23]->Clase = "disabled";
        $objetos[23]->Valor = ['EN PROCESO' => 'EN PROCESO'];

        unset($objetos[6]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);
        unset($objetos[27]);

        if ($tabla->correo_electronico == null) {
            unset($objetos[9]);
        }

        $edit = null;
        $btnguardar = null;
        $variableControl = "NO";

        if (request()->has("list")) $btnguardar = 1;

        $this->configuraciongeneral[3] = "10";
        $this->configuraciongeneral[4] = null;

        // $tipoArchivo = ['pdf', 'docx', 'doc'];
        $archivos = ArchivosModel::leftJoin('users as u', 'u.id', 'tmov_archivos.id_usuario')
            ->select('tmov_archivos.*', 'u.name')
            ->where(['id_referencia' => $id, 'tipo' => '10'])->get();
            // ->whereIn('tipo_archivo', $tipoArchivo)

        $archivos_total = ArchivosModel::where(['tipo' => '10', 'id_referencia' => $tabla->id])->count();
        $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 10])
            ->where('hojas', '>', 0)->get();

        $alcance = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')
            ->where(['id_referencia' => $id, 'hojas' => -$id, 'tipo' => 10, 'u.id_perfil' => 20])->get();
        // dd($this->getTimeLineCab($id, 1));
        // return view('vistas.create', [
        return view('tramites::tramitesexternos.create', [
            'archivos' => $archivos,
            'archivos_total' => $archivos_total,
            "objetos" => $objetos,
            "variableControl" => $variableControl,
            "tabla" => $tabla,
            "alcance" => $alcance,
            // "btnDonacion" => 'SI',
            'anexos' => $anexos,
            "edit" => $edit,
            "labels_tramite" => true,
            'scriptjs' => true,
            'timelineTramites' => $this->getTimeLineCab($id, 1),
            "desvinculados" => $desvinculados,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs,
            "btnguardar" => $btnguardar,
        ]);
    }

    public function devolver($id, $timeline)
    {
        if (request()->ajax())
        {
            DB::beginTransaction();
            try
            {
                $asignacion = TramAlcaldiaAsiganrModel::find($id);
                $direccion = direccionesModel::find($asignacion->id_direccion, ['direccion']);

                $asignacion->respondido = 0;
                $asignacion->estado = 'ASIGNADO';
                $asignacion->save();

                $cab = TramAlcaldiaCabModel::find($asignacion->id_cab);
                $cab->disposicion = 'EN PROCESO';
                $cab->observacion_2 = request()->observacion;
                $cab->save();

                $perfiles = User::SOLO_DIRECTORES;
                $user = User::whereIn('id_perfil', $perfiles)
                    ->where(['id_direccion' => $asignacion->id_direccion])
                    ->first(['id','name', 'email']);

                if(!$user)
                {
                    $user = User::find($asignacion->id_direccion);
                }

                $this->Notificacion->notificacionesweb('El trámite N° ' . $cab->numtramite . ' ha sido devuelto desde secretaría general.', 'tramites/finalizartramite?id=' . $asignacion->id_cab, $user->id, '2c438f');

                $msg = 'Estimado ' . $user->name .  ', el trámite N°' . $cab->numtramite . ' ha sido devuelto por la siguente razón: ' . request()->observacion;
                $ruta = 'tramites/finalizartramite?id=' . $cab->id;
                $this->Notificacion->EnviarEmail($user->email, 'Trámite Devuelto', '', $msg, $ruta,'vistas.emails.email');

                $tl = $this->guardarTimeline($asignacion->id_cab, $cab, 'Trámite devuelto al director');
                $observacion = isset($direccion->direccion) ? 'Trámite devuelto al director: ' . $direccion->direccion : 'Trámite devuelto al director';
                $histo = TramAlcaldiaCabDetaModel::find($tl);
                $histo->observacion = $observacion;
                $histo->disposicion = 'EN PROCESO';
                $histo->save();

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'El trámite se ha devuelto al director'
                ]);
            }
            catch (\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => true,
                    'message' => 'No se pudo devolver el trámite',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        } else abort(404);
    }

    public function editFinalizar()
    {
        $id = request()->id;
        $tabla = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'id' => $id])->first();

        $informeUnidad = null;
        if(!$tabla)
        {
            $id = request()->id;
            $id = explode('-', $id);
            $id = $id[0];
            $tabla = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'id' => $id])->first();
        }

        $variableControl = null;
        $asignacion = null;
        $btnElaborarRespuesta = null;
        $perfilesRevision = collect(User::REVISAR_TRAMITES);
        $revisar = $perfilesRevision->contains(Auth::user()->id_perfil);

        if (!$tabla) return back()->with('message', 'No se encontró el trámite al que quiere acceder.');
        // if ($tabla && $tabla->disposicion == 'FINALIZADO' && (Auth::user()->id_perfil != 50 || Auth::user()->id_perfil == self::SECRETARIO_GENERAL)) {
        if ($tabla && $tabla->disposicion == 'FINALIZADO' && (!$revisar || Auth::user()->id_perfil == User::SECRETARIO_GENERAL)) {
            return back()->with('message', 'El trámite ' . $tabla->numtramite . ' ya ha sido finalizado');
        }

        $archivos = ArchivosModel::leftJoin('users as u', 'u.id', 'tmov_archivos.id_usuario')
            ->select('tmov_archivos.id', 'tmov_archivos.nombre', 'tmov_archivos.id_usuario', 'tmov_archivos.created_at','tmov_archivos.ruta', 'u.name', 'tmov_archivos.tipo')
            ->where(['id_referencia' => $id, 'tipo' => '10'])->get();

        $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.created_at', 'ax.ruta', 'u.name', 'ax.tipo')
            ->where(['tipo' => 11, 'id_tramite_externo' => $id])->get();

        $archivos = array_merge($archivos->toArray(), $archivos_->toArray());

        $anexos = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 10])
            ->where('hojas', '>', 0)->get();

        $alcance = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.*', 'u.name')
            ->where(['id_referencia' => $id, 'hojas' => -$id, 'tipo' => 10, 'u.id_perfil' => 20])->get();

        $archivos_total = ArchivosModel::where(['tipo' => '10', 'id_referencia' => $tabla->id])->count();

        $revision = request()->revision;
        $estado = request()->estado;
        if(isset($estado))
        {
            $estado = Crypt::decrypt($estado);
        }

        $objetos = json_decode($this->objetos);

        $objetos[1]->Tipo = "textdisabled2";
        $objetos[4]->Clase = "disabled";
        // $objetos[5]->Clase = "disabled";
        $objetos[7]->Clase = "disabled";
        $objetos[8]->Clase = "disabled";
        $objetos[9]->Clase = "disabled";
        $objetos[10]->Clase = "disabled";
        $objetos[13]->Clase = "disabled";
        $objetos[17]->Clase = "disabled";
        unset($objetos[26]);
        $btnDonacion = null;

        $parroquia = ParroquiaModel::find($tabla->id_parroquia);
        $objetos[11]->Valor = $tabla->id_parroquia == 10 ? [10 => 'Parroquia sin especificar'] : [$tabla->id_parroquia => $parroquia->parroquia];

        $barrio = $tabla->id_parroquia == 10 ? CantonModel::find($tabla->id_barrio) : BarrioModel::find($tabla->id_barrio);
        $objetos[12]->Valor = [$tabla->id_barrio => $tabla->id_parroquia == 10 ? $barrio->nombre_canton : $barrio->barrio];
        $objetos[12]->Descripcion = $tabla->id_parroquia == 10 ? 'Ciudad*' : 'Barrio*';

        $area = AreasModel::find($tabla->id_area);
        if($area)
        {
            $objetos[2]->Valor = [$area->id => $area->area];
        }
        else unset($objetos[2]);

        $tipo_tramite = TipoTramiteModel::find($tabla->tipo_tramite);
        if($tipo_tramite)
        {
            if(Auth::user()->id_perfil == 62 && Auth::user()->id_direccion == 21 && $area->id == 32)
            {
                $tiposTramiteTransito = TipoTramiteModel::where(['id_area' => $area->id])->pluck('valor', 'id')->all();
                $objetos[3]->Tipo = 'select-actualizar-tipo-tramite';
                $objetos[3]->Clase = 'chosen-select';
                $objetos[3]->Valor = $tiposTramiteTransito;
            }
            else
            {
                $objetos[3]->Valor = [$tipo_tramite->id => $tipo_tramite->valor];
            }
        }
        else unset($objetos[3]);

        $this->configuraciongeneral[2] = "editar";
        $correoTramite = null;
        $btnFinalizarCoordinador = null;

        if ($tabla->obtener_respuesta == 'SI') {
            $objetos[20]->Descripcion = 'Respuesta del trámite';
            $objetos[20]->Tipo = 'htmlplantilla';
            $objetos[20]->Valor = $this->respuesta_si;
        } else if ($tabla->obtener_respuesta == 'NO') {
            $objetos[20]->Descripcion = 'Respuesta del trámite';
            $objetos[20]->Tipo = 'htmlplantilla';
            $objetos[20]->Valor = $this->respuesta_no;
        }
        $contestados = 0;
        $dirAsignados = 0;
        $timeline = $tabla->tipo == 'TRAMITES MUNICIPALES' ? null : $this->getTimeLineCab($id, 1);

        $btnEp = null;
        $btnResponder = null;
        $btnAsignarAnalistas = null;
        $btnAsignarAnalistas2 = null;
        $btnAnalista = null;
        $analistasAsignados = null;
        $informeOtrasAreasAnalista = null;
        $eSubdirector = null;

        if(Auth::user()->id_perfil == 50)
        {
            $desvinculados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
                ->join('tmae_direcciones as dir', 'da.id_direccion', 'dir.id')
                ->select('dir.direccion', 'dir.alias', 'da.estado', 'da.observacion')
                ->where(['da.id_cab' => $id, 'da.estado' => 'DESVINCULADO'])->get();
        }
        else
        {
            $desvinculados = null;
        }

        if ($tabla->tipo == 'TRAMITES MUNICIPALES')
        {
            $res_dir = null;
            $respuestasInforme = null;
            $btnFinalizarTramite = null;
            $btnDevolver = null;
            $btnFinalizar = null;
            $btnCoordinador = null;
            $botonesCoordinadorTramites = null;
            $btnGuardarInforme = null;
            $btnguardar = 'ocultar';
            $alcance = null;
            unset($objetos[14]);
            unset($objetos[15]);
            unset($objetos[17]);
            unset($objetos[19]);
            unset($objetos[20]);
            unset($objetos[25]);
            unset($objetos[27]);

            $analistasAsignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['id_cab' => $id, 'direccion_solicitante' => Auth::user()->id_direccion])
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado', 'dira.observacion', 'dira.created_at')->get();

            $analistas = collect(User::ANALISTAS);
            $perfil = $analistas->contains(Auth::user()->id_perfil);

            $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id,
                'estado' => 'ANALISTA'
            ])->first();

            if($perfil && $analistaAsignado)
            {
                $asignados = explode('|', $tabla->direccion_atender);

                $direcciones = direccionesModel::where(['estado' => 'ACT'])
                    ->whereNotIn('id', $asignados)
                    ->orderBy('direccion')->pluck('direccion', 'id')->all();
                unset($objetos[26]);

                $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id,
                    'estado' => 'ANALISTA'
                ])->first();

                if($analistaAsignado->estado == 'ANALISTA-CONTESTADO')
                {
                    return back()->with('message', 'El trámite ya ha sido contestado');
                }

                $btnDevolver = null;
                $btnFinalizar = null;
                $btnCoordinador = null;
                $botonesCoordinadorTramites = null;
                $btnGuardarInforme = null;
                $btnguardar = 'ocultar';
                $btnAnalista = 'SI';
                $btnElaborarRespuesta = Crypt::encrypt("NO-{$tabla->numtramite}");
                $objetos[21]->Clase = 'disabled';
            }
            else
            {
                $direcciones = null;
                $btnElaborarRespuesta = Crypt::encrypt("NO-{$tabla->numtramite}");
                $analistas = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id, 'estado' => 'ANALISTA',
                    'direccion_solicitante' => Auth::user()->id_direccion
                ])->count();

                if($analistas > 0)
                {
                    $btnResponder = null;
                }
                else
                {
                    if(Auth::user()->id_perfil == 67)
                    {
                        $btnAsignarAnalistas = 'SI';
                        // $btnResponder = null;
                        $btnResponder = 'SI';
                    }
                    else $btnResponder = 'SI';
                }
                // $archivos = null;
                $opciones = '[{"Tipo":"text","Descripcion":"Tipo de respuesta*","Nombre":"tipo_respuesta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
                $opciones = json_decode($opciones);

                $opciones[0]->Valor = [
                    'FINALIZAR' => 'FINALIZAR',
                    'DEVOLVER CON OBSERVACIONES' => 'DEVOLVER CON OBSERVACIONES',
                ];
            }
        }
        elseif ($revision == true || $revision == 'true')
        {
            $btnElaborarRespuesta = Crypt::encrypt("NO-{$tabla->numtramite}-REVISION");

            $contestados = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'direccion_solicitante' => null
            ])->where(function ($q) {
                $q->where(['respondido' => 1])
                    ->orWhere(['respondido' => 2]);
            })->count();

            $dirAsignados = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'direccion_solicitante' => null
            ])->where('estado', '<>', 'CON COPIA')->count();

            $correoTramite = $tabla->correo_electronico == null ? 'NO' : 'SI';
            $btnDevolver = null;
            $direcciones = null;
            $btnFinalizar = null;
            $btnCoordinador = null;
            $botonesCoordinadorTramites = null;
            $respuestasInforme = null;
            $btnGuardarInforme = null;
            $btnguardar = 'ocultar';
            $btnFinalizarTramite = 'SI';
            $culminado = TramAlcaldiaAsiganrModel::select(DB::raw("COUNT(*) as culminado"))
            ->where(['respondido' => 0, 'id_cab' => $id, 'estado' => 'ASIGNADO'])->first();

            $res_dir = null;
            $objetos[27]->Valor = ['2' => 'Finalizar'];
            $objetos[27]->Descripcion = 'Estado';

            if ($culminado->culminado == 0) {
                $btnFinalizarTramite = 'SI';
            } else $btnFinalizarTramite = 'SI';

            $js = '[{"Tipo":"htmlplantilla","Descripcion":"Aprobaciones","Nombre":"js_indicadores_plantilla","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
            $js = json_decode($js);

            $culminado = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                ->join('tram_peticiones_cab as cab', 'cab.id', 'asig.id_cab')
                ->join('tram_peticiones_cab_deta as deta', 'deta.id_tram_cab', 'cab.id')
                ->select('deta.id as timeline', 'asig.updated_at  as fecha_respuesta', 'asig.id', 'asig.estado', 'asig.respondido', 'asig.observacion', 'dir.direccion', 'dir.alias')
                ->where(['cab.id' => $id, 'asig.direccion_solicitante' => null]) //'deta.disposicion' => 'RESPONDIDO',
                ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])
                ->groupBy('dir.id')->get();

            $atender = explode('|', $tabla->direccion_atender);
            if(count($atender) == 1 && count($culminado) == 0)
            {
                $culminado = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tram_peticiones_cab as cab', 'cab.id', 'asig.id_cab')
                ->join('tram_peticiones_cab_deta as deta', 'deta.id_tram_cab', 'cab.id')
                ->select('deta.id as timeline', 'asig.updated_at  as fecha_respuesta', 'asig.estado', 'asig.id', 'asig.respondido', 'asig.observacion')
                ->where(['cab.id' => $id, 'deta.disposicion' => 'RESPONDIDO', 'asig.direccion_solicitante' => null])
                ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])
                ->get();
            }
            else if(count($culminado) > 0 && count($atender) > count($culminado))
            {
                $culminado_2 = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tram_peticiones_cab as cab', 'cab.id', 'asig.id_cab')
                ->join('users as u', 'u.id', 'asig.id_direccion')
                ->join('tram_peticiones_cab_deta as deta', 'deta.id_tram_cab', 'cab.id')
                ->select('deta.id as timeline', 'asig.updated_at  as fecha_respuesta', 'asig.estado', 'asig.id', 'asig.respondido', 'asig.observacion', 'u.id_perfil')
                ->where(['u.id_perfil' => 55, 'cab.id' => $id, 'deta.disposicion' => 'RESPONDIDO', 'deta.id_usuario' => 1470])
                ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])
                ->whereNull('asig.direccion_solicitante')->get();

                $culminado = collect(array_merge($culminado_2->toArray(), $culminado->toArray()));
            }

            $html = '<script>
                $(document).ready(function () {
                    var data_=' . json_encode($culminado) . ';
                    var btn_guardar=false;
                    $.each(data_, function(key, element) {
                        var estado="";
                        var direccion = element.direccion == undefined ? \'DELGADO CHOEZ GABRIELA ALEXANDRA\' : element.direccion;
                        var obs = element.observacion ? element.observacion : "";
                        var fecha_res = element.fecha_respuesta ? element.fecha_respuesta : "";
                        if(element.respondido==1){
                            estado="CONTESTADO";
                            if(element.estado == \'ARCHIVADO\')
                            {
                                $("#tabla_indicadores").append(\'<tr id="fila_\'+element.id+\'" ><td>\'+direccion+\'</td><td>\'+obs+\'</td><td>\'+element.fecha_respuesta+\'</td><td id="estado_\'+element.id+\'">\'+element.estado+\' </td></tr>\');
                            }
                            else
                            {
                                $("#tabla_indicadores").append(\'<tr id="fila_\'+element.id+\'" ><td>\'+direccion+\'</td><td>\'+obs+\'</td><td>\'+element.fecha_respuesta+\'</td><td id="estado_\'+element.id+\'">\'+element.estado+\' </td><td> <button type="button" id="btn_\'+element.id+\'" onclick="devolver(\'+element.id+\', \'+element.timeline+\')" class="btn" ><i  class="fa fa-reply" aria-hidden="true"> </i> DEVOLVER </button></td></tr>\');
                            }
                        }else{
                            estado=element.respondido==2 ? "RESPUESTA ENVIADA AL CIUDADANO" : "SIN RESPONDER";
                            btn_guardar=true;
                            $("#tabla_indicadores").append(\'<tr><td>\'+direccion+\'</td><td>\'+obs+\'</td><td>\'+element.fecha_respuesta+\'</td><td id="estado_\'+element.id+\'">\'+estado+\' </td> <td> </td></tr>\');
                        }
                    });
                });

                async function devolver(id, timeline) {

                    var ruta_delete="' . URL::to('tramites/devolver') . '/"+id+"/"+timeline+"?_token=' . csrf_token() . '";

                    const { value: observacion } = await Swal.fire({
                        input: \'textarea\',
                        width: \'600px\',
                        inputPlaceholder: \'Ingrese observación\',
                        inputAttributes: {
                            \'aria-label\': \'Type your message here\'
                        },
                        showCancelButton: true,
                        reverseButtons: true,
                        confirmButtonColor: \'#3085d6\',
                        cancelButtonColor: \'#d33\',
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        inputValidator: (value) => {
                        if (!value) {
                            return \'Debe ingresar una observación!\'
                        }
                        }
                    })

                    if (observacion) {
                        $.post(ruta_delete,{ observacion }, function(data) {
                            if(data.ok) {
                                toastr["success"](data.message);
                                document.getElementById("estado_"+id).innerHTML="NOTIFICADO";
                                $("#btn_"+id).hide();
                                $("#btn_guardar").hide();
                            } else {
                                toastr["error"](data.message);
                            }
                        });
                    }
                }
                </script>
                <div>
                <br>
                <table class="table table-striped" id="tabla_indicadores">
                <thead>
                    <tr>
                    <th scope="col">Dirección</th>
                    <th scope="col">Respuesta</th>
                    <th scope="col">Fecha</th>
                    <th scope="col">Estado</th>
                    <th scope="col">Accion</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
                </table></div>';

            $js[0]->Valor = $html;
            $objetos = array_merge($objetos, $js);

            $this->configuraciongeneral[0] = "Aprobar culminación del Trámite";
            $this->configuraciongeneral[10] = "tramites/tramitesrevision";

            unset($objetos[13]);
            unset($objetos[14]);
            unset($objetos[15]);
            unset($objetos[19]);
            unset($objetos[20]);
            unset($objetos[21]);
            unset($objetos[22]);
            unset($objetos[24]);
            unset($objetos[25]);
            // dd($objetos);
            // $objetos[18]->Tipo = "textarea2";
            $objetos[18]->Clase = "disabled";
        }
        else
        {
            $puedeEnviarADonaciion = collect([1, 35])->contains(Auth::user()->id_direccion);
            if(Auth::user()->id_perfil == 16 && $puedeEnviarADonaciion)
            {
                $btnDonacion = 'SI';
            }
            $btnFinalizarTramite = null;
            $query = TramAlcaldiaCabModel::join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'tram_peticiones_cab.id')
                ->select('tram_peticiones_cab.id', 'tram_peticiones_cab.numtramite', 'tram_peticiones_cab.remitente',
                    'tram_peticiones_cab.observacion_2', 'tram_peticiones_cab.asunto', 'tram_peticiones_cab.peticion',
                    'tram_peticiones_cab.prioridad', 'tram_peticiones_cab.recomendacion', 'tram_peticiones_cab.disposicion',
                    'tram_peticiones_cab.observacion', 'tram_peticiones_cab.fecha_ingreso', 'tram_peticiones_cab.fecha_fin',
                    'tram_peticiones_cab.estado_proceso', 'tram_peticiones_cab.direccion_atender', 'tram_peticiones_cab.cedula_remitente',
                    'tram_peticiones_cab.correo_electronico', 'tram_peticiones_cab.telefono', 'tram_peticiones_cab.direccion',
                    'tram_peticiones_cab.referencia', 'tram_peticiones_cab.obtener_respuesta', 'tram_peticiones_cab.observacion_general',
                    'tram_peticiones_cab.tipo_tramite', 'da.observacion', 'tram_peticiones_cab.concejales'
                );

            $asignados = explode('|', $tabla->direccion_atender);
            $copia = collect(explode('|', $tabla->direccion_informar));
            $direc = $copia->contains(Auth::user()->id_direccion);

            $informe = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id_direccion
            ])->whereIn('estado', ['INFORME', 'ANALISTA-INFORME'])
            ->whereNotIn('id_direccion', [$asignados])->count();
            // dd($informe);

            $collectAsig = collect($asignados);
            $asignado = $collectAsig->contains(Auth::user()->id_direccion);

            $analistas = collect(User::ANALISTAS);
            $perfilAnalista = $analistas->contains(Auth::user()->id_perfil);

            $analistaAsignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id])
            ->whereIn('id_direccion', [Auth::user()->id, Auth::user()->id_direccion])
            ->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO', 'SUB-ANALISTA', 'SUB-ANALISTA-CONTESTADO'])->first();

            // if($esCoordinador && !$direc && $informe == 0 && !$asignado)
            // {
            //     $contestados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'respondido' => 1])->count();

            //     if ($tabla->obtener_respuesta == 'NO') $btnFinalizarCoordinador = 'SI';
            //     $objetos[20]->Clase = "disabled";
            //     $botonesCoordinadorTramites = null;
            //     $req = Crypt::decrypt(request()->id);
            //     $req = explode('-', $req);
            //     $id = $req[0];
            //     $id_direccion = $req[1];
            //     $btnguardar = "ocultar";
            //     $btnGuardarInforme = null;

            //     $tabla = $query->where(['tram_peticiones_cab.id' => $id, 'da.id_direccion' => $id_direccion])->first();

            //     $btnDevolver = null;
            //     $direcciones = null;
            //     $btnCoordinador = null;
            //     $btnFinalizar = null;

            //     $solicitudesRevision = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
            //         ->join('tmae_direcciones as dir', 'da.id_direccion', 'dir.id')
            //         ->select('da.id_cab', 'dir.id as id_direccion', 'dir.direccion', 'dir.alias', 'da.observacion', 'da.estado')
            //         ->where(['da.id_cab' => $id])
            //         ->where(function ($q) {
            //             $q->where(['da.estado' => 'REVISION-COORDINADOR'])
            //                 ->orWhere(['da.estado' => 'DEVUELTO-COORDINADOR']);
            //         })->get();

            //     $solicitudesRevisionCount = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as da')
            //         ->join('tmae_direcciones as dir', 'da.id_direccion', 'dir.id')
            //         ->where(['da.id_cab' => $id, 'da.estado' => 'REVISION-COORDINADOR'])->count();

            //     if (count($solicitudesRevision) == $solicitudesRevisionCount) {
            //         if (count($asignados) == 1) $btnFinalizar = 'COORDINADOR';
            //         $botonesCoordinadorTramites = 'SI';
            //     }

            //     $revision = '[{"Tipo":"htmlplantilla","Descripcion":"Solicitudes de revisión/aprobación","Nombre":"revision","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" }]';
            //     $revision = json_decode($revision);
            //     $data = $solicitudesRevision;
            //     // dd($data);
            //     $html = '<script>
            //     $(document).ready(function () {
            //         var data_=' . json_encode($data) . ';
            //         var btn_guardar=false;
            //         $.each(data_, function(key, element) {
            //             let obs = element.observacion == null ? \'\' : element.observacion;
            //             let estado = element.estado == \'DEVUELTO-COORDINADOR\' ? \'DEVUELTO\' : \'<button type="button" id="btn_\'+element.id_direccion+\'" onclick="devolverDirector(\'+element.id_cab+\', \'+element.id_direccion+\')" class="btn" ><i  class="fa fa-reply" aria-hidden="true"> </i> DEVOLVER </button>\';
            //             $("#tabla_revision").append(\'<tr id="fila_\'+element.id_direccion+\'" ><td>\'+element.direccion+\'</td><td>\'+obs+\'</td><td>\' +estado+ \' </td></tr>\');
            //         });
            //     });
            //     </script>
            //     <div>
            //     <br>
            //     <table class="table table-striped" id="tabla_revision">
            //     <thead>
            //         <tr>
            //         <th scope="col">Dirección</th>
            //         <th scope="col">Observación</th>
            //         <th scope="col">Acción</th>
            //         </tr>
            //     </thead>
            //     <tbody>
            //     </tbody>
            //     </table></div>';
            //     $revision[0]->Valor = $html;
            //     // dd($revision);
            //     $objetos = array_merge($objetos, $revision);
            //     unset($objetos[21]);
            // }

            if(($perfilAnalista && $analistaAsignado) || (isset($estado) && $estado == 'ANALISTA'))
            {
                $direcciones = direccionesModel::where(['estado' => 'ACT'])
                    ->whereNotIn('id', $asignados)
                    ->orderBy('direccion')->pluck('direccion', 'id')->all();

                $btnAsignarAnalistas2 = User::esAnalistaSubarea() ? 'SI' : null;
                $btnElaborarRespuesta = Crypt::encrypt("NO-{$tabla->numtramite}");

                $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id
                ])->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO', 'SUB-ANALISTA', 'SUB-ANALISTA-CONTESTADO'])->first();

                $query = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as diras')
                    ->join('tmae_direcciones as dir', 'dir.id', 'diras.id_direccion')
                    ->where(['diras.id_cab' => $id, 'diras.direccion_solicitante' => Auth::user()->id]);

                if(User::esAnalistaSubarea())
                {
                    $eSubdirector = User::subdirector();
                    $informeOtrasAreasAnalista = $query->get(['dir.direccion', 'diras.observacion', 'diras.estado']);
                    $analistaInforme = $query->where(['diras.estado' => 'ANALISTA-INFORME'])->count();

                    $analistasAsignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
                    ->join('users as u', 'dira.id_direccion', 'u.id')
                    ->where(['id_cab' => $id, 'direccion_solicitante' => Auth::user()->id])
                    ->whereIn('dira.estado', ['SUB-ANALISTA', 'SUB-ANALISTA-CONTESTADO'])
                    ->select('u.name', 'dira.estado', 'dira.observacion', 'dira.created_at')->get();

                    $subAnalistas = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id,
                        'direccion_solicitante' => Auth::user()->id,
                        'estado' => 'SUB-ANALISTA'
                    ])->count();

                    $btnAnalista = $subAnalistas > 0 || $analistaInforme > 0 ? null : 'SI';
                }
                else
                {
                    $informeOtrasAreasAnalista = $query->get(['dir.direccion', 'diras.observacion', 'diras.estado']);
                    $analistaInforme = $query->where(['diras.estado' => 'ANALISTA-INFORME'])->count();
                    $btnAnalista = $analistaInforme == 0 ? 'SI' : null;
                }

                if(!$analistaAsignado)
                {
                    $analistaAsignado = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id,
                        'id_direccion' => Auth::user()->id_direccion
                    ])->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->first();
                }

                if($analistaAsignado->estado == 'ANALISTA-CONTESTADO' || $analistaAsignado->estado == 'SUB-ANALISTA-CONTESTADO')
                {
                    return back()->with('message', 'El trámite ya ha sido contestado');
                }

                $btnDevolver = null;
                $btnFinalizar = null;
                // $direcciones = null;
                $btnCoordinador = null;
                $botonesCoordinadorTramites = null;
                $btnGuardarInforme = null;
                $btnguardar = 'ocultar';
                $objetos[21]->Clase = 'disabled';
                $obsAnalista = '[{"Tipo":"textarea","Descripcion":"Respuesta","Nombre":"analista_informe","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
                $obsAnalista = json_decode($obsAnalista);
                $objetos = array_merge($objetos, $obsAnalista);
            }
            else if(Auth::user()->id_perfil == 55)
            {
                $btnElaborarRespuesta = Crypt::encrypt("NO-{$tabla->numtramite}");
                $asignacion = TramAlcaldiaAsiganrModel::where(['id_cab' => $id])
                ->where(function($q) {
                    $q->where(['id_direccion' => Auth::user()->id])
                    ->orWhereIn('id_direccion', [76, 77]);
                })->whereIn('estado', ['INFORME', 'ASIGNADO', 'CON COPIA'])->first();

                if(isset($asignacion->respondido)  && ($asignacion->respondido == 1 || $asignacion->respondido == 2))
                {
                    return back()->with('message', 'No puede ingresar al trámite, ya ha sido ' . $asignacion->estado);
                }

                $btnDevolver = null;
                $btnFinalizar = null;
                $direcciones = null;
                $btnCoordinador = null;
                $botonesCoordinadorTramites = null;
                $btnGuardarInforme = null;
                $btnguardar = null;
                $btnAnalista = null;
                $btnEp = 'SI';
                if($asignacion->estado == 'CON COPIA')
                {
                    $btnEp = null;
                    $btnguardar = 'ocultar';
                }

                if (isset($tabla->observacion_2)) {
                    $observacion_2 = '[{"Tipo":"htmlplantilla","Descripcion":"Correcciones","Nombre":"observacion_2","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" }]';
                    $observacion_2 = json_decode($observacion_2);
                    $data = $tabla;
                    $html_ = '<script>
                            $(document).ready(function () {
                                var data_=' . json_encode($data) . ';
                                $("#tabla_indicadores_2").append(\'<tr id="fila_\'+data_.id+\'" ><td>\'+data_.observacion_2+\'</td>\');
                            });
                            </script>

                            <table class="table table-striped" id="tabla_indicadores_2">
                            <tbody>
                            </tbody>
                            </table>';
                    $observacion_2[0]->Valor = $html_;
                    $objetos = array_merge($objetos, $observacion_2);
                }
            }
            else
            {
                $esCoordinador = User::esCoordinador();
                if($esCoordinador)
                {
                    $analistasAsignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
                    ->join('tmae_direcciones as dir', 'dira.id_direccion', 'dir.id')
                    ->where(['id_cab' => $id, 'direccion_solicitante' => Auth::user()->id_direccion])
                    ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
                    ->select('dir.direccion as name', 'dira.estado', 'dira.observacion', 'dira.created_at')->get();
                }
                else
                {
                    $analistasAsignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
                    ->join('users as u', 'dira.id_direccion', 'u.id')
                    ->where(['id_cab' => $id, 'direccion_solicitante' => Auth::user()->id_direccion])
                    ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
                    ->select('u.name', 'dira.estado', 'dira.observacion', 'dira.respondido as respuesta', 'dira.created_at')->get();
                }

                $btnguardar = null;
                $btnFinalizar = null;
                $botonesCoordinadorTramites = null;
                $asignacion = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion
                ])->where('estado', '<>', 'CON COPIA')->first();

                if(isset($asignacion->estado) && ($asignacion->estado == 'ASIGNADO' || $asignacion->estado == 'DEVUELTO-COORDINADOR'))
                {
                    $btnElaborarRespuesta = Crypt::encrypt("{$tabla->obtener_respuesta}-{$tabla->numtramite}");
                }

                $direccionesAsignadas = TramAlcaldiaAsiganrModel::where(['id_cab' => $id])
                ->whereIn('estado', ['PENDIENTE', 'ASIGNADO', 'REVISION-COORDINADOR', 'DEVUELTO-COORDINADOR', 'CONTESTADO'])->count();

                $queryCoor = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
                    ->join('tram_coordinador_asignado as coor', 'cab.id', 'coor.id_cab')
                    ->select('coor.*')
                    ->where(['cab.id' => $id, 'cab.estado' => 'ACT']);

                $coor = $queryCoor->count();
                $coorEstado = $queryCoor->where(['id_direccion' => Auth::user()->id_direccion])->first();
                // dd($coorEstado);
                $direccionInformar = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion
                ])->whereNotIn('id_direccion', [$asignados])
                    ->whereIn('estado', ['INFORME', 'ANALISTA-INFORME'])
                    ->select('direccion_solicitante as id')->first();

                $direccionInformarRespondido = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion
                ])->whereIn('estado', ['INFORME-RESPONDIDO', 'ANALISTA-INFORME-RESPONDIDO', 'ANALISTA-ARCHIVADO'])
                ->whereNotIn('id_direccion', [$asignados])
                    ->select('direccion_solicitante as id')->first();

                if(isset($asignacion->estado) && ($asignacion->estado == 'ASIGNADO' || $asignacion->estado == 'REVISION-COORDINADOR' || $asignacion->estado == 'DEVUELTO-COORDINADOR'))
                {
                    $btnDevolver = null;
                    $direcciones = null;
                    $btnGuardarInforme = null;
                    // $btnCoordinador = $coor >= 1 || $direccionInformar ? null : 'SI';
                    // $btnCoordinador = $coor >= 1 ? null : 'SI';
                    $btnCoordinador = $coorEstado != null ? null : 'SI';

                    $btnguardar = isset($coorEstado->estado) && $coorEstado->estado == 'ASIGNADO' ? 'ocultar' : null;

                    $analistas = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id, 'estado' => 'ANALISTA',
                        'direccion_solicitante' => Auth::user()->id_direccion
                    ])->get();

                    if(count($analistas) > 0)
                    {
                        $btnCoordinador = null;
                        $btnguardar = 'ocultar';
                    }

                    if ($direccionesAsignadas >= 1 && $tabla->correo_electronico != null && $tabla->obtener_respuesta == 'NO') {
                        $btnguardar = 'ocultar';
                        // $btnFinalizar = Auth::user()->id_perfil == self::ANALISTA_DIRECTOR ? null : 'DIRECTOR';
                        $btnFinalizar = $this->analistaDirector() || count($analistas) > 0 ? null : 'DIRECTOR';
                    }
                    else if(Auth::user()->id_direccion == 21 && $tabla->obtener_respuesta == 'NO')
                    {
                        $btnFinalizar = 'DIRECTOR';
                    }
                    if (isset($coorEstado->estado) && $coorEstado->estado == 'DEVUELTO') {
                        $btnCoordinador = 'SI';
                        $observacionCoordinador = '[{"Tipo":"htmlplantilla","Descripcion":"Respuesta del coordinador","Nombre":"respuesta_coordinador","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" }]';
                        $observacionCoordinador = json_decode($observacionCoordinador);
                        $data = $coorEstado;

                        $html = '<script>
                                $(document).ready(function () {
                                    var data_=' . json_encode($data) . ';
                                    $("#tabla_indicadores_").append(\'<tr id="fila_\'+data_.id+\'" ><td>\'+data_.observacion+\'</td>\');
                                });
                                </script>

                                <table class="table table-striped" id="tabla_indicadores_">
                                <tbody>
                                </tbody>
                                </table>';
                        $observacionCoordinador[0]->Valor = $html;
                        if ($coorEstado->observacion != null) $objetos = array_merge($objetos, $observacionCoordinador);
                    }
                } elseif ($direccionInformar) {
                    $objetos[21]->Clase = 'disabled';
                    $direccionRespuestaInforme = '[{"Tipo":"textarea2","Descripcion":"Informe","Nombre":"direccion_informe","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
                    $direccionRespuestaInforme = json_decode($direccionRespuestaInforme);
                    $objetos = array_merge($objetos, $direccionRespuestaInforme);
                    $btnguardar = "ocultar";

                    $analistasInformar = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id, 'estado' => 'ANALISTA',
                        'direccion_solicitante' => Auth::user()->id_direccion
                    ])->count();

                    $btnGuardarInforme = $analistasInformar > 0 || $perfilAnalista ? null : 'SI';
                    $informeUnidad = $btnGuardarInforme == 'SI' ? null : 'SI';
                    $btnElaborarRespuesta = Crypt::encrypt("{$tabla->obtener_respuesta}-{$tabla->numtramite}");
                    $btnDevolver = null;
                    $direcciones = null;
                    $btnCoordinador = null;
                }
                elseif ($direccionInformarRespondido)
                {
                    return back()->with('message', 'El trámite N° ' . $tabla->numtramite . ' ya ha sido contestado.');
                }
                else
                {
                    $copia = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id,
                        'id_direccion' => Auth::user()->id_direccion,
                        'estado' => 'CON COPIA'
                    ])->first();

                    $analistas = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id, 'estado' => 'ANALISTA',
                        'direccion_solicitante' => Auth::user()->id_direccion
                    ])->get();

                    if ($copia)
                    {
                        $btnguardar = 'ocultar';
                        $btnDevolver = null;
                        $direcciones = null;
                        $btnCoordinador = null;
                        $btnGuardarInforme = null;
                        unset($objetos[21]);
                    }
                    else
                    {
                        $informeAnalistas = TramAlcaldiaAsiganrModel::where([
                            'id_cab' => $id,
                            'direccion_solicitante' => Auth::user()->id_direccion
                        ])->where('estado', '<>', 'ANALISTA')
                        ->where('estado', '<>', 'ANALISTA-CONTESTADO')
                        ->select('direccion_solicitante as id', 'observacion')->first();

                        $asignadosInforme = TramAlcaldiaAsiganrModel::where(['id_cab' => $id])
                            ->where('direccion_solicitante', '<>', null)
                            ->where('estado', '<>', 'ANALISTA')
                            ->where('estado', '<>', 'ANALISTA-CONTESTADO')
                            ->where('direccion_solicitante', Auth::user()->id_direccion)
                            ->select('direccion_solicitante as id', 'observacion')->count();

                        $asignadosInformeRespondidos = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'respondido' => 1])
                            ->where('direccion_solicitante', '<>', null)
                            ->where('estado', '<>', 'ANALISTA')
                            ->where('estado', '<>', 'ANALISTA-CONTESTADO')
                            ->where('direccion_solicitante', Auth::user()->id_direccion)
                            ->select('direccion_solicitante as id', 'observacion')->count();

                        $btnDevolver = $informe ? null : (isset($asignacion->respondido) && $asignacion->respondido == 2 ? null : 'SI');

                        $btnguardar = $asignadosInforme == $asignadosInformeRespondidos ? null : "ocultar";

                        $btnGuardarInforme = null;

                        if(count($analistas) > 0)
                        {
                            $btnDevolver = null;
                            $btnguardar = 'ocultar';
                        }

                        $btnCoordinador = null;
                        // $dirIna = array_merge(User::DIRECCIONES_INA, $asignados);

                        $direcciones = direccionesModel::where(['estado' => 'ACT'])
                            ->whereNotIn('id', $asignados)
                            ->orderBy('direccion')->pluck('direccion', 'id')->all();
                    }
                }

                $direccion = $direccionInformar ?? User::join('tmae_direcciones as dir', 'dir.id', 'users.id_direccion')
                    ->select('dir.id')->where('users.id', Auth::user()->id)->first();

                $tabla = $query->where(['tram_peticiones_cab.id' => $id, 'da.id_direccion' => $direccion->id])->first();

                if (isset($tabla->observacion_2)) {
                    $observacion_2 = '[{"Tipo":"htmlplantilla","Descripcion":"Correcciones","Nombre":"observacion_2","Clase":"disabled","Valor":"Null","ValorAnterior" :"Null" }]';
                    $observacion_2 = json_decode($observacion_2);
                    $data = $tabla;
                    $html_ = '<script>
                            $(document).ready(function () {
                                var data_=' . json_encode($data) . ';
                                $("#tabla_indicadores_2").append(\'<tr id="fila_\'+data_.id+\'" ><td>\'+data_.observacion_2+\'</td>\');
                            });
                            </script>

                            <table class="table table-striped" id="tabla_indicadores_2">
                            <tbody>
                            </tbody>
                            </table>';
                    $observacion_2[0]->Valor = $html_;
                    $objetos = array_merge($objetos, $observacion_2);
                }
                // $objetos[17]->Clase = "textarea2";
            }

            $res_dir = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                ->select('asig.*', 'dir.direccion', 'dir.alias')
                ->where(['asig.id_cab' => $id, 'asig.respondido' => 1, 'asig.direccion_solicitante' => null])->get();

            $respuestasInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                ->select('asig.*', 'dir.direccion', 'dir.alias')
                ->where(['asig.id_cab' => $id])
                ->whereNotNull('asig.direccion_solicitante')
                ->whereIn('asig.estado', ['INFORME', 'INFORME-RESPONDIDO', 'ARCHIVADO', 'INFORME-DESVINCULADO'])->get();

            if ($tabla == null) {
                unset($objetos[20]);
                $btnguardar = "ocultar";
                $tabla = TramAlcaldiaCabModel::find($id);
            }

            $this->configuraciongeneral[15] = "tramites/tramitesrespondidos";
            $this->configuraciongeneral[7] = "editar";
            $this->configuraciongeneral[5] = "Finalizar Trámite";

            $objetos[18]->Tipo = "textdisabled2";
            $objetos[19]->Tipo = "textarea2";
            $objetos[19]->Clase = "disabled";
            $perfilesAnalistas = collect(User::ANALISTAS);
            $analista = $perfilesAnalistas->contains(Auth::user()->id_perfil);
            $conCopia = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'estado' => 'CON COPIA', 'id_direccion' => Auth::user()->id_direccion])->first();
            if(isset($asignacion->estado) && $asignacion->estado != 'INFORME' && $asignacion->estado != 'ANALISTA-INFORME' || Auth::user()->id_perfil == 50 || $analista || $conCopia || Auth::user()->id_perfil == 55)
            {
                if($analista || (isset($asignacion->estado) && $asignacion->estado == 'ASIGNADO' && $tabla->observacion_2 != null))
                {
                    if($analista && isset($asignacion->estado))
                    {
                        $objetos[27]->Valor = ['1' => 'Enviar a revisión'];
                        $objetos[27]->Clase = "hidden";
                    }
                    else
                    {
                        $analistaActual = TramAlcaldiaAsiganrModel::where([
                            'id_cab' => $id, 'id_direccion' => Auth::user()->id
                        ])->whereIn('estado', ['ANALISTA', 'SUB-ANALISTA'])->first();

                        if($analistaActual)
                        {

                        }
                        else
                        {
                            if(!isset($tabla->observacion_2))
                            {
                                $objetos[27]->Valor = ['1' => 'Enviar a revisión'];
                                $objetos[27]->Clase = "hidden";
                            }
                        }
                    }
                }
                else
                {
                    if(isset($asignacion->estado) && $asignacion->estado == 'DEVUELTO-COORDINADOR')
                    {

                    }
                    else
                    {
                        $objetos[27]->Valor = ['1' => 'Enviar a revisión'];
                        $objetos[27]->Clase = "hidden";
                    }
                }
            }
            else if($asignacion == null && (isset($estado) && $estado != 'ANALISTA'))
            {
                return back()->with('message', 'Trámite no asignado a esta dirección');
            }
            if(isset($asignacion->estado) && ($asignacion->estado == 'INFORME' || $asignacion->estado == 'ANALISTA-INFORME'))
            {
                if(Auth::user()->id_perfil == 55)
                {
                    $btnGuardarInforme = 'SI';
                    $btnguardar = 'ocultar';
                    $btnEp = null;
                    $objetos[21]->Clase = 'disabled';
                    $objetos[27]->Tipo = 'textarea';
                    $objetos[27]->Descripcion = 'Informe';
                    $objetos[27]->Nombre = 'direccion_informe';
                    $objetos[27]->Clase = "Null";
                    $objetos[27]->Valor = "Null";
                }
                else
                {
                    // dd($objetos[27]);
                    if($objetos[27]->Tipo == 'select')
                    {
                        $btnGuardarInforme = 'SI';
                        $btnguardar = 'SI';
                        $objetos[27]->Tipo = 'textarea2';
                        $objetos[27]->Descripcion = 'Informe';
                        $objetos[27]->Nombre = 'direccion_informe';
                    }
                    else
                    {
                        $objetos[27]->Clase = "Null";
                        $objetos[27]->Valor = "Null";
                    }
                }
            }

            unset($objetos[15]);
            unset($objetos[25]);
            unset($objetos[26]);

            $direccion_atender = explode('|', $tabla->direccion_atender);
            $direccionesAsignadas = direccionesModel::whereIn('id', $direccion_atender)->pluck('direccion', 'id');
            $objetos[14]->Valor = $direccionesAsignadas;
            $direcciones_ = array();
            $direcciones_ = explode('|', $tabla->direccion_atender);
            $direcciones_v = array();
            foreach ($direcciones_ as $key => $value) {
                $direcciones_v[$value] = $value;
            }

            $objetos[14]->ValorAnterior = $direcciones_v;
        }

        unset($objetos[6]);
        unset($objetos[16]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        // unset($objetos[14]);
        if($tabla->tipo == 'TRAMITES MUNICIPALES' && $perfil)
        {
            $obsAnalista = '[{"Tipo":"textarea","Descripcion":"Respuesta","Nombre":"analista_informe","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }]';
            $obsAnalista = json_decode($obsAnalista);
            $objetos = array_merge($objetos, $obsAnalista);
        }

        $edit = null;
        $variableControl = "NO";
        $this->configuraciongeneral[2] = "editar";
        $this->configuraciongeneral[3] = "10"; //Tipo de Referencia de Archivo
        $this->configuraciongeneral[4] = null;

        if ($tabla->recomendacion == null) {
            unset($objetos[18]);
        }
        if ($tabla->correo_electronico == null) {
            $objetos[10]->Tipo = 'htmlplantilla';
            $objetos[10]->Valor = $this->sin_correo;
        }

        return view('tramites::tramitesexternos.create', [
            'archivos' => $archivos,
            'archivos_total' => $archivos_total,
            'objetos' => $objetos,
            'variableControl' => $variableControl,
            'tabla' => $tabla,
            'edit' => $edit,
            'aceptarDocs' => 'SI',
            'res_dir' => $res_dir,
            'alcance' => $alcance,
            'btnDonacion' => $btnDonacion,
            'anexos' => $anexos,
            'btnAsignarAnalistas' => $btnAsignarAnalistas,
            'btnAsignarAnalistas2' => $btnAsignarAnalistas2,
            'btnElaborarRespuesta' => $btnElaborarRespuesta,
            'informeUnidad' => $informeUnidad,
            'eSubdirector' => $eSubdirector,
            'btnEp' => $btnEp,
            'desvinculados' => $desvinculados,
            'analistas_asignados' => $analistasAsignados,
            'informeOtrasAreasAnalista' => $informeOtrasAreasAnalista,
            'btnResponder' => $btnResponder,
            'btnAnalista' => $btnAnalista,
            'correoTramite' => $correoTramite,
            'respuestasInforme' => $respuestasInforme,
            'scriptjs' => true,
            'contestados' => $contestados,
            'dirAsignados' => $dirAsignados,
            'btnFinalizarCoordinador' => $btnFinalizarCoordinador,
            'btnFinalizarTramite' => $btnFinalizarTramite,
            'btnDevolver' => $btnDevolver,
            'btnFinalizar' => $btnFinalizar,
            'asignacion' => $asignacion,
            'direcciones' => $direcciones,
            'btnCoordinador' => $btnCoordinador,
            'analistas' => $this->obtenerAnalistas(Auth::user()->id_direccion),
            'botonesCoordinadorTramites' => $botonesCoordinadorTramites,
            'btnGuardarInforme' => $btnGuardarInforme,
            'configuraciongeneral' => $this->configuraciongeneral,
            'validarjs' => $this->validarjs,
            'btnguardar' => $btnguardar,
            'verObservaciones' => 'si',
            'timelineTramites' => $timeline
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $tabla = TramAlcaldiaCabModel::find($id);
            $tabla->estado = 'INA';
            $tabla->id_usuario = Auth::user()->id;
            $tabla->save();

            $tabla3 = TramAlcaldiaCabDetaModel::where("id_tram_cab", $id)->get();
            foreach ($tabla3 as $key => $value) {
                $dele = TramAlcaldiaCabDetaModel::find($value->id);
                $dele->estado = 'INA';
                $dele->save();
            }

            DB::commit();
            Session::flash('message', 'El registro se eliminó Exitosamente!');
            if (request()->has('vista')) return 'El registro se eliminó Exitosamente!';

            return Redirect::to($this->configuraciongeneral[1]);
        } catch (\Exception $e) {
            DB::rollback();
            Session::flash('message', 'El registro no pudo ser elminado!');
            return Redirect::to($this->configuraciongeneral[1]);
        }
    }

    public function getTimeLineCab($id, $tipo)
    {
        $sw = 0;
        switch ($tipo) {
            case 1:
                $tabla = TramAlcaldiaCabDetaModel::select("tram_peticiones_cab_deta.*", "u.name")
                    ->join("users as u", "u.id", "=", "tram_peticiones_cab_deta.id_usuario")
                    ->where('id_tram_cab', $id)
                    ->orderBy("tram_peticiones_cab_deta.updated_at", "ASC")->get();

                $cab = TramAlcaldiaCabModel::find($id, ['id_usuario_externo']);
                if($cab->id_usuario_externo != null)
                {
                    $usuario = $this->obtenerUsuario($cab->id_usuario_externo);
                    $usuario = json_decode($usuario->content());
                    $tabla[0]->name = $usuario->data->name;
                }

                return $tabla;
                break;

            case 2:
                $personas[] = [];
                $tabla = TramAlcaldiaCabDetaModel::select("delegados_json")->where('id_tram_cab', $id)->get();

                foreach ($tabla as $valor) {
                    $personas = json_decode($valor->delegados_json);
                    unset($tabla);

                    if (isset($personas)) {
                        foreach ($personas as $p) {
                            if ($p->id = " ") {
                                $tabla[] = ["name" => " "];
                            } else {
                                $tabla[] = DB::table("users")->where("id", $p->$id)->select("name")->get();
                                $sw = 1;
                            }
                        }
                    }
                }
                if ($sw == 0) {
                    $tabla = array();
                }
                return $tabla;
                break;
        }
    }

    public function obtenerBarrios($parroquia)
    {
        if (request()->ajax()) {
            if ($parroquia == 10) {
                $barrios = CantonModel::whereNotIn('id', [146])
                    ->orderBy('nombre_canton')->get();
            } else {
                $barrios = barrioModel::where(['estado' => 'ACT', 'id_parroquia' => $parroquia])
                    ->orderBy('barrio')->get();
            }
            return response()->json($barrios);
        } else abort(401);
    }

    public function guardarTimeline($id, $data, $observacion = null, $estado = 0): int
    {
        $timeline = new TramAlcaldiaCabDetaModel;
        $timeline->id_usuario = Auth::user()->id;
        $timeline->id_tram_cab = $id;
        $timeline->numtramite = $data->numtramite;
        $timeline->remitente = $data->remitente;
        $timeline->asunto = $data->asunto;
        $timeline->peticion = $data->peticion;
        $timeline->prioridad = $data->prioridad;
        $timeline->disposicion = $data->disposicion;
        $timeline->observacion = $observacion;
        $timeline->ip = request()->ip();
        $timeline->pc = request()->getHttpHost();
        $timeline->fecha_ingreso = $data->fecha_ingreso;
        if($observacion == 'Trámite en proceso de gestión')
        {
            $timeline->fecha_respuesta = now();
        }
        $timeline->estado_proceso = $estado;
        $timeline->fecha_fin = $data->fecha_fin;
        $timeline->save();

        return $timeline->id;
    }

    function recibo($id)
    {
        try {
            $tabla = TramAlcaldiaCabModel::findOrFail($id);

            $anexos = AnexosModel::from('tmov_tram_anexos as ax')
                ->join('users as u', 'ax.usuario_id', 'u.id')
                ->select('ax.*', 'u.name')->where(['id_referencia' => $id, 'tipo' => 10])
                ->where('hojas', '>', 0)->get();


            return view('tramites::recibo', ['tabla' => $tabla, 'anexos' => $anexos]);
        } catch (\Exception $e) {
            $mensaje = "Error de conexión al servidor. " . $e->getMessage();
            return redirect()->back()->withErrors([$mensaje]);
        }
    }

    public function obtenerAnalistas($direccion)
    {
        $esAnalistaSubarea = User::esAnalistaSubarea();
        $subdirector = User::subdirector();
        // $director = collect(User::SOLO_DIRECTORES)->contains(Auth::user()->id_perfil);
        // $analistaEspecial = collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
        $id = Auth::user()->id;

        if($subdirector)
        {
            $perfiles = User::SUB_ANALISTAS;
            $analistas = User::where(['id_direccion' => $direccion])
            ->whereIn('id_perfil', $perfiles)
            ->select('cargo', 'name', 'id')->orderBy('name')->get();
        }
        else if($id == 1809 || $id == 1742)
        {
            $perfiles = User::ANALISTAS;
            $analistas = User::whereIn('id_perfil', $perfiles)
                ->select('cargo', 'name', 'id')->orderBy('name')->get();
        }
        else if($esAnalistaSubarea && Auth::user()->id != 133)
        {
            $analistas = User::where(['id_direccion' => Auth::user()->id_direccion])
                ->where('id', '<>', Auth::user()->id)
                ->whereIn('id_perfil', User::ANALISTAS)
                ->select('cargo', 'name', 'id')->orderBy('name')->get();
        }
        else
        {
            $perfiles = $esAnalistaSubarea && Auth::user()->id == 133 ? User::SUB_ANALISTAS
                : array_merge(User::ANALISTAS, User::SUB_ANALISTAS);

            // $unidades = SubDireccionModel::where(['id_direccion' => $direccion])
            // ->orWhere(['area' => $direccion])->get(['area', 'id_direccion']);
            // $unidad = isset($unidades[0]) ? $unidades[0]->id_direccion : null;

            // if(Auth::user()->id == 133)
            // {

            // }
            $analistas = User::where(function($q) use($direccion) {
                // $q->whereIn('id_direccion', $unidades->pluck('area'))
                $q->where(['id_direccion' => $direccion]);
                // ->orWhere(['id_direccion' => $unidad]);
            })->whereIn('id_perfil', $perfiles)->select('cargo', 'name', 'id')->orderBy('name')->get();
        }

        return $analistas;
    }

    public function tramites()
    {
        $this->configuraciongeneral[4] = 'Trámites ingresados';
        return view('tramites::tramitesexternos.tramitesexternos', [
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    public function tramitesPortal()
    {
        $this->configuraciongeneral[4] = 'Trámites ingresados por Portal Ciudadano';
        return view('tramites::tramitesexternos.tramites', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'analistas' => []
        ]);
    }

    public function tramitesAjax()
    {
        $tramites = TramAlcaldiaCabModel::with('area', 'tipoTramite')->where(['tipo' => 'VENTANILLA', 'estado' => 'ACT']);

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
            return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id])
            // ->where('asig.estado', '<>', 'CON COPIA')->get();
            ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])->get();

            return $atender->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('copia', function($tramite) {
            $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();

            return $copia->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('origen', function($tramite) {
            return $tramite->tipo == 'VENTANILLA' ? 'VENTANILLA' : ($tramite->tipo == 'TRAMITE MUNICIPAL' ? 'PORTAL CIUDADANO' : '');
        })->addColumn('igresado_por', function($tramite) {
            if($tramite->creado_por != null)
            {
                $usuario = User::find($tramite->creado_por);
                return $usuario->name;
            }
            else return 'CIUDADANO';
        })->addColumn('archivo', function($tramite) {
            $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->get();
            $archivos = '';

            if(count($archivo) == 1) $archivos= "<a href='" . URL::to('/archivos_sistema/' . $archivo[0]->ruta) . "' class='btn btn-success dropdown-toggle divpopup' type='button' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
            else
            {
                foreach($archivo as $ar)
                {
                    $archivos.= "<a href='" . URL::to('/archivos_sistema/' . $ar->ruta) . "' class='btn btn-success btn-xs dropdown-toggle divpopup' type='button' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                }
            }
            return $archivos;
        })->addColumn('respuesta', function($tramite) {
            $respuesta = isset($tramite->obtener_respuesta) && $tramite->obtener_respuesta == 'SI' ? $tramite->obtener_respuesta : 'NO';
            return $respuesta;
        })->addColumn('action', function($tramite) {
            return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>
            <a href="' . route('tramite.editfinalizar') . "?id=$tramite->id" . '">
            <i class="fa fa-pencil-square-o"></i></a>';
        })->rawColumns(['archivo', 'action'])->make(true);
    }

    public function tramitesBusqueda()
    {
        $perfiles = collect([1, 59, 80]);
        $verArchivos = $perfiles->contains(Auth::user()->id_perfil);

        $direcciones = direccionesModel::where(['estado' => 'ACT'])
        ->orderBy('direccion')->pluck('direccion', 'id');

        $this->configuraciongeneral[4] = 'Trámites ingresados';

        return view('tramites::tramitesexternos.busqueda', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'verArchivos' => $verArchivos,
            'direcciones' => $direcciones
        ]);
    }

    public function tramitesBusquedaAjax(Request $request)
    {
        $tramites = TramAlcaldiaCabModel::with('area', 'tipoTramite', 'asignados')->where(['estado' => 'ACT']);

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
            return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id])
            ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])->get();

            $atender_ep = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('users as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.name as direccion')
            ->where(['asig.id_cab' => $tramite->id, 'asig.id_direccion' => 1470])
            ->whereIn('asig.estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO'])->get();

            if(count($atender_ep) > 0)
            {
                $atender = array_merge($atender->toArray(), $atender_ep->toArray());
                $atender = collect($atender);
            }

            return $atender->map(function($asignado) {
                return $asignado['direccion'];
            })->implode(' - ');
        })->addColumn('copia', function($tramite) {
            $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();

            return $copia->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('analista', function($tramite) {
            $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['dira.id_cab' => $tramite->id])
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado')->get();

            return $analistas->map(function($item, $key) {
                $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                return "{$item->name} ({$estado})";
            })->implode(', ');

        })->addColumn('origen', function($tramite) {
            $ingresadoPor = '';
            if($tramite->creado_por != null)
            {
                $usuario = User::find($tramite->creado_por);
                $ingresadoPor = $usuario->name;
            }
            elseif($tramite->id_usuario_externo != null)
            {
                $usuario = $this->obtenerUsuario($tramite->id_usuario_externo);
                $usuario = json_decode($usuario->content());

                $ingresadoPor = $usuario->data->name;
            }
            else $ingresadoPor = 'CIUDADANO';

            return $tramite->tipo == 'TRAMITE MUNICIPAL' || $tramite->tipo == 'SECRETARIA GENERAL' ? 'PORTAL CIUDADANO' . " ({$ingresadoPor})" : "VENTANILLA ({$ingresadoPor})";
        })->addColumn('igresado_por', function($tramite) {
            if($tramite->creado_por != null)
            {
                $usuario = User::find($tramite->creado_por);
                return $usuario->name;
            }
            else return 'CIUDADANO';
        })->addColumn('archivos', function($tramite) {
            $archivosTramite = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->get();

            if($tramite->tipo == 'TRAMITES MUNICIPALES')
            {
                // $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->get();
                $archivos = AnexosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->get();
                $archivosTramite = array_merge($archivosTramite->toArray(), $archivos->toArray());
            }

            $archivos = '';
            foreach($archivosTramite as $archivo)
            {
                $ruta = isset($archivo->ruta) ? $archivo->ruta : $archivo['ruta'];
                $path = Str::contains($ruta, 'Tramite_interno') ? 'archivos_firmados/' : 'archivos_sistema/';
                $archivos .= '<a href="' . asset($path . $ruta) . '" class="btn btn-success btn-xs dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }

            return $archivos;
        })->addColumn('action', function($tramite) {
            $acciones = '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>';

            if(Auth::user()->id_perfil == 1)
            {
                $acciones .= ' <a href="' . route('administracion.edit', ['id' => Crypt::encrypt($tramite->id)]) . '">
                <i class="fa fa-edit"></i></a>
                <a class="" onclick="inactivarTramite(\'' . Crypt::encrypt($tramite->id) . '\')"><i class="fa fa-trash-o"></i></i></a>';
            }
            return $acciones;
        })->filter(function($query) use($request) {
            if ($request->has('numtramite') && $request->numtramite != '') {
                $query->where('numtramite', 'LIKE', "%$request->numtramite%");
            }
            if ($request->has('cedula') && $request->cedula != '') {
                $query->where(['cedula_remitente' => $request->cedula]);
            }
            if ($request->has('disposicion') && $request->disposicion != '') {
                $query->where(['disposicion' => $request->disposicion]);
            }
            if ($request->has('id_direccion') && $request->id_direccion != '') {
                if($request->id_direccion == 76 || $request->id_direccion == 77)
                {
                    $user = User::where(['id_perfil' => 55])->first()->id;

                    $query->whereHas('asignados', function($q) use($user) {
                        $q->where(['id_direccion' => $user])
                        ->whereIn('estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO']);
                    });
                }
                else
                {
                    $query->whereHas('asignados', function($q) use($request) {
                        $q->where(['id_direccion' => $request->id_direccion])
                        ->whereIn('estado', ['PENDIENTE', 'ASIGNADO', 'CONTESTADO']);
                    });
                }
            }
            if ($request->has('tipo') && $request->tipo != '') {
                if($request->tipo == 'PERMISOS MUNICIPALES')
                {
                    $query->where(['tipo' => 'TRAMITES MUNICIPALES']);
                }
                else
                {
                    $query->whereIn('tipo', ['SECRETARIA GENERAL', 'VENTANILLA']);
                }
            }
            if($request->has('analistas') && $request->analistas != '')
            {
                $query->whereHas('asignados', function($q) use($request) {
                    $q->where(['id_direccion' => $request->analistas, 'direccion_solicitante' => $request->id_direccion]);
                });
            }
            if($request->has('remitente') && $request->remitente != '')
            {
                $query->where('remitente', 'LIKE', "%$request->remitente%");
            }
            if($request->has('asunto') && $request->asunto != '')
            {
                $query->where('asunto', 'LIKE', "%$request->asunto%");
            }
        })->rawColumns(['archivos', 'action'])->make(true);
    }

    public function inactivarTramite($id)
    {
        try
        {
            $id = Crypt::decrypt($id);
            $tramite = TramAlcaldiaCabModel::find($id);
            $tramite->estado = 'INA';
            $tramite->save();

            return response()->json([
                'ok' => true,
                'message' => 'Trámite inactivado correctamente'
            ]);
        }
        catch(\Exception $ex)
        {
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo cambiar el estado del trámite',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function obtenerUsuario($id)
    {
        $client = new GuzzleHttpClient(['verify' => false]);
        $url = "https://portalciudadano.manta.gob.ec/obtenerusuario/{$id}";
        $consulta = $client->request('GET',  $url);
        $usuario = json_decode($consulta->getBody()->getContents());

        return response()->json($usuario);
    }

    public function PolicyUsoDeMediosElectronicos(Request $request)
    {
        $pdf = App::make('dompdf.wrapper');
        $context = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE
            ]
        ]);

        $pdf->getDompdf()->setHttpContext($context);
        $data = request()->all();

        $fecha = Carbon::createFromFormat('Y-m-d', date('Y-m-d'), new DateTimeZone(config('app.timezone')))
        ->isoFormat('dddd D [de] MMMM [de] YYYY');

        $pdf->loadView('tramites::tramitesexternos.contrato_medios',[
            'data' => $data,
            'fecha' => $fecha
        ])->setPaper('a4', 'portrait');

        return $pdf->stream("Acuerdo de Responsabilidad - ".$request["remitente"].".pdf");
    }

    public function obtenerAnalistasPorDireccion($id)
    {
        if(request()->ajax())
        {
            $analistas = $this->obtenerAnalistas($id);

            if(count($analistas) > 0)
            {
                return response()->json([
                    'ok' => true,
                    'analistas' => $analistas
                ]);
            }
            else
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'No hay analistas para la dirección seleccionada'
                ]);
            }
        }
        else abort(401);
    }

    public function analistaDirector()
    {
        return collect(User::ANALISTA_DIRECTOR)->contains(Auth::user()->id_perfil);
    }

    public function indexConcejales()
    {
        $secretario = Auth::user()->id_perfil == User::SECRETARIO_GENERAL;

        if($secretario)
        {
            $this->configuraciongeneral[4] = 'Trámites concejales';
            return view('tramites::tramitesexternos.indexconcejales', [
                'configuraciongeneral' => $this->configuraciongeneral
            ]);
        }
        else return back();
    }

    // public function tramitesConcejalesAjax(Request $request)
    // {
    //     $tramites = TramAlcaldiaCabModel::whereHas('asignados', function($q) {
    //         $q->where(['id_direccion' => Auth::user()->id_direccion])
    //         ->whereIn('estado', ['PENDIENTE', 'ASIGNADO']);
    //     })->where(['estado' => 'ACT', 'concejales' => 1])->whereIn('tipo', ['VENTANILLA', 'SECRETARIA GENERAL'])
    //     ->with('area', 'tipoTramite')->orderBy('id_prioridad', 'ASC');

    //     return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite) {
    //         return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
    //     })->editColumn('prioridad', function($tramite) {
    //         $color = $tramite->prioridad == 'ALTA' ? 'danger' : ($tramite->prioridad == 'MEDIA' ? 'warning' : 'default');
    //         $label = '<span class="label label-'.$color.'">'.$tramite->prioridad.'</span>';
    //         return $label;
    //     })->addColumn('atender', function($tramite) {
    //         $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
    //         ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
    //         ->select('dir.direccion')
    //         ->where(['asig.id_cab' => $tramite->id])
    //         ->where('asig.estado', '<>', 'CON COPIA')->get();

    //         return $atender->map(function($asignado) {
    //             return $asignado->direccion;
    //         })->implode(' - ');
    //     })->addColumn('copia', function($tramite) {
    //         $copia = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
    //         ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
    //         ->select('dir.direccion')
    //         ->where(['asig.id_cab' => $tramite->id, 'asig.estado' => 'CON COPIA'])->get();

    //         return $copia->map(function($asignado) {
    //             return $asignado->direccion;
    //         })->implode(' - ');
    //     })->addColumn('analistas', function($tramite) {
    //         $direcciones = explode('|', $tramite->direccion_atender);
    //         $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
    //         ->join('users as u', 'dira.id_direccion', 'u.id')
    //         ->where(['dira.id_cab' => $tramite->id])
    //         ->whereIn('dira.direccion_solicitante', $direcciones)
    //         ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
    //         ->select('u.name', 'dira.estado')->get();

    //         return $analistas->map(function($item, $key) {
    //             $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
    //             return "{$item->name} ({$estado})";
    //         })->implode(', ');

    //     })->addColumn('adjunto', function($tramite) {
    //         $adjunto = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])->first(['ruta']);

    //         if(isset($adjunto->ruta))
    //         {
    //             $archivos = '<a href="' . asset('archivos_sistema/' . $adjunto->ruta) . '" class="btn btn-success btn-sm dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
    //             <i class="fa fa-file-pdf-o"></i></i></a>';
    //         }
    //         else $archivos = '';

    //         return $archivos;
    //     })->addColumn('action', function($tramite) {
    //         return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
    //         <i class="fa fa-newspaper-o"></i></a>
    //         <a href="' . route('tramite.editfinalizar') . "?id=$tramite->id" . '">
    //         <i class="fa fa-pencil-square-o"></i></a>';
    //     })->filter(function($query) use($request) {
    //         if($request->has('analista') && $request->analista != '' && $request->has('estado') && $request->estado != '')
    //         {
    //             $query->whereHas('asignados', function($q) use($request) {
    //                 $q->where(['id_direccion' => $request->analista])
    //                 ->where(['estado' => $request->estado]);
    //             });
    //         }
    //         else if($request->has('analista') && $request->analista != '' && $request->has('estado') && $request->estado != '' && $request->has('numtramite') && $request->numtramite != '')
    //         {
    //             $query->whereHas('asignados', function($q) use($request) {
    //                 $q->where(['id_direccion' => $request->analista])
    //                 ->where(['estado' => $request->estado]);
    //             })->where('numtramite', 'LIKE', "%$request->numtramite%");
    //         }
    //         else
    //         {
    //             if($request->has('analista') && $request->analista != '')
    //             {
    //                 $query->whereHas('asignados', function($q) use($request) {
    //                     $q->where(['id_direccion' => $request->analista]);
    //                 });
    //             }
    //             if($request->has('estado') && $request->estado != '')
    //             {
    //                 $query->whereHas('asignados', function($q) use($request) {
    //                     $q->where(['estado' => $request->estado]);
    //                 });
    //             }
    //             if ($request->has('numtramite') && $request->numtramite != '')
    //             {
    //                 $query->where('numtramite', 'LIKE', "%$request->numtramite%");
    //             }
    //         }
    //     })->rawColumns(['action', 'adjunto', 'prioridad'])->make(true);
    // }

    // public function marcarComoConcejales($id)
    // {
    //     $tramite = TramAlcaldiaCabModel::find($id);
    //     $tramite->concejales = 1;
    //     $tramite->save();

    //     return response()->json([
    //         'ok' => true,
    //         'message' => 'Trámite enviado a la bandeja de concelajes'
    //     ]);
    // }
}
