<?php

namespace Modules\Tramites\Http\Controllers;

use App\ArchivosModel;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\NotificacionesController;
use Modules\Tramites\Entities\AcuerdosModel;
use Modules\Tramites\Entities\AnexosModel;
use Modules\Tramites\Entities\direccionesModel;
use Modules\Tramites\Entities\TipoTramiteModel;
use Modules\Tramites\Entities\TramAlcaldiaAsiganrModel;
use Modules\Tramites\Entities\TramAlcaldiaCabDetaModel;
use Modules\Tramites\Entities\TramAlcaldiaCabModel;

class TramAlcaldiaProcesosController extends Controller
{
    public $configuraciongeneral = ['Gestión de trámites internos', 'tramitesinternos.store', 'tramitesinternos.form', 'crear', '', 6 => ''];

    const ID_PERFIL_SECRETARIA = [20, 80];

    const ID_DESPACHO_ALCALDIA = 8;

    const DIRECTORES = [
        0 => 3,
        1 => 9,
        2 => 5,
        3 => 16,
        4 => 42,
        5 => 22,
        6 => 25,
        7 => 37,
        8 => 15,
        9 => 21,
        11 => 52,
        12 => 54
      ];

    protected $Notificacion;
    protected $tramites_cab;
    private $archivos_path;

    public function __construct(NotificacionesController $Notificacion, TramAlcaldiaCabController $tramites_cab)
    {
        $this->middleware('auth');
        $this->archivos_path = public_path('archivos_sistema');
        $this->Notificacion = $Notificacion;
        $this->tramites_cab = $tramites_cab;
    }


    public function tramitesNotificar()
    {
        $objetos = json_decode($this->tramites_cab->objetos);

        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);

        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);

        unset($objetos[26]);
        unset($objetos[27]);

        $tabla = [];
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral = $this->tramites_cab->configuraciongeneral;
        $configuraciongeneral[0] = "Trámites para notificar de forma física";
        $configuraciongeneral[6] = "tramites/tramitesnotificarajax";
        $imprimir = "si";

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => [],
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no",
            "imprimir" => $imprimir,
        ]);
    }

    public function tramitesNotificarAjax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'fecha_ingreso',
            2 => 'numtramite',
            3 => 'remitente',
            4 => 'asunto',
            5 => 'peticion',
            6 => 'direccion_atender',
            7 => 'fecha_fin',
            8 => 'recomendacion',
            9 => 'disposicion',
            10 => 'observacion',
            12 => 'archivo',
            13 => 'fecha_respuesta',
            14 => 'acciones',
        );

        $perfiles = collect(self::DIRECTORES);
        $esDirector = $perfiles->contains(Auth::user()->id_perfil);
        $idDireccion = Auth::user()->id_direccion;
        if($esDirector &&  $idDireccion == 21)
        {
            $query = TramAlcaldiaCabModel::whereHas('asignados', function($q) use($idDireccion) {
                $q->where(['id_direccion' => $idDireccion, 'estado' => 'ASIGNADO']);
            })->whereNull('correo_electronico');
        }
        else
        {
            $query = TramAlcaldiaCabModel::where(['disposicion' => 'APROBADA', 'estado_proceso' => 1])
            ->whereNull('correo_electronico');
        }

        $totalData = $query->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')))
        {
            if ($limit == '-1')
            {
                $posts = $query->groupBy('id')->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->groupBy('id')->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
        }
        else
        {
            $search = $request->input('search.value');

            $posts = TramAlcaldiaCabModel::where(["estado" => "ACT", "estado_proceso" => 1])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->whereNull('correo_electronico')->groupBy('id')->offset($start)
                ->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = TramAlcaldiaCabModel::where([["estado", "ACT"], ["estado_proceso", "=", 1]])
                ->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('remitente', 'LIKE', "%{$search}%")
                        ->orWhere('asunto', 'LIKE', "%{$search}%");
                })->whereNull('correo_electronico')->count();
        }

        $data = array();

        if (!empty($posts)) {
            foreach ($posts as $post) {
                if($esDirector &&  $idDireccion == 21)
                {
                    $aciones = link_to_route(str_replace("/", ".", $this->tramites_cab->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp
                    <a href='" . URL::to('tramites/finalizartramite?id=' . $post->id)."'><i class='fa fa-pencil-square-o'></i></a>";
                }
                elseif(Auth::user()->id_perfil == User::SECRETARIO_GENERAL)
                {
                    $aciones = link_to_route(str_replace("/", ".", $this->tramites_cab->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp
                    <a href='" . URL::to('tramites/finalizartramite?id=' . $post->id) . "&revision=true'><i class='fa fa-pencil-square-o'></i></a>";
                }
                else
                {
                    $aciones = link_to_route(str_replace("/", ".", $this->tramites_cab->configuraciongeneral[7]) . '.show', '', array($post->id), array('class' => 'fa fa-newspaper-o divpopup', 'target' => '_blank', 'onclick' => 'popup(this)')) . "&nbsp;&nbsp";
                }

                $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->first();
                if(!$archivo) $adjunto = '';
                else $adjunto = "<a href='" . URL::to('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";

                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $archivo_finalizacion = "<a href='" . URL::to('/archivos_sistema/' . $post->archivo_finalizacion) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank' onclick='popup(this)'><i class='fa fa-file-pdf-o'></i></a>";

                $nestedData['id'] = $post->id;
                $nestedData['cedula'] = $post->cedula;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                $direc = str_replace('|', ',', $post->direccion_atender);
                $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                if ($post->direccion_informar) {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                } else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin;
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['fecha_respuesta'] = $post->fecha_respuesta;
                $nestedData['archivo'] = $adjunto;
                $nestedData['archivo_finalizacion'] = $archivo_finalizacion;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
        );

        return response()->json($json_data);
    }

    public function enviarDonacion(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $asignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id_direccion
            ])->whereIn('estado', ['ASIGNADO', 'PENDIENTE'])->first();

            $asignado->estado = 'ARCHIVADO';
            $asignado->respondido = 1;
            $asignado->observacion = $request->observacion;
            $asignado->save();

            $tramite->donaciones = 1;
            $tramite->save();

            $this->guardarTimeline($id, $tramite, 'Trámite archivado, enviado a donación', 0, 'ARCHIVADO');

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            // $nombre = isset($direccion->direccion) ? $direccion->direccion : Auth::user()->name;
            $nombre = Auth::user()->id_perfil == 55 ? Auth::user()->name : $direccion->direccion;
            $mensaje = 'El trámite N° ' . $tramite->numtramite . ' ha sido archivado por parte de ' . $nombre;
            $ruta = 'tramites/tramitesarchivados';

            if(isset($tramite->creado_por))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->creado_por, '2c438f');
            }
            if(isset($tramite->asignado_a))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->asignado_a, '2c438f');
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'El trámite se ha guardado como donación correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar el trámite, inténtelo de nuevo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function devolverVentanilla(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::find($id);
            $tramite->disposicion = 'FINALIZADO';
            $tramite->estado_proceso = 2;
            $tramite->observacion = $request->observacion;
            $tramite->save();

            $idtl = $this->guardarTimeline($id, $tramite, 'Trámite finalizado');
            $timeline = TramAlcaldiaCabDetaModel::find($idtl);
            $timeline->recomendacion = 'Trámite finalizado';
            $timeline->observacion = 'Trámite finalizado';
            $timeline->recomendacion = $request->observacion;
            $timeline->disposicion = 'FINALIZADO';
            $timeline->save();

            $mensaje = 'Sr.(a) ' . $tramite->remitente . ' Su trámite N°: ' . $tramite->numtramite . ' ha sido rechazado. ' . $request->observacion;
            $this->Notificacion->EnviarEmailTram($tramite->correo_electronico, 'Trámite Rechazado', '', $mensaje);

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha devuelto el trámite al ciudadano'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo realizar la acción',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function tramitesIngresadoPortal()
    {
        $analistas = $this->tramites_cab->obtenerAnalistas(Auth::user()->id_direccion);
        $this->configuraciongeneral[4] = 'Trámites ingresados';
        return view('tramites::tramitesexternos.tramites', [
            'configuraciongeneral' => $this->configuraciongeneral,
            'analistas' => $analistas
        ]);
    }

    public function tramitesIngresadoPortalAjax(Request $request)
    {
        if(Auth::user()->id_perfil == 50 || Auth::user()->id_perfil == 57 || Auth::user()->id_perfil == 1)
        {
            $tramites = TramAlcaldiaCabModel::with('area', 'tipoTramite')
            ->whereIn('tipo', ['TRAMITES MUNICIPALES', 'SECRETARIA GENERAL']);
        }
        else
        {
            $tramites = TramAlcaldiaCabModel::whereHas('area', function($q) {
                $q->where(['id_direccion' => Auth::user()->id_direccion]);
            })->whereHas('asignados', function($q) {
                $q->where(['estado' => 'PENDIENTE']);
            })->where(['tipo' => 'TRAMITES MUNICIPALES'])->with('area', 'tipoTramite');
        }

        return DataTables::of($tramites)->editColumn('tipo_tramite', function($tramite){
            return $tramite->tipo_tramite == null ? '' : $tramite->tipoTramite->valor;
        })->addColumn('origen', function($tramite) {
            return $tramite->id_usuario_externo == null ? 'PORTAL CIUDADANO' : 'PERMISOS MUNICIPALES';
        })->addColumn('atender', function($tramite) {
            $atender = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $tramite->id])
            // ->where('asig.estado', '<>', 'CON COPIA')->get();
            ->whereIn('asig.estado', ['PENDIENTE', 'REVISION-COORDINADOR', 'DEVUELTO-COORDINADOR', 'DEVUELTO'])->get();

            return $atender->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');
        })->addColumn('action', function($tramite) {
            return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>
            <a href="' . route('tramite.editfinalizar') . "?id=$tramite->id" . '">
            <i class="fa fa-pencil-square-o"></i></a>';
        })->addColumn('analistas', function($tramite) {
            $direcciones = explode('|', $tramite->direccion_atender);
            $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['dira.id_cab' => $tramite->id])
            ->whereIn('dira.direccion_solicitante', $direcciones)
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado')->get();

            return $analistas->map(function($item, $key) {
                $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                return "{$item->name} ({$estado})";
            })->implode(', ');

        })->filter(function($query) use($request) {
            if($request->has('analista') && $request->analista != '')
            {
                $query->whereHas('asignados', function($q) use($request) {
                    $q->where(['id_direccion' => $request->analista]);
                });
            }
            if($request->has('estado') && $request->estado != '')
            {
                $query->whereHas('asignados', function($q) use($request) {
                    $q->where(['estado' => $request->estado]);
                });
            }
            if ($request->has('numtramite') && $request->numtramite != '')
            {
                $query->where('numtramite', 'LIKE', "%$request->numtramite%");
            }
        })->make(true);
    }

    public function tramitesFinalizadosPortal()
    {
        $this->configuraciongeneral[4] = 'Trámites finalizados';
        return view('tramites::tramitesexternos.tramitesfinalizados', [
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    public function tramitesFinalizadosPortalAjax()
    {
        $id = Auth::user()->id;
        $tramites = TramAlcaldiaCabModel::whereHas('area', function($q) {
            $q->where(['id_direccion' => Auth::user()->id_direccion]);
        })->whereHas('asignados', function($q) {
            $q->where(['estado' => 'CONTESTADO']);
        })->where(['tipo' => 'TRAMITES MUNICIPALES', 'estado' => 'ACT'])->with('area', 'tipoTramite');

        return DataTables::of($tramites)->addColumn('analistas', function($tramite) {
            $analistas = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as dira')
            ->join('users as u', 'dira.id_direccion', 'u.id')
            ->where(['dira.id_cab' => $tramite->id, 'dira.direccion_solicitante' => Auth::user()->id_direccion])
            ->whereIn('dira.estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])
            ->select('u.name', 'dira.estado')->get();

            return $analistas->map(function($item, $key) {
                $estado = $item->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO';
                return "{$item->name} ({$estado})";
            })->implode(', ');
        })->addColumn('adjuntos', function($tramite) {
            $adjuntos = AnexosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id, 'usuario_id' => 1])->get();
            $archivos = '';

            foreach($adjuntos as $adjunto)
            {
                $archivos .= '<a href="' . asset('archivos_sistema/' . $adjunto->ruta) . '" class="btn btn-success btn-xs dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                        <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            return $archivos;
        })->addColumn('archivo', function($tramite) use($id) {
            if($id == 33)
            {
                $archivosEnviados = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])
                ->whereIn('id_usuario', [1878, $id])->get();
            }
            else
            {
                $archivosEnviados = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id, 'id_usuario' => Auth::user()->id])->get();
            }

            $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.created_at', 'ax.ruta', 'u.name')
            ->where(['tipo' => 11, 'id_tramite_externo' => $tramite->id, 'ax.usuario_id' => Auth::user()->id])
            ->get();

            $archivosEnviados = array_merge($archivosEnviados->toArray(), $archivos_->toArray());

            $archivo = '';

            if(isset($tramite->archivo_finalizacion))
            {
                $archivo = '<a href="' . asset('archivos_sistema/' . $tramite->archivo_finalizacion) . '" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            elseif(count($archivosEnviados) > 0)
            {
                if(count($archivosEnviados) == 1)
                {
                    $path = Str::contains($archivosEnviados[0]['ruta'], 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                    $archivo = '<a href="' . URL::to($path . $archivosEnviados[0]['ruta']) . '" class="btn btn-success dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                    <i class="fa fa-file-pdf-o"></i></i></a>';
                }
                else
                {
                    foreach($archivosEnviados as $archivo_)
                    {
                        $path = Str::contains($archivo_['ruta'], 'Tramite_interno') ? '/archivos_firmados/' : '/archivos_sistema/';
                        $archivo .= '<a href="' . URL::to($path . $archivo_['ruta']) . '" class="btn btn-success btn-xs dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                        <i class="fa fa-file-pdf-o"></i></i></a>';
                    }
                }
            }
            else $archivo = '';

            return $archivo;
        })->addColumn('origen', function($tramite) {
            return $tramite->id_usuario_externo == null ? 'PORTAL CIUDADANO' : 'PERMISOS MUNICIPALES';
        })->addColumn('action', function($tramite) {
            return '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
            <i class="fa fa-newspaper-o"></i></a>';
        })->editColumn('tipoTramite.valor', function($tramite) {
            return isset($tramite->tipoTramite->valor) ? $tramite->tipoTramite->valor : '';
        })->rawColumns(['archivo', 'action', 'adjuntos'])->make(true);
    }

    public function finalizarImcompleto(Request $request, $tramite)
    {
        DB::beginTransaction();
        try
        {
            $guardar = TramAlcaldiaCabModel::findOrFail($tramite);

            $docs = $request->archivo_finalizacion;
            $dir = public_path() . '/archivos_sistema/';
            $usuario = Auth::user();
            $perfil = Auth::user()->id_perfil;
            $redireccionar = null;

            $perfiles = collect(self::DIRECTORES);
            $esDirector = $perfiles->contains(Auth::user()->id_perfil);

            // if($perfil == self::ID_PERFIL_DIRECTOR_PLAN || $perfil == self::ID_PERFIL_DIRECTOR
            if($esDirector && $perfil != 54)
            {
                $redireccionar = '/tramites/tramitesrevision';
                $asignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $tramite,
                    'id_direccion' => Auth::user()->id_direccion
                ])->first();

                // $direccionesAsignadas = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite])
                // ->whereNull('direccion_solicitante')->count();
                $direccionesAsignadas = explode('|', $guardar->direccion_atender);
                $direccionesAsignadas = count($direccionesAsignadas);

                $direccionesAsignadasContestados = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite, 'estado' => 'CONTESTADO'])
                ->whereNull('direccion_solicitante')->count();

                $total = $direccionesAsignadas - $direccionesAsignadasContestados;

                if($total == 1)
                {
                    $asignado->respondido = 2;
                    $asignado->estado = 'CONTESTADO';
                    $asignado->save();

                    // $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite, 'id_usuario' => $usuario->id])->first();
                    $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite, 'id_usuario' => $usuario->id])->get();
                    if(count($archivo) == 0)
                    {
                        return response()->json([
                            'ok' => false,
                            'message' => 'Debe subir su informe de este trámite para poder responder al ciudadano'
                        ]);
                    }

                    TramAlcaldiaCabModel::where([["id", $guardar->id], ['estado', 'ACT']])
                        ->update(['estado_proceso' => 2, 'disposicion' => 'FINALIZADO', 'fecha_respuesta' => now()]);

                    $ventanilla = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                        ->select("users.id")->whereIn("ap.id", self::ID_PERFIL_SECRETARIA)->get();
                    foreach ($ventanilla as $key => $venta)
                    {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
                    }

                    $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                    ->select("users.id")->where("ap.id", User::ID_SECRETARIA_GENERAL)->get();
                    foreach ($usuariosByperfil as $key => $value)
                    {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
                    }

                    $direcciones_id = explode('|', $guardar->direccion_atender);
                    $direcciones_informar = $guardar->direccion_informar ? explode('|', $guardar->direccion_informar) : null;
                    $direcciones_informe = TramAlcaldiaAsiganrModel::where(['id_cab' => $guardar->id, 'estado' => 'INFORME-RESPONDIDO'])->get();

                    if($direcciones_informar != null)
                    {
                        $direcciones_id = array_merge($direcciones_id, $direcciones_informar);
                    }
                    if(count($direcciones_informe) >= 1)
                    {
                        $array_dirIn = array();
                        foreach($direcciones_informe as $dirIn)
                        {
                            $direcciones_id = array_merge($direcciones_id, $dirIn->id_direccion);
                            // $array_dirIn = array_push($array_dirIn, $dirIn);
                        }
                    }

                    $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
                    $mensaje = 'El trámite número ' . $guardar->numtramite . ' ha finalizado.';
                    $ruta = 'tramites/tramitesfinalizados';

                    foreach ($directores as $key => $value)
                    {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido finalizado', $ruta, $value->id, '2c438f');
                        $this->Notificacion->EnviarEmail($value->email, 'Trámite Finalizado', '', $mensaje, $ruta, 'vistas.emails.email');
                    }

                    $direcciones_ = explode('|', $guardar->direccion_atender);

                    if($guardar->correo_electronico != null)
                    {
                        $mensaje = 'Sr.(a) ' . $guardar->remitente . ' su trámite N° ' . $guardar->numtramite . ' ha sido finalizado, gracias por la espera';
                        // $this->Notificacion->EnviarEmailTramiteFin($guardar->correo_electronico, 'Trámite Finalizado', '', $mensaje, $pdf);
                        $this->Notificacion->EnviarEmailTramiteFinArray($guardar->correo_electronico, 'Trámite Finalizado', '', $mensaje, $archivo);
                    }

                    $idtl = $this->guardarTimeline($tramite, $guardar, 'Trámite finalizado');
                    $timeline = TramAlcaldiaCabDetaModel::find($idtl);
                    $timeline->recomendacion = 'Trámite finalizado';
                    $timeline->observacion = 'Trámite finalizado';
                    $timeline->disposicion = 'FINALIZADO';
                    $timeline->save();

                    DB::commit();
                    return response()->json([
                        'ok' => true,
                        'message' => 'Se ha enviado la respuesta al ciudadano correctamente.',
                        'redireccionar' => $redireccionar
                    ]);
                }
                else
                {
                    if($asignado->respondido == 2)
                    {
                        return response()->json([
                            'ok' => false,
                            'message' => 'Su respuesta ya fue enviada al ciudadano'
                        ]);
                    }

                    $asignado->respondido = 2;
                    $asignado->estado = 'CONTESTADO';
                    $asignado->save();

                    $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite, 'id_usuario' => $usuario->id])->first();
                    if(!$archivo)
                    {
                        return response()->json([
                            'ok' => false,
                            'message' => 'Debe subir su informe de este trámite para poder responder al ciudadano'
                        ]);
                    }
                    $pdf = public_path() . '/archivos_sistema/' . $archivo->ruta;
                }
            }
            else
            {
                $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite, 'id_usuario' => $usuario->id])->get();
                $contestados = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $tramite,
                    'estado' => 'CONTESTADO',
                    'respondido' => 1
                ])->get();

                if(count($contestados) == 0)
                {
                    return response()->json([
                        'ok' => false,
                        'message' => 'Ya ha sido enviada la respuesta al ciudadano'
                    ]);
                }
                else if(count($archivo) > 0)
                {
                    $direcciones_id = [];
                    foreach($contestados as $direccion)
                    {
                        $direcciones_id = array_merge($direcciones_id, [$direccion->id_direccion]);
                        $direccion->respondido = 2;
                        $direccion->estado = 'CONTESTADO';
                        $direccion->save();
                    }

                    $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
                    $mensajeDirector = 'Se ha enviado su respuesta al ciudadano del trámite número ' . $guardar->numtramite;
                    $ruta = 'tramites/tramitesrevision';

                    foreach ($directores as $value)
                    {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $guardar->numtramite . ' ha sido contestado', $ruta, $value->id, '2c438f');
                        $this->Notificacion->EnviarEmail($value->email, 'Trámite Contestado', '', $mensajeDirector, $ruta, 'vistas.emails.email');
                    }
                }
                else
                {
                    return response()->json([
                        'ok' => false,
                        'message' => 'Debe subir al menos un archivo para poder responder al ciudadano'
                    ]);
                }
                $pdf = public_path() . '/archivos_sistema/' . $guardar->archivo_finalizacion;
            }

            $this->guardarTimeline($tramite, $guardar, 'Trámite en proceso de gestión');

            if($guardar->correo_electronico != null)
            {
                $mensaje = 'Sr.(a) ' . $guardar->remitente . ' su trámite N° ' . $guardar->numtramite . ' ha sido contestado, gracias por la espera.';
                // $this->Notificacion->EnviarEmailTramiteFinArray('darquiroz@gmail.com', 'Trámite Finalizado Parcial', '', $mensaje, $archivo);
                $correoGestor = ConfigSystem('correo_gestor');
                $this->Notificacion->EnviarEmailTramiteFinArray($correoGestor, 'Trámite Finalizado', '', $mensaje, $archivo);
                $this->Notificacion->EnviarEmailTramiteFinArray($guardar->correo_electronico, 'Trámite Finalizado', '', $mensaje, $archivo);
                // $this->Notificacion->EnviarEmailTramiteFin($guardar->correo_electronico, 'Trámite contestado', '', $mensaje, $pdf);
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha enviado la respuesta al ciudadano correctamente.',
                'redireccionar' => $redireccionar
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo enviar la respuesta al ciudadano',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }

    }

    public function conCopia()
    {
        $objetos = json_decode($this->tramites_cab->objetos);
        $configuraciongeneral = $this->tramites_cab->configuraciongeneral;
        $configuraciongeneral[0] = 'Trámites con copia';
        $configuraciongeneral[1] = 'tramites/concopia';
        $configuraciongeneral[6] = "tramites/tramitesconcopiasajax";

        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[26]);
        unset($objetos[27]);

        $objetos = array_values($objetos);
        $tabla = [];

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no"
        ]);
    }

    public function tramitesConCopiasAjax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'numtramite',
            2 => 'fecha_ingreso',
            3 => 'tipo_tramite',
            4 => 'asunto'
        );

        if(Auth::user()->id_perfil == 55)
        {
            $query = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
            ->join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'cab.id')
            ->select('cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
            ->where(['cab.estado' => 'ACT', 'da.estado' => 'CON COPIA'])
            ->where('disposicion', '<>', 'FINALIZADO')
            ->whereIn('da.id_direccion', [76, 77]);
        }
        else
        {
            $query = TramAlcaldiaCabModel::from('tram_peticiones_cab as cab')
            ->join('tram_peticiones_direccion_asignada as da', 'da.id_cab', 'cab.id')
            ->select('cab.*', 'da.respondido', 'da.id_direccion', 'da.estado as estado_asignacion')
            ->where(['cab.estado' => 'ACT', 'da.id_direccion' => Auth::user()->id_direccion, 'da.estado' => 'CON COPIA'])
            ->where('disposicion', '<>', 'FINALIZADO');
        }

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')))
        {
            // $posts = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            $posts = $query->limit($limit)->orderBy($order, $dir)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $posts = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('fecha_ingreso', 'LIKE', "%{$search}%");
            })->offset($start)->limit($limit)->orderBy($order, $dir)->get();

            $totalFiltered = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('fecha_ingreso', 'LIKE', "%{$search}%");
            })->count();
        }

        $data = array();
        if (!empty($posts))
        {
            foreach ($posts as $post)
            {
                $tipo_tramite = TipoTramiteModel::find($post->tipo_tramite);
                $acciones = "<a href='" . url('tramites/finalizartramite?id=' . $post->id) . "'><i class='fa fa-pencil-square-o'></i></a>";

                $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $post->id])->get();
                $adjunto = '';
                foreach ($archivos as $idx => $archivo)
                {
                    $adjunto .= "<a href='" . url('/archivos_sistema/' . $archivo->ruta) . "' class='btn btn-success dropdown-toggle divpopup' target='_blank'><i class='fa fa-file-pdf-o'></i></a>";
                    if ($idx % 2 == 0) $adjunto .= '&nbsp;&nbsp;';
                }

                $nestedData['id'] = $post->id;
                $nestedData['cedula_remitente'] = $post->cedula_remitente;
                $nestedData['tipo_tramite'] = $tipo_tramite->valor;
                $nestedData['telefono'] = $post->telefono;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['remitente'] = $post->remitente;
                $nestedData['correo_electronico'] = $post->correo_electronico;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['peticion'] = $post->peticion;
                $nestedData['prioridad'] = $post->prioridad;
                if($post->direccion_atender)
                {
                    $direc = str_replace('|', ',', $post->direccion_atender);
                    $direciones_text = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_atender from tmae_direcciones where id in (' . $direc . ') ');
                    $nestedData['direccion_atender'] = $direciones_text[0]->direccion_atender;
                }
                else $nestedData['direccion_atender'] = '';
                if($post->direccion_informar)
                {
                    $direc_inf = str_replace('|', ',', $post->direccion_informar);
                    $direciones_text_inf = DB::select('select GROUP_CONCAT(direccion SEPARATOR ",") as direccion_informar from tmae_direcciones where id in (' . $direc_inf . ') ');
                    $nestedData['direccion_informar'] = $direciones_text_inf[0]->direccion_informar;
                }
                else $nestedData['direccion_informar'] = '';
                $nestedData['fecha_fin'] = $post->fecha_fin ?? '';
                $nestedData['recomendacion'] = $post->recomendacion;
                $nestedData['disposicion'] = $post->disposicion;
                $nestedData['observacion'] = $post->observacion;
                $nestedData['archivo'] = $adjunto;
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }

        return response()->json([
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ]);
    }

    public function tramitesReservados()
    {
        $objetos = json_decode($this->tramites_cab->objetos);
        $configuraciongeneral = $this->tramites_cab->configuraciongeneral;
        $configuraciongeneral[0] = 'Trámites reservados';
        $configuraciongeneral[1] = 'tramites/reservados';
        $configuraciongeneral[6] = "tramites/tramitesreservadosajax";

        unset($objetos[2]);
        unset($objetos[3]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[8]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        unset($objetos[14]);
        unset($objetos[15]);
        unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[18]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[25]);
        unset($objetos[26]);
        unset($objetos[27]);
        // dd($objetos);

        $objetos = array_values($objetos);
        $tabla = [];

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => null,
            "edit" => 'no',
            "verid" => null,
            "create" => "no"
        ]);
    }

    public function tramitesReservadosAjax(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'numtramite',
            2 => 'fecha_ingreso',
            3 => 'tipo_tramite',
            4 => 'asunto'
        );

        $query = TramAlcaldiaCabModel::where(['estado' => 'ACT', 'disposicion' => 'RESERVADO', 'creado_por' => Auth::user()->id]);

        $totalData = $query->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value')))
        {
            if ($limit == '-1')
            {
                $posts = $query->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }
        }
        else
        {
            $search = $request->input('search.value');

            if ($limit == '-1')
            {
                $posts = $query->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('fecha_ingreso', 'LIKE', "%{$search}%");
                })->limit($limit)->orderBy($order, $dir)->get();
            }
            else
            {
                $posts = $query->where(function ($query) use ($search) {
                    $query->where('numtramite', 'LIKE', "%{$search}%")
                        ->orWhere('fecha_ingreso', 'LIKE', "%{$search}%");
                })->offset($start)->limit($limit)->orderBy($order, $dir)->get();
            }

            $totalFiltered = $query->where(function ($query) use ($search) {
                $query->where('numtramite', 'LIKE', "%{$search}%")
                    ->orWhere('fecha_ingreso', 'LIKE', "%{$search}%");
            })->count();
        }

        $data = array();
        if (!empty($posts))
        {
            foreach ($posts as $post)
            {
                $acciones = "<a href='" . url('tramites/tramites/' . $post->id. '/edit') . "'><i class='fa fa-pencil-square-o'></i></a>";
                $nestedData['id'] = $post->id;
                $nestedData['numtramite'] = $post->numtramite;
                $nestedData['fecha_ingreso'] = $post->fecha_ingreso;
                $nestedData['asunto'] = $post->asunto;
                $nestedData['acciones'] = $acciones;
                $data[] = $nestedData;
            }
        }

        return response()->json([
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ]);
    }

    public function reservarTramite(Request $request)
    {
        DB::beginTransaction();
        try
        {
            $tramite = new TramAlcaldiaCabModel;
            $tramite->numtramite = $request->numtramite;
            $tramite->asunto = $request->asunto;
            $tramite->peticion = $request->peticion;
            $tramite->cedula_remitente = $request->cedula_remitente;
            $tramite->remitente = $request->remitente;
            $tramite->telefono = $request->telefono;
            $tramite->correo_electronico = $request->correo_electronico;
            $tramite->id_parroquia = $request->id_parroquia;
            $tramite->id_barrio = $request->id_barrio;
            $tramite->direccion = $request->direccion;
            $tramite->referencia = $request->referencia;
            $tramite->prioridad = $request->prioridad;
            $tramite->observacion_general = $request->observacion_general;
            $tramite->creado_por = Auth::user()->id;
            $tramite->id_usuario = Auth::user()->id;
            $tramite->disposicion = 'RESERVADO';
            $tramite->fecha_ingreso = date('Y-m-d');
            $tramite->save();

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Trámite reservado correctamente'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo reservar el trámite, inténtelo de nuevo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }


    /**
     * ACCIONES DEL COORDINADOR
     */

    public function enviarAprobacion(Request $request, $id)
    {
        if($request->ajax())
        {
            DB::beginTransaction();
            try
            {
                $req = Crypt::decrypt($request->parametros);
                $req = explode('-', $req);
                $id_tramite = $req[0];
                $id_direccion = $req[1];
                $area = $request->area;
                // dd($id_direccion);
                $tramite = TramAlcaldiaCabModel::findOrFail($id_tramite);
                $tramite->disposicion = 'APROBADA';
                $tramite->finalizar_por = $area;
                $tramite->save();

                // $direccionActual = TramAlcaldiaAsiganrModel::where(["id_cab" => $id_tramite, 'id_direccion' => $id_direccion])->first();
                // $direccionActual->respondido = 1;
                // $direccionActual->estado = 'CONTESTADO';
                // $direccionActual->observacion = $tramite->observacion;
                // $direccionActual->save();
                $direccionesAsignadas = TramAlcaldiaAsiganrModel::where(["id_cab" => $id_tramite, 'estado' => 'REVISION-COORDINADOR'])->get();
                foreach($direccionesAsignadas as $da)
                {
                    $da->respondido = 1;
                    $da->estado = 'CONTESTADO';
                    $da->save();
                }

                $notifica = TramAlcaldiaAsiganrModel::Select(DB::raw("COUNT(*) as total"))->where([["id_cab", $tramite->id], ['respondido', '=', 0]])->first();
                $perfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("users.id");

                if ($notifica->total == 0)
                {
                    $usuariosByperfil = $area == 'GENERAL' ?
                        $perfil->where("ap.id", User::ID_SECRETARIA_GENERAL)->get()
                        : $perfil->where("ap.id", self::ID_DESPACHO_ALCALDIA)->get();

                    foreach ($usuariosByperfil as $key => $value)
                    {
                        $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' se encuentra disponible para su revisión', 'tramites/finalizartramite?id=' . $tramite->id . '&revision=true', $value->id, '2c438f');
                    }

                    if($tramite->correo_electronico)
                    {
                        $mensaje = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' se encuentra en proceso de gestión.';
                        $this->Notificacion->EnviarEmailTram($tramite->correo_electronico, 'Trámite en Proceso de Gestión', '', $mensaje);
                    }
                }

                $director = User::directores()->where('users.id_direccion', $id_direccion)->get();
                $ruta = 'tramites/tramitesrevision';
                $mensaje = "Se ha aprobado el trámite N° $tramite->numtramite por parte del coordinador";

                foreach($director as $dir)
                {
                    $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $dir->id, '2c438f');
                    $this->Notificacion->EnviarEmail($dir->email, 'Trámite aprobado', '', $mensaje, $ruta, 'vistas.emails.email');
                }

                $timeline_id = $this->guardarTimeline($id, $tramite, 'Trámite en proceso de gestión');
                $timeline = TramAlcaldiaCabDetaModel::find($timeline_id);
                $timeline->disposicion = 'RESPONDIDO';
                $timeline->save();

                DB::commit();

                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha aprobado el trámite'
                ]);
            }
            catch(\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => 'No se pudo realizar la petición, inténtelo de nuevo',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }

        }
        else abort(401);
    }

    /**
     * ACCIONES DIRECCIONES
     */

    public function solicitarInforme($id)
    {
        if(request()->ajax())
        {
            DB::beginTransaction();
            try
            {
                $tramite = TramAlcaldiaCabModel::findOrFail($id);
                // $tramite->observacion = request()->observacion;
                // $tramite->save();

                $direccionesInformar = request()->direcciones;
                $unidades = collect(User::UNIDADES);
                $usuariosUnidades = collect(User::USUARIOS_UNIDADES);

                foreach($direccionesInformar as $di)
                {
                    $informe = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                    ->join('tmae_direcciones as dir', 'dir.id', 'asig.id_direccion')
                    ->join('tmae_direcciones as dirs', 'dirs.id', 'asig.direccion_solicitante')
                    ->select('dir.direccion as asignada', 'dirs.direccion as solicitante')
                    ->where(['id_cab' => $id, 'id_direccion' => $di])
                    ->whereIn('asig.estado', ['INFORME'])->first();

                    if($informe)
                    {
                        return response()->json([
                            'ok' => false,
                            'message' => "No puede asignar a {$informe->asignada} ya que fue asignada para informe por parte de {$informe->solicitante}"
                        ]);
                    }

                    $unidad = $unidades->contains($di);
                    if($unidad)
                    {
                        $index = $unidades->search($di);
                        $usuario = $usuariosUnidades[$index];
                        $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => $usuario])
                        ->whereIn('estado', ['ANALISTA', 'ANALISTA-CONTESTADO'])->first();

                        if($asignado)
                        {
                            $direccion = direccionesModel::find($di, ['direccion']);
                            return response()->json([
                                'ok' => false,
                                'message' => "No puede asignar a {$direccion->direccion}, ya está asignada para contestar este trámite"
                            ]);
                        }
                        $area = 'SI';
                    }
                    else $area = 'NO';
                }

                $informar = direccionesModel::whereIn('id', $direccionesInformar)->pluck('direccion');
                $enviarInformar = $informar->implode(' - ');

                $this->guardarTimeline($id, $tramite, 'Solicitar informe de otras áreas. ' . $enviarInformar);

                $direccionSolicitante = direccionesModel::where('id', Auth::user()->id_direccion)->first();

                $direccionAsignada = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => Auth::user()->id_direccion
                ])->first();

                if(!$direccionAsignada)
                {
                    $direccionAsignada = TramAlcaldiaAsiganrModel::where([
                        'id_cab' => $id,
                        'id_direccion' => Auth::user()->id
                    ])->first();
                }

                $direccionAsignada->observacion = request()->observacion;
                $direccionAsignada->save();

                $analista = collect(User::ANALISTAS)->contains(Auth::user()->id_perfil);
                $estado = $analista ? 'ANALISTA-INFORME' : 'INFORME';
                $solicitante = $analista ? Auth::user()->id : Auth::user()->id_direccion;

                if($analista)
                {
                    $mensaje = Auth::user()->name . ' le ha solicitado informe sobre el trámite N° ' . $tramite->numtramite;
                }
                else
                {
                    $mensaje = 'La dirección de '. $direccionSolicitante->direccion .', le ha solicitado informe sobre el trámite N° ' . $tramite->numtramite;
                }

                $ruta = 'tramites/finalizartramite?id=' . $tramite->id;

                $direccionesCopia = collect(explode('|', $tramite->direccion_informar));
                foreach($direccionesInformar as $value)
                {
                    $unidad = $unidades->contains($value);
                    if($unidad)
                    {
                        $index = $unidades->search($value);
                        $area = 'SI';
                    }
                    else $area = 'NO';

                    $esCopia = $direccionesCopia->contains($value);
                    if($esCopia)
                    {
                        $key = $direccionesCopia->search($value);
                        $direccionesCopia->forget($key);
                        $tramite->direccion_informar = $direccionesCopia->implode('|');
                        $tramite->save();
                    }

                    TramAlcaldiaAsiganrModel::updateOrCreate([
                        'id_cab' => $id,
                        'id_direccion' => $value], [
                        'direccion_solicitante' => $solicitante,
                        'estado' => $estado
                    ]);

                    if($area == 'SI')
                    {
                        $director = User::find($usuariosUnidades[$index], ['id', 'email']);
                        $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                        $this->Notificacion->EnviarEmail($director->email, 'Informe de Trámite', '', $mensaje, $ruta, 'vistas.emails.email');
                    }
                    else
                    {
                        $directores = User::where(['id_direccion' => $value])
                        ->whereIn('id_perfil', User::DIRECTORES)->get();

                        foreach($directores as $director)
                        {
                            $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                            $this->Notificacion->EnviarEmail($director->email, 'Informe de Trámite', '', $mensaje, $ruta, 'vistas.emails.email');
                        }
                    }
                }

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha notificado correctamente a las direcciones seleccionadas.'
                ]);
            }
            catch(\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => 'No se pudo notificar a las direcciones seleccionada, inténtelo de nuevo.',
                    'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        }
        else abort(401);
    }

    public function guardarInforme($id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);
            $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id, 'id_usuario' => Auth::user()->id])->get();
            $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                    ->join('users as u', 'ax.usuario_id', 'u.id')
                    ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                    ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => Auth::user()->id])->get();
            $archivos = array_merge($archivos->toArray(), $archivos_->toArray());

            if(count($archivos) == 0)
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'Debe subir al menos un archivo para responder.'
                ]);
            }

            if(Auth::user()->id_perfil == 55)
            {
                $asignadoInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.alias', 'asig.*')
                ->where(['id_cab' => $id])
                ->whereIn('id_direccion', [76, 77])
                ->whereNotNull('direccion_solicitante')->first();
            }
            else
            {
                $asignadoInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.alias', 'asig.*')
                ->where(['id_cab' => $id, 'id_direccion' => Auth::user()->id_direccion])
                ->whereNotNull('direccion_solicitante')->first();
            }

            $estadoInforme = $asignadoInforme->estado == 'ANALISTA-INFORME' ? 'ANALISTA-INFORME-RESPONDIDO' : 'INFORME-RESPONDIDO';

            $asignadoInforme->observacion = request()->observacion;
            $asignadoInforme->estado = $estadoInforme;
            $asignadoInforme->respondido = 1;
            $asignadoInforme->save();

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            $this->guardarTimeline($id, $tramite, 'Respuesta a informe solicitado: ' . $direccion->direccion);

            $directores = User::directores()
                ->where('users.id_direccion', $asignadoInforme->direccion_solicitante)->get();

            $ruta = 'tramites/finalizartramite?id=' . $tramite->id;
            $mensaje = 'La dirección de '. $asignadoInforme->alias .', ha respondido al informe solicitado sobre el trámite N° ' . $tramite->numtramite;

            if($estadoInforme == 'ANALISTA-INFORME-RESPONDIDO')
            {
                $direccionEnviar = User::findOrFail($asignadoInforme->direccion_solicitante, ['name']);
                $message = 'Se ha enviado su respuesta a  ' . $direccionEnviar->name;
                $analista = User::findOrFail($asignadoInforme->direccion_solicitante, ['id', 'email']);
                $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $analista->id, '2c438f');
                $this->Notificacion->EnviarEmail($analista->email, 'Respuesta a informe solicituda', '', $mensaje, $ruta, 'vistas.emails.email');
            }
            else
            {
                $direccionEnviar = direccionesModel::findOrFail($asignadoInforme->direccion_solicitante, ['direccion']);
                $message = 'Se ha enviado su respuesta a la dirección de ' . $direccionEnviar->direccion;

                foreach($directores as $director)
                {
                    if(Str::contains($director->cargo, 'DIRECTOR') || Str::contains($director->cargo, 'PROCURADOR') || Str::contains($director->cargo, 'SECRETARIO'))
                    {
                        $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                        $this->Notificacion->EnviarEmail($director->email, 'Respuesta a informe solicituda', '', $mensaje, $ruta, 'vistas.emails.email');
                    }
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => $message
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo guardar, inténtelo de nuevo.',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function solicitudesInforme()
    {
        $objetos = json_decode($this->tramites_cab->objetos);
        unset($objetos[2]);
        unset($objetos[5]);
        unset($objetos[6]);
        unset($objetos[7]);
        unset($objetos[9]);
        unset($objetos[10]);
        unset($objetos[11]);
        unset($objetos[12]);
        unset($objetos[13]);
        // unset($objetos[16]);
        unset($objetos[17]);
        unset($objetos[19]);
        unset($objetos[20]);
        unset($objetos[21]);
        unset($objetos[22]);
        unset($objetos[23]);
        unset($objetos[24]);
        unset($objetos[26]);
        unset($objetos[27]);

        $estado = '[{"Tipo":"text","Descripcion":"Estado","Nombre":"estado","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" }]';
        $estado = json_decode($estado);
        $objetos = array_merge($objetos, $estado);

        $tabla = [];
        $edit = 'no';
        $verid = null;
        $editfinalizartramite = 'si';

        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("ap.tipo")->where("users.id", Auth::user()->id)->first();

        if ($id_tipo_pefil->tipo == 2) {
            $editfinalizartramite = null;
        }

        $configuraciongeneral = $this->tramites_cab->configuraciongeneral;
        $configuraciongeneral[0] = 'Solicitudes de informe';
        $configuraciongeneral[6] = 'tramites/tramitesAlcaldiaDespachadosajax?estado=INFORME';
        $imprimir = 'si';

        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $configuraciongeneral,
            "delete" => "no",
            "edit" => $edit,
            "verid" => $verid,
            "create" => "no",
            "editfinalizartramite" => $editfinalizartramite,
            "imprimir" => $imprimir,
        ]);
    }

    public function desvincularTramite($id)
    {
        if(request()->ajax())
        {
            DB::beginTransaction();
            try
            {
                $tramite = TramAlcaldiaCabModel::findOrFail($id);
                $direcciones = explode('|', $tramite->direccion_atender);
                $direccion = Auth::user()->id_direccion;
                $direccion_asignada = direccionesModel::findOrFail($direccion);

                foreach($direcciones as $idx => $dir)
                {
                    if($dir == $direccion || (Auth::user()->id_perfil == 55 && ($dir == 76 || $dir == 77)))
                    {
                        unset($direcciones[$idx]);
                    }
                }

                $direcciones = implode('|', $direcciones);
                $tramite->direccion_atender = $direcciones;
                $tramite->disposicion = 'PENDIENTE';
                $tramite->estado_proceso = 0;
                $tramite->save();

                $this->guardarTimeline($tramite->id, $tramite, 'Trámite no corresponde a la dirección asignada', request()->observacion);

                $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => $direccion])->update([
                    'estado' => 'DESVINCULADO',
                    'observacion' => request()->observacion
                ]);

                if(!$asignado)
                {
                    $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => Auth::user()->id])->update([
                        'estado' => 'DESVINCULADO',
                        'observacion' => request()->observacion
                    ]);
                }

                $ruta = 'tramites/despachartramite?id=' . $tramite->id . '&estado=1';
                $mensaje = 'La dirección de ' . $direccion_asignada->alias . ' se ha desvinculado del trámite N° ' . $tramite->numtramite . 'por la siguiente razón: ' . request()->observacion;
                $mensajeWeb = 'La dirección de ' . $direccion_asignada->alias . ' se ha desvinculado del trámite N° ' . $tramite->numtramite;

                // $usuarios = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                //     ->select("users.id", "users.email")
                //     ->where("ap.id", self::ID_SECRETARIA_GENERAL)->get();
                $usuario = User::find($tramite->asignado_a);
                $this->Notificacion->notificacionesweb($mensajeWeb . '', $ruta, $usuario->id, '2c438f');
                $this->Notificacion->EnviarEmail($usuario->email, 'Trámite Asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                // foreach($usuarios as $usuario)
                // {
                //     $this->Notificacion->notificacionesweb($mensajeWeb . '', $ruta, $usuario->id, '2c438f');
                //     $this->Notificacion->EnviarEmail($usuario->email, 'Trámite Asignado', '', $mensaje, $ruta, 'vistas.emails.email');
                // }

                DB::commit();
                return response()->json([
                    'ok' => true,
                    'message' => 'Se ha desvinculado del trámite N° ' . $tramite->numtramite
                ]);
            }
            catch(\Exception $ex)
            {
                DB::rollback();
                return response()->json([
                    'ok' => false,
                    'message' => $ex->getMessage() . ' - ' . $ex->getLine()
                ]);
            }
        }
        else abort(401);
    }

    public function finalizarConPago(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            if($request->observacion == null || $request->valorCancelar == null)
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'La observación y el valor a pagar son requeridos'
                ]);
            }
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id]);
            $usuario = User::find(Auth::user()->id);

            $archivo = $archivo->where('id_usuario', $usuario->id)->get();

            if(count($archivo) > 0)
            {
                $mensajeCiudadano = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, con un valor a pagar de $'.number_format($request->valorCancelar, 2) . '. ' . $request->observacion;
            }
            else
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'No se puede finalizar el trámite hasta que suba su informe'
                ]);
            }

            $tramite->disposicion = 'FINALIZADO PENDIENTE DE PAGO';
            $tramite->estado_proceso = 2;
            $tramite->finalizar_por = $request->tipo;
            $tramite->observacion = $request->observacion;
            $tramite->fecha_respuesta = now();
            $tramite->save();

            $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => $tramite->direccion_atender])->first();
            $asignado->respondido = 1;
            $asignado->estado = 'CONTESTADO';
            $asignado->observacion = $request->observacion  . '. Con un valor a cancelar de $' . $request->valorCancelar;
            $asignado->save();

            $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                ->select("users.id")->where("ap.id", User::ID_SECRETARIA_GENERAL)->get();

            $ventanilla = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("users.id")->whereIn("ap.id", self::ID_PERFIL_SECRETARIA)->get();

            foreach($ventanilla as $key => $venta)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
            }

            foreach($usuariosByperfil as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $value->id, '2c438f');
            }

            // $this->Notificacion->EnviarEmailTramiteFinArray('darquiroz@gmail.com', 'Trámite Finalizado Por Pagar', '', $mensajeCiudadano, $archivo);
            $correoGestor = ConfigSystem('correo_gestor');
            $this->Notificacion->EnviarEmailTramiteFinArray($correoGestor, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);

            if($tramite->correo_electronico != null)
            {
                $this->Notificacion->EnviarEmailTramiteFinArray($tramite->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);
            }

            $this->guardarTimeline($id, $tramite, 'Trámite finalizado con valores a cancelar de $' . $request->valorCancelar);

            $direcciones_id = explode('|', $tramite->direccion_atender);
            $direcciones_informar = $tramite->direccion_informar ? explode('|', $tramite->direccion_informar) : null;
            $direcciones_informe = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite->id, 'estado' => 'INFORME-RESPONDIDO'])->get();

            if($direcciones_informar != null)
            {
                $direcciones_id = array_merge($direcciones_id, $direcciones_informar);
            }
            if(count($direcciones_informe) >= 1)
            {
                foreach($direcciones_informe as $dirIn)
                {
                    $direcciones_id = array_merge($direcciones_id, [$dirIn->id_direccion]);
                }
            }

            $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
            $mensaje = 'El trámite número ' . $tramite->numtramite . ' ha finalizado.';
            $ruta = 'tramites/tramitesfinalizados';

            foreach($directores as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', $ruta, $value->id, '2c438f');
                $this->Notificacion->EnviarEmail($value->email, 'Trámite Finalizado', '', $mensaje, $ruta, 'vistas.emails.email');
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Trámite finalizado correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' =>false,
                'message' => 'No se pudo finalizar el trámite, inténtelo de nuevo',
                'exception' => $ex-> getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function finalizarTramiteDirector($id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id]);

            $usuario = User::find(Auth::user()->id);
            $MessageMe = '';

            if(($usuario->esDirector() || Auth::user()->id_direccion == 1) && isset($tramite->correo_electronico))
            {
                $MessageMe = 'Trámite Finalizado por Director';

                $archivo = $archivo->where('id_usuario', $usuario->id)->get(['id', 'id_usuario', 'ruta']);
                $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                    ->join('users as u', 'ax.usuario_id', 'u.id')
                    ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                    ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario->id])->get();
                $archivo = array_merge($archivo->toArray(), $archivos_->toArray());

                if(count($archivo) > 0)
                {
                    $mensajeCiudadano = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera';
                    // $pdf = public_path() . '/archivos_sistema/' . $archivo->ruta;
                }
                else
                {
                    return response()->json([
                        'ok' => false,
                        'message' => 'No se puede finalizar el trámite hasta que suba su informe'
                    ]);
                }
            }
            else if(Auth::user()->id_perfil == User::SECRETARIO_GENERAL)
            {
                $MessageMe = 'Trámite Finalizado por SG sin correo';
                $archivo = $archivo->where('id_usuario', $usuario->id)->get();
                $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                    ->join('users as u', 'ax.usuario_id', 'u.id')
                    ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                    ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario->id])->get();
                $archivo = array_merge($archivo->toArray(), $archivos_->toArray());

                if(count($archivo) == 0)
                {
                    return response()->json([
                        'ok' => false,
                        'message' => 'No se puede finalizar el trámite hasta que suba su informe'
                    ]);
                }

                $mensajeCiudadano = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera';
            }
            else
            {
                $MessageMe = 'Trámite Finalizado por Otro';
                $archivo = $archivo->where('id_usuario', $usuario->id)->get();
                $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
                    ->join('users as u', 'ax.usuario_id', 'u.id')
                    ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
                    ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario->id])->get();
                $archivo = array_merge($archivo->toArray(), $archivos_->toArray());

                $mensajeCiudadano = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera';
            }

            $dirAsignadas = explode('|', $tramite->direccion_atender);

            $asignados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'estado' => 'CONTESTADO'])
            ->whereIn('id_direccion', $dirAsignadas)->count();

            if(count($dirAsignadas) == ($asignados + 1) || count($dirAsignadas) == $asignados)
            {
                $tramite->disposicion = 'FINALIZADO';
                $tramite->estado_proceso = 2;
                $tramite->finalizar_por = request()->tipo;
                $tramite->observacion = request()->observacion;
                $tramite->fecha_respuesta = now();
                $tramite->save();
            }

            if(Auth::user()->id_perfil != User::SECRETARIO_GENERAL)
            {
                $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => Auth::user()->id_direccion])->first();
                $asignado->respondido = 1;
                $asignado->estado = 'CONTESTADO';
                $asignado->observacion = request()->observacion;
                $asignado->save();
            }

            $usuariosByperfil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
                ->select("users.id")->whereIn("ap.id", User::ID_SECRETARIA_GENERAL)->get();

            $ventanilla = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")
            ->select("users.id")->whereIn("ap.id", self::ID_PERFIL_SECRETARIA)->get();

            foreach ($ventanilla as $key => $venta)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $venta->id, '2c438f');
            }

            foreach ($usuariosByperfil as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $value->id, '2c438f');
            }

            // $this->Notificacion->EnviarEmailTramiteFin($tramite->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $pdf);
            // $this->Notificacion->EnviarEmailTramiteFinArray('darquiroz@gmail.com', $MessageMe, '', $mensajeCiudadano, $archivo);
            $correoGestor = ConfigSystem('correo_gestor');
            $this->Notificacion->EnviarEmailTramiteFinArray($correoGestor, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);

            if($tramite->correo_electronico != null)
            {
                $this->Notificacion->EnviarEmailTramiteFinArray($tramite->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);
            }

            $this->guardarTimeline($id, $tramite, 'Trámite finalizado');

            $direcciones_id = explode('|', $tramite->direccion_atender);
            $direcciones_informar = $tramite->direccion_informar ? explode('|', $tramite->direccion_informar) : null;
            $direcciones_informe = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite->id, 'estado' => 'INFORME-RESPONDIDO'])->get();

            if($direcciones_informar != null)
            {
                $direcciones_id = array_merge($direcciones_id, $direcciones_informar);
            }
            if(count($direcciones_informe) >= 1)
            {
                $array_dirIn = array();
                foreach($direcciones_informe as $dirIn)
                {
                    // $array_dirIn = array_push($array_dirIn, $dirIn);
                    $direcciones_id = array_merge($direcciones_id, [$dirIn->id_direccion]);
                }
            }

            $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
            $mensaje = 'El trámite número ' . $tramite->numtramite . ' ha finalizado.';
            $ruta = 'tramites/tramitesfinalizados';

            foreach ($directores as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', $ruta, $value->id, '2c438f');
                $this->Notificacion->EnviarEmail($value->email, 'Trámite Finalizado', '', $mensaje, $ruta, 'vistas.emails.email');
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Trámite finalizado correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' =>false,
                'message' => 'No se pudo finalizar el trámite, inténtelo de nuevo',
                'exception' => $ex-> getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function finalizarTramiteSubdirector(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $usuario = User::find(Auth::user()->id, ['id']);
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $archivo = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $tramite->id])
                ->where('id_usuario', $usuario->id)->get(['id', 'id_usuario', 'ruta']);

            $archivos_ = AnexosModel::from('tmov_tram_anexos as ax')
            ->join('users as u', 'ax.usuario_id', 'u.id')
            ->select('ax.id', 'ax.nombre', 'ax.usuario_id as id_usuario', 'ax.ruta')
            ->where(['tipo' => 11, 'id_tramite_externo' => $id, 'usuario_id' => $usuario->id])->get();

            $archivo = array_merge($archivo->toArray(), $archivos_->toArray());
            $MessageMe = 'Trámite Finalizado por Subdirector';

            if(count($archivo) > 0)
            {
                $mensajeCiudadano = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera';
            }
            else
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'No se puede finalizar el trámite hasta que suba su respuesta'
                ]);
            }

            $dirAsignadas = explode('|', $tramite->direccion_atender);
            $asignados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'estado' => 'CONTESTADO'])
            ->whereIn('id_direccion', $dirAsignadas)->count();

            $analistasAsignados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id])
                ->whereIn('estado', ['ANALISTA'])
                ->where('id_direccion', '<>' , Auth::user()->id)->count();

            $asignadoSubdirector = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => $usuario->id,
                'estado' => 'ANALISTA'
            ])->first();

            if(count($dirAsignadas) == ($asignados + 1) && $analistasAsignados == 0)
            {
                $tramite->disposicion = 'FINALIZADO';
                $tramite->estado_proceso = 2;
                $tramite->finalizar_por = $request->tipo;
                $tramite->observacion = $request->observacion;
                $tramite->fecha_respuesta = now();
                $tramite->save();

                $director = User::obtenerDirector();

                $asignadoDirector = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id,
                    'id_direccion' => $director['id_direccion']
                ])->whereIn('estado', ['PENDIENTE', 'ASIGNADO'])->first();

                $asignadoDirector->respondido = 1;
                $asignadoDirector->estado = 'CONTESTADO';
                $asignadoDirector->save();

                $this->guardarTimeline($id, $tramite, 'Trámite finalizado');

                $asignadoSubdirector->respondido = 1;
            }
            else
            {
                $this->guardarTimeline($id, $tramite, 'Respuesta enviada al ciudadano');
                $asignadoSubdirector->respondido = 2;
            }


            $asignadoSubdirector->estado = 'ANALISTA-CONTESTADO';
            $asignadoSubdirector->observacion = $request->observacion;
            $asignadoSubdirector->save();

            $notificar = [$tramite->asignado_a, $tramite->creado_por];

            foreach ($notificar as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', 'tramites/tramitesfinalizados', $value, '2c438f');
            }

            // $this->Notificacion->EnviarEmailTramiteFinArray('darquiroz@gmail.com', $MessageMe, '', $mensajeCiudadano, $archivo);
            $correoGestor = ConfigSystem('correo_gestor');
            $this->Notificacion->EnviarEmailTramiteFinArray($correoGestor, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);

            if($tramite->correo_electronico != null)
            {
                $this->Notificacion->EnviarEmailTramiteFinArray($tramite->correo_electronico, 'Trámite Finalizado', '', $mensajeCiudadano, $archivo);
            }

            $direcciones_id = explode('|', $tramite->direccion_atender);
            $direcciones_informar = $tramite->direccion_informar ? explode('|', $tramite->direccion_informar) : null;
            $direcciones_informe = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite->id, 'estado' => 'INFORME-RESPONDIDO'])->get();

            if($direcciones_informar != null)
            {
                $direcciones_id = array_merge($direcciones_id, $direcciones_informar);
            }
            if(count($direcciones_informe) >= 1)
            {
                $array_dirIn = array();
                foreach($direcciones_informe as $dirIn)
                {
                    $direcciones_id = array_merge($direcciones_id, [$dirIn->id_direccion]);
                }
            }

            $directores = User::directores()->whereIn('users.id_direccion', $direcciones_id)->get();
            $mensaje = 'El trámite número ' . $tramite->numtramite . ' ha finalizado.';
            $ruta = 'tramites/tramitesfinalizados';

            foreach ($directores as $key => $value)
            {
                $this->Notificacion->notificacionesweb('El trámite N° ' . $tramite->numtramite . ' ha sido finalizado', $ruta, $value->id, '2c438f');
                // $this->Notificacion->EnviarEmail($value->email, 'Trámite Finalizado', '', $mensaje, $ruta, 'vistas.emails.email');
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Trámite finalizado correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' =>false,
                'message' => 'No se pudo finalizar el trámite, inténtelo de nuevo',
                'exception' => $ex-> getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function enviarRespuestaCiudadano(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            if($request->devolver == 'false')
            {
                $archivos = ArchivosModel::where(['tipo' => 10, 'id_referencia' => $id, 'id_usuario' => Auth::user()->id])->get();
                if(count($archivos) == 0)
                {
                    return response()->json([
                        'ok' => false,
                        'message' => 'Debe subir al menos un archivo para enviar la respuesta al ciudadano'
                    ]);
                }

                $mensaje = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera.';
                $this->Notificacion->EnviarEmailTramiteFinArray('darquiroz@gmail.com', 'Trámite Finalizado PM', '', $mensaje, $archivos);
                $this->Notificacion->EnviarEmailTramiteFinArray($tramite->correo_electronico, 'Trámite Finalizado', '', $mensaje, $archivos);
            }
            else
            {
                $mensaje = $request->observacion;
                $this->Notificacion->EnviarEmailTram('darquiroz@gmail.com', 'Trámite devuelto PM', '', $mensaje);
                $this->Notificacion->EnviarEmailTram($tramite->correo_electronico, 'Trámite devuelto', '', $mensaje);
            }

            $tramite->disposicion = 'FINALIZADO';
            $tramite->id_usuario = Auth::user()->id;
            $tramite->estado_proceso = 2;
            $tramite->fecha_respuesta = now();


            // if($request->archivo_fin)
            // {
            //     $docs = $request->archivo_fin;
            //     $dir = public_path() . '/archivos_sistema/';

            //     if ($docs)
            //     {
            //         $fileName = "Finalizacion_tramite-$id-" . date("YmdHis") . "." . $docs->getClientOriginalExtension(); //$docs->getClientOriginalName();
            //         $ext = $docs->getClientOriginalExtension();
            //         $perfiles = explode("|", ConfigSystem("archivospermitidos"));

            //         if (!in_array($ext, $perfiles))
            //         {
            //             DB::rollback();
            //             $mensaje = "No se admite el tipo de archivo " . strtoupper($ext);
            //             return redirect()->back()->withErrors([$mensaje])->withInput();
            //         }

            //         $oldfile = $dir . $tramite->archivo_finalizacion;
            //         File::delete($oldfile);
            //         $tramite->archivo_finalizacion = $fileName;
            //         $docs->move($dir, $fileName);
            //     }

            //     $mensaje = 'Sr.(a) ' . $tramite->remitente . ' su trámite N° ' . $tramite->numtramite . ' ha sido finalizado, gracias por la espera.';
            //     $pdf = public_path() . '/archivos_sistema/' . $tramite->archivo_finalizacion;
            //     $this->Notificacion->EnviarEmailTramiteFin($tramite->correo_electronico, 'Trámite Finalizado', '', $mensaje, $pdf);
            // }
            // else
            // {
            //     $mensaje = $request->observacion;
            //     $this->Notificacion->EnviarEmailTram($tramite->correo_electronico, 'Trámite devuelto', '', $mensaje);
            // }

            $tramite->save();

            $asignado = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'id_direccion' => Auth::user()->id_direccion])->first();
            $asignado->respondido = 1;
            $asignado->estado = 'CONTESTADO';
            $asignado->observacion = $request->observacion;
            $asignado->updated_at = now();
            $asignado->save();

            $idtl = $this->guardarTimeline($id, $tramite, 'Trámite finalizado', 0, 'FINALIZADO');
            if($request->devolver == 'true')
            {
                $timeline = TramAlcaldiaCabDetaModel::find($idtl);
                $timeline->recomendacion = $request->observacion;
                $timeline->save();
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha enviado la respuesta al ciudadano correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo enviar la respuesta al ciudadano.',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function obtenerTiposTramite($area)
    {
        if(request()->ajax())
        {
            if($area == 'OTROS')
            {
                $tiposTramite = collect(['id' => 'OTROS', 'dias_despachar' => 0, 'id_area' => 'EP', 'valor' => 'OTROS']);
            }
            else if($area == 'EP')
            {
                $tiposTramite = collect(['id' => 'OTROS', 'dias_despachar' => 0, 'id_area' => 'EP', 'valor' => 'EP']);
            }
            else
            {
                $tiposTramite = TipoTramiteModel::where(['estado' => 'ACT', 'id_area' => $area])
                ->orderBy('valor')->get();
            }

            return response()->json([
                'ok' => true,
                'tiposTramite' => $tiposTramite
            ]);

        } else abort(401);
    }

    public function archivarTramite(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);
            $esImpugnacion = collect(User::IMPUGNACIONES)->contains($tramite->tipo_tramite);
            $otroTipo = collect(User::OTRO_TIPO)->contains($tramite->tipo_tramite);

            $asignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id, 'estado' => 'ASIGNADO',
                'id_direccion' => Auth::user()->id_direccion
            ])->first();

            if(!$asignado)
            {
                $asignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id, 'estado' => 'ASIGNADO',
                    'id_direccion' => Auth::user()->id
                ])->first();
            }

            $asignado->estado = 'ARCHIVADO';
            $asignado->respondido = 1;
            $asignado->observacion = $request->observacion;
            $asignado->save();

            $direccionesAtender = explode('|', $tramite->direccion_atender);
            $contestados = TramAlcaldiaAsiganrModel::where(['id_cab' => $id, 'respondido' => 1])
            ->whereIn('id_direccion', $direccionesAtender)->count();

            if(count($direccionesAtender) == $contestados)
            {
                $tramite = TramAlcaldiaCabModel::findOrFail($id);
                $tramite->disposicion = 'FINALIZADO';
                $tramite->finalizar_por = 'DIRECTOR';
                $tramite->fecha_respuesta = now();
                $tramite->estado_proceso = 2;
                $tramite->save();
            }

            $this->guardarTimeline($id, $tramite, 'Trámite archivado', 0, 'ARCHIVADO');

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            // $nombre = isset($direccion->direccion) ? $direccion->direccion : Auth::user()->name;
            $nombre = Auth::user()->id_perfil == 55 ? Auth::user()->name : $direccion->direccion;
            $mensaje = 'El trámite N° ' . $tramite->numtramite . ' ha sido archivado por parte de ' . $nombre;
            $ruta = 'tramites/tramitesarchivados';

            if(isset($tramite->creado_por))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->creado_por, '2c438f');
            }
            if(isset($tramite->asignado_a))
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $tramite->asignado_a, '2c438f');
            }

            if($tramite->correo_electronico != null)
            {
                $correoGestor = ConfigSystem('correo_gestor');
                $correo = config('app.env') == 'local' ? 'darquiroz@gmail.com' : $tramite->correo_electronico;
                if($esImpugnacion)
                {
                    $mensaje = 'Estimado(a) ciudadano(a) su trámite '. $tramite->numtramite .' ha sido atendido, en los próximos días podrá consultar en la página https://manta.gob.ec/multas-de-transito/';
                    $this->Notificacion->EnviarEmailTram($correo, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                    $this->Notificacion->EnviarEmailTram($correoGestor, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                }
                else if($otroTipo)
                {
                    $mensaje = 'Estimado(a) ciudadano(a) su trámite '. $tramite->numtramite .' ha sido atendido, usted puede retirar en horarios laborables la resolución en nuestra ventanilla en la dirección municipal de Tránsito para que continúe con su proceso.';
                    $this->Notificacion->EnviarEmailTram($correo, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                    $this->Notificacion->EnviarEmailTram($correoGestor, 'Trámite Finalizado', '', $mensaje, 'vistas.email', 'NO');
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'se ha archivado el trámite correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo archivar el trámite, inténtelo de nuevo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function devolverTramiteInforme(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            if(Auth::user()->id_perfil == 55)
            {
                $asignadoInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.alias', 'dir.direccion', 'asig.*')
                ->where(['id_cab' => $id])
                ->whereIn('id_direccion', [76, 77])
                ->whereNotNull('direccion_solicitante')->first();
            }
            else
            {
                $asignadoInforme = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
                ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
                ->select('dir.alias', 'dir.direccion', 'asig.*')
                ->where(['id_cab' => $id, 'id_direccion' => Auth::user()->id_direccion])
                ->whereNotNull('direccion_solicitante')->first();
            }

            $estadoInforme = $asignadoInforme->estado == 'ANALISTA-INFORME' ? 'ANALISTA-INFORME-DESVINCULADO' : 'INFORME-DESVINCULADO';

            $asignadoInforme->observacion = request()->observacion;
            $asignadoInforme->estado = $estadoInforme;
            $asignadoInforme->respondido = 1;
            $asignadoInforme->save();

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            $this->guardarTimeline($id, $tramite, 'Solicitud de informe desvinculado: ' . $direccion->direccion);

            $directores = User::where(['id_direccion' => $asignadoInforme->direccion_solicitante])
            ->whereIn('id_perfil', User::DIRECTORES)->get();

            $ruta = 'tramites/finalizartramite?id=' . $tramite->id;
            $mensaje = 'La dirección de '. $asignadoInforme->direccion .', se ha desvinculado a la solicitud de informe sobre el trámite N° ' . $tramite->numtramite;

            if($estadoInforme == 'ANALISTA-INFORME-DESVINCULADO')
            {
                // $direccionEnviar = User::findOrFail($asignadoInforme->direccion_solicitante, ['name']);
                // $message = 'Se ha enviado su respuesta a  ' . $direccionEnviar->name;
                $analista = User::findOrFail($asignadoInforme->direccion_solicitante, ['id', 'email']);
                $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $analista->id, '2c438f');
                $this->Notificacion->EnviarEmail($analista->email, 'Respuesta a informe solicituda', '', $mensaje, $ruta, 'vistas.emails.email');
            }
            else
            {
                // $direccionEnviar = direccionesModel::findOrFail($asignadoInforme->direccion_solicitante, ['direccion']);
                // $message = 'Se ha enviado su respuesta a la dirección de ' . $direccionEnviar->direccion;

                foreach($directores as $director)
                {
                    if(Str::contains($director->cargo, 'DIRECTOR') || Str::contains($director->cargo, 'PROCURADOR') || Str::contains($director->cargo, 'SECRETARIO'))
                    {
                        $this->Notificacion->notificacionesweb($mensaje . '', $ruta, $director->id, '2c438f');
                        $this->Notificacion->EnviarEmail($director->email, 'Respuesta a informe solicituda', '', $mensaje, $ruta, 'vistas.emails.email');
                    }
                }
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'Se ha desvinculado de la solicitud de informe del trámite N° ' . $tramite->numtramite . ' correctamente'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo desvincular del trámite',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function archivarTramiteInforme(Request $request, $id)
    {
        DB::beginTransaction();
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($id);

            $asignado = TramAlcaldiaAsiganrModel::where([
                'id_cab' => $id,
                'id_direccion' => Auth::user()->id_direccion
            ])->whereIn('estado', ['INFORME', 'ANALISTA-INFORME'])->first();

            if($asignado->estado == 'INFORME')
            {
                $solicitante = User::directores()->where('users.id_direccion', $asignado->direccion_solicitante)->get();
            }
            else
            {
                $solicitante = User::where(['id' => $asignado->direccion_solicitante])->get(['id', 'name', 'cedula']);
            }

            if(!$asignado)
            {
                $asignado = TramAlcaldiaAsiganrModel::where([
                    'id_cab' => $id, 'estado' => 'ASIGNADO',
                    'id_direccion' => Auth::user()->id
                ])->first();
            }

            $estado = $asignado->estado == 'INFORME' ? 'ARCHIVADO' : 'ANALISTA-ARCHIVADO';
            $asignado->estado = $estado;
            $asignado->respondido = 1;
            $asignado->observacion = $request->observacion;
            $asignado->save();

            $this->guardarTimeline($id, $tramite, 'Trámite archivado', 0, 'ARCHIVADO');

            $direccion = direccionesModel::find(Auth::user()->id_direccion);
            // $nombre = isset($direccion->direccion) ? $direccion->direccion : Auth::user()->name;
            $nombre = Auth::user()->id_perfil == 55 ? Auth::user()->name : $direccion->direccion;
            $mensaje = 'El trámite N° ' . $tramite->numtramite . ' ha sido archivado por parte de ' . $nombre;
            $ruta = 'tramites/finalizartramite?id='.$id;

            foreach($solicitante as $dir)
            {
                $this->Notificacion->notificacionesweb($mensaje, $ruta, $dir->id, '2c438f');
            }

            DB::commit();
            return response()->json([
                'ok' => true,
                'message' => 'se ha archivado el trámite correctamente.'
            ]);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo archivar el trámite, inténtelo de nuevo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function tramitesArchivados()
    {
        $this->configuraciongeneral[4] = 'Trámites archivados';
        $this->configuraciongeneral[0] = 'Trámites archivados';
        return view('tramites::tramitesexternos.archivados', [
            'configuraciongeneral' => $this->configuraciongeneral
        ]);
    }

    public function tramitesArchivadosAjax()
    {
        $perfil = Auth::user()->id_perfil;
        $perfilesRevision = collect([50, 57]);
        $revisar = $perfilesRevision->contains(Auth::user()->id_perfil);
        $sg = collect(User::ID_SECRETARIA_GENERAL)->contains($perfil);
        $ingresaTramites = collect(self::ID_PERFIL_SECRETARIA)->contains(Auth::user()->id_perfil);

        // if($perfil == self::ID_PERFIL_SECRETARIA || $perfil == self::ID_SECRETARIA_GENERAL || $perfil == self::SECRETARIO_GENERAL || $revisar)
        // if($perfil == self::ID_PERFIL_SECRETARIA || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar)
        if($ingresaTramites || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar)
        {
            $tramites = TramAlcaldiaAsiganrModel::with('cabecera.tipoTramite')->where(['tram_peticiones_direccion_asignada.estado' => 'ARCHIVADO']);
        }
        elseif($perfil == 55)
        {
            $tramites = TramAlcaldiaCabModel::whereHas('asignados', function($q) {
                $q->where(['id_direccion' => Auth::user()->id, 'estado' => 'ARCHIVADO']);
            })->with('area', 'tipoTramite', 'asignados')->where(['estado' => 'ACT']);
        }
        else
        {
            $tramites = TramAlcaldiaCabModel::whereHas('asignados', function($q) {
                $q->where(['id_direccion' => Auth::user()->id_direccion])
                ->whereIn('estado', ['ARCHIVADO', 'ANALISTA-ARCHIVADO']);
            })->with('area', 'tipoTramite', 'asignados')->where(['estado' => 'ACT']);
        }

        return DataTables::of($tramites)->addColumn('direccion_atender', function($tramite) use($perfil, $revisar, $sg, $ingresaTramites) {
            // $id_cab = $perfil == self::ID_PERFIL_SECRETARIA || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar
            $id_cab = $ingresaTramites || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar
            ? $tramite->cabecera->id : $tramite->id;

            $asignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $id_cab])
            ->where('asig.estado', '<>', 'CON COPIA')->get();

            $direcciones = explode('|', $tramite->direccion_atender);

            if(count($asignados) != count($direcciones))
            {
                $asignados = direccionesModel::whereIn('id', $direcciones)->select('direccion')->get();
            }

            return $asignados->map(function($asignado) {
                return $asignado->direccion;
            })->implode(' - ');

        })->addColumn('tipoTramite', function($tramite) use($perfil, $revisar, $sg, $ingresaTramites) {
            // if($perfil == self::ID_PERFIL_SECRETARIA || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar)
            if($ingresaTramites || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar)
            {
                $tipoTram = $tramite->cabecera->tipoTramite->valor;
            }
            else $tipoTram = $tramite->tipoTramite->valor;

            return $tipoTram;
        })->addColumn('archivado_por', function($tramite) use($perfil, $revisar, $sg, $ingresaTramites) {
            // $id_cab = $perfil == self::ID_PERFIL_SECRETARIA || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar
            $id_cab = $ingresaTramites || $sg || $perfil == User::SECRETARIO_GENERAL || $revisar
            ? $tramite->cabecera->id : $tramite->id;
            $asignados = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('tmae_direcciones as dir', 'asig.id_direccion', 'dir.id')
            ->select('dir.direccion')
            ->where(['asig.id_cab' => $id_cab])
            ->whereIn('asig.estado', ['ARCHIVADO', 'ANALISTA-ARCHIVADO'])->get();

            $asignados_2 = TramAlcaldiaAsiganrModel::from('tram_peticiones_direccion_asignada as asig')
            ->join('users as u', 'asig.id_direccion', 'u.id')
            ->select('u.name as direccion')
            ->where(['asig.id_cab' => $id_cab, 'asig.estado' => 'ARCHIVADO', 'u.id_perfil' => 55])->get();

            $asignados = collect(array_merge($asignados->toArray(), $asignados_2->toArray()));
            return $asignados->map(function($asignado) {
                return $asignado['direccion'];
            })->implode(' - ');
        })->addColumn('action', function($tramite) use($revisar) {
            if($revisar)
            {
                $accion = '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a> <a href="' . route('tramite.editfinalizar') . "?id=$tramite->id" . '">
                <i class="fa fa-pencil-square-o"></i></a>';
            }
            else
            {
                $accion = '<a href="' . route('tramites.show', ['id' => $tramite->id]) . '" class="divpopup" onclick="popup(this)" target="_blank">
                <i class="fa fa-newspaper-o"></i></a>';
            }
            return $accion;
        })->addColumn('archivo', function($tramite) {
            $archivo = ArchivosModel::where(['id_referencia' => $tramite->id, 'tipo' => 10])->first();
            return isset($archivo->ruta) ? '<a href="' . asset('archivos_sistema/' . $archivo->ruta) . '" class="btn btn-success btn-sm dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                    <i class="fa fa-file-pdf-o"></i></i></a>' : '';
        })->addColumn('archivos_director', function($tramite) {
            $archivos = ArchivosModel::where(['id_referencia' => $tramite->id, 'tipo' => 10, 'id_usuario' => Auth::user()->id])
                ->get(['ruta']);

            $archivos_ = AnexosModel::where(['tipo' => 11, 'id_tramite_externo' => $tramite->id])
                ->get(['ruta']);

            $archivos = array_merge($archivos->toArray(), $archivos_->toArray());
            $archivos_director = '';

            foreach($archivos as $archivo)
            {
                $path = str_contains($archivo['ruta'], 'Tramite_interno') ? 'archivos_firmados/' : 'archivos_sistema/';
                $archivos_director .= '<a href="' . asset($path . $archivo['ruta']) . '" class="btn btn-success btn-xs dropdown-toggle divpopup" target="_blank" onclick="popup(this)">
                <i class="fa fa-file-pdf-o"></i></i></a>';
            }
            return $archivos_director;
        })->rawColumns(['archivo', 'action', 'archivos_director'])->make(true);
    }

    public function guardarTimeline($id, $data, $observacion = null, $estado = 0, $disposicion = null):int
    {
        $timeline = new TramAlcaldiaCabDetaModel;
        $timeline->numtramite = $data->numtramite;
        // $timeline->recomendacion = 'Solicitud ingresada satisfactoriamente';
        $timeline->observacion = $observacion;
        $timeline->remitente = $data->remitente;
        $timeline->asunto = $data->asunto;
        $timeline->peticion = $data->peticion;
        $timeline->prioridad = $data->prioridad;
        $timeline->direccion_atender = $data->direccion_atender;
        $timeline->fecha_fin = $data->fecha_fin;
        $timeline->estado_proceso = $estado;
        $timeline->disposicion = $disposicion == null ? $data->disposicion : $disposicion;
        // $timeline->observacion = $data->observacion;
        $timeline->id_usuario = Auth::user()->id;
        $timeline->ip = request()->ip();
        $timeline->pc = request()->getHttpHost();
        $timeline->id_tram_cab = $id;
        $timeline->archivo = $data->archivo;
        $timeline->fecha_ingreso = $data->fecha_ingreso;
        if($observacion == 'Trámite en proceso de gestión')
        {
            $timeline->fecha_respuesta = now();
        }
        $timeline->save();

        return $timeline->id;
    }

    public function eliminarArchivo(Request $request)
    {
        try
        {
            $tabla = $request->tipo == 11 ? AnexosModel::find($request->id) : ArchivosModel::find($request->id);
            $ruta = public_path() . '/archivos_sistema/' . $tabla->ruta;

            if (file_exists($ruta))
            {
                unlink($ruta);
            }

            $tabla->delete();

            return response()->json([
                'ok' => true,
                'message' => 'Archivo eliminado'
            ]);
        }
        catch(\Exception $ex) {
            return response()->json([
                'ok' => false,
                'message' => 'El archivo no pudo ser eliminado',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function eliminarArchivoAlcance(Request $request)
    {
        try
        {
            $tabla = AnexosModel::find($request->id);

            $ruta = public_path() . '/archivos_sistema/' . $tabla->ruta;

            if (file_exists($ruta))
            {
                unlink($ruta);
            }

            $tabla->delete();

            return response()->json([
                'ok' => true,
                'message' => 'Archivo eliminado'
            ]);
        }
        catch(\Exception $ex) {
            return response()->json([
                'ok' => false,
                'message' => 'El archivo no pudo ser eliminado',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function subirArchivo(Request $request)
    {
        try
        {
            $archivo = $request->archivo;
            $name = 'Archivo_Ref_' . $request->referencia . '_tipo_' . 10 . '_' . sha1(date('YmdHis') . Str::random(10));
            $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

            $archivo->move($this->archivos_path, $resize_name);

            $upload = new ArchivosModel;
            $upload->tipo =  10;
            $upload->id_referencia =  $request->referencia;
            $upload->nombre = basename($archivo->getClientOriginalName());
            $upload->tipo_archivo = $archivo->getClientOriginalExtension();;
            $upload->ruta = $resize_name;
            $upload->id_usuario = Auth::user()->id;
            $upload->save();

            return response()->json([
                'ok' => true,
                'message' => 'Archivo subido',
                'archivo' => $upload
            ]);
        }
        catch(\Exception $ex)
        {
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo subir el archivo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function subirArchivoAcuerdo(Request $request)
    {
        try
        {
            $archivo = $request->archivo;
            $name = 'Acuerdo_Ref_' . $request->cedula . '_tipo_' . 100 . '_' . sha1(date('YmdHis') . Str::random(10));
            $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

            $archivo->move($this->archivos_path, $resize_name);
            $upload = AcuerdosModel::updateOrCreate(['cedula' => $request->cedula], [
                'id_usuario' => Auth::user()->id,
                'ruta' => $resize_name,
                'nombre' => basename($archivo->getClientOriginalName()),
                'tipo' => 100
            ]);

            return response()->json([
                'ok' => true,
                'message' => 'Acuerdo de responsabilidad subido',
                'archivo' => $upload
            ]);
        }
        catch(\Exception $ex)
        {
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo subir el archivo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function eliminarArchivoAcuerdo(Request $request)
    {
        try
        {
            $tabla = AcuerdosModel::find($request->id);
            $ruta = public_path() . '/archivos_sistema/' . $tabla->ruta;

            if (file_exists($ruta))
            {
                unlink($ruta);
            }

            $tabla->delete();

            return response()->json([
                'ok' => true,
                'message' => 'Archivo eliminado'
            ]);
        }
        catch(\Exception $ex) {
            return response()->json([
                'ok' => false,
                'message' => 'El archivo no pudo ser eliminado',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }

    public function cargarArchivoAcuerdo($cedula)
    {
        if(request()->ajax())
        {
            $acuerdo = AcuerdosModel::from('tmov_tram_acuerdos as ac')
            ->join('users as u', 'ac.id_usuario', 'u.id')
            ->select('u.name as usuario', 'ac.*')
            ->where(['ac.cedula' => $cedula, 'ac.estado' => 'ACT'])->first();

            if($acuerdo)
            {
                return response()->json([
                    'ok' => true,
                    'message' => 'El ciudadano ya cuenta con un acuerdo de responsabilidad',
                    'archivo' => $acuerdo
                ]);
            }
            else
            {
                return response()->json([
                    'ok' => false,
                    'message' => 'Aún no cuenta con un acuerdo de responsabilidad',
                    'usuario' => Auth::user()->name
                ]);
            }
        }
        else abort(401);
    }

    public function subirArchivoAlcance(Request $request)
    {
        try
        {
            $tramite = TramAlcaldiaCabModel::findOrFail($request->referencia);
            $asignados = TramAlcaldiaAsiganrModel::where(['id_cab' => $tramite->id, 'estado' => 'CONTESTADO'])->get();
            $directores = User::DIRECTORES;

            $archivo = $request->archivo;
            $name = 'Archivo_Alcance_' . $request->referencia . '_tipo_' . 10 . '_' . sha1(date('YmdHis') . Str::random(10));
            $resize_name = $name . Str::random(2) . '.' . $archivo->getClientOriginalExtension();

            $archivo->move($this->archivos_path, $resize_name);
            // $nHojas = 10 + intval($request->referencia);
            $upload = new AnexosModel;
            $upload->usuario_id = Auth::user()->id;
            $upload->id_referencia = $request->referencia;
            $upload->ruta = $resize_name;
            $upload->nombre = basename($archivo->getClientOriginalName());
            $upload->hojas = - $request->referencia;
            $upload->descripcion = $request->descripcion;
            $upload->tipo = 10;
            $upload->save();

            if($tramite->disposicion == 'FINALIZADO')
            {
                $tramite->disposicion = 'EN PROCESO';
                $tramite->estado_proceso = 0;
                $tramite->save();
            }
            else
            {
                $tramite->estado_proceso = 0;
                $tramite->save();
            }

            if(count($asignados) > 0)
            {
                foreach($asignados as $asignado)
                {
                    $asignado->estado = 'PENDIENTE';
                    $asignado->respondido = 0;
                    $asignado->save();
                }
            }

            $this->guardarTimeline($tramite->id, $tramite, 'Subir archivo alcance');

            $this->Notificacion->notificacionesweb('Nuevo alcance agregado al trámite N° ' . $tramite->numtramite, 'tramites/despachartramite?id=' . $tramite->id . '&estado=1', $tramite->asignado_a, '2c438f');

            return response()->json([
                'ok' => true,
                'message' => 'Archivo subido',
                'archivo' => $upload
            ]);
        }
        catch(\Exception $ex)
        {
            return response()->json([
                'ok' => false,
                'message' => 'No se pudo subir el archivo',
                'exception' => $ex->getMessage() . ' - ' . $ex->getLine()
            ]);
        }
    }
}
