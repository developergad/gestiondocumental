<?php

namespace Modules\Tramites\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Tramites\Entities\AreasModel;
use Modules\Tramites\Entities\direccionesModel;

class AreasController extends Controller
{
    var $configuraciongeneral = ["Áreas", "tramites/areas", "index", 6 => 'tramites/areasajax'];

    var $objetos = '[{"Tipo":"select","Descripcion":"Dirección","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Área","Nombre":"area","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Estado","Nombre":"estado","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';

    var $validarjs = [
        'id_direccion' => 'id_direccion: {
            required: true
        }',
        'area' => 'area: {
            required: true
        }'
    ];

    var $escoja = [
        null => "Escoja opción..."
    ];

    var $estado = [
        'ACT' => 'ACTIVO',
        'INA' => 'INACTIVO'
    ];

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $objetos = json_decode($this->objetos);
        $tabla = [];

        return view('vistas.index', [
            'objetos' => $objetos,
            'tabla' => $tabla,
            'configuraciongeneral' => $this->configuraciongeneral,
            'delete' => 'si',
            'create' => 'si'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[2] = "crear";
        $tabla = new AreasModel;
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + direccionesModel::where(['estado' => 'ACT'])
        ->orderBy('direccion')->pluck('direccion', 'id')->all();
        $objetos[2]->Valor = $this->estado;

        return view('vistas.create', [
            'configuraciongeneral' => $configuraciongeneral,
            'tabla' => $tabla,
            'objetos' => $objetos,
            'validarjs' => $this->validarjs
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->guardar($request);
    }


    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $configuraciongeneral = $this->configuraciongeneral;
        $configuraciongeneral[2] = "editar";
        $id = Crypt::decrypt($id);
        $tabla = AreasModel::findOrFail($id);
        $objetos = json_decode($this->objetos);
        $objetos[0]->Valor = $this->escoja + direccionesModel::where(['estado' => 'ACT'])
        ->orderBy('direccion')->pluck('direccion', 'id')->all();;
        $objetos[2]->Valor = $this->estado;

        return view('vistas.create', [
            'configuraciongeneral' => $configuraciongeneral,
            'tabla' => $tabla,
            'objetos' => $objetos,
            'validarjs' => $this->validarjs
        ]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($request, $id);
    }

    public function guardar($request, $id = null)
    {
        DB::beginTransaction();
        try
        {
            $validator = Validator::make($request->all(), [
                'id_direccion' => 'required',
                'area' => 'required',
                'estado' => 'required'
            ]);

            if ($validator->fails()) return back()->withErrors($validator)->withInput();

            $message = $id == null ? 'Área creada correctamente.' : 'Área actualizada correctamente.';

            AreasModel::updateOrCreate(['id' => $id], [
                'id_direccion' => $request->id_direccion,
                'area' => $request->area,
                'estado' => $request->estado
            ]);

            DB::commit();
            return redirect()->route('areas.index')->with('message', $message);
        }
        catch(\Exception $ex)
        {
            DB::rollback();
            return back()->withErrors([$ex->getMessage() . ' - ' . $ex->getLine()])->withInput();
        }
    }


    public function areasajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'id_direccion',
            2=> 'area'
        );

        $totalData = AreasModel::count();

        $totalFiltered = $totalData;

        $data = AreasModel::from('tram_peticiones_areas as area')
        ->join('tmae_direcciones as dir', 'area.id_direccion', 'dir.id')
        ->select('area.*', 'dir.direccion', 'dir.alias')
        ->where(['area.estado' => 'ACT']);

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = $data->offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $posts = $data->where('area','LIKE',"%{$search}%")
            ->orWhere('direccion','LIKE',"%{$search}%")->offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();

            $totalFiltered = $data->where('area','LIKE',"%{$search}%")
                ->orWhere('direccion','LIKE',"%{$search}%")->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $aciones = link_to_route('areas.edit','', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o')).'';

                $nestedData['id'] = $post->id;
                $nestedData['id_direccion'] = $post->direccion;
                $nestedData['area'] = $post->area;
                $nestedData['estado'] = $post->estado == 'ACT' ? 'ACTIVO' : 'INACTIVO';
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;
            }
        }

        return response()->json([
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        ]);
    }
}
