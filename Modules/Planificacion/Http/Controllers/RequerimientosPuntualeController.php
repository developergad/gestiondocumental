<?php

namespace Modules\CoordinacionCronograma\Http\Controllers;

use stdClass;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\NotificacionesController;
use Modules\Planificacion\Entities\RequerimientosPuntualesDelegadosModel;
use Modules\Planificacion\Entities\RequerimientosPuntualesHistoryModel;
use Modules\Planificacion\Entities\RequerimientosPuntualesModel;
use Modules\Tramites\Entities\direccionesModel;

class RequerimientosPuntualeController extends Controller
{
    var $configuraciongeneral = array("Requerimientos y compromisos reuniones con externos", "coordinacioncronograma/requerimientos_puntuales", "index", 6 => "coordinacioncronograma/requerimientos_puntualesajax", 7 => "requerimientos_puntuales");
    var $escoja = array(null => "Escoja opción...");
    var $objetos = '[
        {"Tipo":"textarea","Descripcion":"Actividad(*)","Nombre":"actividad","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Direccion(*)","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select-multiple","Descripcion":"Responsables(*)","Nombre":"idusuario","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Día de notificación(*)","Nombre":"dia_notificacion","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"5" },
        {"Tipo":"select","Descripcion":"Parroquia(*)","Nombre":"id_parroquia","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Barrio(*)","Nombre":"id_barrio","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ubicación / Dirección(*)","Nombre":"ubicacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Asignación(*)","Nombre":"fecha_asignacion","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Cumplimiento(*)","Nombre":"fecha_cumplimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"datetext","Descripcion":"Fecha Reprogramación","Nombre":"fecha_reprogramacion","Clase":"moneda Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"textarea","Descripcion":"Observaciones","Nombre":"observaciones","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Procentaje(*)","Nombre":"porcentaje","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Estado(*)","Nombre":"estado_activida","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
        ]';

    var $validarjs = array(
        "actividad" => "actividad: {
                            required: true
                        }",
        "id_direccion" => "id_direccion: {
                            required: true
                        }",
        "fecha_asignacion" => "fecha_asignacion: {
                            required: true
                        }",
        // "fecha_reprogramacion" => "fecha_reprogramacion: {
        //                     required: true
        //                 }",
        "fecha_cumplimiento" => "fecha_cumplimiento: {
                            required: true
                        }"
    );
    protected $notificacion;

    public function __construct(NotificacionesController $notificacion)
    {
        $this->middleware('auth');
        $this->notificacion = $notificacion;
    }
    public function perfil($verificar = 'SI')
    {
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        $object = new stdClass;
        if ($id_tipo_pefil->tipo == 1 || $id_tipo_pefil->tipo == 4 || $id_tipo_pefil->tipo == 10) { //1 Administrador 4 Coordinador
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'SI';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'SI';
        } else if ($id_tipo_pefil->tipo == 3) {
            $object->crear = 'SI';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'NO';
        } else {
            $object->crear = 'NO';
            $object->ver = 'SI';
            $object->editar = 'SI';
            $object->eliminar = 'NO';
            $object->editar_parcialmente = 'SI';
            $object->guardar = 'NO';
        }

        return $object;
    }


    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $tabla = [];
        $objetos = json_decode($this->objetos);
        $this->configuraciongeneral['selector'] = 'Si';
        $rutas = [
            'coordinacioncronograma/requerimientos_puntuales' => 'Requerimientos y compromisos reuniones con externos',
            'coordinacioncronograma/cronogramacompromisos' => 'Disposiciones generales',
            'coordinacioncronograma/compromisosalcaldiablue' => 'Compromisos territoriales'
        ];
        $ruta_actual = ['coordinacioncronograma/requerimientos_puntuales'];
        return view('vistas.index', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "delete" => "si",
            "create" => "si",
            "rutas" => $rutas,
            "ruta_actual" => $ruta_actual,
            "configuraciongeneral" => $this->configuraciongeneral,
            "permisos" => $this->perfil()
        ]);
    }

    public function formularioscrear($id = "")
    {
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        /*QUITAR CAMPOS*/
        $objetos = json_decode($this->objetos);
        $seleccionar_direccion = direccionesModel::orderby("direccion", "asc")->pluck("direccion", "id")->all();
        $objetos[1]->Valor = $this->escoja + $seleccionar_direccion;
        $objetos[2]->Valor = [];

        $parroquias = parroquiaModel::where('estado', 'ACT')
            ->whereNotIn('id', [8, 9, 10])->orderBy('parroquia')->pluck('parroquia', 'id')->all();
        $objetos[4]->Valor = $this->escoja + $parroquias;
        $objetos[5]->Valor = $this->escoja;


        $objetos[12]->Valor = [
            'CUMPLIDO' => 'CUMPLIDO',
            'DENTRO DEL PLAZO' => 'DENTRO DEL PLAZO',
            'INCUMPLIDO' => 'INCUMPLIDO',
            'REPROGAMADO CUMPLIDO' => 'REPROGAMADO CUMPLIDO',
            'SOLICITUD DE REPROGRAMACIÓN' => 'SOLICITUD DE REPROGRAMACIÓN'
        ];
        $porcentaje = [];
        for ($i = 0; $i <= 100; $i++) {
            # code...
            array_push($porcentaje, $i);
        }
        $objetos[11]->Valor = $porcentaje;



        $objetosmain = $this->verpermisos($objetos, "crear");
        $datos = array();
        if ($id == "") {
            $this->configuraciongeneral[2] = "crear";
            $datos = [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs
            ];
        } else {
            $this->configuraciongeneral[2] = "editar";

            // dd($tabla);
            $dicecionesDocumentos = null;
            $dicecionesImagenes = null;



            if ($id != null || $id != "") {
                $id = intval(Crypt::decrypt($id));
                $tabla = $objetosmain[1]->where("tmov_requerimientos_puntuales.id", $id)->first();
                if ($this->configuraciongeneral[2] == "editar") {
                    $this->configuraciongeneral[3] = "66"; //Tipo de Referencia de Archivo
                    $this->configuraciongeneral[4] = "si";
                    $dicecionesDocumentos = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "=", "pdf"], ["tipo", "66"]])->get();
                    // dd($dicecionesDocumentos);
                    $dicecionesImagenes = ArchivosModel::where([["id_referencia", $id], ["tipo_archivo", "<>", "pdf"], ["tipo", "66"]])->get();
                }
            }
            $objetos[2]->Valor = User::where(['id_direccion' => $tabla->id_direccion])->pluck('name', 'id')->all();


            $delegados = RequerimientosPuntualesDelegadosModel::where(["id_requerimiento" => $id, 'estado' => 'ACT'])
                ->pluck('id_usuario', 'id_usuario')->all();
            $objetos[2]->ValorAnterior = $delegados;
            $barrio = barrioModel::where([
                'id' => $tabla->id_barrio
            ])->first();

            $objetos[4]->ValorAnterior = (isset($barrio)) ? $barrio->id_parroquia : null;
            $objetos[5]->Valor =  $this->escoja + barrioModel::where([
                'estado' => 'ACT'
            ])->pluck('barrio', 'id')->all();
            $objetos[5]->ValorAnterior = $tabla->id_barrio;
            // show($objetos);


            $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();

            if ($id_tipo_pefil->tipo == 2 || $id_tipo_pefil->tipo == 3) {
                $objetos[0]->Tipo = 'html2';
                $objetos[0]->Valor = $tabla->actividad;

                $objetos[1]->Tipo = 'html2';
                $direccion = direccionesModel::where(['id' => $tabla->id_direccion])->first();
                $objetos[1]->Valor = $direccion->direccion;

                $objetos[2]->Tipo = 'html2';
                $delegados = RequerimientosPuntualesDelegadosModel::select(DB::raw('GROUP_CONCAT(name SEPARATOR ", ") as delegados'))->where(["id_requerimiento" => $id, 'tmov_requerimientos_delegados.estado' => 'ACT'])
                    ->join('users as u', 'u.id', '=', 'tmov_requerimientos_delegados.id_usuario')->groupby('name')->first();
                $objetos[2]->Valor = $delegados->delegados;




                $objetos[3]->Tipo = 'html2';
                $objetos[3]->Valor = $tabla->dia_notificacion;


                $objetos[4]->Tipo = 'html2';
                if ($barrio) {
                    $parroquia = parroquiaModel::where('id', $barrio->id_parroquia)->first();
                    $objetos[4]->Valor = $parroquia->parroquia;
                }

                $objetos[5]->Tipo = 'html2';
                $objetos[5]->Valor = (isset($barrio)) ? $barrio->barrio : null;


                $objetos[6]->Tipo = 'html2';
                $objetos[6]->Valor = $tabla->direccion;

                $objetos[7]->Tipo = 'html2';
                $objetos[7]->Valor = $tabla->fecha_asignacion;

                $objetos[8]->Tipo = 'html2';
                $objetos[8]->Valor = $tabla->fecha_cumplimiento;

                $objetos[9]->Tipo = 'html2';
                $objetos[9]->Valor = $tabla->fecha_reprogramacion;

                $objetos[10]->Tipo = 'html2';
                $objetos[10]->Valor = $tabla->observaciones;

                $objetos[11]->Tipo = 'html2';
                $objetos[11]->Valor = $tabla->porcentaje . '%';

                $objetos[12]->Tipo = 'html2';
                $objetos[12]->Valor = $tabla->estado_activida;
            }


            return view('vistas.create', [
                "objetos" => $objetos,
                "configuraciongeneral" => $this->configuraciongeneral,
                "validarjs" => $this->validarjs,
                "tabla" => $tabla,
                'dicecionesImagenes' => $dicecionesImagenes,
                'dicecionesDocumentos' => $dicecionesDocumentos,
                "permisos" =>  $this->perfil(),
                'timeline' => $this->construyetimeline($id)
            ]);
        }

        return view('vistas.create', $datos);
    }

    public function construyetimeline($id)
    {
        /*Historial Tabla*/
        $historialtable = RequerimientosPuntualesHistoryModel::join("users as a", "a.id", "=", "tmov_requerimientos_puntuales_history.id_usuario")
            ->join("tmae_direcciones as d", "d.id", "=", "tmov_requerimientos_puntuales_history.id_direccion")
            ->where("tmov_requerimientos_puntuales_history.id_cab", $id)
            ->leftjoin('barrio as b', 'b.id', '=', 'tmov_requerimientos_puntuales_history.id_barrio')
            ->leftjoin('parroquia as p', 'p.id', '=', 'b.id_parroquia')
            ->select(
                "tmov_requerimientos_puntuales_history.*",
                'b.barrio',
                'p.parroquia',
                "a.name as usuario",
                "d.direccion",
                DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from tmov_requerimientos_delegados AS y inner join users as x on x.id = y.id_usuario where y.id_requerimiento = tmov_requerimientos_puntuales_history.id_cab and y.id_cab_history=tmov_requerimientos_puntuales_history.id limit 1) as idusuario")
            )
            ->orderby("tmov_requerimientos_puntuales_history.updated_at")
            ->get();
        //show($historialtable);
        $obaddjs = json_decode($this->objetos);
        //            $obaddjs=$objetos;
        //show($obaddjs);
        //$obaddjs[2]->Nombre="parroquia";
        $obaddjs[1]->Nombre = "direccion";

        $obaddjs[4]->Nombre = "parroquia";
        $obaddjs[5]->Nombre = "barrio";



        $object = new stdClass;
        $object->Tipo = "text";
        $object->Descripcion = "Modificado por";
        $object->Nombre = "usuario";
        $obaddjs[] = $object;
        return array($historialtable, $obaddjs);
    }

    public function verpermisos($objetos = array(), $tipo = "index", $anio = '2019')
    {


        $tabla = RequerimientosPuntualesModel::select(
            'd.direccion',
            'tmov_requerimientos_puntuales.*',
            'b.barrio',
            'p.parroquia',
            DB::raw("(select GROUP_CONCAT(x.name SEPARATOR ', ') from tmov_requerimientos_delegados AS y inner join users as x on x.id = y.id_usuario where y.id_requerimiento = tmov_requerimientos_puntuales.id and y.estado='ACT' limit 1) as idusuario")
        )
            ->join('tmae_direcciones as d', 'd.id', '=', 'tmov_requerimientos_puntuales.id_direccion')
            ->leftjoin('barrio as b', 'b.id', '=', 'tmov_requerimientos_puntuales.id_barrio')
            ->leftjoin('parroquia as p', 'p.id', '=', 'b.id_parroquia')
            ->where('tmov_requerimientos_puntuales.estado', 'ACT');

        // show($tabla->get()->toarray());
        return array($objetos, $tabla);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return $this->formularioscrear();
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return $this->guardar($request, $request, 0);
    }


    public function guardar(Request $request, $id)
    {
        $input = $request->all();

        $ruta = $this->configuraciongeneral[1];

        if ($id == 0) {
            $ruta .= "/create";
            $guardar = new RequerimientosPuntualesModel();
            $guardar->id_usuario = Auth::user()->id;
            $msg = "Registro Creado Exitosamente...!";
            $msgauditoria = "Registro de Datos";
        } else {
            $ruta .= "/" . Crypt::encrypt($id) . "/edit";
            $guardar = RequerimientosPuntualesModel::find($id);
            // dd($id);
            $msg = "Registro Actualizado Exitosamente...!";
            $msgauditoria = "Actualización de Datos";
        }

        $input = $request->all();
        // show($input);
        $arrapas = array();

        $validator = Validator::make($input, RequerimientosPuntualesModel::rules($id));
        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        } else {
            try {
                DB::beginTransaction();
                $timeline = new RequerimientosPuntualesHistoryModel();
                foreach ($input as $key => $value) {
                    if ($key != "_method" && $key != "_token" && $key != "idusuario" && $key != "id_parroquia") {
                        $guardar->$key = $value;
                        $timeline->$key = $value;
                    }
                }
                $guardar->save();
                $idcab = $guardar->id;
                $timeline->id_cab = $idcab;
                $timeline->id_usuario = Auth::user()->id;
                $timeline->save();



                /*Detalle Personas a Cargo / Fiscalizadores*/
                if ($request->has("idusuario")) {
                    $multiple = $request->idusuario;
                    $guardarres = RequerimientosPuntualesDelegadosModel::where("id_requerimiento", $idcab)
                        ->update([
                            'estado' => 'INA'
                        ]);
                    foreach ($multiple as $key => $value) {
                        # code...
                        //Responsables
                        $guardarres = new RequerimientosPuntualesDelegadosModel;
                        $guardarres->id_usuario = $value;
                        $guardarres->id_requerimiento = $idcab;
                        $guardarres->id_cab_history = $timeline->id;
                        $guardarres->save();
                        //
                        $usuario = User::select("users.*")
                            ->where("id", $guardarres->id_usuario)
                            ->first();
                        //show($actividad);
                        $this->notificacion->notificacionesweb("Se le ha asignado un REQUERIMIENTOS Y COMPROMISOS REUNIONES CON EXTERNOS <b>#</b>" . $idcab . "", $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', $usuario->id, "2c438f");
                        $this->notificacion->EnviarEmail($usuario->email, "Nueva Actividad", "Se le ha asignado un REQUERIMIENTOS Y COMPROMISOS REUNIONES CON EXTERNOS", "<b>ACTIVIDAD: <b>#</b>" . $idcab . "</b> <br>" . $guardar->actividad, $this->configuraciongeneral[1] . '/' . Crypt::encrypt($idcab) . '/edit', 'vistas.emails.email');
                    }


                    //$json[] = $value;
                }
                DB::commit();
                Session::flash('message', $msg);
                $redir = $this->configuraciongeneral[1];
                return Redirect::to($redir);
            } //Fin Try


            catch (\Exception $e) {
                // ROLLBACK
                DB::rollback();
                $mensaje = $e->getMessage() . " - " . $e->getLine();
                return redirect()->back()->withErrors([$mensaje])->withInput();
            }
        }
        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

    public function requerimientos_puntualesajax(Request $request)
    {
        $objetos = json_decode($this->objetos);

        $tabla = $this->verpermisos($objetos);
        $objetos = $tabla[0];

        // show($objetos);
        $tabla = $tabla[1];

        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", 'id_direccion')->where("users.id", Auth::user()->id)->first();
        // dd($id_tipo_pefil);
        if ($id_tipo_pefil->tipo == 3) {
            $subarea = SubDireccionModel::where("area", $id_tipo_pefil->id_direccion)->first();
            if ($subarea)
                $tabla->where('id_direccion', $subarea->id_direccion);
            else
                $tabla->where('id_direccion', $id_tipo_pefil->id_direccion);
        }
        if ($id_tipo_pefil->tipo == 2) {
            $tabla =
                $tabla->join('tmov_requerimientos_delegados as del', 'del.id_requerimiento', 'tmov_requerimientos_puntuales.id')
                ->where(['del.estado' => 'ACT', 'del.id_usuario' => Auth::user()->id]);
        }

        ////////
        if ($id_tipo_pefil->tipo == 10) {
            $direcciones = getDireccionesCoor(Auth::user()->id);
            $tabla = $tabla->whereIn("id_direccion", explode(",", $direcciones->id_direccion));
        }
        //show($tabla->get());
        ///
        $columns = array();
        $columns[] = 'id';
        foreach ($objetos as $key => $value) {
            # code...
            $columns[] = $value->Nombre;
        }
        $columns[] = 'acciones';

        $totalData = $tabla->count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if (empty($request->input('search.value'))) {
            $posts = $tabla->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir)
                ->get();
        } else {
            $search = $request->input('search.value');

            $posts = $tabla->where(function ($query) use ($search) {
                $query->where('actividad', 'LIKE', "%{$search}%");
            })
                ->offset($start)
                ->limit($limit)
                ->orderBy($order, $dir);
            $totalFiltered = $posts->count();
            $posts = $posts->get();
        }

        $data = array();
        if (!empty($posts)) {
            // show($posts);
            foreach ($posts as $post) {
                $permisos = $this->perfil();

                $acciones = '';
                $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
                $edit = '';
                if ($permisos->editar == 'SI') {
                    $edit = link_to_route('' . $this->configuraciongeneral[7] . '.edit', ' EDITAR', array(Crypt::encrypt($post->id)), array('class' => 'fa fa-pencil-square-o btn btn-light', 'style' => 'color: #fff;background-color: #17a2b8;border-color: #17a2b8'));
                }
                $acciones = ' ' . $edit;
                if ($permisos->eliminar == 'SI') {
                    $acciones .= '&nbsp;&nbsp;<a onClick="eliminar(' . $post->id . ')" class="btn btn-danger"><i class="fa fa-trash"></i> Eliminar</a>
                    <div style="display: none;">
                    <form method="POST" action="' . $this->configuraciongeneral[7] . '/' . $post->id . '" accept-charset="UTF-8" id="frmElimina' . $post->id . '" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="' . csrf_token() . '">
                        <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                    </form>
                    </div>';
                }
                $nestedData['id'] = $post->id;
                foreach ($objetos as $key => $value) {
                    # code...
                    $campo = $value->Nombre;
                    $nestedData["$value->Nombre"] = $post->$campo;
                }


                $nestedData['acciones'] = $acciones;
                // dd($post->direccion);
                $nestedData['id_direccion'] = $post->direccion;
                $nestedData['id_parroquia'] = $post->parroquia;
                $nestedData['id_barrio'] = $post->barrio;
                $data[] = $nestedData;
            }
        }
        //show($data);
        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {

        return redirect()->back();
        ///return view('coordinacioncronograma::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        // return redirect()->back();
        return $this->formularioscrear($id);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
        // $id = intval(Crypt::decrypt($id));

        return $this->guardar($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tabla = RequerimientosPuntualesModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        RequerimientosPuntualesHistoryModel::where('id_cab', $id)
            ->update(['estado' => 'INA']);
        RequerimientosPuntualesDelegadosModel::where("id_requerimiento", $id)
            ->update([
                'estado' => 'INA'
            ]);
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
