@extends('coordinacioncronograma::layouts.master')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('.contact-box').each(function() {
            animationHover(this, 'pulse');
        });
        $(".ajusteperfil").html($("#animacion").html());
        $('.gallery-slideshow').slideshow({
            interval: 3000,
            width: 580,
            height: 350
        });
    });
</script>

<style type="text/css">
    .iconpng {
        margin-left: auto;
        margin-right: auto;
        display: block;
        width: auto;
        height: 6em;
    }
    .textTitle{
        font-size: 2em;
        font-weight: bold;
        color: #000;
        padding-top: 2%;
    }
</style>
@stop

@section ('contenido')
    <p class="text-center textTitle" style="color: #000; text-transform: uppercase; font-weight: bold;text-shadow: 1px 1px 1px #f5f5fb;">{{ $modulo->nombre }} </p>
    @if (Session::has('message'))
        <script>
            $(document).ready(function() {
                toastr["success"]("{{ Session::get('message') }}");
            });
        </script>
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif

    <div class="content">
        @php $k=0;$j=0;$nivelant=0;@endphp
        @foreach($iconos as $key => $value)
            @if($value->nivel==1)
                @if($nivelant!=$value->nivel && $j==3)
                @endif
                @php $k=0; @endphp
                <div>
                    <div class="col-lg-12 wrapper">
                        <h2 style="color: #000;font-weight: bold;text-shadow: 1px 1px 1px #f5f5fb;">{{ $value->menu }}</h2>
                    </div>
                </div>
            @else
                @php $k++;$j=$k%4; @endphp
                @if($j==1)
                    <div> <div>
                        @endif
                        <div class="col-lg-2 col-md-3" style=" margin-top: 15px;" >
                            <div class=" zoom circulo"  style=" height: 100px; padding: 1%;     ">
                            <a  href="{{ URL::asset($value->ruta) }}" style="width: 100%;  height: 100%;">
                                <div class="btn btn-primary" style="width: 100%; height: 100%;     background-color: #3e495f;">
                                <div style="padding: 3%">
                                @if($value->iconpng=="")
                                    <i style="width: 40px; height: 8%;font-size: 40px;" class="{{ $value->icono }} fa-5x"></i>
                                    <div><p style="  font-size: 0.7em; text-transform: uppercase; white-space:normal;">{{ $value->menu }}</p></div>
                                @else
                                    <img style="width: 40px; height: 8%;font-size: 40px;" src="{{ asset($value->iconpng) }}" class="img-circle img-responsive iconpng" />
                                    <div><p style="  font-size: 0.7em; text-transform: uppercase; white-space:normal;">{{ $value->menu }}</p></div>
                                @endif
                                </div>
                                </div>
                            </a>
                            </div>
                        </div>
                        @if($j==0)
                    </div>
                @endif
            @endif
            @php $nivelant=$value->nivel;@endphp
        @endforeach
    </div>

    @if (isset($estadistica))
        <div class="col-lg-2">
            <div class="ibox float-e-margins">
                <div class="ibox-title  boton-{{ $estadistica[3] }}">
                    <h5>{{ $estadistica[0] }}</h5>
                </div>
                <div class="ibox-content cuadro-{{ $estadistica[3] }}" style="text-align: center;">
                    @if(Auth::user()->id==1)
                        <a href="{{ URL::to($estadistica[1]) }}">
                            <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1>
                        </a>
                    @else
                        <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1>
                    @endif
                </div>
            </div>
        </div>
    @endif
@stop
