@extends('layouts.auth')

@section('titulo')
    {{ trans('formauth.login.title') }}
@endsection

@section('cabecera')
    {{ trans('formauth.login.titlesub') }}
@endsection

@section('errores')
    @if($errors->all())
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["error"]("{{ HTML::ul($errors->all()) }}");
            });
        </script>

        <div class="alert alert-danger" style="text-align: left;" id='diverror'>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <h4>Error!</h4>
            {{ HTML::ul($errors->all()) }}
        </div>
    @endif
    @if (Session::has('message'))
        <div class="alert alert-warning">{{ Session::get('message') }}</div>
    @endif

<script type="text/javascript">
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    $(document).ready(function() {
        $("#login").click(
            function(){
                if(isMobile.any()) {
                    swal("Iniciando sesión", "Espere un momento por favor...", "success");
                }
            }
        );
    });
</script>
@endsection

@section('formulario')
    {{ Form::open(array('route' => 'login','class'=>'form-signin','role'=>'form')) }}
        <div class="form-group">
            <div class="input-group m-b">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                {{ Form::text('cedula', null, ['class' => 'form-control','placeholder'=>trans('formauth.label.cedula'),'required'=>'', 'autofocus'=>'']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="input-group m-b">
                <span class="input-group-addon"><i class="fa fa-unlock"></i></span>
                {{ Form::password('password', ['class' => 'form-control','placeholder'=>trans('formauth.label.password'),'required'=>'']) }}
            </div>
        </div>
        <button type="submit" class="btn btn-primary block full-width m-b" id="login">{{ trans('formauth.login.submit') }}</button>
    {{ Form::close() }}
@endsection
