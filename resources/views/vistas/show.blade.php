<?php
if(!isset($nobloqueo))
    //Autorizar(Request::path());
?>
@extends ('layouts.layout_basic_no_head')
@section ('titulo') {!! $configuraciongeneral[0] !!} @stop
@section ('scripts')
<script type="text/javascript">
    function imprimirdiv(obj){
        $(".graficos").show();
        var printWindow = window.open("", "Imprimir" );
        $("link, style").each(function() {
            $(printWindow.document.head).append($(this).clone())
        });
        //$(printWindow.document.head).append(estilo);
        var toInsert = $(obj).clone();//.html();
//        toInsert= toInsert.find('#divpagoonline').remove().end();
        //var tituloreporte= $("#tituloreporte").val();
        //$("#tdtitulo").text(tituloreporte);
        var divheader= $("#divheader").html();
        toInsert= toInsert.html(divheader+toInsert.html());
        $(printWindow.document.body).append(toInsert);
        setTimeout(function(){
            printWindow.print();
            //printWindow.close();
            $(".graficos").hide();
        },1000);
        //printWindow.print();
        //printWindow.close();
    }
</script>
@stop
@section ('contenido')
    <!-- Cabecera de Impresión-->
    <div id="divheader" style="display:none;">
        <center>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img style="width: 200px; height: auto;" src="{{ asset("img/mantafirmes.png") }}" />
                    </td>
                    <td style="text-align: center;">
                        <h2>GOBIERNO AUTÓNOMO DESCENTRALIZADO MUNICIPAL DEL CANTÓN MANTA</h2>
                        <h3>COORDINACIÓN GENERAL DE PLANIFICACIÓN PARA EL DESARROLLO</h3>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="3"><img align="center" alt="Image" border="0" style="height: 5px;" src="{{ asset("img/lineafirmes.png")}}"  title="Image" width="100%"/></td></tr>
                <!--
                <tr>
                    <td colspan="3"><div id="tdtitulo"></div></td>
                </tr>
                -->
            </table>
        </center>
    </div>
    <!-- Fin Cabecera-->
    <div class="pull-right">
        <input id="btnImprime" type="button" class="btn btn-danger" value="Imprimir" onclick="return imprimirdiv('#restable')">
    </div>
<div class="container" id="restable">
<!-- /.panel-heading -->
<h1>{!! $configuraciongeneral[0] !!}</h1>
<hr />
 @if(!isset($detalle))
 <p>
  @foreach($objetos as $key => $valuecam)
    <?php
    if(!isset($validararry))
      $cadena="echo \$tabla->".$valuecam->Nombre.";";
  	else
  	{
  		if(!in_array("div-".$valuecam->Nombre,$validararry))
  			$cadena="echo \$tabla->".$valuecam->Nombre.";";
  		else{
  			continue;
  		}
  	}
  		echo '<strong> '. str_replace("(*)","",$valuecam->Descripcion).': </strong>&nbsp;&nbsp;&nbsp;';
      	eval($cadena);
    ?>
    <br>
@endforeach
</p>
@else
	<table class="table table-striped table-bordered table-hover">
	@foreach($objetos as $key => $value)
		<th>{{ str_replace("(*)","",$value->Descripcion) }}</th>
	@endforeach
	@foreach($tabla as $key => $value)
		<tr>
		@foreach($objetos as $keycam => $valuecam)
			<td>
			<?php
				$cadena="echo \$value->".$valuecam->Nombre.";";
					eval($cadena);
				//echo $cadena;
				?>
			</td>
        @endforeach
        </tr>
    @endforeach
    </table>

@endif
@if(isset($mapa['map']['js']))

    <?php echo $mapa['map']['js']; ?>
    <?php echo $mapa['map']['html']; ?>

@endif
@if($configuraciongeneral[1]=='coordinacioncronograma/cronogramacompromisos')
		<div class="col-lg-12">

                            <div class="ibox float-e-margins">
                            <div class="ibox-title">

							<strong>Responsables</strong>
							</div>

							<table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                    <thead>
                        <tr>
                           <th>Nombre</th>
                        	<th>Correo</th>
                    </tr>
                    </thead>


					<tbody>
                    @foreach($tabla2 as $key => $value)
                    <tr>
                         <td>{{ $value->name }} </td>
						 <td>{{ $value->email }} </td>

                    </tr>
                      @endforeach
                     </tbody>

                </table>


				{{-- $tabla2->links() --}}
                    </div>

                    @if(isset($poaact))

                    <div class="col-lg-12">

                      <div class="ibox float-e-margins">
                      <div class="ibox-title">

                        <strong>POA</strong>
                        </div>

                        <table class="table table-striped table-bordered table-hover display">
                            <tr >
                              <th>Financiamineto</th>
                              <th>Costo Institucional</th>
                              <th>Costo Proveedor</th>
                              <th>Costo Inicial</th>
                              <th>Costo Actual</th>
                              <th>Diferencia</th>
                            </tr>
                            <tr>
                            <th>
                                {{$poaact->financiamiento}}
                              </th>
                              <th>
                                  {{$poaact->costo_institucion}}
                              </th>
                              <th>
                                  {{$poaact->costo_prov}}
                              </th>
                              <th>
                                  {{$poaact->costo_inicial}}
                              </th>

                              <th>
                                  {{$poaact->costo_actual}}

                              </th>

                               <th>
                                  {{$poaact->diferencia}}

                              </th>
                            </tr>
                          </table>
                  <button></
  {{-- $tabla2->links() --}}
              </div>
                    @endif

@endif
@if($configuraciongeneral[1]=='tramites/tramites')
@if (isset($tabla2))
<div class="col-lg-12">

    <div class="ibox float-e-margins">
    <div class="ibox-title">

<strong>Delegado</strong>
</div>

<table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
<thead>
<tr>
   <th>Nombre</th>
  <th>Correo</th>
</tr>
</thead>


<tbody>
@foreach($tabla2 as $key => $value)
<tr>
 <td>{{ $value->name }} </td>
<td>{{ $value->email }} </td>

</tr>
@endforeach
</tbody>

</table>


{{-- $tabla2->links() --}}
</div>
@endif

@endif

@if(isset($tablaDirectores) && count($tablaDirectores) > 0)
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <strong>Direcciones Asignadas</strong>
            </div>
            <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                <thead>
                    <tr>
                        <th>Dirección</th>
                        <th>Observación</th>
                        @if ($tabla->disposicion == 'FINALIZADO')
                            <th>Fecha de Respuesta</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($tablaDirectores as $key => $value)
                        <tr>
                            <td>{{ (isset($value->direccion))?$value->direccion:'' }} </td>
                            <td>{{ isset($value->observacion) ?  $value->observacion : 'NINGUNA' }}</td>
                            @if ($tabla->disposicion == 'FINALIZADO')
                                <td>{{ $value->respuesta }} </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

@if(isset($solicitudesInforme) && count($solicitudesInforme) > 0)
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <strong>Solicitudes de informe</strong>
            </div>
            <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display" >
                <thead>
                    <tr>
                        <th>Dirección solicitante</th>
                        <th>Dirección asignada</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($solicitudesInforme as $key => $value)
                        <tr>
                            <td>{{ $value->solicitante }} </td>
                            <td>{{ $value->direccion_asignada }}</td>
                            <td>{{ $value->estado == 'INFORME' ? 'PENDIENTE' : 'CONTESTADO' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

@if (isset($analistas) && count($analistas) > 0)
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <strong>Analistas Asignadas</strong>
            </div>
            <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
                <thead>
                    <tr>
                        <th>Dirección</th>
                        <th>Analista</th>
                        <th>Observación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($analistas as $key => $value)
                        <tr>
                            <td>{{ (isset($value['direccion']))?$value['direccion']:'' }} </td>
                            <td>{{ $value['analista'] }}</td>
                            <td>{{ isset($value['observacion']) ? $value['observacion'] : 'NINGUNA' }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

@if (isset($archivados) && count($archivados) > 0)
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <strong>Archivados</strong>
            </div>
            <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
                <thead>
                    <tr>
                        <th>Dirección</th>
                        <th>Observación</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($archivados as $key => $value)
                        <tr>
                            <td>{{ $value['direccion'] }} </td>
                            <td>{{ $value['observacion'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif

@if (isset($timeline_agenda))
		<h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">

                @foreach ($timeline_agenda as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">

                        @if ($contador==0)
                        <h2><i class="fa fa-save"></i>  Inicial</h2>

                        @elseif($contador>=1)
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        @endif
                       <div hidden> {{$contador+=1}} </div>

                  <p>
                        <pre style="text-align: left; white-space: pre-line;">
                        <strong>Actividad: </strong>{{$item->nombre}}
                        <strong>Descripcion: </strong> {{$item->descripcion}}
                        <strong>Dirección: </strong>   {{ (isset($value->direccion))?$value->direccion:'' }}
                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                        <strong>Fecha de fin: </strong>  {{$item->fecha_fin}}
                        <strong>Observacion: </strong>  {{(isset($item->observacion))?$item->observacion:'Ninguna'}}
                        <strong>N° Asistentes: </strong>  {{$item->n_beneficiarios}}
                        <strong>Responsable: </strong>  {{$item->responsable}}
                        <strong>Email: </strong>  {{$item->email_solicitante}}
                        <strong>Teléfono: </strong>  {{$item->telefono_solicitante}}
                        <strong>Estado de la actividad: </strong>  <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_agenda)  }};">{{$item->estado_agenda}}</span>
                        <i><strong>Modificado: </strong>  {{$item->name}}</i>
                      </pre>
                      </p>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach

            </div>


        </div>

        @endif






@if (isset($timeline_obras))
		<h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">

                @foreach ($timeline_obras as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">

                        @if ($contador==0)
                        <h2><i class="fa fa-save"></i>  Inicial</h2>

                        @elseif($contador>=1)
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        @endif
                       <div hidden> {{$contador+=1}} </div>

                  <p>
                        <pre style="text-align: left; white-space: pre-line;">
                        <strong>Nombre de Obra: </strong>{{$item->nombre_obra}}
                        <strong>Parroquia: </strong> {{$item->parroquia}}
                        <strong>Calle: </strong>  {{$item->calle}}
                         <strong>Tipo de obra: </strong> {{$item->tipo}}
                            <strong>Monto: </strong> <span class="label label-primary" style="font-size: 1em;">$ {{ number_format ($item->monto,2) }}</span>
                                    <strong>Contratista: </strong>  {{$item->contratista}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_final}}
                                            <strong>Fisxalizador: </strong>  {{$item->fiscalizador}}
                                            <strong>Plazo (DÍAS): </strong>  {{$item->plazo}}
                                            <strong>Observacion: </strong>  {{$item->observacion}}
                                            <strong>Estado de la obra: </strong>  <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_obra)  }};">{{$item->estado_obra}}</span>
                                            <br>
                                            <i><strong>Modificado: </strong>  {{$item->name}}</i>



                      </pre>
                      </p>
                      <div class="ibox-content">
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                          </div>
                        </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach

            </div>


        </div>

        @endif



        @if (isset($timelineTramite))
        <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div>
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">

                    @foreach ($timelineTramite as $item)
                    <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

                  ?>
                    <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            {{-- <i class="fa fa-briefcase"></i> --}}
                        </div>
                      <div class="vertical-timeline-content">

                            @if ($contador==0)
                            <h2><i class="fa fa-save"></i>  Inicial</h2>

                            @elseif($contador>=1)
                            <h2><i class="fa fa-pencil"></i> Actualización</h2>
                            @endif
                           <div hidden> {{$contador+=1}} </div>

                      <p>
                        <pre style="text-align: left; white-space: pre-line;">
                            <strong>Fecha de registro: </strong>{{$item->created_at	}}
                            <strong>Numero de trámite: </strong>{{$item->numtramite}}
                            <strong>Remitente: </strong>  {{$item->remitente}}
                                            <strong>Asunto: </strong>  {{$item->asunto}}
                                                <strong>Petición: </strong>  {{$item->peticion}}
                            <strong>Prioridad: </strong>  {{$item->prioridad	}}
                            <strong>Recomendacion: </strong>  {{$item->recomendacion	}}
                            <strong>Disposicion	: </strong> <span class="label" style="font-size: 1em;background-color: ">{{$item->disposicion}}</span>
                            <strong>Observacion: </strong>  {{$item->observacion}}
                            <strong>Fecha de Repuesta: </strong>  {{$item->fecha}}
                            <br>
                            <strong>Modificado Por: </strong>  {{ $item->name == 'ADMINISTRADOR' ? (isset($ingresadoPor) ? $ingresadoPor : 'CIUDADANO') : $item->name }}

                </pre>

                @if (isset($timelineTramiteResp))
                <strong><i class="fa fa-users"></i> Delegados</strong>
                <pre style="text-align: left; white-space: pre-line;">
                  @foreach ($timelineTramiteResp as  $i)
                  <strong>Nombre: </strong>{{$i->name}}
                  @endforeach

                </pre>
                @endif
                        </p>
                    </div>
                    @endforeach

                </div>


            </div>

            @endif

<!-- ======================================= -->
<!-- ===========ARCHIVOS============ -->
@if(isset($dicecionesImagenes) && $dicecionesImagenes->count() > 0)
<div class="row">
                <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <h2>Imágenes</h2>
                        <div class="lightBoxGallery" id="lightBoxGallery">
                          @foreach ($dicecionesImagenes as $item)
                          <a href="{{asset('archivos_sistema/'.$item->ruta)}}" title="{{$item->nombre}}" data-gallery=""><img width="200" height="100" src="{{asset('archivos_sistema/'.$item->ruta)}}"></a>
                          @endforeach
                            <!-- The Gallery as lightbox dialog, should be a child element of the document body -->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                        </div>

                    </div>
                </div>
            </div>


        </div>
@endif
@if(isset($dicecionesDocumentos) && $dicecionesDocumentos->count() == 0)
                    <h2><i>Carpeta Vacía, no existen archivos</i></h2>
                @endif
@if(isset($dicecionesDocumentos) && $dicecionesDocumentos->count() > 0)
            <div class="row">
                <div class="col-lg-12">


                    <div class="ibox-content">

                        <h2>Archivos</h2>
                        <div>
                          <!-- @ foreach ($dicecionesDocumentos as $item)
                        <a href="{asset('archivos_sistema/'.$item->ruta)}}" type="button" class="btn btn-success dropdown-toggle divpopup" target="_blank"><i class="fa fa-file-pdf-o" style="font-size: 2em;"></i> </a> {$item->nombre}}
                          @ endforeach
                              -->
                              <table class="table table-striped table-bordered table-hover">
                                  <tr>
                                      <th>Tipo</th>
                                      <th>Nombre de Archivo</th>
                                      <th>Fecha de Registro</th>
                                      <th>Usuario</th>
                                      <th>Tamaño KB / MB</th>
                                      <th>Ver</th>
                                  </tr>
                                  @foreach ($dicecionesDocumentos as $item)
                                      <tr>
                                          <td>{{ pathinfo(asset('archivos_sistema/'.$item->ruta))["extension"] }}</td>
                                          <td>{{ $item->nombre }}</td>
                                          <td>{{ $item->created_at }}</td>
                                          <td>{{ (isset($item->name))?$item->name: 'No disponible' }}</td>
                                          <td>
                                              {!!
                                              (is_file(public_path()."/archivos_sistema/".$item->ruta))?round(filesize(public_path()."/archivos_sistema/".$item->ruta)/1024,2)
                                              : "-" !!} /
                                              {!!
                                              (is_file(public_path()."/archivos_sistema/".$item->ruta))?round(filesize(public_path()."/archivos_sistema/".$item->ruta)/1024/1024,2)
                                              : "-" !!}
                                          </td>

                                          <td>
                                              @if(is_file(public_path()."/archivos_sistema/".$item->ruta))
                                                  <a href="{{ asset('archivos_sistema/'.$item->ruta) }}" type="button"
                                                     class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                                      <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                                  </a>
                                              @else
                                                  <a href="javascript:" type="button" class="btn btn-danger dropdown-toggle">
                                                      <i class="fa fa-times-circle-o" style="font-size: 2em;"></i>
                                                  </a>
                                              @endif
                                          </td>
                                      </tr>
                                  @endforeach
                              </table>
                        </div>

                </div>


            </div>
        </div>
@endif
<!-- ======================================= -->
		@if (isset($timelineActividades))
		<h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">

                @foreach ($timelineActividades as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">

                        @if ($contador==0)
                        <h2><i class="fa fa-save"></i>  Inicial</h2>

                        @elseif($contador>=1)
                        <h2><i class="fa fa-pencil"></i> Actualización</h2>
                        @endif
                       <div hidden> {{$contador+=1}} </div>

                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Actividad: </strong>{{$item->actividad}}
                        <strong>Dirección a cargo: </strong>  {{ (isset($value->direccion))?$value->direccion:'' }}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_fin}}
                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                        <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_actividad)  }};">{{$item->estado_actividad}}</span>
                        <strong>Observación: </strong>  {{$item->observacion}}
                        <br>
                        <strong>Modificado por: </strong>  {{$item->name}}

            </pre>
					  <strong><i class="fa fa-users"></i> Responsables</strong>
					  @if (isset($timelineActividadesResp))
					  <pre style="text-align: left; white-space: pre-line;">
						  @foreach ($timelineActividadesResp as  $i)
						  <strong>Nombre: </strong>{{$i->name}}
						  @endforeach

					  </pre>
					  @endif
                    </p>
                    <div class="ibox-content">
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                          </div>
                        </div>
                      <span class="vertical-date">
                          {{fechas(2,$item->updated_at)}} <br/>
                          <small>{{fechas(1000,$item->updated_at)}} </small>
                      </span>
                  </div>
                </div>
                @endforeach

            </div>


        </div>

        @endif



		@if (isset($avancesTimeline))
		<h1>Historial</h1>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">

                @foreach ($avancesTimeline as $item)
                <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
                <div class="vertical-timeline-block">
                <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        {{-- <i class="fa fa-briefcase"></i> --}}
                    </div>
                  <div class="vertical-timeline-content">


                        <h2><i class="fa fa-pencil"></i> Actualización</h2>


                  <p>
                    <pre style="text-align: left; white-space: pre-line;">
                        <strong>Parroquia: </strong>{{$item->parroquia}}
                        <strong>Barrio:</strong> {{$item->barrio}}
                        <strong>Modificado por el usuario:</strong>  {{$item->name}}
                        <span class="vertical-date">
                            {{fechas(2,$item->updated_at)}} <br/>
                            <small>{{fechas(1000,$item->updated_at)}} </small>
                        </span>

                      </pre>
                    </p>
                    <div class="ibox-content">
                            <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                            <div class="progress progress-striped active m-b-sm">
                              <div style="width: {{round($item->avance,0)}}%;" class="progress-bar"></div>
                          </div>
                        </div>

                  </div>
                </div>
                @endforeach

            </div>


        </div>

        @endif


        @if(isset($timelineProyecto))

        <h1>Historial</h1>
            <div hidden>{{$contador=0}}</div>
            <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
                <div class="vertical-timeline-block">

                    @foreach ($timelineProyecto as $item)
                    <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

                  ?>
                    <div class="vertical-timeline-block">
                    <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                            {{-- <i class="fa fa-briefcase"></i> --}}
                        </div>
                      <div class="vertical-timeline-content">

                          @if ($contador==0)
                          <h2><i class="fa fa-save"></i>  Inicial</h2>

                          @elseif($contador>=1)
                          <h2><i class="fa fa-pencil"></i> Actualización</h2>
                          @endif
                         <div hidden> {{$contador+=1}} </div>
                      <p>
                        <pre style="text-align: left; white-space: pre-line;">
                            <strong>Nombre: </strong>{{$item->nombre}}
                            <strong>Monto: </strong>  {{$item->monto}}
                            <strong>Linea Base: </strong>  {{$item->linea_base}}
                            <strong>Meta al: </strong>  {{$item->meta}} <strong>% </strong>
                            <strong>Descripción: </strong>  {{$item->descripcion}}
                        <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: /*colortimelineestado($item->estado_proyecto)*/ ;">{{$item->estado_proyecto}}</span>                      </p>
                          <strong>Observación: </strong>  {{$item->observacion}}
                          <span class="vertical-date">
                              {{fechas(2,$item->updated_at)}} <br/>
                              <small>{{fechas(1000,$item->updated_at)}} </small>
                          </span>
                      </div>
                    </div>
                    @endforeach

                </div>


            </div>

            @endif


@if($configuraciongeneral[1]=='comunicacionalcaldia/comunicacion')

@endif

<!-- Historial Nuevo-->
    @include("vistas.timeline")
<!-- Fin Historial Nuevo-->


@stop
