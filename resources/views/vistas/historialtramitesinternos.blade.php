@extends ('layouts.layout_basic_no_head')
@section ('titulo') {!! $configuraciongeneral[0] !!} @stop

@section ('contenido')
<div class="container">
    <h1>{!! $configuraciongeneral[0] !!}</h1>
    <hr/>

    @if (isset($archivados) && count($archivados) > 0)
        <div class="col-sm-10 col-sm-offset-1">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <strong>Archivados</strong>
                </div>
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            <th>Observación</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($archivados as $key => $value)
                            <tr>
                                <td>{{ $value->usuario }} </td>
                                <td>{{ isset($value->observacion) ? $value->observacion : 'NINGUNA'  }}</td>
                                <td>{{ $value->fecha }} </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @endif

    @if (isset($analistasAsignados) && count($analistasAsignados) > 0)
        <div class="col-sm-10 col-sm-offset-1">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <strong>Analistas asignados</strong>
                </div>
                <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>
                            <th>Usuario</th>
                            {{-- <th>Observación</th> --}}
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($analistasAsignados as $key => $value)
                            <tr>
                                <td>{{ $value->name }} </td>
                                {{-- <td>{{ isset($value->observacion) ? $value->observacion : 'NINGUNA' }}</td> --}}
                                <td>{{ $value->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO' }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
    @endif
</div>

<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h1>Historial</h1>
        <div hidden>{{ $contador=0 }}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
            <div class="vertical-timeline-block">
                @foreach ($timelineTramiteInternos as $item)
                    @php
                        $arraycolores = ["#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080"];
                        $color_aleatoreo = $arraycolores[array_rand($arraycolores)];
                    @endphp
                    <div class="vertical-timeline-block">
                        <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                        </div>
                        <div class="vertical-timeline-content">
                            @if ($contador==0)
                                <h2><i class="fa fa-save"></i>  Inicial</h2>
                            @elseif($contador>=1)
                                <h2><i class="fa fa-pencil"></i> Actualización</h2>
                            @endif
                            <div hidden> {{$contador+=1}} </div>
                            <p>
                                <pre style="text-align: left; white-space: pre-line;">
                                    <strong>Fecha de registro: </strong>{{ (isset($item->fecha)) ? $item->fecha : $item->created_at}}
                                    <strong>Numero de trámite: </strong>{{ $item->numtramite }}
                                    <strong>Remitente: </strong>  {{ $item->remitente }}
                                    <strong>Asunto: </strong>  {{ $item->asunto }}
                                    <strong>Tipo de trámite: </strong>  {{ $item->tipo_tramite }}
                                    <strong>Estado: </strong>  {{ mb_strtoupper($item->estado) }}
                                    <br>
                                    <strong>Modificado Por: </strong>{{ $item->usuario }}
                                </pre>
                                @if (isset($timelineTramiteResp))
                                    <strong><i class="fa fa-users"></i> Delegados</strong>
                                    <pre style="text-align: left; white-space: pre-line;">
                                        @foreach ($timelineTramiteResp as  $i)
                                            <strong>Nombre: </strong>{{$i->name}}
                                        @endforeach
                                    </pre>
                                @endif
                            </p>
                            <span class="vertical-date">
                                {{fechas(2,(isset($item->fecha)) ? $item->fecha : $item->updated_at)}} <br/>
                                <small>{{fechas(1000,(isset($item->fecha)) ? $item->fecha : $item->updated_at)}} </small>
                            </span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</div>
@stop
