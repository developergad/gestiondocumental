<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function(){
            $("#id_indicador_mae").trigger("change");
        },1000);
        /*Coordinacion*/
        $("#id_coordinacion").change(function(){

            var id=this.value;
            var ruta= ' {{ URL::to('coordinacioncronograma/getdireccionescoordinacion') }}?id='+id;
            $.get(ruta, function(data) {
                $("#id_direccion").empty();
                $("#id_direccion").append("<option value>Escoja opción...</option>");
                $.each(data, function(key, element) {
                    $("#id_direccion").append("<option value='" + key + "'>" + element + "</option>");
                });
                $('#id_direccion').trigger("chosen:updated");
            });
        });
        /**/
        $("#id_indicador_mae").change(function(){
            var id=this.value;
            var parro=$("#id_parroquia").val();
            $("#divresul").html("Espere...");
            var ruta= '<?php echo URL::to('coordinacioncronograma/getcamposdatos'); ?>?id='+id+"&id_parroquia="+parro;
            $.get(ruta, function(data) {
                $("#divresul").html(data);
            });
        });

        $("#id_sub_direccion").change(function(){

            var id=this.value;
            var ruta= '<?php echo URL::to('comunicacionalcaldia/getusuariosdireccion'); ?>?id='+id;
            $.get(ruta, function(data) {
                $("#idusuario").empty();
                $.each(data, function(key, element) {
                    $("#idusuario").append("<option value='" + key + "'>" + element + "</option>");
                });
                $('#idusuario').trigger("chosen:updated");
            });


        });



        $("#div_id_sub_direccion").hide();

        $("#id_direccion_act").change(function(){

            var id=this.value;
            var ruta= '<?php echo URL::to('comunicacionalcaldia/getusuariosdireccion'); ?>?id='+id;
            $.get(ruta, function(data) {
                $("#idusuario_act").empty();
                $.each(data, function(key, element) {
                    $("#idusuario_act").append("<option value='" + key + "'>" + element + "</option>");
                });
                $('#idusuario_act').trigger("chosen:updated");
            });
        });


        //$("#id_direccion").trigger("change");
        $("#componente").change(function(){

            var id=this.value;
            //Objetivo
            var ruta= '<?php echo URL::to('coordinacioncronograma/getcomponenteobjetivo'); ?>?id='+id;
            $.get(ruta, function(data) {
                $("#objetivo").empty();
                $.each(data, function(key, element) {
                    $("#objetivo").append("<option value='" + key + "'>" + element + "</option>");
                });
                //$('#objetivo').trigger("chosen:updated");
            });
            //$("#programa").empty();
            setTimeout(function(){
                $("#objetivo").trigger("change");
            },1000);

        });
        $("#objetivo").change(function(){
            var id=this.value;
            ///P    rograma
            ruta= '<?php echo URL::to('coordinacioncronograma/getcomponenteprograma'); ?>?id='+id;
            $.get(ruta, function(data) {
                $("#programa").empty();
                $.each(data, function(key, element) {
                    $("#programa").append("<option value='" + key + "'>" + element + "</option>");
                });
                //$('#objetivo').trigger("chosen:updated");
            });
            //Fin Programa
        });

        $("#div_clave_catastral_html").hide();

        $("#cedula_infractor").blur(function(){
            var id=this.value;
            var tipo=1015;
            // alert(id);
            // ///Programa

            ruta= '<?php echo URL::to('consultardatosseguro_data'); ?>?documento='+id+'&tipo='+tipo;
            $.get(ruta, function(data) {

                var nombre_servicio=data[0].nombre.split(' ');
                $("#nombres_infractor").val(nombre_servicio[0]+" "+nombre_servicio[1]);
                $("#apellidos_infractor").val(nombre_servicio[2]+" "+nombre_servicio[3]);
                if(data[0].dicapacitado){
                    $("#discapacitado").val(data[0].dicapacitado);
                }

            });
            if($('#div_clave_catastral_html').length){

            ruta = '<?php echo URL::to('getPredios'); ?>?documento=' + id;
            $.get(ruta, function(data) {
                if (data[0].error) {
                    Swal.fire({
                        position: 'center',
                        type: 'info',
                        title: 'No se encuentra registrado ningun predio con la cédula '+id,
                        showConfirmButton: false,
                        timer: 3000
                    });

                }else{
                    if(Object.keys(data).length>=0){
                        $("#codigo_catastral_infractor").val(data[0].clavecatastral.replace(/-/g,""));
                    }

                }
            });
        }

            //Fin Programa
        });





    });
</script>
