<script type="text/javascript" src="{{asset('js/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
  $(document).ready(function() {
  setTimeout(function(){
    var lon= $('#id_usuario option').size();
    //console.log("Lon: "+lon);
    //if(parseInt(lon)==0)
      //$("#id_direccion").trigger("change");
  },1000);


  $('.selective-normal').selectize();



	$('.solonumeros').on('input', function () {
              this.value = this.value.replace(/[^0-9.]/g,'');

            });
  	$('.cedularuc').blur(function (event) {
              if(this.value.length==10){
                    $.ajax({
                    type: "GET",
                    url: "{{URL::to('')}}/consultardatosseguro_data?documento="+this.value+"&tipo=1015",
                    data:null,
                    // dataType: "dataType",
                    success: function (response) {

                        if(response[0].nombre){
                          $("#nombre_razon").val(response[0].nombre);
                        }else{
                          toastr['error']('No se pudo consultar');
                          $("#nombre_razon").val('');
                        }

                    }
                  });
              }else if(this.value.length==13){
                  $.ajax({
                    type: "GET",
                    url: "{{URL::to('')}}/consultardatosseguro_ruc?documento="+this.value+"&tipo=1021",
                    data:null,
                    // dataType: "dataType",
                    success: function (response) {
                        if(response[0].razonSocial){
                          $("#nombre_razon").val(response[0].razonSocial);
                        }else{
                          $("#nombre_razon").val('');
                          toastr['error']('No se pudo consultar');
                        }
                    }
                  });

              }


      });
  	$('.cedula').keyup(function (event) {
        if(this.value.length==10) {
            $.ajax({
                type: "GET",
                url: "{{URL::to('')}}/consultardatosusuario?documento="+this.value+"&tipo=1015",
                data:null,
                success: function (response) {
                    console.log(response)
                    if(response.ok) {
                        $('#nombre').val(response.usuario.name)
                        $('#correo').val(response.usuario.email)
                        $('#cargo').val(response.usuario.cargo)
                        $('#id_direccion').val(response.usuario.id_direccion).trigger('chosen:updated')
                        $('#id_perfil').val(response.usuario.id_perfil).trigger('chosen:updated')
                    }
                }
            });
        }
    });

    $(".cedula").attr('maxlength',10);
    $(".cedularuc").attr('maxlength',13);
    $(".cedularuc").attr('maxlength',13);
    $("#nombre_razon").attr("readonly",true);
    $(".telefono").attr('maxlength',10);
    $(".telefono").attr('minlength',10);


    $('.telefono').on('blur', function() {
    var email = $(this).val();
    var regex_numeros=/^(09)?([0-9][ -]*){8}$/;
    if (regex_numeros.test(email)==false ){
      toastr["error"]("Teléfono no valido");
      $(this).focus();
    }
});
$('#placa').on('blur', function() {

    var placa = $(this).val();
    $(".multas_consulta").show();
    $.ajax({
            type: "GET",
            url: "{{URL::to('')}}/coordinacioncronograma/consultar_placa/"+this.value,
            data:null,
            // dataType: "dataType",
            success: function (response) {

                if(response[0] && response[0].propietario!=""){
                  $("#nombre").val(response[0].propietario);
                  $("#cedula").val(response[0].docPropietario);
                  $(".multas_consulta").hide();
                  console.log(response);
                }else{
                  console.log('SSS');
                    $(".multas_consulta").hide();
                    toastr["error"]("😔 No se pudo consultar el propietario por placa, ya que no se encuentra registrado en base de datos de la ANT. Por favor intente por cédula.😀");
                }

          }
        });

});







  $('input.moneda').keyup(function(event) {

     // skip for arrow keys
  if(event.which >= 37 && event.which <= 40){
    event.preventDefault();
  }

  $(this).val(function(index, value) {
    return value
      .replace(/\D/g, "")
      .replace(/([0-9])([0-9]{2})$/, '$1.$2')
      .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",")
    ;
  });

});

$('.numeros-letras-guion').on('input', function () {
              //alert("ss");
              this.value = this.value.replace(/[^0-9a-zA-Z-]/g,'');
            });
$('#placa').attr("maxlength","7");
$('#placa').on('blur', function () {
  var email = $(this).val();

    var regex = /^[A-Z]{3}[0-9]{4}$/;

    if (regex.test(email)==false ){
     toastr["error"]("Placa no valida (Ej. MBA0123)");
     $(this).focus();
    }


});

try {
    $('.editor_html').trumbowyg({
    lang: 'es',btns: [
        ['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        // ['insertImage'],
        // ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        // ['fullscreen']
    ]
});
    $('.editor_html2').trumbowyg({
    lang: 'es',btns: [
        ['viewHTML'],
        ['undo', 'redo'], // Only supported in Blink browsers
        ['formatting'],
        ['strong', 'em', 'del'],
        ['superscript', 'subscript'],
        ['link'],
        // ['insertImage'],
        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
        ['unorderedList', 'orderedList'],
        ['horizontalRule'],
        ['removeformat'],
        // ['fullscreen']
    ]
});



$('.summernote, .summernote-full-width').summernote({
  height : '230px',
  toolbar: [
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['table', ['table']],
    ['insert', ['link']],
    ['height', ['height']],
    ['view', ['help']],
  ]
});
$('.summernote-airmode').summernote({
  airMode: false
});
$('#demo-edit-text').on('click', function(){
  $('.summernote-edit').summernote({focus: true});
});


$('#demo-save-text').on('click', function(){
  $('.summernote-edit').summernote('destroy');
});





} catch (error) {
  console.log(error);

}
$(".mayuscula").on('keyup', function(e) {
    $(this).val($(this).val().toUpperCase());
});
/*
$(document).on('keyup', function(e) {
    $("#asunto").val($("#asunto").val().toUpperCase());
    $("#remitente").val($("#remitente").val().toUpperCase());
    $("#numtramite").val($("#numtramite").val().toUpperCase());
})
*/

$("#descargarFormatoAutorizacion").click(function (e) {
        let cedula= $("#cedula").val();
        let nombre= $("#nombre").val();
        let direccion= $("#id_dirección option:selected").text();
        let cargo= $("#cargo").val();
        let email= $("#email").val();
        let numero= $("#numero").val();
        window.open('{{URL::to('')}}/tramites/descargarFormatoAutorizacion/0?nombre='+nombre+'&cedula='+cedula+'&direccion='+direccion+'&cargo='+cargo+'&email='+email+'&numero='+numero,'_blank');

});


var texto = $(".porcentaje").text();
console.log(texto);
  $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es',
                orientation: 'auto bottom'
            });
  $('.fecha_input').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: false, //True para mostrar Numero de semanas
                autoclose: true,
                format: 'yyyy-mm-dd',
                language: 'es',
                orientation: 'auto bottom'
            });

@if(isset($validar_Agenda))
var setMin = function( currentDateTime ){
  this.setOptions({
      minDate:'-1970/01/02'
  });
  this.setOptions({
      minTime:'11:00'
  });
};


  $('div#date_fecha_inicio.input-group.datetime').datetimepicker({
              format: 'YYYY-MM-DD HH:mm',
              locale: 'es',
              sideBySide: true,
              stepping: 15,
              // minTime:'09:00'
              // minView: 2,
              // enabledHours: [6,7,8,9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21,22]
          }).on('keypress paste', function (e) {

      e.preventDefault();
      return false;
  });

  // var nextElem = $("div#date_fecha_inicio.input-group.datetime");
  //   if (nextElem.length > 0) {
  //       nextElem.data("DateTimePicker").minDate("{{date('Y-m-d')}}")
  //   }

  // var fecha_inicio=$("#fecha_inicio").val();
  // console.log(fecha_inicio);
  $('div#date_fecha_fin.input-group.datetime').datetimepicker({
              format: 'YYYY-MM-DD HH:mm',
              locale: 'es',
              sideBySide: true,
              stepping: 15,
              // minDate: fecha_inicio
              // minTime:'09:00'
              // minView: 2,
              // enabledHours: [6,7,8,9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,21,22]
          }).on('keypress paste', function (e) {

            e.preventDefault();
      return false;
  });


  $("#fecha_inicio").blur(function (e) {
    var fecha_inicio=$("#fecha_inicio").val();
    // alert('s');
    console.log(fecha_inicio);
    var nextElem = $("div#date_fecha_fin.input-group.datetime");
    if (nextElem.length > 0) {
        nextElem.data("DateTimePicker").minDate(fecha_inicio)
    }
  });





@else
  $('.input-group.datetime').datetimepicker({
              format: 'YYYY-MM-DD HH:mm',
              locale: 'es',
              sideBySide: true,
          }).on('keypress paste', function (e) {
      e.preventDefault();
      return false;
  });
@endif



// $('.email').on('blur', function() {
//     var email = $(this).val();
//     var regex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

//     if ( regex.test(email)==false ){
//       toastr["error"]("Email no valido");
//       $(this).focus();
//     }
// });


// $('.telefono').keypress(function(e) {
//     e.length();
//     let numero = $(this).text();
// });


    @if(isset($validarjs))
    //Validacion
    jQuery.extend(jQuery.validator.messages, {
      required: "Este campo es obligatorio.",
      remote: "Por favor, rellena este campo.",
      email: "Por favor, escribe una dirección de correo válida",
      url: "Por favor, escribe una URL válida.",
      date: "Por favor, escribe una fecha válida.",
      dateISO: "Por favor, escribe una fecha (ISO) válida.",
      number: "Por favor, escribe un número entero válido.",
      digits: "Por favor, escribe sólo dígitos.",
      creditcard: "Por favor, escribe un número de tarjeta válido.",
      equalTo: "Por favor, escribe el mismo valor de nuevo.",
      accept: "Por favor, escribe un valor con una extensión aceptada.",
      maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
      minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
      rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
      range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
      max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
      min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
  });
    /*$("form").submit(function(event){
        res=confirm("Seguro de Continuar?")
        if(!res)
          return false;
        //$(this).submit();
        event.preventDefault();
        $(this).unbind('submit').submit();
    });*/
      $('form').validate({
          rules: {
            @foreach($validarjs as $key)
              {!! $key !!},
            @endforeach
            /*
              clave: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              valor: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
            */
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if(element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          },
          submitHandler : function(form) {
          //do something here

              $('#gif_final').show();
              res=confirm("Seguro de Continuar?")
              if(!res){
                return false;
                $('#gif_final').hide();
                $("#btn_guardar").removeAttr('disabled');
              }else{

                $('#gif_final').show();
                QueryLoader2(document.querySelector("body"), {
                  barColor: "#00000",
                  backgroundColor: "##ffffffc7",
                  // percentage: true,
                  barHeight: 1,
                  minimumTime: 2000,
                  // fadeOutTime: 1000
              });
                  $("#btn_guardar").attr('disabled', true);
                  window.load();

              }


              /*var frm = $('#form1');
              frm.submit(function (ev) {
                ev.preventDefault();
                res=confirm("Seguro de Continuar?")
                if(!res)
                  return false;
                //$(this).submit();
                frm.unbind('submit').submit();
                //alert("Hola");
                //form.submit();
            }); */
        }
      });

@endif
$(".chosen-select").chosen(
  {
    no_results_text: "No existe coincidencia con lo que busca...",
    placeholder_text_single: "Seleccione...",
    placeholder_text_multiple: "Seleccione...",
    width: "100%",
    // height:"1000"
});

  //Fin Validacion

 $('.selective-tags').selectize({
          plugins: ['remove_button'],
          persist: false,
          create: true,
          render: {
            item: function(data, escape) {
              return '<div>"' + escape(data.text) + '"</div>';
            }
          },
          onDelete: function(values) {
            //return confirm(values.length > 1 ? 'Esta seguro de borrar la selección ' + values.length : '');
            return confirm('Esta seguro de borrar la selección?');
          }
        });
})
</script>
