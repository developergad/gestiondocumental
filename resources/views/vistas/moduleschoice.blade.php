@extends('layouts.layout_basic')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
    $('.contact-box').each(function() {
    animationHover(this, 'pulse');
    });
    });
    setTimeout(function() {
    toastr.options = {
    closeButton: true,
    progressBar: true,
    showMethod: 'slideDown',
    timeOut: 4000,
    "positionClass": "toast-bottom-right"
    };
    toastr.success('{{ trans('html.main.sistema') }}', 'Bienvenido');

    }, 1300);

    $(function() {
    $('.gallery-slideshow').slideshow({
    // default: 2000
    interval: 3000,
    // default: 500
    width: 800,
    // default: 350
    height: 500
    });
    });

    </script>
    <style>
        .icon-bar {
            width: 100%;
            background-color: #555;
            overflow: auto;
        }

        .icon-bar a {
            float: left;
            width: 20%;
            text-align: center;
            padding: 12px 0;
            transition: all 0.3s ease;
            color: white;
            font-size: 36px;
            display: block;
        }

        .icon-bar a:hover {
            background-color: #000;
            display: block;
        }

        .active {
            background-color: #4CAF50;
            display: block;
        }
        .iconpng{
            margin-left: auto;
            margin-right: auto;
            display: block;
            width: auto;
            height: 10em;
        }
    </style>
    @stop
    @section ('contenido')
    <h1 class="titulo">
        Un nuevo modelo de Gestión
    </h1>
    @if (Session::has('message'))
    <script>
        $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]();
    //$.notify("{{ Session::get('message') }}","success");
});
    </script>
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <hr>


    <h1 class="titulo2">
        Escoja módulo para continuar
    </h1>
    <div class="row">

        <div class="col-lg-12" id="centraricons">

{{--            <div class="navbar icon-bar">--}}

{{--            <a class="active" href="{{ URL::to("/") }}"><i class="fa fa-home"></i><br>Inicio</a>--}}
            @foreach($tabla as $key => $value)
{{--                <a href="{{ URL::asset($value->ruta) }}"><i class="fa fa-cube"></i><span class="menuvisible"><br>{{ $value->nombre }}</span></a>--}}
            <div class="col-lg-6 col-md-6">
                <div class="contact-box">
                    <a href="{{ URL::asset($value->ruta) }}">
                        <div class="col-sm-12">
                            <div class="text-center">
                                @if($value->icono!="")
                                    <img src="{{ asset("img/icons/".$value->icono)  }}" class="img-circle img-responsive circle-border iconpng">
                                @else
                                    <i class="fa fa-cube fa-5x"></i>
                                @endif
                                    <h3 style="text-align: center;"><strong>{{ $value->nombre }}</strong></h3>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </a>
                </div>
            </div>
            @endforeach
{{--            </div>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12" style="display: flex;justify-content: center;">

                <div style="padding: 10px;  margin: 10px;" class="img-responsive">
                    <?php $imagenes= explode('|',ConfigSystem('imagenes-slide')) ?>
                    <ul class="gallery-slideshow img-responsive imgresponsiveandres">
                        @foreach ($imagenes as $key => $item)
                            <li><img src="{{asset('img_gallery/'.$item)}}" width="100%" height="100%" /></li>
                        @endforeach

                    </ul>
                </div><!-- Div Hijo -->

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <center>
                <img src="{{asset('img/mantafirmes.png') }}" alt="" class="img-responsive"
                    style="width: 450px;">
            </center>
        </div>
    </div>
<div class="clearfix"></div>
    @stop
