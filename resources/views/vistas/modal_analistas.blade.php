@if(count($analistas) > 0)
    <div class="modal fade" id="modal-analistas" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Elija a quién desea asignar el trámite</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label for="observacion" id="label_observacion" class="col-lg-12  control-label">Observación:</label>
                            <div class="col-lg-12" id="div-observacion">
                                <textarea class="form-control" placeholder="Ingrese Observación" name="observacion" cols="50" rows="4" id="observacion_director"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <select name="analistas[]" id="analistas" multiple="multiple" class="selective-tags" data-placeholder="Seleccione">
                                    <option value="">Seleccione</option>
                                    @foreach ($analistas as $idx => $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    @if (isset($tabla->id))
                        <button type="button" onclick="asignarTramiteAnalista({{ $tabla->id }})" class="btn btn-primary">Solicitar</button>
                    @else
                    <input type="text" name="id_tramite" id="id_tramite">
                    <button type="button" onclick="asignarTramiteAnalista()" class="btn btn-primary">Solicitar</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif
