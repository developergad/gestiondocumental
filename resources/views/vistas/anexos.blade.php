<div class="row">
    <div class="form-group col-md-12 col-lg-12">
        <div class="card fadeIn" id="card_beneficiarios">
            <button type="button" class="btn btn-info" id="add_archivo" onclick="agregarAnexo()">
                <i class="fa fa-plus-circle" style="font-size: 1.5em;"> </i> Agregar anexo
            </button>
            <div class="card-body">
                <table id="tabla" class="table table-striped table-bordered table-hover display">
                    <thead>
                        <tr>
                            <th><b>NO.</b></th>
                            <th><b>ARCHIVO</b></th>
                            <th><b>HOJAS</b></th>
                            <th><b>DESCRIPCIÓN</b></th>
                            <th style="text-align: center;"><b>ACCIÓN</b></th>
                        </tr>
                    </thead>
                    <tbody>
                        @isset($anexos)
                        @foreach($anexos as $k => $valor)
                        <tr id="fila{{$k+1}}">
                            <th>
                                <div>{{$k+1}}</div>
                                <input type="number" hidden class="estado" value="{{$k+1}}">
                                <input type="number" hidden class="{{$k+1}}_id_anexo" id="{{$k+1}}_id_anexo" value="{{$valor->id}}">
                            </th>
                            <th>
                                <div class="col-lg-8 col-md-12 col-sm-12">
                                    <div class="input-group" style="text-align: left !important;">
                                        <div class="custom-file">
                                            <input required type="file" onchange="showIconImage(this)" class="custom-file-input" name="archivo_anexo[]" id="archivo_anexo{{$k+1}}" aria-describedby="imagen_lote{{$k+1}}">
                                            <label class="custom-file-label" for="archivo_anexo{{$k+1}}"></label>
                                        </div>

                                    </div>

                                </div>
                                <a href="{{ asset('archivos_sistema/'.$valor->ruta) }}" type="button" class="btn btn-success dropdown-toggle divpopup" target="_blank">
                                    <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                                </a>
                            </th>
                            <th>
                                <input type="number" value="{{$valor->hojas}}" step="any" required onblur="calcularCampos(this)" class="anchoinput form-control {{$k+1}}_hojas" id="{{$k+1}}_hojas">
                            </th>
                            <th>
                                <input type="text" required value="{{$valor->descripcion}}" onblur="calcularCampos(this)" class="anchoinput form-control {{$k+1}}_descripcion" id="{{$k+1}}_descripcion">
                            </th>
                            <th style="text-align: center;">
                                <img width="25px" height="25px" src="{{URL::to('')}}/img/cancel.png" onClick="borrarfila({{$k+1}})" />
                            </th>
                        </tr>

                        @endforeach
                        @endisset
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- <input class="form-control " placeholder="Ingrese json_anexos" name="anexos_json" value="" id="anexos_json"> -->
<input class="form-control " placeholder="Ingrese json_anexos" name="anexos_json"  type="hidden" value="" id="anexos_json">


<script>
    $(document).ready(function() {
        @isset($anexos)
        calcularCampos()
        @endisset

    })

    function agregarAnexo(e) {
        let id = obtenerNoLote();
        let input_file = `<div class="col-lg-8 col-md-12 col-sm-12">
            <div class="input-group" style="text-align: left !important;">
                <div class="custom-file">
                    <input required type="file" onchange="showIconImage(this)" class="custom-file-input" name="archivo_anexo[]" id="archivo_anexo${id}" aria-describedby="imagen_lote${id}">
                    <label class="custom-file-label" for="archivo_anexo${id}"></label>
                </div>
            </div>
        </div>`

        let html = `<tr id="fila${id}">
            <th>
                <div>${id}</div>
                <input type="number" hidden class="estado" value="${id}">
                <input type="number" hidden class="${id}_id_anexo" id="${id}_id_anexo" value="0">
            </th>
            <th>${input_file}</th>
            <th>
                <input type="number" step="any" required onblur="calcularCampos(this)" class="anchoinput form-control ${id}_hojas"  id="${id}_hojas">
            </th>
            <th>
                <input type="text" required onblur="calcularCampos(this)" class="anchoinput form-control ${id}_descripcion"  id="${id}_descripcion">
            </th>
            <th style="text-align: center;">
                <img width="25px" height="25px" src="{{URL::to('')}}/img/cancel.png" onClick="borrarfila(${id})"/>
            </th>
        </tr>`;
        $('#tabla tr:last').after(html);

        calcularCampos();
    }

    function obtenerNoLote() {
        let camposImput = document.getElementsByClassName("estado");
        let no_lote = camposImput.length + 1;
        return no_lote;
    }

    function calcularCampos() {
        var camposImput = document.getElementsByClassName("estado");
        var viewData = [];
        for (i = 0; i < camposImput.length; i++) {
            var id_etiqueta = camposImput[i].getAttribute('value');
            var valor = document.getElementsByClassName(id_etiqueta + "_hojas");
            var nombres = document.getElementsByClassName(id_etiqueta + "_descripcion");
            var ids_anexo = document.getElementsByClassName(id_etiqueta + "_id_anexo");
            console.log(ids_anexo);
            for (let index = 0; index < valor.length; index++) {
                var hojas = valor[index].value;
                var descripcion = nombres[index].value;
                var id_anexo = ids_anexo[index].value;
                var jsonData = {};
                jsonData['id_anexo'] = id_anexo;
                jsonData['hojas'] = hojas;
                jsonData['archivo'] = null;
                jsonData['descripcion'] = descripcion;
                viewData.push(jsonData);
            }
        }
        $('#anexos_json').val('');
        $('#anexos_json').val(JSON.stringify(viewData));
    }

    function borrarfila(id) {
        $("#fila" + id).remove();
        $('#resultado_avaluo').hide();
        obtenerNoLote();
        calcularCampos();
    }

    function showIconImage(input, soloPdf = false) {
        let img = [];
        if(soloPdf == true) img = ['pdf', 'PDF'];
        else img = ['pdf', 'PDF', 'xlsx', 'xls', 'docx'];
        let input_result = $("#" + input.id);
        if (input_result[0].files && input_result[0].files[0]) {
            let fileName = input_result[0].files[0].name;
            let extn = fileName.split('.').pop();
            if (isExtension(extn, img)) {
                let totalByte = input_result[0].files[0].size;
                let totalSize = totalByte / Math.pow(1024, 2);
                let tamanio = 50;
                if (totalSize.toFixed(2) <= tamanio) {
                    input_result.next().text(fileName);
                    $('#div-icon-' + input.id).show();
                } else {
                    input_result.next().text('Seleccione un archivo');
                    $('#div-icon-' + input.id).hide();
                    input_result.val("");
                    swal.fire({
                        title: 'Tamaño máximo superado',
                        text: "Solo se admiten documentos PDF que no superen los " + tamanio + " Megabytes",
                        type: 'error',
                        confirmButtonText: 'Ok',
                        reverseButtons: true
                    });
                }
            } else {
                input_result.next().text('Seleccione un archivo');
                $('#div-icon-' + input.id).hide();
                input_result.val("");
                swal.fire({
                    title: 'Formato incorrecto',
                    text: "Solo se admiten archivos en formato PDF.",
                    type: 'error',
                    confirmButtonText: 'Ok',
                    reverseButtons: true
                });
            }
        } else {
            input_result.next().text('Seleccione un archivo');
            $('#div-icon-' + input.id).hide();
            input_result.val("");
        }
    }

    function isExtension(ext, extnArray) {
        var result = false;
        var i;
        if (ext) {
            ext = ext.toLowerCase();
            for (i = 0; i < extnArray.length; i++) {
                if (extnArray[i].toLowerCase() === ext) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
</script>
