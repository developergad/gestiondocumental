<?php
if(!isset($nobloqueo))
    // Autorizar(Request::path());
?>
@extends(request()->has("menu") ? 'layout_basic_no_head':'layouts.layout' )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
  {!! HTML::script('js/jquery.blockUI.js') !!}
  {!! HTML::script('gridedit/mindmup-editabletable.js') !!}
  {!! HTML::script('gridedit/numeric-input-example.js') !!}

<style type="text/css">
#mapa-modal-evento {
  height: 200px;
  width: 70%;
}
  #contenidodiv {
    height: 150px;
    overflow: auto;


  }


.loader {
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  height: 90%;
  z-index: 9999;
  background: url({{ asset('img/loading35.gif') }}) 50% 50% no-repeat rgb(249,249,249);
  opacity: .8;
}

  .disabled {
    pointer-events: none;
    color: #AAA;
    background: #F5F5F5;
  }

  .anchoinput {
    width: 7em;
  }


  table {
    width: 100%;
  }

  th {
    height: 10px;
  }

  #label_archivo_final {
    display: none;
  }


  .demo-gallery>ul {
    margin-bottom: 0;
    padding-left: 15px;
  }

  .demo-gallery>ul>li {
    margin-bottom: 15px;
    width: 180px;
    display: inline-block;
    margin-right: 15px;
    list-style: outside none none;
  }

  .demo-gallery>ul>li a {
    border: 3px solid #FFF;
    border-radius: 3px;
    display: block;
    overflow: hidden;
    position: relative;
    float: left;
  }

  .demo-gallery>ul>li a>img {
    -webkit-transition: -webkit-transform 0.15s ease 0s;
    -moz-transition: -moz-transform 0.15s ease 0s;
    -o-transition: -o-transform 0.15s ease 0s;
    transition: transform 0.15s ease 0s;
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
    height: 100%;
    width: 100%;
  }

  .demo-gallery>ul>li a:hover>img {
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  .demo-gallery>ul>li a:hover .demo-gallery-poster>img {
    opacity: 1;
  }

  .demo-gallery>ul>li a .demo-gallery-poster {
    background-color: rgba(0, 0, 0, 0.1);
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    -webkit-transition: background-color 0.15s ease 0s;
    -o-transition: background-color 0.15s ease 0s;
    transition: background-color 0.15s ease 0s;
  }

  .demo-gallery>ul>li a .demo-gallery-poster>img {
    left: 50%;
    margin-left: -10px;
    margin-top: -10px;
    opacity: 0;
    position: absolute;
    top: 50%;
    -webkit-transition: opacity 0.3s ease 0s;
    -o-transition: opacity 0.3s ease 0s;
    transition: opacity 0.3s ease 0s;
  }

  .demo-gallery>ul>li a:hover .demo-gallery-poster {
    background-color: rgba(0, 0, 0, 0.5);
  }

  .demo-gallery .justified-gallery>a>img {
    -webkit-transition: -webkit-transform 0.15s ease 0s;
    -moz-transition: -moz-transform 0.15s ease 0s;
    -o-transition: -o-transform 0.15s ease 0s;
    transition: transform 0.15s ease 0s;
    -webkit-transform: scale3d(1, 1, 1);
    transform: scale3d(1, 1, 1);
    height: 100%;
    width: 100%;
  }

  .demo-gallery .justified-gallery>a:hover>img {
    -webkit-transform: scale3d(1.1, 1.1, 1.1);
    transform: scale3d(1.1, 1.1, 1.1);
  }

  .demo-gallery .justified-gallery>a:hover .demo-gallery-poster>img {
    opacity: 1;
  }

  .demo-gallery .justified-gallery>a .demo-gallery-poster {
    background-color: rgba(0, 0, 0, 0.1);
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    -webkit-transition: background-color 0.15s ease 0s;
    -o-transition: background-color 0.15s ease 0s;
    transition: background-color 0.15s ease 0s;
  }

  .demo-gallery .justified-gallery>a .demo-gallery-poster>img {
    left: 50%;
    margin-left: -10px;
    margin-top: -10px;
    opacity: 0;
    position: absolute;
    top: 50%;
    -webkit-transition: opacity 0.3s ease 0s;
    -o-transition: opacity 0.3s ease 0s;
    transition: opacity 0.3s ease 0s;
  }

  .demo-gallery .justified-gallery>a:hover .demo-gallery-poster {
    background-color: rgba(0, 0, 0, 0.5);
  }

  .demo-gallery .video .demo-gallery-poster img {
    height: 48px;
    margin-left: -24px;
    margin-top: -24px;
    opacity: 0.8;
    width: 48px;
  }

  .demo-gallery.dark>ul>li a {
    border: 3px solid #04070a;
  }

  #swal2-content{
    font-size: 1.5em;
  }





  <?php

      if(isset($validar_Agenda)){
        echo '.swal2-modal{width:auto !important;}';
      }

  ?>

</style>


@if (isset($configuraciongeneral[7]) && $configuraciongeneral[7]=='agendavirtual')
<style>
  #label_longitud {
    display: none
  }

  #label_latitud {
    display: none
  }
</style>
@endif

@include("vistas.includes.mainjs")
{{-- AIzaSyDy1ViaIr_ziZYThMeiNuvrZK7pUb7X_SI --}}
{{-- AIzaSyCp0wkPAEubZZb6s09ajbOv3a93wHocW6k --}}
@if (isset($scriptjsagenda))
  <script src="{{ asset('js/tramites-agenda.js') }}"></script>
@endif

@php
    $url = explode('/', Request::path());
@endphp
{{-- @if($url[0] == 'tramites')
  <link rel="stylesheet" href="{{ asset('css/tramites.css') }}">
@endif --}}

<script type="text/javascript">
function mostrarImagen(ruta) {
    Swal.fire({
        imageUrl: ruta,
        width:800,
        imageHeight: 400
    })

}
function mostrarImagen_2(ruta) {
    Swal.fire({
        imageUrl: "/archivos_sistema/"+ruta,
        width:800,
        imageHeight: 400
    })

}
@if(isset($autoLocalizar))
function localizar()
{
    navigator.geolocation.getCurrentPosition(function(posicion){
    var latitud = posicion.coords.latitude;
    var longitud = posicion.coords.longitude;
    var precision = posicion.coords.accuracy;

    $('#longitud').val(longitud);
    $('#latitud').val(latitud);
    setTimeout(() => {
    // initMap(latitud ,longitud);
    inicializar_mapBox('mapa',longitud,latitud)
    }, 1000);
    },function(error){
        console.log(error);
    });
}
@endif

function tipo_documento() {
    var tipo=$("#tipo_documento").val();
      // alert('ss');
            // $("#nombre_beneficiario").val('');
            // $("#cedula_beneficiario").val('');
          if(tipo=='PASAPORTE'){
            $("#cedula_beneficiario").attr("maxlength","15");
            $("#nombre_beneficiario").removeAttr("readonly");
          }else{
            $("#nombre_beneficiario").attr("readonly",true);
            $("#cedula_beneficiario").attr("maxlength","10");
          }

  }

$(function () {
    ///indicadores 2020
    @if(isset($autoLocalizar))

      var lat=$('#latitud').val();
      var lng=$('#longitud').val();

      $("#div_longitud").hide();
      $("#div_latitud").hide();

      if(lat!='' && lng!='' && lat!=undefined && lng!=undefined){

       console.log(new Number(lat),new Number(lng));
        // localizar();
        setTimeout(() => {
          // initMap(new Number(lat),new Number(lng));
          inicializar_mapBox('mapa',new Number(lng),new Number(lat));
        }, 1000);

      }else{
        localizar();
      }



      // var date=new Date().toISOString().slice(0,10);;
      // console.log(date);
      // console.log($("#fecha_entrega").val());
      // $("#fecha_entrega").val(date);
      $("#id_donacion").change(function (e) {
        validarDonacion();
      });
      $("#id_donacion").trigger('change');
      $("#tipo_documento").change(function (e) {
        tipo_documento();
      });
      $("#tipo_documento").trigger('change');


    @endif
    $("#id_indicador").change(function (e) {
      window.location.href = '{{ URL::to("")."/".$configuraciongeneral[1]}}/create?tipo='+this.value;
    });



    $("#anonimo").click(function (e) {
      if($('#anonimo').is(':checked')) {
          $("#cedula_razon").val('ANÓNIMO');
          $("#nombre_razon").val('ANÓNIMO');
      } else {
          $("#cedula_razon").val('');
          $("#nombre_razon").val('');
      }


    });

    ///multas unaser
    $("#id_tipo_multa").change(function (e) {
      $.ajax({
              type: "GET",
              url: "{{ URL::to('consultarvalorMulta') }}?tipo="+this.value,
              success: function (response) {
                if(response.estado){
                  $("#valor").val(response.valor);
                }
              }
          });
    });


  $('#label_id_usuario_masivo').css('visibility','hidden');
  $('#label_bonton_add').css('visibility','hidden');
  $('#div-id_donacion').removeClass('col-lg-8');
  $('#div-id_donacion').addClass('col-lg-6');
  $('#div-cantidad').removeClass('col-lg-8');
  $('#div-cantidad').addClass('col-lg-1');
  $('#div-correo').removeClass('col-lg-8');
  $('#div-correo').addClass('col-lg-2');
  $('#div-telefono').removeClass('col-lg-8');
  $('#div-telefono').addClass('col-lg-2');
  $('#div-cedula_beneficiario').removeClass('col-lg-8');
  $('#div-cedula_beneficiario').addClass('col-lg-2');
  // $('#div-direccion').removeClass('col-lg-8');
  // $('#div-direccion').addClass('col-lg-2');
  $('#cantidad').val(1);


  $("#agregar_item").click(function (e) {

      const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger',
              //  container: 'sweet_containerImportant',
              popup:'sweet_popupImportant'

          },
          buttonsStyling: false
      });

      swalWithBootstrapButtons.fire({
              title: 'Ingrese el nombre item',
              input: 'text',
              inputAttributes: {
              autocapitalize: 'off'
              },
              showCancelButton: true,
              confirmButtonText: 'Agregar!',
              cancelButtonText: 'No, cancelar',
              showLoaderOnConfirm: true,
              //customClass:'swal-height-show-agenda_2',
              preConfirm: (obs) => {
                  $.ajax({
                      type: "POST",
                      data: {
                          "_token": "{{ csrf_token() }}",
                          "nombre": obs,
                      },
                      url: '{{URL::to('')}}/agregar_item',
                      success: function (data) {
                          try {

                              if (data['estado'] == 'ok') {
                                  Swal.fire({
                                      position: 'center',
                                      type: 'success',
                                      title: data['msg'],
                                      showConfirmButton: false,
                                      timer: 1500
                                  });
                                         $("#id_donacion").empty();
                                        $.each(data['datos'], function(key, element) {
                                          $("#id_donacion").append("<option value=\'" + element.id + "\'>" + element.nombre+"</option>");
                                        });
                                        $("#id_donacion").trigger("chosen:updated");
                                        $("#id_donacion").val(data['seleccionado']);
                                        $("#id_donacion").trigger("chosen:updated");


                              } else {
                                  Swal.fire({
                                      type: 'error',
                                      title: data['msg']
                                      // text: data['msg']
                                  });
                                  $("#id_donacion").empty();
                                        $.each(data['datos'], function(key, element) {
                                          $("#id_donacion").append("<option value=\'" + element.id + "\'>" + element.nombre+"</option>");
                                        });
                                        $("#id_donacion").trigger("chosen:updated");
                              }

                          } catch (error) {

                          }

                      },
                      error: function (data) {
                          //swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                      }
                  });
              }
        });

      });
  });





var fecha_inicio_agenda="";
var fecha_fina_agenda="";
var bandera=0;


   var actividad_poa;
  function llenarcombo(id,tipo,identificador,campo){
      $.ajax({
        type: "GET",
        url: "{{ URL::to('') }}/coordinacioncronograma/combos/"+id+"/"+tipo,
        success: function (response) {
          // console.log(response);
          var select = document.getElementById(identificador);

          response.forEach(element => {
            var option = document.createElement("option"); //Creamos la opcion
            option.value = element.id; //Metemos el texto en la opción
            option.innerHTML = element[campo]; //Metemos el texto en la opción
            select.appendChild(option); //Metemos la opción en el select
          });
          if(tipo==1){
              $('#programa').html(response[0]['programa']);
              $('#conceptualizacion').html(response[0]['conceptualizacion']);
              // $('#indicador').html(response[0]['indicador']);
              $('#codigo').html(response[0]['codigo']);
              $('#vision').html(response[0]['vision']);
            }
        }
      });

  }


  function borrarImagen(id){
    // alert(id);
    if(confirm("Eliminación de archivo !  \n \n ¿Desea continuar?")) {
    $.ajax({

      url: "{{URL::to('')}}/eliminarArchivo/"+id,
      type: "GET",
      data: null,
      success: function (data) {
        try {

            if (data.estado) {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    title: data['message'],
                    showConfirmButton: false,
                    timer: 1500
                }).then(() => {
                    location.reload();

                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: data['error']
                });
            }

        } catch (error) {

        }

    },
    error: function (data) {
      Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    text: "No se pudo eliminar"
                });
    }
    });
    }

  }


  @if(isset($validar_Agenda))



$(document).ready(function () {
  $('#informe').prop('checked', false)

  if($('#otros').is(':checked')){
      $('#id_usuario_masivo').prop('disabled', false).trigger("chosen:updated");

      // $('#delegaciones').prop('disabled', true).trigger("chosen:updated");
      // $('#delegaciones').attr('checked', false);
      // $('#disposicion_alcalde').val('');
      // $('#delegacion_alcalde').val('').trigger("chosen:updated");



    }else{
      $('#id_usuario_masivo').prop('disabled', true).trigger("chosen:updated");
      $('#id_usuario_masivo').val('').trigger("chosen:updated");
      // $('#delegaciones').prop('disabled', false).trigger("chosen:updated");
      // $('#delegaciones').attr('checked', true);
    }

  $('#otros').click(function (e) {
    // console.log();


    if($(this).is(':checked')){
      $('#id_usuario_masivo').prop('disabled', false).trigger("chosen:updated");
      // $('#delegaciones').prop('disabled', true).trigger("chosen:updated");
      // $('#delegaciones').attr('checked', false);
      // $('#disposicion_alcalde').val('');
      // $('#delegacion_alcalde').val('').trigger("chosen:updated");

    }else{


      $('#id_usuario_masivo').prop('disabled', true).trigger("chosen:updated");
      $('#id_usuario_masivo').val('').trigger("chosen:updated");
      // $('#delegaciones').prop('disabled', false).trigger("chosen:updated");
      // $('#delegaciones').attr('checked', true);


    }


  });
  $("#label_delegacion_alcalde").css('visibility','hidden');
  if($('#delegaciones').is(':checked')){
      $('#delegacion_alcalde').prop('disabled', false).trigger("chosen:updated");
      $('#disposicion_alcalde').prop('disabled', false);

      // $('#otros').prop('disabled', true).trigger("chosen:updated");
      // $('#otros').attr('checked', false);
    }else{
      // $('#otros').prop('disabled', false).trigger("chosen:updated");
      $('#delegacion_alcalde').prop('disabled', true).trigger("chosen:updated");
      $('#disposicion_alcalde').prop('disabled', true);
      $('#delegacion_alcalde').val('').trigger("chosen:updated");
    }

  $('#delegaciones').click(function (e) {
    if($(this).is(':checked')){

      $('#delegacion_alcalde').prop('disabled', false).trigger("chosen:updated");
      $('#disposicion_alcalde').prop('disabled', false);

      // $('#otros').prop('disabled', true).trigger("chosen:updated");
      // $('#otros').attr('checked', false);

      // $('#id_usuario_masivo').val('').trigger("chosen:updated");
    }else{
      $('#delegacion_alcalde').prop('disabled', true).trigger("chosen:updated");
      $('#disposicion_alcalde').prop('disabled', true);
      // $('#otros').prop('disabled', false).trigger("chosen:updated");
      // $('#otros').attr('checked', true);
      $('#delegacion_alcalde').val('').trigger("chosen:updated");
      $('#disposicion_alcalde').val('');


    }


  });


  $('#delegacion_alcalde').on('change', function(evt, params) {
      var selectedValue = params.selected;
      var fecha_inicio=$("#fecha_inicio").val();
      var fecha_fin=$("#fecha_fin").val();

      $.ajax({
        type: "GET",
        url: "{{URL::to('')}}/verificarDisponibilidad/"+ selectedValue +"/"+fecha_inicio+"/"+fecha_fin,
        success: function (response) {
          if(response.respuesta){
            $("#delegacion_alcalde option[value='" + selectedValue + "']").prop('selected', false);
            $("#delegacion_alcalde").trigger("chosen:updated");
            toastr['error'](response.mensaje);
          }

        }
      });

      console.log(selectedValue);
  });

});

  function initMap(lat, lng) {
    myLatlng = new google.maps.LatLng(lat, lng);

    let myOptions = {
      zoom: 18,
      zoomControl: true,
      center: myLatlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    var input = document.getElementById("direccion");
    //   var autocomplete = new google.maps.places.Autocomplete(input);

    myMarker = new google.maps.Marker({
      position: myLatlng,
      draggable: true
    });

    myMarker.setMap(map);

    google.maps.event.addListener(myMarker, "dragend", function(event) {
      // $("#ubicacion").val(event.latLng.lat() + "," + event.latLng.lng());
      $("#latitud").val(event.latLng.lat());
      $("#longitud").val(event.latLng.lng());
    });

    google.maps.event.addListener(map, "click", function(event) {
      myMarker.setPosition(event.latLng);
      $("#latitud").val(event.latLng.lat());
      $("#longitud").val(event.latLng.lng());
    });

    //   autocomplete.addListener("place_changed", function () {
    //     var place = autocomplete.getPlace();
    //     console.log('Place: ', place)
    //   });
  }
@endif


function mostrarIndicadores(id,id_cab){
  $("#divresindiajax").html('');
  $.ajax({
              type: "GET",
              url: "{{ URL::to('') }}/coordinacioncronograma/getIndicadoresGestion?id_gestion="+id+"&id="+id_cab+"&t=3",
              success: function (response) {
                  console.log(response);

                  $("#divresindiajax").html(response);
                  $('html, body').animate({
                      scrollTop: $("#divresindiajax").offset().top
                  }, 2000);
                  //$('#programa').html(response['programa']);
              }
          });

}


function mostrarBarrios(){
 var parroquia= $("#indicadores_gestion_parroquias").val();

  $.ajax({
              type: "GET",
              url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+parroquia,
              success: function (response) {
                $("#indicadores_gestion_barrios").empty();
                $.each(response, function(key, element) {
                  $("#indicadores_gestion_barrios").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                });
                $("#indicadores_gestion_barrios").trigger("chosen:updated");


              }
          });

}
function mostrarBarriosDeta(id){
//  alert(id);
  $.ajax({
              type: "GET",
              url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+id+'?tipo=1',
              success: function (response) {
                Swal.fire({
                title: response.msg,
                text: "DETALLE",
                html:response,
                showCancelButton: false,
                confirmButtonText: 'OK',
                width:800,
                showLoaderOnConfirm: true,
                preConfirm: (codigo) => {
                },
                allowOutsideClick: () => !Swal.isLoading()
            });

              }
          });

}


function obtenerUbicacion() {
  let direccion = $('#direccion').val()
  let parroquia_id = $('#parroquia_id').val()
  let parroquia =   $('#parroquia_id option:selected').text();

  if(parroquia_id!=8){
    direccion=parroquia+","+direccion
  }

  if(direccion==''){
    direccion='GAD MANTA';
  }
      $.ajax({
        type:'GET',
        url: `{{URL::to('')}}/tramites/obtener_ubicacion/${direccion}`,
      }).done(function(data) {
        console.log(data['status']);
        if(data['status'])
        {
          let lat = data['results'][0].geometry['location'].lat;
          let lng = data['results'][0].geometry['location'].lng;
          // initMap(lat, lng)
          inicializar_mapBox('mapa',lng,lat);


          $("#longitud").val(lng);
          $("#latitud").val(lat);

          // var myLatlng = new google.maps.LatLng(lat, lng);
				  // var myOptions_ = {
			  	// 	zoom: 13,
					//   center: myLatlng,
			  	// 	mapTypeId: google.maps.MapTypeId.ROADMAP};

          //   map = new google.maps.Map(document.getElementById("map_canvas"), myOptions_);

          // marker_0.setOptions({
          //   zoom: 16,
					//   // center: new google.maps.LatLng(lat,lng),
          //   position: new google.maps.LatLng(lat,lng)
          // });


        }
      })
}

  $(document).ready(function() {



    let params = (new URL(document.location)).searchParams;
    let revision = params.get('revision'); // is the string "Jonathan Smith".
    if(revision == null) {
      $("#label_estado_proceso").css('visibility', 'hidden');
    }


      $( "#cedula_beneficiario" ).dblclick(function() {
          var cedula= $(this).val();
          var tipo=$("#tipo_documento").val();
          if(tipo=='PASAPORTE'){

          }else{
            $.ajax({
              type: "GET",
              url: "{{ URL::to('consultardatosseguro') }}?documento="+cedula+"&tipo=1015",
              success: function (response) {
                  console.log(response);
                  $("#divresajax").html(response);
                  $('html, body').animate({
                      scrollTop: $("#divresajax").offset().top
                  }, 2000);
                  //$('#programa').html(response['programa']);
              }
          });
          }

      });


  $("#cedula_beneficiario").blur(function (e) {

            $.ajax({
              type: "GET",
              url: "{{ URL::to('consultardinaciones') }}?documento="+this.value,
              success: function (data) {
                console.log(data);
                if (data['estado'] == 'ok' && data['tabla']!=null) {
                    Swal.fire({
                        // position: 'center',
                        // type: 'success',
                        html: data['tabla'],
                        width:600,
                        showConfirmButton: true
                        // timer: 1500
                    });
                }

              }
            });

  });


//Evento change al escoger en Select
$("#label_json_indicadores").hide();
$("#btnmas").click(function(){
    $('#contenidodiv').css("overflow","hidden").css("height","auto");
    $(this).hide();
});
$('#id_componente').change(function (e) {
  $('#id_objetivo_estra').empty();
  llenarcombo($(this).val(),1,'id_objetivo_estra','objetivo');
  $('#id_ods').empty();
  llenarcombo($(this).val(),2,'id_ods','ods');
  $('#id_meta_ods').empty();
  setTimeout(() => {
    llenarcombo($('#id_ods').val(),3,'id_meta_ods','meta_ods');
  }, 1000);


});


@if(isset($configuraciongeneral[7]) &&  $configuraciongeneral[7]=="proyecto" &&  $configuraciongeneral[2]=="editar" )
var id=$('#id_componente').val();
var tipo=6;
$.ajax({
  type: "GET",
  url: "{{ URL::to('') }}/coordinacioncronograma/combos/"+id+"/"+tipo,
  success: function (response) {
    console.log(response);
              $('#programa').html(response['programa']);
              $('#conceptualizacion').html(response['conceptualizacion']);
              // $('#indicador').html(response['indicador']);
              $('#codigo').html(response['codigo']);
        }
      });

var id_plan=$('#id_tipo_proyecto').val();
$.ajax({
        type: "GET",
        url: "{{ URL::to('') }}/coordinacioncronograma/combos/"+id_plan+"/5",
        success: function (response) {
          $('#codigo_plan').html(response['codigo']);
        }
});
@endif





$('#id_objetivo_estra').change(function (e) {
  var id= $(this).val();
  $.ajax({
        type: "GET",
        url: "{{ URL::to('') }}/coordinacioncronograma/combos/"+id+"/4",
        success: function (response) {
          $('#programa').html(response['programa']);
          $('#conceptualizacion').html(response['conceptualizacion']);
          // $('#indicador').html(response['indicador']);
          $('#codigo').html(response['codigo']);
        }
      });
});

$('#id_tipo_proyecto').change(function (e) {
  var id= $(this).val();
  $.ajax({
        type: "GET",
        url: "{{ URL::to('') }}/coordinacioncronograma/combos/"+id+"/5",
        success: function (response) {
          $('#codigo_plan').html(response['codigo']);
        }
      });
});

$('#id_ods').change(function (e) {

  $('#id_meta_ods').empty();
  setTimeout(() => {
    llenarcombo($(this).val(),3,'id_meta_ods','meta_ods');
  }, 10);
});
@if(!isset($botonguardaravance))
$("#avance").change(function(){
    var avance=$(this).val();
     if(avance==100){
        $('#archivo_final').removeClass('hidden');
        $('#label_archivo_final').css('display','block');
        $('#label_archivo_final').removeClass('hidden');
        $('#btn_guardar').addClass('hidden');
        $('#add_actividad').addClass('hidden');
      }else{
        $('#archivo_final').addClass('hidden');
        $('#label_archivo_final').css('display','none');
        $('#label_archivo_final').addClass('hidden');
        $('#btn_guardar').removeClass('hidden');
        $('#add_actividad').removeClass('hidden');
      }
});


$("#avance_actividad").change(function(){
    var avance=$(this).val();
     if(avance==100){
        $('#archivo_final').removeClass('hidden');
        $('#label_archivo_final').css('display','block');
        $('#label_archivo_final').removeClass('hidden');
        $('#btn_guardar').addClass('hidden');
        $('#add_actividad').addClass('hidden');
      }else{
        $('#archivo_final').addClass('hidden');
        $('#label_archivo_final').css('display','none');
        $('#label_archivo_final').addClass('hidden');
        $('#btn_guardar').removeClass('hidden');
        $('#add_actividad').removeClass('hidden');
      }
});
@else
$('#archivo_final').removeClass('hidden');
        $('#label_archivo_final').css('display','block');
        $('#label_archivo_final').removeClass('hidden');
@endif
$("#archivo_final").on('change', function() {
    if ($('#archivo_final').val()) {
      $('#btn_guardar').removeClass('hidden');
      $('#add_actividad').removeClass('hidden');
    }
});
$('#fecha_inicio').focusout(function (e) {
   $('#fecha_fin').val($('#fecha_inicio').val());
  //  console.log('sssssss');
});






$('#label_observacion_verificacion').css('display','none');
$('#observacion_verificacion').css('display','none');
$('#label_observacion_verificacion_obra_act').css('display','none');
$('#observacion_verificacion_obra_act').css('display','none');
verificacion2020();
$("#verificacion").on('change', function() {
  verificacion2020();
});
$("#verificacion_obra_act").on('change', function() {
  verificacion2020();
});



$("#agregar_barrio").click(function (e) {
  var indicador=$('#indicadores_gestion option:selected').val();
  var indicador_text=$('#indicadores_gestion option:selected').text();
  var parroquia=$('#indicadores_gestion_parroquias option:selected').val();
  var parroquiatext=$('#indicadores_gestion_parroquias option:selected').text();
  var barrio=$('#indicadores_gestion_barrios option:selected').val();
  var barriotext=$('#indicadores_gestion_barrios option:selected').text();

  if(parroquia!=null &&  barrio!=null){
    $("#tabla_indicadores").append('<tr id="fila_'+n_filas+'" ><td>'+indicador_text+'<br></td><td><b>'+parroquiatext+'</b></td><td><select><option value="'+barrio+'">'+barriotext+'</option></select></td><td><input hidden  value="'+parroquia+'"><input type="number"  onkeyup="armar_json(\'+n_filas+\')"  value="0" class="valores"><input type="number" hidden  value="3"></td><td><input type="number" step="any"  onkeyup="armar_json(\'+n_filas+\')" class="valores" value="0"></td><td><input type="number"  onkeyup="armar_json(\'+n_filas+\')" class="valores " value="0"></td><td><i onclick="borrar('+n_filas+')" class="fa fa-times-circle" style="font-size: x-large;color: red;" aria-hidden="true"></i></td></tr>');

  }
  n_filas++;
});
$("#indicadores_gestion_parroquias").change(function (e) {
  mostrarBarrios();
});




@if(isset($validar_Agenda))


// obtenerUbicacion();

$('#direccion').on('blur', function() {
  obtenerUbicacion();
});






$('.consultar_fecha').on('blur',function (e) {


      fecha_inicio_agenda=$("#fecha_inicio").val();
      fecha_fina_agenda=$("#fecha_fin").val();
      if(fecha_fina_agenda==""){
        fecha_fina_agenda="0";
      }
      if(fecha_inicio_agenda==""){
        fecha_inicio_agenda="0";
      }
        e.preventDefault();


      $.ajax({
        type: "GET",
        url: "{{URL::to('')}}/coordinacioncronograma/verficar_existente/"+fecha_inicio_agenda+"/"+fecha_fina_agenda+"/1",
        data: null,
        dataType: "json",
        success: function (response) {
          if(response.estado=="false"){

              Swal.fire({
                title: response.msg,
                html: response.actividades,
                showCancelButton: false,
                confirmButtonText: 'OK',
                cancelButtonText: 'NO',
                showLoaderOnConfirm: true,
                preConfirm: (codigo) => {
                    // if(codigo){
                    //   $('<input>').attr({
                    //       type: 'hidden',
                    //       id: 'en_espera',
                    //       name: 'en_espera',
                    //       value: "SI"
                    //   }).appendTo('form');
                    // }else{
                    //   try {
                    //     $('#en_espera').remove();
                    //   } catch (error) {
                    //   }
                    // }
                },
                allowOutsideClick: () => !Swal.isLoading()
            });


          }else{
            // try {
            //             $('#en_espera').remove();
            //           } catch (error) {
            //           }
          }

        }
      });
});


$("#div_js_indicadores_plantilla").hide();
if($("#id_tipo").val()==18){
  // $("#div_notificaciones").hide();
  // $("#div_otros").hide();
  // $("#div_id_usuario_masivo").hide();
  // $("#div_delegaciones").hide();
  // $("#div_delegacion_alcalde").hide();
  // $("#div_disposicion_alcalde").hide();

}

$("#id_tipo").change(function (e) {
  if(this.value==18){

  //   $("#div_notificaciones").hide();
  // $("#div_otros").hide();
  // $("#div_id_usuario_masivo").hide();
  // $("#div_delegaciones").hide();
  // $("#div_delegacion_alcalde").hide();
  // $("#div_disposicion_alcalde").hide();



  }else{
    $("#div_notificaciones").show();
  $("#div_otros").show();
  $("#div_id_usuario_masivo").show();
  $("#div_delegaciones").show();
  $("#div_delegacion_alcalde").show();
  $("#div_disposicion_alcalde").show();
  }

});

@endif




@if(isset($aceptarDocs))
  let tipoArchivos = ".pdf,.doc,.docx";
@else
  let tipoArchivos = ".jpg,.jpeg,.png,.pdf";
@endif
Dropzone.options.myDropzone = {
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 3,
            maxFiles: 2,
            acceptedFiles: tipoArchivos,
            addRemoveLinks: true,

            init: function() {
                var submitBtn = document.querySelector("#submit");
                myDropzone = this;

                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                    //console.log(e);
                });

                this.on("addedfile", function(file) {
                  console.log(file.size);
                  if(file.size > (1024 * 1024 * 10)) // not more than 5mb
                  {
                    toastr["error"]("El peso máximo por archivo es de 10MB.");
                    this.removeFile(file);
                  }else{
                    toastr["success"]("Se Agregó un Nuevo Archivo al Gestor");
                  }


                });


                this.on("complete", function(file) {
                    myDropzone.removeFile(file);



                });

                this.on("success", function(data) {
                  var txt = data.xhr.responseText;
                  myDropzone.processQueue.bind(myDropzone);
                   //$("#ModalArchivos").hide();
                   toastr["success"]("Archivos Agregados Correctamente. "+txt);
                   location.reload();

                  //console.log(data.xhr.responseText);
                });


                this.on("maxfilesexceeded", function(file){
                  toastr["error"]("Solo puede agregar 2 elementos al gestor de archivos");
                });

            }
};
Dropzone.options.mydropzoneactividades = {
            autoProcessQueue: false,
            uploadMultiple: true,
            maxFilezise: 5,
            maxFiles: 2,
            acceptedFiles:".jpg,.jpeg,.png,.pdf",
            addRemoveLinks: true,

            init: function() {

                var submitBtn = document.querySelector("#submit_actividades");
                mydropzoneactividades = this;

                submitBtn.addEventListener("click", function(e){
                    e.preventDefault();
                    e.stopPropagation();
                  // alert('s');
                    mydropzoneactividades.processQueue();
                    console.log(mydropzoneactividades.processQueue());
                });
                this.on("addedfile", function(file) {
                  console.log(file.size);
                  if(file.size > (1024 * 1024 * 10)) // not more than 5mb
                  {
                    toastr["error"]("El peso máximo por archivo es de 10MB.");
                    this.removeFile(file);
                  }else{
                    toastr["success"]("Se Agregó un Nuevo Archivo al Gestor");
                  }

                });

                this.on("complete", function(file) {
                    mydropzoneactividades.removeFile(file);



                });

                this.on("success", function(data) {
                  var txt = data.xhr.responseText;
                  mydropzoneactividades.processQueue.bind(mydropzoneactividades);
                   //$("#ModalArchivos").hide();
                   toastr["success"]("Archivos Agregados Correctamente. "+txt);
                   location.reload();

                  //console.log(data.xhr.responseText);
                });
                this.on("maxfilesexceeded", function(file){
                  toastr["error"]("Solo puede agregar 2 elementos al gestor de archivos");
                });

            }
};


@if($configuraciongeneral[2]=="crear")
$('#label_detalle_poa').hide();
$('#detalle_poa').hide();
// $("#coordinacion_actividad").val(null);
@endif

$("#id_tipo_actividad").change(function(){
    var id=this.value;
    // var ruta= '<?php echo URL::to('getmenumodulos'); ?>?idmenu='+id;
    // $.get(ruta, function(data) {
    //   console.log(data);
    //   $("#id_menu_hijo").empty();
    //   /*$("#id_menu_hijo").append("<option value>Escoja...</option>");*/
    //   $.each(data, function(key, element) {
    //     $("#id_menu_hijo").append("<option value='" + key + "'>" + element + "</option>");
    //   });
    //   $('#id_menu_hijo').trigger("chosen:updated");
    // });
      $('#label_detalle_poa').hide();
      $('#detalle_poa').hide();
    if(id==2){
      $('#detalle_poa').show();
      $('#label_detalle_poa').show();

          @if(isset($edit))
            $('#idusuario_chosen').hide();
            $('#label_detalle_poa').hide();
            $('#detalle_poa').hide();
            $('#label_idusuario').hide();
          @endif


    }



 });

 @if(isset($edit))
  $('#idusuario_chosen').hide();
  $('#label_detalle_poa').hide();
  $('#detalle_poa').hide();
  $('#label_idusuario').hide();
@endif



 $("#id_menu").change(function(){
    var id=this.value;
    var ruta= '<?php echo URL::to('getmenumodulos'); ?>?idmenu='+id;
    $.get(ruta, function(data) {
      console.log(data);
      $("#id_menu_hijo").empty();
      /*$("#id_menu_hijo").append("<option value>Escoja...</option>");*/
      $.each(data, function(key, element) {
        $("#id_menu_hijo").append("<option value='" + key + "'>" + element + "</option>");
      });
      $('#id_menu_hijo').trigger("chosen:updated");
    });
 });





//if($configuraciongeneral[2]=="crear")

  //$("#estado_obra").hide();
  //$("label[for='estado_obra']").hide();

//endifmessage

  $(".divpopup").colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30)});
});

function popup(obj)
{
    //console.log(obj);
    //console.log($(obj).parent().parent());
    var row= $(obj).parent().parent();
    row.css("background-color","#cdf7c8");
    $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
}


  function verificacion2020(){
    if ($('#verificacion').val()=="VERIFICADO CON OBSERVACIONES" || $('#verificacion_obra_act').val()=="VERIFICADO CON OBSERVACIONES") {
      $('#observacion_verificacion_obra_act').removeClass('hidden');
      $('#observacion_verificacion_obra_act').css('display','block');
      $('#label_observacion_verificacion_obra_act').css('display','block');

      $('#observacion_verificacion').removeClass('hidden');
      $('#observacion_verificacion').css('display','block');
      $('#label_observacion_verificacion').css('display','block');

    }else{
      $('#observacion_verificacion_obra_act').addClass('hidden');
      $('#observacion_verificacion_obra_act').css('display','none');
      $('#label_observacion_verificacion_obra_act').css('display','none');
      $('#observacion_verificacion_obra_act').val('');

      $('#observacion_verificacion').addClass('hidden');
      $('#observacion_verificacion').css('display','none');
      $('#label_observacion_verificacion').css('display','none');
      $('#observacion_verificacion').val('');
    }

    $('#btn_guardar').removeClass('hidden');
    $('#add_actividad').removeClass('hidden');
  }
</script>
@if (isset($scriptjs))
  <script src="{{ asset('js/tramites.js').'?v='.rand(1,1000) }}"></script>
@endif


@isset($validar_AgendaTurismo)
@include("vistas.includes.funcionesAgendaTurismo")
@endisset




@include("vistas.includes.jsfunciones")
@stop
@section ('contenido')
@if (Session::has('message'))
<script>
  $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
    $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)});
        });
});
</script>
<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
{{-- {{dd(Session::all())}} --}}


@if (Session::has('warning'))
<script>
  $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["warning"]("{{ Session::get('warning') }}");
    //$.notify("{{ Session::get('message') }}","success");
    // $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.50), innerHeight:screen.height -(screen.height * 0.55)});
    //     });
});
</script>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div class="alert alert-warning text-center">⚠ Advertencia ⚠ <br>{{ Session::get('warning') }}</div>
  </div>
</div>
@endif




@if (Session::has('error'))
<script>
  $(document).ready(function() {
    toastr["error"]("{{ Session::get('error') }}");


});
</script>
<div class="alert alert-danger">{{ Session::get('error') }}</div>
@endif
<h1>
  @if($configuraciongeneral[2]=="crear")
  {!! $configuraciongeneral[0] !!}
  @else
  {!! $configuraciongeneral[0] !!} <i class="fa fa-edit"></i>
  @endif
</h1>

<div class="ibox float-e-margins">
  <div class="ibox-title">
    <div class="">
    @if(!request()->has("menu"))
      @if(!isset($variableControl))
      <a href="{{ URL::to($configuraciongeneral[1]).((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-primary "><i class="fa fa-align-justify"></i> Todos</a>
            @if(isset($permisos))
                @if($permisos->crear=="SI")
                    <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default ">Nuevo</a>
                @endif
            @else
                <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default ">Nuevo</a>
            @endif
        {{--
              @if($configuraciongeneral[2]=="crear")
                <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default "><i class="fa fa-file-o"></i> Nuevo</a>
              @else
                <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default "><i class="fa fa-edit"></i> Nuevo</a>
              @endif
--}}
      @endif
    @endif
    </div>
    <!-- <a href="" id="enlace_tipo_indicador">Abrir ventana modal</a> -->

    <!--Enviar errores de validacion en caso de que los haya-->
    @if($errors->any())
    <script>
      $(document).ready(function() {
        //    $.notify("Error al guardar!","error");
        toastr["error"]($("#diverror").html());
        });
    </script>
    <div class="alert alert-danger" style="text-align: left;" id='diverror'>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4>Error!</h4>
      <div id="diverror">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    </div>
    @endif
    @if(isset($alerta) && $alerta!="")
    <div class="alert alert-danger" style="text-align: left;" id='diverror'>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <h4>Error!</h4>
      <ul>
        <li>{!! $alerta !!}</li>
      </ul>
    </div>
    @endif
    <!--  -->
    @if (Session::has('message'))
    <script>
      $(document).ready(function() {
      //toastr.succes("{{ Session::get('message') }}");
      toastr["success"]("{{ Session::get('message') }}");
      //$.notify("{{ Session::get('message') }}","success");
  });
    </script>
    <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
  </div>

  <div class="ibox-content">
    @php $menupo=""; @endphp
    @if(request()->has("menu"))
      @php $menupo="?menu=no"; @endphp
    @endif
    @if($configuraciongeneral[2]=="crear")
    <!--{!  Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal'))  !!} -->
    {!! Form::open(array('url' => $configuraciongeneral[1].$menupo,'role'=>'form' , 'id'=>'form1','class'=>'form-horizontal',
    'files' => true,'style'=>"padding-bottom: 10%;")) !!}
    @else
    {!! Form::model($tabla, array('url' => $configuraciongeneral[1].'/'. $tabla->id.$menupo, 'method' =>
    'PUT','class'=>'form-horizontal', 'id'=>'form1', 'files' => true )) !!}
    @endif


    @if (isset($labels_tramite))
      <div class="loader" id="position" style="diplay: none;"></div>
    @endif

    @foreach($objetos as $i => $campos)
    <div class="form-group " id="div_{{$campos->Nombre}}">
      @if($campos->Tipo!="divresul")
      {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("id"=>"label_".$campos->Nombre,"class"=> "col-lg-3  control-label class_".$campos->Nombre)) !!}
      @endif
      <div class="col-lg-{{(isset($campos->Adicional))?'6':'8'}}" id="div-{{$campos->Nombre}}">
        @if($campos->Tipo=="funcionjs")
        {!! $campos->Valor !!}
        @elseif($campos->Tipo=="date")
        <!-- {! Form::input('date',$campos->Nombre,$campos->Valor, ['class' => 'form-control', 'placeholder' => 'Date']) !!}   -->
        {!! Form::text($campos->Nombre,null,['data-placement'=>'top','data-toggle'=>'tooltip','class'=>'form-control
        datefecha','placeholder'=>$campos->Descripcion, $campos->Nombre, 'readonly'=>'readonly']) !!}
        @elseif($campos->Tipo=="html")
        <p id="{{$campos->Nombre}}" class="form-control" style="height: auto;">

        </p>
        @elseif($campos->Tipo=="html2")

        <div class="col-lg-12">
          <?php echo $campos->Valor; ?>
        </div>


        @elseif($campos->Tipo=="datetext")
        <div class="input-group date">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

          {!! Form::text($campos->Nombre, (($campos->Valor != 'Null')?$campos->Valor:null), array(
          'data-placement' =>'top',
          'data-toogle'=>'tooltip',
          'class' => 'form-control datefecha '.$campos->Clase,
          'placeholder'=>$campos->Descripcion,
          $campos->Nombre,
          'readonly'=>'readonly'
          )) !!}
        </div>
        @elseif($campos->Tipo=="datetimetext")
        <div id="date_{{$campos->Nombre}}" class="input-group datetime">
          <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>

          {!! Form::text($campos->Nombre, (($campos->Valor != 'Null')?$campos->Valor:null) , array(
          'data-placement' =>'top',
          'data-toogle'=>'tooltip',
          'class' => 'form-control datefecha '.$campos->Clase,
          'placeholder'=>str_replace("(*)","",$campos->Descripcion),
          $campos->Nombre,
          )) !!}
        </div>

        @elseif($campos->Tipo=="select")
        {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) :
        ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
        "form-control ".$campos->Clase)) !!}
        @elseif($campos->Tipo=="telefonos")
          <div class="row">
            <div class="col-md-6">
              {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
              'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion)) !!}
            </div>
            <div class="col-md-6">
              {!! Form::text($campos->Nombre.'_2',old($campos->Valor), array('class' =>
              'form-control solonumeros ' . $campos->Clase, 'placeholder'=>'Ingrese '. $campos->Descripcion . ' 2')) !!}
            </div>
          </div>
        @elseif($campos->Tipo=="numtramite")
          <div class="row">
            <div class="col-md-6">
              {!! Form::text($campos->Valor, $campos->ValorAnterior, array('class' =>
              'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion, 'id' => $campos->Nombre, 'name' => $campos->Nombre)) !!}
            </div>
            <div class="col-lg-2">
              <button class="btn btn-info" type="button" onclick="actualizarNumTramiteExterno()">
                  <i class="fa fa-history"></i>
              </button>
          </div>
          </div>
        @elseif($campos->Tipo=="direccion")
          <div class="row">
            <div class="col-md-6">
              {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
              'form-control ' . $campos->Clase,'placeholder'=>'Ingrese '. $campos->Descripcion)) !!}
            </div>
            <div class="col-md-6">
              {!! Form::text('referencia',old($campos->Valor), array('class' =>
              'form-control '. $campos->Clase, 'placeholder'=>'Ingrese una referencia de su dirección*')) !!}
            </div>
          </div>
        @elseif($campos->Tipo=="select2")
        {!! Form::select($campos->Nombre,$campos->Valor, $campos->ValorAnterior, array('class' => "form-control
        ".$campos->Clase)) !!}
        @elseif($campos->Tipo=="select-multiple")
        {!! Form::select($campos->Nombre."[]",$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre)
        : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
        "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre))!!}
        @elseif($campos->Tipo=="file")
        {!! Form::file($campos->Nombre, array('class' => 'form-control '.$campos->Clase)) !!}
        @if(isset($tabla))
        <br>
        <?php
           $id_c= $campos->Nombre;
          if (strpos($tabla->$id_c, 'pdf') === false) {
            $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a onclick=\"mostrarImagen(\''.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").'\')\" class=\"btn btn-info dropdown-toggle\"><i class=\"fa fa-picture-o\"></i> Ver imagen</a></div>';";
          } else {
              $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a></div>';";
          }
          // dd($cadena);
          eval($cadena);
                          ?>
        @endif
        @elseif($campos->Tipo=="file-solo")
        @if(isset($tabla))
        <br>
        <?php
           $id_c= $campos->Nombre;
           if (strpos($tabla->$id_c, 'pdf') === false) {
             $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a onclick=\"mostrarImagen(\''.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").'\')\" class=\"btn btn-info dropdown-toggle\"><i class=\"fa fa-picture-o\"></i> Ver imagen</a></div>';else echo 'Sin archivo';";
            } else {
              $cadena="if(\$tabla->".$campos->Nombre."!='') echo '<div id=\"archivo\"><span> $campos->Descripcion </span>  <a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a></div>'; else echo 'Sin archivo';";
            }
            // dd($id_c);
            // dd($cadena);
          eval($cadena);
                          ?>
        @endif

        @elseif($campos->Tipo=="file-multiple")
          <script type="text/javascript">
            var contador_archivos=0;
            $(function () {
              $("#agregar_{{$campos->Nombre}}").click(function (e) {
                 $("#multiple_{{$campos->Nombre}}").append('<br><input type="file" name="{{$campos->Nombre}}[]" id="archivo_'+contador_archivos+'" class="form-control">');

              });

            });

            function borrarArchivo(id){
              Swal.fire({
                title: '¿Desea eliminar este archivo?',
                showCancelButton: true,
                confirmButtonText: 'ELIMINAR',
                cancelButtonText: 'CANCELAR',
                showLoaderOnConfirm: true,
                preConfirm: (codigo) => {
                    $.ajax({
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        url: '{{URL::to('')}}/coordinacioncronograma/{{(isset($configuraciongeneral[11]))?$configuraciongeneral[11]:"eliminar_archivo"}}/'+id,
                        success: function (data) {
                            try {

                                if (data.estado == 'ok') {
                                    Swal.fire({
                                        position: 'center',
                                        type: 'success',
                                        title: data['msg'],
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        location.reload();

                                    });
                                } else {
                                    Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                                }

                            } catch (error) {

                            }

                        },
                        error: function (data) {
                          Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                        }
                    });

                },
                allowOutsideClick: () => !Swal.isLoading()
            });

            }


          </script>

          <button type="button" id="agregar_{{$campos->Nombre}}" class="btn btn-info" > <i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar item</button>
          <br>
          <br>
           <div id="multiple_{{$campos->Nombre}}">

             @php
              $valores=$campos->Valor;
              if(is_array($valores) && !empty($valores)){
                $tabla="<table class='table'><tbody><tr><th>Archivo</th></tr></tbody>";

                foreach ($valores as $key => $value) {
                    $tabla.='<tr><td>'.$value.'&nbsp&nbsp&nbsp<a href='.URL::to("").'/archivos_sistema/'.$value.' class="btn btn-success divpopup" target="_blank"><i class="fa fa-file-pdf-o"></i> Ver Archivo</a> <button type="button"  class="btn btn-danger" onclick="borrarArchivo('.$key.')"> <i class="fa fa-minus-circle" aria-hidden="true"></i> Borrar archivo</button></td></tr>';

                }
                $tabla.=" </table>";
                echo $tabla;

              }else{
                echo '<input type="file" name="'.$campos->Nombre.'[]" id="archivo_0" class="form-control">';
              }

             @endphp

          </div>

        <!-- {!! Form::file($campos->Nombre.'[]', array('class' => 'form-control '.$campos->Clase, 'multiple'=>'multiple')) !!} -->

        @elseif($campos->Tipo=="checkbox")

        {!! Form::checkbox($campos->Nombre,null,$campos->Valor) !!}

        @elseif($campos->Tipo=="textdisabled")

        {!! Form::text($campos->Nombre,old($campos->Valor), array('class' =>
        'form-control','placeholder'=>'Ingrese'. ((isset($camposcaption[$i]))?$camposcaption[$i]:"") ,'readonly')) !!}
        @elseif($campos->Tipo=="textdisabled3")

        {!! Form::text($campos->Nombre,$campos->Valor, array('class' =>
        'form-control','placeholder'=>'Ingrese'. ((isset($camposcaption[$i]))?$camposcaption[$i]:"") ,'readonly')) !!}

        @elseif($campos->Tipo=="dropzone")
        {{-- <form action="#" class="dropzone" id="dropzoneForm"> --}}
          <div class="fallback">
              <input name="file" type="file" multiple />
          </div>
      {{-- </form>  --}}
        @elseif($campos->Tipo=="textarea")
        @if (isset($verObservaciones) && $campos->Clase=="mostrarobservaciones" )
        <div id="contenidodiv">
          @php $fechi=""; $txtob=""; @endphp
          @foreach ($timelineActividades as $item)
          @php $fechi=$item->updated_at; $txtob=$item->observacion; @endphp
          @endforeach
          <p style="text-align: justify;" class="text-muted"> <strong class="badge badge-primary"><i
                class="fa fa-clock-o"></i> {{$fechi}} </strong> - {{$txtob }}</p>
        </div>
        <a class="btn btn-xs btn-primary" href="javascript:" id="btnmas"><i class="fa fa-search-plus"></i> Mostrar Todo
        </a>
        <script type="text/javascript">
          $(document).ready(function() {
                          //Evento change al escoger en Select
                          $('#contenidodiv').scrollTop($('#contenidodiv')[0].scrollHeight);
                          $("#observacion").val("");
                          $("#observacion").focus();
                          //$(window).scrollTop($('#observacion').offset().top);
                          //$("#idusuario")[0].scrollIntoView(true);
                        });
        </script>
        @endif
        <?php
                          if($campos->Clase!="Null")
                          {
                            $bus=strpos($campos->Clase,":");
                            if($bus)
                            {
                              $bus=explode(":",$campos->Clase);
                        ?>
        <script type="text/javascript">
          $(document).ready(function() {
                            $("#{{ $campos->Nombre}}").attr('maxlength',{{$bus[1]}});
                          });
        </script>
        <?php
                            }
                          }
                        ?>
        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
        $campos->Clase ,'placeholder'=>'Ingrese '.str_replace("(*)","",$campos->Descripcion))) !!}
        @elseif($campos->Tipo=="textarea2")
        @if (isset($verObservaciones) && $campos->Clase=="mostrarobservaciones" )
        <div id="contenidodiv">
          @php $fechi=""; $txtob=""; @endphp
          @foreach ($timelineTramites as $item)
          @php $fechi=$item->updated_at; $txtob=$item->observacion; @endphp
          @endforeach
          <p style="text-align: justify;" class="text-muted"> <strong class="badge badge-primary"><i
                class="fa fa-clock-o"></i> {{$fechi}} </strong> - {{$txtob }}</p>
        </div>
        <a class="btn btn-xs btn-primary" href="javascript:" id="btnmas"><i class="fa fa-search-plus"></i> Mostrar Todo
        </a>
        <script type="text/javascript">
          $(document).ready(function() {
                          //Evento change al escoger en Select
                          $('#contenidodiv').scrollTop($('#contenidodiv')[0].scrollHeight);
                          $("#observacion").val("");
                          $("#observacion").focus();
                          //$(window).scrollTop($('#observacion').offset().top);
                          //$("#idusuario")[0].scrollIntoView(true);
                        });
        </script>
        @endif
        <?php
                          if($campos->Clase!="Null")
                          {
                            $bus=strpos($campos->Clase,":");
                            if($bus)
                            {
                              $bus=explode(":",$campos->Clase);
                        ?>
        <script type="text/javascript">
          $(document).ready(function() {
                            $("#{{ $campos->Nombre}}").attr('maxlength',{{$bus[1]}});
                          });
        </script>
        <?php
                            }
                          }
                        ?>
        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion)) !!}




        @elseif($campos->Tipo=="FormHidden")
              {!! Form::hidden($campos->Nombre,$campos->Valor ,array("id"=>$campos->Nombre)) !!}
        @elseif($campos->Tipo=="htmlplantilla")
              <div class="form-group col-md-12 col-lg-12" id="obj-{{$campos->Nombre}}">
                  <div class="col-lg-12 " id="{{$campos->Nombre}}">
                      <?php echo $campos->Valor; ?>
                  </div>
              </div>
        @elseif($campos->Tipo=="textmayus")
        {!! Form::text($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' . $campos->Clase
        ,'placeholder'=>'Ingrese '.$campos->Descripcion,'onkeyup'=>'aMays(event, this)')) !!}
        @elseif($campos->Tipo=="div")
        <br>
        <table id="{{$campos->Nombre}}" class="table table-striped table-bordered table-hover display">
          <tr>
            <th>Financiamiento</th>
            <th>Costo Institucional</th>
            <th>Costo Proveedor</th>
            <th>Costo Inicial</th>
            <th>Costo Actual</th>
            <th>Diferencia</th>
          </tr>
          <tr>
            <th>
              <select style="width:10em" name="financiamiento">
                @foreach($tipopoa as $tipo)

                <option value="{{$tipo}}"> {{$tipo}} </option>

                @endforeach

              </select>
            </th>
            <th>
              <input type="number" onkeyup="calcular()" class="anchoinput" id="costo_institucion"
                name="costo_institucion" value="0">
            </th>
            <th>
              <input type="number" onkeyup="calcular()" class="anchoinput" id="costo_prov" name="costo_prov" value="0">
            </th>
            <th>
              <input type="number" onkeyup="calcular()" class="anchoinput" id="costo_inicial" name="costo_inicial"
                value="0">
            </th>

            <th>
              <input type="number" onkeyup="calcular()" class="anchoinput" id="costo_actual" name="costo_actual"
                value="0">
            </th>

            <th>
              <input type="number" class="anchoinput" id="diferencia" name="diferencia" value="0">

            </th>
          </tr>
        </table>



        @elseif($campos->Tipo=="password")
        {!! Form::password($campos->Nombre,array('class' => 'form-control','placeholder'=>'Ingrese '.$campos->Nombre))
        !!}
        @elseif($campos->Tipo=="textdisabled2")
        {!! Form::text($campos->Nombre,old($campos->Valor), array('class' => 'form-control
        '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
        @elseif($campos->Tipo=="selectdisabled")
        {!! Form::select($campos->Nombre,$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre) :
        ($campos->ValorAnterior !="Null") )? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
        "form-control selectdisabled".$campos->Clase, 'readonly')) !!}
        @elseif($campos->Tipo=="select-multiple-disabled")
        {!! Form::select($campos->Nombre."[]",$campos->Valor, (old($campos->Nombre) ? old($campos->Nombre)
        : ($campos->ValorAnterior !="Null")) ? $campos->ValorAnterior : old($campos->Nombre), array('class' =>
        "form-control ".$campos->Clase,'multiple'=>'multiple',"id"=>$campos->Nombre,'readonly'))!!}
        @elseif($campos->Tipo=="textarea-disabled")
        {!! Form::textarea($campos->Nombre, old($campos->Valor) , array('class' => 'form-control ' .
        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion,'readonly')) !!}
        @elseif($campos->Tipo=="textarea-2")
        {!! Form::textarea($campos->Nombre, $campos->Valor , array('class' => 'form-control ' .
        $campos->Clase ,'placeholder'=>'Ingrese '.$campos->Descripcion,"rows"=>3)) !!}
        @elseif($campos->Tipo=="mapa")
        <div id="{{$campos->Tipo}}" class="mapa"></div>
        <script  type="text/javascript">
          $(document).ready(function() {
            var longitud_{{$campos->Tipo}}=$("#longitud").val();
            var latitud_{{$campos->Tipo}}=$("#latitud").val();
            if(longitud_{{$campos->Tipo}}==undefined || longitud_{{$campos->Tipo}}==""){
              longitud_{{$campos->Tipo}}=-80.7214469337831;
            }
            if(latitud_{{$campos->Tipo}}==undefined || latitud_{{$campos->Tipo}}==""){
              latitud_{{$campos->Tipo}}=-0.9485041219484316;
            }
            inicializar_mapBox("{{$campos->Tipo}}",longitud_{{$campos->Tipo}},latitud_{{$campos->Tipo}});
          });
        </script>


        @elseif($campos->Tipo=="divresul")
        <div id="{{ $campos->Nombre }}">{{ $campos->Valor }}</div>
@elseif($campos->Tipo=="link")
                <div class="form-group" id="divobj-{{ $campos->Nombre }}">
                    {{--
                    @if($campos->Valor!='#')
                        {!! Form::label($campos->Nombre,$campos->Descripcion.'123:',array("class"=>"col-lg-3 control-label")) !!}
                    @else
                        <div class="col-lg-3 control-label">&nbsp;</div>
                    @endif
--}}
                    <div class="col-lg-8" id="div-{{$campos->Nombre}}">
                        <a href="{{ ($campos->Valor=='#' ? 'javascript:' : URL::to($campos->Valor) ) }}" target="_blank" class="btn btn-w-m btn-{{ ($campos->Valor=='#' ? 'danger' : 'success') }}" id="{{ $campos->Nombre }}">
                            @if($campos->Valor!='#')
                                <i class="fa fa-external-link-square"></i>
                            @else
                                <i class="fa fa-mail-forward"></i>
                            @endif
                            &nbsp;&nbsp;  {{ ($campos->Valor=='#' ? $campos->Descripcion : ' Visualizar' ) }}
                        </a>
                    </div>
                </div>
            @else
        {!! Form::text($campos->Nombre, $campos->ValorAnterior!="Null" ? $campos->ValorAnterior :
        old($campos->Valor) , array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.str_replace("(*)","",$campos->Descripcion))) !!}
        @endif
        </div>
        @php echo ((isset($campos->Adicional))?$campos->Adicional:'') @endphp
    </div>

    @endforeach



    <div style="text-align: center;" hidden id="gif_final">
      <img src="{{asset('img/loading35.gif')}}" alt="" style="width: 90%;">
    </div>

    @if(isset($res_dir) && $res_dir != null && count($res_dir) > 0)
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
            <h3>Respuestas</h3>
            <hr>
            <div class="form-group">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Dirección</th>
                    <th>Respuesta</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($res_dir as $res)
                    <tr>
                      <th>{{ $res->direccion}}</th>
                      <th>{{ $res->observacion }}</th>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @elseif(isset($desvinculados) && count($desvinculados) > 0)
      <hr>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
            <div class="form-group">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Dirección</th>
                    <th>Observación</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($desvinculados as $item)
                    <tr>
                      <th>{{ $item->alias }}</th>
                      <th>{{ $item->observacion }}</th>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @elseif(isset($respuestasInforme) && $respuestasInforme != null && count($respuestasInforme) > 0)
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
            <h3>Informe de otras áreas</h3>
            <hr>
            <div class="form-group">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Dirección</th>
                    <th>Respuesta</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($respuestasInforme as $res)
                    <tr>
                      <th>{{ $res->direccion }}</th>
                      <th class="text-justify">{{ $res->observacion }}</th>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @endif
    @if(isset($analistas_asignados) && count($analistas_asignados) > 0)
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">
            <h3>Informe de analista(s)</h3>
            <hr>
            <div class="form-group">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Analista(s)</th>
                    <th>Estado</th>
                    <th>Observación</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($analistas_asignados as $res)
                    <tr>
                      <th>{{ $res->name }}</th>
                      <th class="text-justify">{{ $res->estado == 'ANALISTA' ? 'PENDIENTE' : 'CONTESTADO' }}</th>
                      <th class="text-justify">{{ isset($res->observacion) ? $res->observacion : 'NINGUNA' }}</th>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @endif

    @if(isset($archivos) && isset($archivos_total))
      <hr>
      @include('vistas.archivos', [
        'archivos' => $archivos,
        'archivos_total' => $archivos_total,
        'referencia' => $tabla->id
      ])
    @endif

    @if(isset($configuraciongeneral[10]))
    {!!Form::text('ruta', $configuraciongeneral[10], array('class' => 'form-control hidden')) !!}
    @endif
    @if(isset($popup))
    <input type="hidden" value="popup" name="txtpopup" />
    <!--else -->
    <!--input type="hidden" value="" name="txtpopup"/-->
    @endif
        @if(isset($permisos))
            @if($permisos->guardar=="SI")
                {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @endif
        @else
        @if(!isset($btnguardar))
              @if(Auth::user()->id_perfil == 55 && isset($btnEp))
                {!! Form::submit("Enviar a secretaría general", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}

                <button type="button" onclick="desvincularTramite({{ $tabla->id }})"
                  id="btn_devolver_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                    ¿No corresponde a la dirección?
                  </button>

                <button type="button" onclick="archivarTramite({{ $tabla->id }})"
                  id="btn_archivar_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                    Archivar trámite
                </button>

              @else
                @if(Auth::user()->id_direccion == 21 && isset($tabla) && isset($btnFinalizar) && $tabla->correo_electronico == null)
                  <button type="button" onclick="finalizarTramiteDirector({{ $tabla->id }}, '{{ $btnFinalizar }}', 'NO')"
                  id="btn_finalizar_tramite" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                    Finalizar
                </button>
                @else
                  {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
                  @if(isset($btnDevolverVentanilla))
                  <button type="button" onclick="devolverVentanilla({{ $tabla->id }})"
                    id="btn_devolver_ventanilla" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                      Devolver
                    </button>
                  @endif
                @endif
              @endif
              @if(isset($btnDevolver))
                @include('vistas.modal_direcciones', ['direcciones' => $direcciones])
                <button type="button" onclick="obtenerDirecciones()"
                id="btn_solicitar_informe" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                  ¿Necesita informe de otras áreas?
                </button>
                <button type="button" onclick="desvincularTramite({{ $tabla->id }})"
                id="btn_devolver_tramite" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                  ¿No corresponde a la dirección?
                </button>

                @if(isset($btnDonacion))
                  <button type="button" onclick="donacion({{ $tabla->id }})"
                    id="btn_donacion" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                      Archivar como donación
                  </button>
                @endif

                @if(count($analistas) > 0)
                  @include('vistas.modal_analistas', ['analistas' => $analistas])
                  <button type="button" onclick="obtenerAnalistas()"
                    id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                      Asignar analista
                  </button>
                @endif
              @endif
          @elseif(isset($btnFinalizar))
            @if($asignacion && $tabla->obtener_respuesta == 'SI')
              {!! Form::submit("Guardar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @else
              <button type="button" onclick="finalizarTramiteDirector({{ $tabla->id }}, '{{ $btnFinalizar }}')"
                id="btn_finalizar_tramite" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                  Finalizar
              </button>
            @endif
          @endif

          @if(isset($btnResponder) && $btnResponder == 'SI' && Auth::user()->id_perfil != 50 && Auth::user()->id_perfil != 57)
            <button type="button" id="btn_finalizar_pago" onclick="finalizarConPago({{ $tabla->id }})"
              class="btn btn-default flotar" style="float:right; margin-right: 7px;">
                Finalizar pendiente de pago
            </button>

            <button type="button" id="btn_finalizar_director" onclick="enviarCiudadano({{ $tabla->id }})"
              class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Finalizar
            </button>

            <button type="button" id="btn_devolver" onclick="enviarCiudadano({{ $tabla->id }}, 'true')"
              class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                Devolver
            </button>

            @if(count($analistas) > 0)
              @include('vistas.modal_analistas', ['analistas' => $analistas])
              <button type="button" onclick="obtenerAnalistas()"
                id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                  Asignar analista
              </button>
            @endif
          @elseif(isset($btnAsignarAnalistas) && $btnAsignarAnalistas == 'SI' && count($analistas) > 0)
              @include('vistas.modal_analistas', ['analistas' => $analistas])
              <button type="button" onclick="obtenerAnalistas()"
                id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                  Asignar analista
              </button>
          @endif

          @if(isset($botonesCoordinadorTramites))
              @if(isset($tabla->obtener_respuesta) && $tabla->obtener_respuesta == 'NO') {{--&& $contestados == 0--}}
                <button type="button" onclick="enviarAprobar({{ $tabla->id }}, 'CIUDADANO')"
                  id="btn_aprobar_coordinador_2" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Responder a ciudadano
                </button>

                <button type="button" onclick="enviarAprobar({{ $tabla->id }}, 'DESPACHO')"
                  id="btn_aprobar_coordinador" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                    Enviar a despacho
                </button>
              @else
                <button type="button" onclick="enviarAprobar({{ $tabla->id }}, 'GENERAL')"
                  id="btn_aprobar_coordinador_1" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                    Enviar a secretaría general
                </button>
              @endif
          @elseif(isset($btnAnalista))
            <button type="button" onclick="guardaInformeAnalista({{ $tabla->id }})"
              id="btn_informe_analista" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Enviar informe
            </button>
          @elseif(isset($btnCoordinador))
            <button type="button" onclick="archivarTramite({{ $tabla->id }})"
              id="btn_archivar_tramite" class="btn btn-warning flotar" style="float:right; margin-right: 7px;">
                Archivar trámite
            </button>

            <button type="button" onclick="notificarCoordinador({{ $tabla->id }}, {{ Auth::user()->id_direccion }})"
              id="btn_solicitar_coordinador" class="btn btn-danger flotar" style="float:right; margin-right: 7px;">
                ¿Necesita aprobación/revisión del coordinador?
            </button>

            @if(isset($btnDonacion))
              <button type="button" onclick="donacion({{ $tabla->id }})"
                id="btn_donacion" class="btn btn-info flotar" style="float:right; margin-right: 7px;">
                  Archivar como donación
              </button>
            @endif

            @if(count($analistas) > 0)
              @include('vistas.modal_analistas', ['analistas' => $analistas])
              <button type="button" onclick="obtenerAnalistas()"
                id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                  Asignar analista
              </button>
            @endif
          @elseif(isset($btnGuardarInforme))
            <button type="button" onclick="guardarInforme({{ $tabla->id }})"
              id="btn_guardar_informe" class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                Guardar
            </button>
            @if(count($analistas) > 0)
                @include('vistas.modal_analistas', ['analistas' => $analistas])
                <button type="button" onclick="obtenerAnalistas()"
                  id="btn_asignar_analista" class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                    Asignar analista
                </button>
              @endif
          @elseif(isset($btnFinalizarTramite))
            @if($contestados > 0 && $contestados == $dirAsignados && isset($tabla->correo_electronico))
              {!! Form::submit("Finalizar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @elseif($contestados > 0 && $tabla->obtener_respuesta == 'NO' && isset($tabla->correo_electronico))
              <input type="hidden" value="{{ $tabla->id }}" id="tramite_id">
              <button type="button" id="finalizarImcompleto"
                class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                  Enviar respuesta a ciudadano
              </button>
            @elseif(!isset($tabla->correo_electronico))
              <button type="button" id="btn_guardar" onclick="finalizarTramiteDirector({{$tabla->id}}, 'GENERAL', 'NO')"
                class="btn btn-primary flotar" style="float:right; margin-right: 7px;">
                  Guardar como notificado
              </button>
            @else
              {!! Form::submit("Finalizar", array('id'=>'btn_guardar','class' => 'btn btn-primary', 'style' => 'float:right' )) !!}
            @endif
          @elseif(isset($btnReservarTramite))
            {!! Form::submit("Reservar Trámite", array('id'=>'btn_reservar','class' => 'btn btn-warning', 'style' => 'float:right;margin-right: 7px;' )) !!}
            <a id="btn_acuerdo" onclick="generarAcuerdo(this)"
              class="btn btn-success flotar" style="float:right; margin-right: 7px;">
                Acuerdo de responsabilidad
            </a>
          @endif
        @endif
        @if(isset($validar_Agenda))

        @endif
        <div class="form-group">

        </div>

        {!! Form::close() !!}

        @isset($history)
<div class="row">
  <div class="col-lg-12">
    <div class="ibox-content">
      <h2>Indicadores</h2>
                @foreach ($history as $k => $item)
                <button type="button" onclick="mostrarIndicadores({{$item['id']}},'{{$item['id_cab_indi']}}')" class="btn btn-primary">{{$item['nombre']}}</button>
                @endforeach
                </div>
                <div id="divresindiajax"></div>

            </div>
      </div>

@endisset


        <div id="divresajax"></div>
    @if(isset($actividades))
    @if(isset($permisos) && $permisos->crear=='SI')
    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modal_actividad">
      <i class="fa fa-plus" style="font-size: 1.5em;"> </i> Agregar Actividad</button><br><br>
    @endif


    <div class="row">
      <div class="col-lg-12">
          <div class="table-responsive">
        <div class="ibox float-e-margins">
          <div class="ibox-content">
            <h2>Actividades</h2>
            <div>
              <table class="table table-striped table-bordered table-hover" > <!-- style="font-size: 10px; width: 50%;" -->
                @if(isset($mult_acttividades[0]))
                <tr>
                  @foreach($mult_acttividades[0] as $i => $act)
                  <th>{{$i}}</th>
                  @endforeach
                </tr>
                @foreach($mult_acttividades as $act)
                <tr>
                  @foreach($act as $i => $ac)
                  <td><?php echo $ac; ?></td>
                  @endforeach
                </tr>
                @endforeach
                @endif
              </table>
            </div>
          </div>
        </div>
      </div> <!-- Div Resposive -->
      </div>
      <script>
      @if(isset($permisos) && $permisos->editar_parcialmente=='SI')
            function editar(id) {
              actividad_poa=id;
              $('#modal_actividad').modal('toggle');
              $("#modal_actividad").on('hidden.bs.modal', function () {
                location.reload();
              });

              $.ajax({
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        url: '{{URL::to('')}}/coordinacioncronograma/{{(isset($configuraciongeneral[10]))?$configuraciongeneral[10]:"get_actividad_poa"}}/'+id,
                        success: function (data) {
                            var responsables;
                          $('#observacion').css('height',data['alto_obs']);
                        for (let i in data['datos']) {
                            // console.log(i);
                            if(i=='id_direccion_act'){
                                setTimeout(function(){
                                    //console.log(i);
                                    $('#id_direccion_act').trigger('change');
                                },500);
                            }else if(i=='parroquia_id'){

                               $('#parroquia_id').val(data['datos'][i]).change();


                            }else
                            if(i=='barrio_id'){
                                  // $('#barrio_id option:selected').val(165).change();
                                  // console.log($('#barrio_id'));
                                  // console.log($('#barrio_id'));
                                  var parroquia=$("#parroquia_id").val();
                                  $.ajax({
                                          type: "GET",
                                          url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+parroquia,
                                          success: function (response) {
                                            $("#barrio_id").empty();
                                            $.each(response, function(key, element) {
                                              $("#barrio_id").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                                            });
                                            $('#barrio_id').val(data['datos'][i]).change();
                                            // $("#barrio_id").trigger("chosen:updated");
                                          }
                                      });

                            }
                            else if(i=='idusuario_act'){
                                responsables=data['datos'][i].split(',');
                                setTimeout(function(){
                                    //console.log("Chosen");
                                    $('#idusuario_act').val(responsables).trigger('chosen:updated');
                                },1000);
                                console.log(responsables);

                              }
                            else if(i=='avance_actividad'){
                                var valor=parseInt(data['datos'][i]);

                                 $("#"+i).val(valor).change();

                              }
                            else if(i=='verificacion'){
                                var valor=data['datos'][i];
                                if(valor!=null){
                                  $("#"+i).val(valor.toString()).change();

                                }
                              }
                            else if(i=='archivo_final'){

                              }
                              else{
                                console.log(i);
                                if(data['datos'][i]!=null && data['datos'][i]!=''){

                                  $("#"+i).val(data['datos'][i]).change();
                                }
                              }

                        }

                        try {
                          agregartareas();
                        } catch (error) {

                        }

                        }
                    });


            }

      @endif

      @if(isset($permisos) && $permisos->eliminar=='SI')
            function eliminar(id) {

              Swal.fire({
                title: '¿Desea eliminar esta actividad?',
                showCancelButton: true,
                confirmButtonText: 'ELIMINAR',
                cancelButtonText: 'CANCELAR',
                showLoaderOnConfirm: true,
                preConfirm: (codigo) => {
                    $.ajax({
                        type: "POST",
                        data: {
                            "_token": "{{ csrf_token() }}",
                        },
                        url: '{{URL::to('')}}/coordinacioncronograma/{{(isset($configuraciongeneral[11]))?$configuraciongeneral[11]:"eliminar_actividad"}}/'+id,
                        success: function (data) {
                            try {

                                if (data.estado == 'ok') {
                                    Swal.fire({
                                        position: 'center',
                                        type: 'success',
                                        title: data['msg'],
                                        showConfirmButton: false,
                                        timer: 1500
                                    }).then(() => {
                                        location.reload();

                                    });
                                } else {
                                    Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                                }

                            } catch (error) {

                            }

                        },
                        error: function (data) {
                          Swal.fire({
                                        type: 'error',
                                        title: 'Oops...',
                                        text: data['msg']
                                    });
                        }
                    });

                },
                allowOutsideClick: () => !Swal.isLoading()
            });

            }


      @endif
    </script>
      @if(isset($permisos) && $permisos->editar_parcialmente=='SI')
      <div id="modal_actividad" class="modal fade">
        <div class="modal-dialog" style="width: 80%;">
          <div class="modal-content">
            <div class="modal-body">
              <div class="ibox-content row">
                @foreach($actividades as $i => $campos_a)
                @if($campos_a->Tipo!="textarea" && $campos_a->Tipo!="select-multiple" && $campos_a->Clase!="12")
                <div class="col-sm-6 col-lg-6 ">
                  @else
                  <div class="col-sm-12 col-lg-12">
                    @endif
                    <div class="form-group">

                      @if($campos_a->Tipo=="funcionjs")
                      {!! $campos_a->Valor !!}

                      @elseif($campos_a->Tipo=="datetext")
                      <div class="input-group date">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        {!! Form::text(null, null , array('id'=>$campos_a->Nombre,
                        'data-placement' =>'top',
                        'data-toogle'=>'tooltip',
                        'class' => 'form-control datefecha',
                        'placeholder'=>$campos_a->Descripcion,
                        $campos_a->Nombre,
                        'readonly'=>'readonly'
                        )) !!}
                      </div>

                      @elseif($campos_a->Tipo=="textarea")
                      @if (isset($verObservaciones) && $campos_a->Clase=="mostrarobservaciones" )
                      <div id="contenidodiv">
                        @php $fechi=""; $txtob=""; @endphp
                        @foreach ($timelineActividades as $item)
                        @php $fechi=$item->updated_at; $txtob=$item->observacion; @endphp
                        @endforeach
                        <p style="text-align: justify;" class="text-muted"> <strong class="badge badge-primary"><i
                              class="fa fa-clock-o"></i> {{$fechi}} </strong> - {{$txtob }}</p>
                      </div>
                      <a class="btn btn-xs btn-primary" href="javascript:" id="btnmas"><i class="fa fa-search-plus"></i>
                        Mostrar Todo
                      </a>
                      <script type="text/javascript">
                        $(document).ready(function() {
                                          //Evento change al escoger en Select
                                          $('#contenidodiv').scrollTop($('#contenidodiv')[0].scrollHeight);
                                          $("#observacion").val("");
                                          $("#observacion").focus();
                                          //$(window).scrollTop($('#observacion').offset().top);
                                          //$("#idusuario")[0].scrollIntoView(true);
                                        });
                      </script>
                      @endif
                      <?php
                                          if($campos_a->Clase!="Null")
                                          {
                                            $bus=strpos($campos_a->Clase,":");
                                            if($bus)
                                            {
                                              $bus=explode(":",$campos_a->Clase);
                                        ?>
                      <script type="text/javascript">
                        $(document).ready(function() {
                                            $("#{{ $campos_a->Nombre}}").attr('maxlength',{{$bus[1]}});
                                          });
                      </script>
                      <?php
                                            }
                                          }
                                        ?>
                      {!! Form::textarea(null, old($campos_a->Valor) , array('id'=>$campos_a->Nombre,'class' =>
                      'form-control ' .
                      $campos_a->Clase ,'placeholder'=>'Ingrese '.$campos_a->Descripcion,'style'=>'height: 100px; width:
                      100%;')) !!}


                      @elseif($campos_a->Tipo=="select")
                      {!!Form::label($campos_a->Nombre,$campos_a->Descripcion.':',array("id"=>"label_".$campos_a->Nombre,"class"=>"col-lg-5
                      control-label"))!!}
                      {!! Form::select(null,$campos_a->Valor, (old($campos_a->Nombre) ?
                      old($campos_a->Nombre) :
                      ($campos_a->ValorAnterior !="Null")) ? $campos_a->ValorAnterior : old($campos_a->Nombre),
                      array('id'=>$campos_a->Nombre,'class' =>
                      "form-control ".$campos_a->Clase)) !!}
                      @elseif($campos_a->Tipo=="select-multiple")
                      {!!Form::label($campos_a->Nombre,$campos_a->Descripcion.':',array("id"=>"label_".$campos_a->Nombre,"class"=>"col-lg-5
                      control-label"))!!}
                      {!! Form::select(null,$campos_a->Valor, (old($campos_a->Nombre) ?
                      old($campos_a->Nombre): ($campos_a->ValorAnterior !="Null")) ? $campos_a->ValorAnterior :
                      old($campos_a->Nombre),array('id'=>$campos_a->Nombre,'class' =>"form-control
                      ".$campos_a->Clase,'multiple'=>'multiple',"id"=>$campos_a->Nombre))!!}
                      @elseif($campos_a->Tipo=="html")
                      {!!Form::label($campos_a->Nombre,$campos_a->Descripcion.':',array("id"=>"label_".$campos_a->Nombre,"class"=>"col-lg-5 control-label"))!!}
                      <div class="form-control" style="height: 0.1%;"> <?php echo $campos_a->Valor ?></div>

                      @elseif($campos_a->Tipo=="file")
                      {!!Form::label($campos_a->Nombre,$campos_a->Descripcion.':',array("id"=>"label_".$campos_a->Nombre,"class"=>"col-lg-3 control-label ".$campos_a->Clase))!!}
                      {!! Form::file($campos_a->Nombre, array( "id"=>$campos_a->Nombre,'class' => 'form-control '.$campos_a->Clase)) !!}
                      @if(isset($tabla))
                      <br>
                      <?php
                                          $cadena="if(\$tabla->".$campos_a->Nombre."!='') echo '<div id=\"archivo\"><span> $campos_a->Descripcion </span>  <a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$tabla->".$campos_a->Nombre.").' class=\"btn btn-success divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i> Ver Archivo</a></div>';";
                                          eval($cadena);
                                        ?>
                      @endif
                      @else

                      {!!Form::label($campos_a->Nombre,$campos_a->Descripcion.':',array("id"=>"label_".$campos_a->Nombre,"class"=>"col-lg-3 control-label".$campos_a->Clase))!!}


                      {!! Form::text(null, $campos_a->ValorAnterior!="Null" ? $campos_a->ValorAnterior : old($campos_a->Valor) , array('id'=>$campos_a->Nombre,'class' => 'form-control'.$campos_a->Clase,'placeholder'=>'Ingrese '.$campos_a->Descripcion)) !!}
                      @endif

                    </div>
                  </div>

                  @endforeach



                  <div  class="col-sm-12 col-lg-12" style="text-align: end;">

                    <button type="button" class="btn btn-success " id="add_actividad"><i style="font-size: 1.5em;"
                        class="fa fa-save"></i> Guardar</button>
                  </div>
<!-- Div De Carga Ajax-->

                </div>
                <script>
                  $(document).ready(function () {
                          $('#add_actividad').click(function (e) {
                            var data = new FormData();
                              @foreach($actividades as $actividad)
                                    @if($actividad->Tipo=='select-multiple')
                                              $("#select-id").chosen().val()
                                      var {{$actividad->Nombre}}=  $("#{{$actividad->Nombre}}").chosen().val();
                                    @elseif($actividad->Tipo=='file')

                                      var {{$actividad->Nombre}}=$('#{{$actividad->Nombre}}')[0].files[0];
                                    @elseif($actividad->Nombre !='tareas')
                                        var {{$actividad->Nombre}}=document.getElementById('{{$actividad->Nombre}}').value;
                                    @endif

                                    @if($actividad->Nombre !='tareas')
                                      data.append("{{$actividad->Nombre}}", {{$actividad->Nombre}});
                                    @endif

                              @endforeach

                                data.append("_token", '{{ csrf_token() }}');
                                data.append("id", actividad_poa);
                                $.ajax({
                                    type: "POST",
                                    url: "{{URL::to('')}}/coordinacioncronograma/{{ (isset($configuraciongeneral[9]))?$configuraciongeneral[9]:''}}",
                                    data: data,
                                    contentType: false,
                                    enctype: 'multipart/form-data',
                                    data: data,
                                    processData: false,
                                    cache: false,
                                    success: function (response) {
                                      if(response.estado=='ok'){
                                        toastr["success"](response.mensaje);
                                        location.reload();
                                      }else{
                                        toastr["error"](response.mensaje);
                                      }

                                    }
                                  });
                            });
                      });
                </script>
              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        @endif

        @endif

        @if($configuraciongeneral[2]=="editar")
          <script>
            function modal_subir(id,tipo) {

              $('#ModalArchivosActividades').modal('toggle');
              $('#id_referencia_act').val(id);
              $('#tipo_act').val(tipo);

            }

          </script>

          <div id="ModalArchivosActividades" class="modal fade">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-body">

                  <div class="ibox-content row">
                    <div class="col-sm-12 offset-sm-1">
                      <h1 class="page-heading"> <i class="fa fa-cloud-upload" style="font-size: 1em;"> Gestor Archivos
                        </i><span id="counter"></span></h1>

                      {!! Form::open(['url'=> '/', 'method' => 'POST', 'files'=>'true', 'id' =>
                      'mydropzoneactividades' , 'class' => 'dropzone','enctype'=>'multipart/form-data']) !!}
                      <div class="dz-message" style="height:200px;">
                        Arrastre los Archivos Aquí!!
                      </div>
                      <input type="text" hidden name="id_referencia" id="id_referencia_act">
                      <input type="text" hidden name="tipo" id="tipo_act" >
                      <div class="dropzone-previews"></div>
                      <button type="button" class="btn btn-success" id="submit_actividades">Agregar</button>
                      {!! Form::close() !!}
                    </div>
                  </div>

                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->

        @endif


        @if($configuraciongeneral[2]=="editar" && isset($configuraciongeneral[3]) && isset($configuraciongeneral[4]))

        <button type="button" class="btn btn-link" data-toggle="modal" data-target="#ModalArchivos">
          <i class="fa fa-cloud-upload" style="font-size: 1.5em;"> </i> Subir Archivos</button><br><br>

        <div id="ModalArchivos" class="modal fade">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">

                <div class="ibox-content row">
                  <div class="col-sm-12 offset-sm-1">
                    <h1 class="page-heading"> <i class="fa fa-cloud-upload" style="font-size: 1em;"> Gestor Archivos
                      </i><span id="counter"></span></h1>

                    {!! Form::open(['route'=> 'subirarchivos.store', 'method' => 'POST', 'files'=>'true', 'id' =>
                    'my-dropzone' , 'class' => 'dropzone']) !!}
                    <div class="dz-message" style="height:200px;">
                      Arrastre los Archivos Aquí!!
                    </div>
                    <input type="text" hidden name="id_referencia" value="{{$tabla->id}}">
                    <input type="text" hidden name="tipo" value="{{$configuraciongeneral[3]}}">
                    <div class="dropzone-previews"></div>
                    <button type="submit" class="btn btn-success" id="submit">Save</button>
                    {!! Form::close() !!}
                  </div>
                </div>

              </div>
            </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="row">
          <div class="col-lg-12">
            <div class="ibox float-e-margins">
              <div class="ibox-content">
                <h2>Galería de imágenes</h2>
                <div class="demo-gallery">
                  <ul id="lightgallery">
                    @foreach ($dicecionesImagenes as $item)
                    <li style="text-align: end;" >
                    @if(isset($eliminar_archivo))
                      <button class="btn btn-danger" onclick="borrarImagen('{{Crypt::encrypt($item->id)}}')"><i class="fa fa-times-circle" aria-hidden="true"></i></button>
                      @endif
                      <div class="imgu" data-responsive="{{asset('archivos_sistema/'.$item->ruta)}}"
                      data-src="{{asset('archivos_sistema/'.$item->ruta)}}" data-sub-html="<p>{{$item->nombre}}</p>">
                      <a href="" >
                        <img class="img-responsive" src="{{asset('archivos_sistema/'.$item->ruta)}}" >
                        <div class="demo-gallery-poster">
                          <img src="{{asset('archivos_sistema/'.$item->ruta)}}" >
                        </div>
                      </a>
                      </div>

                    </li>

                    @endforeach
                  </ul>
                </div>


              </div>

              <script>
                $(document).ready(function () {
            lightGallery(document.getElementById('lightgallery'), {
      selector: ".imgu"
     });



          });

              </script>




            </div>
          </div>


        </div>

        <div class="row">
          <div class="col-lg-12">


            <div class="ibox-content">

              <h2>Archivos</h2>
              <div>
                <table class="table table-striped table-bordered table-hover">
                  <tr>
                    <th>Tipo</th>
                    <th>Nombre de Archivo</th>
                    <th>Fecha de Registro</th>
                    <th>Usuario</th>
                    <th>Tamaño KB / MB</th>
                    <th>Ver</th>
                  </tr>
                  @foreach ($dicecionesDocumentos as $item)
                  <tr>
                    <td>{{ pathinfo(asset('archivos_sistema/'.$item->ruta))["extension"] }}</td>
                    <td>{{ $item->nombre }}</td>
                    <td>{{ $item->created_at }}</td>
                    <td>{{ (isset($item->name))?$item->name: 'No disponible' }}</td>
                    <td>
                      {!!
                      (is_file(public_path()."/archivos_sistema/".$item->ruta))?round(filesize(public_path()."/archivos_sistema/".$item->ruta)/1024,2)
                      : "-" !!} /
                      {!!
                      (is_file(public_path()."/archivos_sistema/".$item->ruta))?round(filesize(public_path()."/archivos_sistema/".$item->ruta)/1024/1024,2)
                      : "-" !!}
                    </td>

                    <td>
                      @if(is_file(public_path()."/archivos_sistema/".$item->ruta))
                      <a href="{{ asset('archivos_sistema/'.$item->ruta) }}" type="button"
                        class="btn btn-success dropdown-toggle divpopup" target="_blank">
                        <i class="fa fa-file-pdf-o" style="font-size: 2em;"></i>
                      </a>
                      @else
                        No disponible
                      @endif
                      @if(isset($eliminar_archivo))
                      <button class="btn btn-danger" onclick="borrarImagen('{{Crypt::encrypt($item->id)}}')"><i class="fa fa-times-circle"  style="font-size: 2em;" aria-hidden="true"></i></button>
                      @endif
                    </td>
                  </tr>
                  @endforeach
                </table>


              </div>

            </div>


          </div>
        </div>
        @endif














        @if (isset($timeline_obras))
        <h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
          <div class="vertical-timeline-block">
            @foreach ($timeline_obras as $item)
            <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];
              ?>
            <div class="vertical-timeline-block">
              <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                {{-- <i class="fa fa-briefcase"></i> --}}
              </div>
              <div class="vertical-timeline-content">

                @if ($contador==0)
                <h2><i class="fa fa-save"></i> Inicial</h2>

                @elseif($contador>=1)
                <h2><i class="fa fa-pencil"></i> Actualización</h2>
                @endif
                <div hidden> {{$contador+=1}} </div>

                <p>
                  <pre style="text-align: left; white-space: pre-line;">
                        <strong>Nombre de Obra: </strong>{{$item->nombre_obra}}
                        <strong>Parroquia: </strong> {{$item->parroquia}}
                        <strong>Calle: </strong>  {{$item->calle}}
                         <strong>Tipo de obra: </strong> {{$item->tipo}}
                            <strong>Monto: </strong> <span class="label label-primary" style="font-size: 1em;">$ {{ number_format ($item->monto,2) }}</span>
                                    <strong>Contratista: </strong>  {{$item->contratista}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_final}}
                                            <strong>Fisxalizador: </strong>  {{$item->fiscalizador}}
                                            <strong>Plazo (DÍAS): </strong>  {{$item->plazo}}
                                            <strong>Observacion: </strong>  {{$item->observacion}}
                                            <strong>Estado de la obra: </strong>  <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_obra)  }};">{{$item->estado_obra}}</span>
                                            <br>
                                            <i><strong>Modificado: </strong>  {{$item->name}}</i>



                      </pre>
                </p>
                <div class="ibox-content">
                  <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                  <div class="progress progress-striped active m-b-sm">
                    <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                  </div>
                </div>
                <span class="vertical-date">
                  {{fechas(2,$item->updated_at)}} <br />
                  <small>{{fechas(1000,$item->updated_at)}} </small>
                </span>
              </div>
            </div>
            @endforeach

          </div>


        </div>

        @endif


        @if (isset($timelineTramites))
        <hr>
        <h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
          <div class="vertical-timeline-block">

            @foreach ($timelineTramites as $item)
            <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
            <div class="vertical-timeline-block">
              <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                {{-- <i class="fa fa-briefcase"></i> --}}
              </div>
              <div class="vertical-timeline-content">

                @if ($contador==0)
                <h2><i class="fa fa-save"></i> Inicial</h2>

                @elseif($contador>=1)
                <h2><i class="fa fa-pencil"></i> Actualización</h2>
                @endif
                <div hidden> {{$contador+=1}} </div>
                <p>
                  <pre style="text-align: left; white-space: pre-line;">
                        <strong>Asunto: </strong>{{$item->asunto}}
                        @if(isset($item->direccion))
                          <strong>Dirección a cargo: </strong>  {{$item->direccion}}
                        @endif
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_ingreso}}
                                          @if(isset($item->fecha_fin))
                                            <strong>Fecha estimada de finalización: </strong>  {{$item->fecha_fin}}
                                          @endif
                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                        <strong>Estado de trámite: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->disposicion)  }};">{{$item->disposicion}}</span>
                        @if(isset($item->observacion))
                          <strong>Observación: </strong>  {{$item->observacion}}
                        @endif
                        <br>
                        <strong>Modificado por: </strong>  {{ $item->name == 'ADMINISTRADOR' ? 'CIUDADANO' : $item->name }}

            </pre>
                  </p>

                <span class="vertical-date">
                  {{fechas(2,$item->updated_at)}} <br />
                  <small>{{fechas(1000,$item->updated_at)}} </small>
                </span>
              </div>
            </div>
            @endforeach

          </div>


        </div>

        @endif



        @if (isset($timelineActividades))
        <h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
          <div class="vertical-timeline-block">

            @foreach ($timelineActividades as $item)
            <?php
                $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

              ?>
            <div class="vertical-timeline-block">
              <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                {{-- <i class="fa fa-briefcase"></i> --}}
              </div>
              <div class="vertical-timeline-content">

                @if ($contador==0)
                <h2><i class="fa fa-save"></i> Inicial</h2>

                @elseif($contador>=1)
                <h2><i class="fa fa-pencil"></i> Actualización</h2>
                @endif
                <div hidden> {{$contador+=1}} </div>
                <p>
                  <pre style="text-align: left; white-space: pre-line;">
                        <strong>Actividad: </strong>{{$item->actividad}}
                        <strong>Dirección a cargo: </strong>  {{$item->direccion}}
                                        <strong>Fecha de inicio: </strong>  {{$item->fecha_inicio}}
                                            <strong>Fecha de fin: </strong>  {{$item->fecha_fin}}
                        <strong>Prioridad: </strong>  {{$item->prioridad}}
                        <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_actividad)  }};">{{$item->estado_actividad}}</span>
                        <strong>Observación: </strong>  {{$item->observacion}}
                        <br>
                        <strong>Modificado por: </strong>  {{$item->name}}

            </pre>
                  <strong><i class="fa fa-users"></i> Responsables</strong>
                  @if (isset($timelineActividadesResp))
                  <pre style="text-align: left; white-space: pre-line;">
              @foreach ($timelineActividadesResp as  $i)
              <strong>Nombre: </strong>{{$i->name}}
              @endforeach

            </pre>
                  @endif
                </p>
                <div class="ibox-content">
                  <h2>Avance Actual: {{round($item->avance,0)}} %</h2>
                  <div class="progress progress-striped active m-b-sm">
                    <div style="width: {{$item->avance}}%;" class="progress-bar"></div>
                  </div>
                </div>
                <span class="vertical-date">
                  {{fechas(2,$item->updated_at)}} <br />
                  <small>{{fechas(1000,$item->updated_at)}} </small>
                </span>
              </div>
            </div>
            @endforeach

          </div>


        </div>

        @endif

        @if(isset($timelineProyecto))

        <h1>Historial</h1>
        <div hidden>{{$contador=0}}</div>
        <div id="vertical-timeline" class="vertical-container dark-timeline center-orientation">
          <div class="vertical-timeline-block">

            @foreach ($timelineProyecto as $item)
            <?php
                    $arraycolores=array("#00CED1","#5F9EA0","#4682B4","#87CEFA","#00BFFF","#7B68EE","#E0FFFF","#AFEEEE","#7FFFD4","#40E0D0","#191970","#000080");
                    $color_aleatoreo = $arraycolores[array_rand($arraycolores )];

                  ?>
            <div class="vertical-timeline-block">
              <div class="vertical-timeline-icon" style='background-color:{{$color_aleatoreo}}'>
                {{-- <i class="fa fa-briefcase"></i> --}}
              </div>
              <div class="vertical-timeline-content">

                @if ($contador==0)
                <h2><i class="fa fa-save"></i> Inicial</h2>

                @elseif($contador>=1)
                <h2><i class="fa fa-pencil"></i> Actualización</h2>
                @endif
                <div hidden> {{$contador+=1}} </div>
                <p>
                  <pre style="text-align: left; white-space: pre-line;">
                            <strong>Nombre: </strong>{{$item->nombre}}
                            <strong>Monto: </strong>  {{$item->monto}}
                            <strong>Linea Base: </strong>  {{$item->linea_base}}
                            <strong>Meta: </strong>  {{$item->meta}}
                            <strong>Descripción: </strong>  {{$item->descripcion}}
                            <strong>Estado de la actividad: </strong> <span class="label" style="font-size: 1em;background-color: {{ colortimelineestado($item->estado_proyecto)  }};">{{$item->estado_proyecto}}</span>                      </p>
                          <strong>Observación: </strong>  {{$item->observacion}}
                          </pre>
                  <!-- <div class="ibox-content">
                              <h2>Meta Actual: {{round($item->meta,0)}} %</h2>
                              <div class="progress progress-striped active m-b-sm">
                                <div style="width: {{$item->meta}}%;" class="progress-bar"></div>
                            </div>
                          </div> -->


                  <span class="vertical-date">
                    {{fechas(2,$item->updated_at)}} <br />
                    <small>{{fechas(1000,$item->updated_at)}} </small>
                  </span>
              </div>
            </div>
            @endforeach

          </div>



        </div>
        @endif
        @if (isset($historialtable))
        <h1>Historial</h1>
        <div class="table-responsive">
          <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display table-responsive">
            <thead>
              <tr>
                @if (!isset($verid))
                <th>ID</th>
                @endif


                @foreach($obadd as $key => $value)
                <th>{{ $value->Descripcion }}</th>
                @endforeach
              </tr>
            </thead>

            <tfoot>
              <tr>
                @if (!isset($verid))
                <th>ID</th>
                @endif
                @foreach($obadd as $key => $value)
                <th>{{ $value->Descripcion }}</th>
                @endforeach
              </tr>
            </tfoot>
            <tbody>

              @foreach($historialtable as $key => $value)
              <tr>
                @if (!isset($verid))
                <td>{{ $value->id }} </td>
                @endif

                @foreach($obadd as $keycam => $valuecam)
                <td>
                  <?php
                  if($valuecam->Tipo=="file")
                  {     $cam=$valuecam->Nombre;
                    if($value->$cam!="")
                      $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$valuecam->Nombre.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                    else
                      $cadena="echo '';";
                  }else
                  {
                    $cadena="echo trim(\$value->".$valuecam->Nombre.");";
                  }

                  ?>
                  @if($valuecam->Nombre=="valor_predefinido")
                  <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena);?>
                                         </textarea>
                  @else
                  <?php eval($cadena); ?>
                  @endif
                </td>
                @endforeach

              </tr>
              @endforeach
            </tbody>
            </tfoot>

          </table>
        </div>
        @endif
<!-- Historial Nuevo-->

<!-- Fin Historial Nuevo-->
      </div>


    </div> <!-- ibox float-e-margins -->

    <script>
      function calcular() {
  var x = document.getElementById("costo_institucion").value;
  var y = document.getElementById("costo_prov").value;
  var z=document.getElementById("costo_inicial").value=(parseFloat(x)+parseFloat(y));

  var b = document.getElementById("costo_actual").value;
  // alert(b);

  var c=document.getElementById("diferencia").value=(parseFloat(z) - parseFloat(b));



}

@if(isset($poaact))

  document.getElementById("financiamiento").value='{{$poaact->financiamiento}}';
  document.getElementById("costo_institucion").value={{$poaact->costo_institucion}};
  document.getElementById("costo_prov").value={{$poaact->costo_prov}};
  document.getElementById("costo_inicial").value={{$poaact->costo_inicial}};
  document.getElementById("costo_inicial").value={{$poaact->costo_inicial}};
  document.getElementById("costo_actual").value={{$poaact->costo_actual}};
  document.getElementById("diferencia").value={{$poaact->diferencia}};

@endif

</script>

<div class="modal fade" id="modal-mapa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Información</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 modal_body_content">
            <div class="form-group">
              <label for="direccion_evento_modal">Dirección del Evento</label>
              <input type="text" class="form-control" id="direccion_evento_modal" placeholder="Ingrese la Dirección del Evento">
            </div>

            <div class="form-group">
              <label for="n_beneficiarios_evento_modal">Número de Beneficiarios</label>
              <input type="number" class="form-control" id="n_beneficiarios_evento_modal" placeholder="Ingrese el Número de Beneficiarios">
            </div>

            <div class="input-group datetime">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
              {!! Form::text('fecha_inicio_evento_modal', null , array(
              'data-placement' =>'top',
              'data-toogle'=>'tooltip',
              'class' => 'form-control datefecha',
              'placeholder'=>'',
              'fecha_inicio_evento_modal',
              )) !!}
            </div>

            <div class="input-group datetime">
              <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
              </span>
              {!! Form::text('fecha_fin_evento_modal', null , array(
              'data-placement' =>'top',
              'data-toogle'=>'tooltip',
              'class' => 'form-control datefecha',
              'placeholder'=>'',
              'fecha_fin_evento_modal',
              )) !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12 modal_body_map">
            <div class="location-map" id="location-map">
              <div style="width: 600px; height: 400px;" id="map_canvas"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
      </div>
    </div>
  </div>
</div>



<script>
$(document).ready(function () {
  $("#parroquia_id").change(function (e) {
      var parroquia=$(this).val();
      $.ajax({
              type: "GET",
              url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+parroquia,
              success: function (response) {
                $("#barrio_id").empty();
                $.each(response, function(key, element) {
                  $("#barrio_id").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                });
                // $('#barrio_id').val(165).change();
                $("#barrio_id").trigger("chosen:updated");
              }
          });

    });

});


</script>


@stop
