<?php
if(!isset($nobloqueo))
//    Autorizar(Request::path());
?>
<!--extends('layout')-->
@extends(request()->has("menu") ? 'layout_basic_no_head':'layouts.layout' )
@section ('titulo') {{ $configuraciongeneral[0] }} @stop
@section ('scripts')
@include("vistas.includes.mainjs")
<script type="text/javascript">
    @if(request()->has("menu"))
    //console.log("Hola");
        setTimeout(function(){
            $("#divindex").hide();
        },500);

    @endif
    setTimeout(function(){
        parent.$.fn.colorbox.close();
    },2000);
var URL_="{!! URL::to("$configuraciongeneral[6]") !!}";
var fecha_selecionada;
var tabla;
var datos;

$(document).ready(function() {
   @if(request()->id)
        window.open('/tramites/recibo/{{request()->id}}');
   @endif

    if ('{!! $configuraciongeneral[6] !!}'=='null') {
                $('#tbbuzonmain').dataTable({
                    responsive : true,
                    language: {
                        "emptyTable":     "No hay datos disponibles en la tabla",
                        "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                        "infoFiltered":   "(filtered from _MAX_ total entries)",
                        "infoPostFix":    "",
                        "thousands":      ",",
                        "lengthMenu":     "Mostrar _MENU_ entradas",
                        "loadingRecords": "Cargando...",
                        "processing":     "Procesando...",
                        "search":         "Buscar:",
                        "zeroRecords":    "No se encontraron registros coincidentes",
                        "paginate": {
                            "first":      "Primero",
                            "last":       "Último",
                            "next":       "Siguiente",
                            "previous":   "Atrás"
                        },
                        "aria": {
                            "sortAscending":  ": activate to sort column ascending",
                            "sortDescending": ": activate to sort column descending"
                        }
                    },
                    "dom": 'Bfrtlip',//'T<"clear">Bfrtlip',
                    "order": ([[ 0, 'desc' ]]),
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>' //text: '<i class="fa fa-print"></i> Imprimir'
                    },
                    {
                        extend: 'copy',
                        text: '<i class="fa fa-copy"></i>' //text: '<i class="fa fa-copy"></i> Copiar'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>' //text: '<i class="fa fa-file-excel-o"></i> Excel'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>' //text: '<i class="fa fa-file-pdf-o"></i> PDF'
                    },
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye-slash"></i>' //text: '<i class="fa fa-eye-slash"></i> Ocultar Columnas'
                    }
                ]

                });

            }else{


            tabla= $('#tbbuzonmain').DataTable({
                responsive : true,
                destroy: true,
                processing: true,
                serverSide: true,
                @if(isset($filtros))
                "searching": false,
                @endif
                lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
                ajax: {
                    url: URL_,
                    dataType: "json",
                    type: "GET",
                    "data": function ( d ) {
                        d._token = "{{csrf_token()}}",
                        @if(isset($filtros))
                            <?php
                                foreach($filtros as $filtro)
                                {
                                    echo 'd.'.$filtro->Nombre.'= $("#'.$filtro->Nombre.'").val();';
                                }
                            ?>
                        @endif
                        console.log()
                    }
                },

                columns:[
                    { "data": 'id' },
                    @foreach($objetos as $key => $value)
                            { "data":'{{$value->Nombre}}' },
                    @endforeach

                    { "data": 'acciones' }
                ],
                language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                dom: 'Bfrtlip',
                order: ([[ 0, 'desc' ]]),
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>',//text: '<i class="fa fa-print"></i> Imprimir'
                        @if ($configuraciongeneral[6]=='coordinacioncronograma/cronogramaajax')
                        exportOptions: {
    					    stripHtml: false,
                            format: {
                                body: function ( data, row, column, node ) {
                                // Strip $ from salary column to make it numeric
                                    if(column === 11)
                                    {
                                        var paser = $.parseHTML("<div>"+String(data)+"</div>");
                                        //console.log(paser);
                                        var obj=$(paser);
                                        var len = obj.find("i").length;
                                        if(len>0) {
                                            obj = obj.find("i");
                                            //console.log(obj[1].outerHTML);
                                            return String(obj[1].outerHTML);
                                        }
                                        return data;
                                    }
                                    return data;
                                    //return column === 10 ?
                                    //    txt.find('a').remove().end() : data;
                                    //data.replace( 'fa', '' ) : data;
                                    //toInsert= toInsert.find('input').remove().end();
                                }
                            }
				        }
				        @endif
                    },
                    {
                        extend: 'copy',
                        text: '<i class="fa fa-copy"></i>' //text: '<i class="fa fa-copy"></i> Copiar'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>' //text: '<i class="fa fa-file-excel-o"></i> Excel'
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>' //text: '<i class="fa fa-file-pdf-o"></i> PDF'
                    },
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye-slash"></i>' //text: '<i class="fa fa-eye-slash"></i> Ocultar Columnas'
                    }
                ]

            });


            }//End IF
            $(".divpopup").colorbox({iframe:true, innerWidth:screen.width -(screen.width * 0.10), innerHeight:screen.height -(screen.height * 0.30)});


            $('#btn_filtrar').click(function(){
                try {
                    llenarcalendario();

                } catch (error) {

                }
                tabla.draw();
        });

        $("#estado").change(function (e) {
            tabla.draw();
        });

        $("#parroquia_id").change(function (e) {
            var parroquia=$(this).val();
            $.ajax({
                    type: "GET",
                    url: "{{ URL::to('') }}/coordinacioncronograma/getBarrios/"+parroquia+'?tipo=2',
                    success: function (response) {
                            $("#barrio_id").empty();
                            $("#barrio_id").append("<option value='0'>TODOS</option>");
                            $.each(response, function(key, element) {
                            $("#barrio_id").append("<option value=\'" + element.id + "\'>" + element.barrio+"</option>");
                            });
                            // $('#barrio_id').val(165).change();
                            $("#barrio_id").trigger("chosen:updated");
                        }
                    });
        });
        $("#dieccion").change(function (e) {
            var parroquia=$(this).val();
            $.ajax({
                    type: "GET",
                    url: "{{ URL::to('') }}/coordinacioncronograma/getInicadoresDireccion/"+parroquia,
                    success: function (response) {
                            $("#id_indicador").empty();
                            $("#id_indicador").append("<option value='0'>TODOS</option>");
                            $.each(response, function(key, element) {
                            $("#id_indicador").append("<option value=\'" + element.id + "\'>" + element.tipo+"</option>");
                            });
                            // $('#id_indicador').val(165).change();
                            $("#id_indicador").trigger("chosen:updated");
                        }
                    });
        });


        });//End Document Ready


function mostrar()
    {
            var cadena= $("#cadena").val();
                $('#tbbuzonmain').DataTable({
                destroy: true,
                responsive : true,
                ajax: {
                url: '{{ URL::to("getUsers") }}'+'?cadena='+cadena,
                dataSrc:"",
                },
                columns: [
                        { data: 'id'},
                        { data: 'cedula'},
                        { data: 'name'},
                        { data: 'telefono'},
                        { data: 'email'},
                        { data: 'fecha_nacimiento'},
                        { data: 'perfil'},
                        { data: 'direccion'},
                        { data: 'cargo'},

                    {data: "id" , render : function ( data, type, row, meta ) {
              return type === 'display'  ?
              '<a href="{{ URL::to($configuraciongeneral[1]) }} /'+data+'" class="divpopup" target="_blank""><i class="fa fa-newspaper-o"></i></a><a href="{{ URL::to($configuraciongeneral[1]) }}/'+data+'/edit"><i class="fa fa-pencil-square-o"></i></a>@if(isset($delete))<a href="javascript::" onclick="eliminar('+data+')"><i class="fa fa-trash"></i> </a>@endif':
                data;
            }}],

                    language: {
                    "emptyTable":     "No hay datos disponibles en la tabla",
                    "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                    "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                    "infoFiltered":   "(filtered from _MAX_ total entries)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "Mostrar _MENU_ entradas",
                    "loadingRecords": "Cargando...",
                    "processing":     "Procesando...",
                    "search":         "Buscar:",
                    "zeroRecords":    "No se encontraron registros coincidentes",
                    "paginate": {
                        "first":      "Primero",
                        "last":       "Último",
                        "next":       "Siguiente",
                        "previous":   "Atrás"
                    },
                    "aria": {
                        "sortAscending":  ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    }
                },
                "dom": 'Bfrtlip',//'T<"clear">Bfrtlip',
                //"dom": 'Bfrtip',
                /*
                "tableTools": {
                    "sSwfPath": "js/plugins/dataTables/swf/copy_csv_xls_pdf.swf"
                },*/
                    "order": ([[ 0, 'desc' ]]),
                buttons: [
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>' //text: '<i class="fa fa-print"></i> Imprimir'
                    },
                    {
                        extend: 'copy',
                        text: '<i class="fa fa-copy"></i>' //text: '<i class="fa fa-copy"></i> Copiar'
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-excel-o"></i>' //text: '<i class="fa fa-file-excel-o"></i> Excel'
                    },
                    {
                        extend: 'pdf',
                        text: '<i class="fa fa-file-pdf-o"></i>' //text: '<i class="fa fa-file-pdf-o"></i> PDF'
                    },
                    {
                        extend: 'colvis',
                        text: '<i class="fa fa-eye-slash"></i>' //text: '<i class="fa fa-eye-slash"></i> Ocultar Columnas'
                    }
                ]

            });


    }
@if(isset($delete))
@if($delete=='si')
    function eliminar($id)
    {
            var r= confirm("Seguro de eliminar este registro?");
            if(r==true)
                $('#frmElimina'+$id).submit();
            else
                return false;
    }
@endif
@endif



function mostrarImagen(ruta) {
    Swal.fire({
        imageUrl: ruta,
        width:800,
        imageHeight: 400
    })

}
function popup(obj)
{
    //console.log(obj);
    //console.log($(obj).parent().parent());
    var row= $(obj).parent().parent();
    row.css("background-color","#cdf7c8");
    $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
}

function atender_donacion(id,tipo,obs){
    const swalWithBootstrapButtons = Swal.mixin({
         customClass: {
             confirmButton: 'btn btn-success',
             cancelButton: 'btn btn-danger',
            //  container: 'sweet_containerImportant',
             popup:'sweet_popupImportant'

         },
         buttonsStyling: false
     })

    if(tipo==1){

     swalWithBootstrapButtons.fire({
             title: 'Entregar donación',
             html:
             '<input id="adjunto" type="file" class="swal2-file form-control">'+
             '<span>Fecha de entrega</span><br>'+
             '<input id="fecha_entrega" type="date" class="swal2-date form-control" value="{{date("Y-m-d")}}">'+
             '<span>Observación</span><br>'+
             '<input id="obs" class="swal2-textarea form-control" value="'+obs+'">',
             showCancelButton: true,
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                var adjunto=$("#adjunto")[0].files[0];//document.getElementById('adjunto');
                 var obs=document.getElementById('obs').value;
                 var fecha_entrega=document.getElementById('fecha_entrega').value;
                 var data = new FormData();
                data.append("id", id);
                data.append("tipo", tipo);
                data.append("obs", obs);
                data.append("adjunto", adjunto);
                data.append("fecha_entrega", fecha_entrega);
                data.append("_token", '{{ csrf_token() }}');

                 // console.log(i,f,obs);
                 $.ajax({
                     type: "POST",
                     contentType: false,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                     cache : false,
                    // processData: false,
                     url: '{{URL::to('')}}/entregar_donacion',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 });

                                 tabla.draw();
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                        // swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

            }

        });
    }else if(tipo==2){
        swalWithBootstrapButtons.fire({
             title: '¿Seguro de Continuar?',
             text: "Ingrese observación por la cual se archiva la solicitud",
             //type: 'warning',
             input: 'textarea',
             inputValue:obs,
             inputAttributes: {
             autocapitalize: 'off'
             },
             showCancelButton: true,
             confirmButtonText: 'Si, continuar!',
             cancelButtonText: 'No, cancelar',
             showLoaderOnConfirm: true,
             //customClass:'swal-height-show-agenda_2',
             preConfirm: (obs) => {
                 $.ajax({
                     type: "POST",
                     data: {
                         "_token": "{{ csrf_token() }}",
                         "id": id,
                         "tipo": tipo,
                         "obs": obs,
                     },
                     url: '{{URL::to('')}}/entregar_donacion',
                     success: function (data) {
                         try {

                             if (data['estado'] == 'ok') {
                                 Swal.fire({
                                     position: 'center',
                                     type: 'success',
                                     title: data['msg'],
                                     showConfirmButton: false,
                                     timer: 1500
                                 });
                                 tabla.draw();
                             } else {
                                 Swal.fire({
                                     type: 'error',
                                     title: 'Uups...',
                                     text: data['msg']
                                 });
                             }

                         } catch (error) {

                         }

                     },
                     error: function (data) {
                        // swal("Cancelled", "Your imaginary file is safe: ) ", " error ");
                     }
                 });

            }

        });
    }

  }
</script>

@stop
@section('estilos')
<style>
    body.DTTT_Print {
        background: #fff;

    }

    .DTTT_Print #page-wrapper {
        margin: 0;
        background: #fff;
    }

    button.DTTT_button,
    div.DTTT_button,
    a.DTTT_button {
        border: 1px solid #e7eaec;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    button.DTTT_button:hover,
    div.DTTT_button:hover,
    a.DTTT_button:hover {
        border: 1px solid #d2d2d2;
        background: #fff;
        color: #676a6c;
        box-shadow: none;
        padding: 6px 8px;
    }

    .dataTables_filter label {
        margin-right: 5px;

    }

    .swing li {
        opacity: 0;
        transform: rotateY(-90deg);
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }

    .colores {
        list-style: none;
        height: 0;
        line-height: 2em;
        margin: 0;
        padding: 0 0.5em;
        background: #fafaf9;
        /* display: none */
        overflow: hidden;
        border: 1px solid #d8d8d8;
        box-shadow: 0 3px 8px rgba(0, 0, 0, 0.25);
        margin-bottom: 1.5% !important;
    }

    li.show {
        height: 10%;
        margin: 2px 0;
    }

    .swing {
        perspective: 100px;
    }

    .swing li {
        /* opacity: 0; */
        transform: rotateX(-90deg);
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }

    .swing li.show {
        /* opacity: 1; */
        transform: none;
        transition: all 0.5s cubic-bezier(.36, -0.64, .34, 1.76);
    }


    .scrollbar {
        overflow: auto;
        width: 0.5em !important;
        scroll-behavior: smooth !important;
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
        background-color: #f2f2f2 !important;
        outline: 1px solid #f2f2f2 !important;
        /* border-radius: 10px !important; */
        height: auto !important;
        width: 100% !important;
    }

    ::-webkit-scrollbar {
        width: 0.5em !important;
        scroll-behavior: smooth !important;
    }

    ::-webkit-scrollbar-track {
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3) !important;
    }

    ::-webkit-scrollbar-thumb {
        background-color: darkgrey !important;
        outline: 1px solid slategrey !important;
        border-radius: 10px !important;
    }
</style>

@stop
@section ('contenido')
<h1 style="background-color: #FFFFFF; padding: 1% 0 1% 2%;"> {{ $configuraciongeneral[0] }}</h1>


<div class="ibox float-e-margins">
    <div class="ibox-title">
        @if (Session::has('message'))
        <script>
            $(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    //"positionClass": "toast-bottom-right"
    toastr.options = {
        "positionClass": "toast-bottom-right"
    };
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
        </script>
        <div class="alert alert-info">{{ Session::get('message') }}</div>
        @endif

        @if (Session::has('error'))
        <script>
            $(document).ready(function() {
    toastr["error"]("{{ Session::get('error') }}");


});
        </script>
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif

        <div class="ibox-tools">
            <a class="collapse-link" href="javascript:">
                <i class="fa fa-chevron-up"></i>
            </a>
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:">
                <i class="fa fa-wrench"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                @if($create=='si')
                <li><a href="{{ URL::to($configuraciongeneral[1]) }}">Listar Todos</a>
                </li>

                <li><a id="btnnuevo" href="{{ URL::to($configuraciongeneral[1]."/create") }}">Nuevo</a>
                </li>
                @endif

            </ul>
            <a class="close-link" href="javascript:">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
    <div id="divindex" class="ibox-content imgback">
        @if (isset($configuraciongeneral[3]))
        {{Form::open(array('method' => 'PUT','action' => $configuraciongeneral[3],'role'=>'form' ,'class'=>'form-inline','id'=>'formu')) }}
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    {{ Form::label($configuraciongeneral[1].'bus', 'Filtrar Busqueda por:',array('class'=>'control-label')) }}
                    {{ Form::select($configuraciongeneral[1].'bus',[null=>'Escoja'] + $listabusqueda ,$valorcmb,array('class' => 'chosen-select','style' => 'width:90%')) }}
                    <br> <br>
                </div>
                <div class="col-md-3">
                    {{ Form::submit('Buscar', array('class' => 'btn btn-primary')) }}
                </div>
            </div>
        </div>
        {{Form::hidden('ruta',URL::to('pensionrecaudacion') ,array('id'=>'ruta'))}}
        {{ Form::close() }}
        @endif
        <div class="">
            @if($configuraciongeneral[1]=='usuarios')

            <label for="cadena"> Nombre</label>
            <input id="cadena" class="cadena" type="text">
            <a onclick="mostrar()" class="btn btn-primary" href="javascript:">Mostrar</a>
            @endif
            @if($create=='si')
            <a href="{{ URL::to($configuraciongeneral[1]).((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-primary ">Todos</a>
            @if(isset($permisos))
            @if($permisos->crear=="SI")
            <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default ">Nuevo</a>
            @endif
            @else
            <a href="{{ URL::to($configuraciongeneral[1]."/create").((isset($configuraciongeneral[8]))?$configuraciongeneral[8]:'') }}" class="btn btn-default ">Nuevo</a>
            @endif
            @endif

            @if(isset($cerpoa))
               <a  href="{{ URL::to("coordinacioncronograma/solicitapoa?anio=".$cerpoa) }}" class="btn btn-info ">Certificaciones</a>
            @endif


            @if (isset($actividades_agenda))
                <a onclick="mostrar()" class="btn btn-default " href="javascript:"><i class="fa fa-eye" aria-hidden="true"></i> Agenda</a>
            @endif




        </div>
        <div class="row">
            <div class="col-md-12">
                @if (isset($filtros))
                {!! Form::open(array('url' => $configuraciongeneral[1],'role'=>'form' ,
                'id'=>'form1','class'=>'form-row')) !!}
                @foreach ($filtros as $key => $campos)

                @if($campos->Tipo=="date")
                <div class="col-md-3 col-lg-2" id="divobj-{{ $campos->Nombre }}">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-6
                    control-label")) !!}
                    <div class="col-lg-12 col-md-12">
                        <div class="input-group date">
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>

                            {!! Form::text($campos->Nombre, null , array(
                            'data-placement' =>'top',
                            'data-toogle'=>'tooltip',
                            'class' => 'form-control datefecha',
                            'placeholder'=>$campos->Descripcion,
                            $campos->Nombre,
                            )) !!}
                        </div>

                    </div>
                </div>
                @elseif($campos->Tipo=="select")
                <div class="col-md-2 col-lg-2" id="group-{{$campos->Nombre}}">
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-12
                    control-label")) !!}
                    <div class="col-lg-12 col-md-12">
                        {!! Form::select($campos->Nombre,$campos->Valor,$campos->ValorAnterior,
                        array('class' => "form-control")) !!}
                    </div>
                </div>
                @else
                <div class="col-lg-2" id="group-{{$campos->Nombre}}" >
                    {!! Form::label($campos->Nombre,$campos->Descripcion.':',array("class"=>"col-lg-12 control-label"))
                    !!}
                    <div class="col-lg-12">
                        {!! Form::text($campos->Nombre, $campos->Valor=="Null" ?
                        old($campos->Valor):$campos->Valor ,
                        array('class' => 'form-control '.$campos->Clase,'placeholder'=>'Ingrese '.$campos->Descripcion
                        )) !!}
                    </div>
                </div>
                @endif
                @endforeach
                <div class="form-group col-md-12 col-lg-12" style="text-align: end;">
                    <br>
                    {!! Form::button("Filtrar", array('id'=>'btn_filtrar','class' => 'btn btn-primary' )) !!}
                </div>
                {!! Form::close() !!}
                @endif
            </div>
        </div>

        @if (isset($actividades_agenda))


            <?php
                $actividad_collect=collect($actividades_agenda);
                $filtered = $actividad_collect->whereBetween('fecha_inicio', [date('Y-m-d').' 00:00:00', date('Y-m-d').' 23:59:59']);
                $datos_filtrados=$filtered->all();

            ?>

            <script>

                function mostrar(){
                    var agenda=document.getElementById('agenda').getAttribute('display');
                    if ($('#agenda').is(':hidden')) {
                        $('#agenda').show();
                    } else {
                        $('#agenda').hide();
                    }


                    // $('#agenda').show();
                }


            </script>

            <div class="ibox float-e-margins">
                <div class="ibox-content imgback" id="agenda" style="display: block;">
                    <div class="row">
                        <div style="background: white;">
                             <div id='calendar'></div>
                            </div>
                    </div>
                    <br>
                    {{--
                    <div class="row">
                        <div class="col-md-5">

                             <div class="calendar"></div>
                        </div>

                        <div class="col-md-7">
                            <div class="scrollbar">
                                <ul id="actividades">
                                </ul>
                            </div>
                        </div>
                    </div>--}}

                </div>
            </div> <!-- ibox-content -->
            @endif

        <table id="tbbuzonmain" class="table table-striped table-bordered table-hover display">
            <thead>
                <tr>
                    @if (!isset($verid))
                    <th>ID</th>
                    @endif


                    @foreach($objetos as $key => $value)
                    <th>{{ str_replace("(*)","",$value->Descripcion) }}</th>
                    @endforeach
                    <th>Acción</th>
                </tr>
            </thead>

            <tfoot>
                <tr>
                    @if (!isset($verid))
                    <th>ID</th>
                    @endif
                    @foreach($objetos as $key => $value)
                    <th>{{ $value->Descripcion }}</th>
                    @endforeach
                    <th>Acción</th>
                </tr>
            </tfoot>
            <tbody>
                @foreach($tabla as $key => $value)
                <tr>
                    @if (!isset($verid))
                    <td>{{ $value->id }} </td>
                    @endif

                    @foreach($objetos as $keycam => $valuecam)
                    <td>
                        <?php
                               if($valuecam->Tipo=="file")
                               {     $cam=$valuecam->Nombre;
                                    if($value->$cam!="")
                                        $cadena="echo '<a href='.URL::to(\"/\").'/archivos_sistema/'.trim(\$value->".$valuecam->Nombre.").' class=\"btn btn-success dropdown-toggle divpopup\" target=\"_blank\"><i class=\"fa fa-file-pdf-o\"></i></a>';";
                                    else
                                        $cadena="echo '';";
                               }else
                               {
                                $cadena="echo trim(\$value->".$valuecam->Nombre.");";
                               }

                                ?>
                        @if($valuecam->Nombre=="valor_predefinido")
                        <textarea style="margin: 0px; width: 343px; height: 185px;" disabled>
                                            <?php eval($cadena);?>
                                         </textarea>
                        @else
                        <?php eval($cadena); ?>
                        @endif
                    </td>
                    @endforeach
                    <td>
                        <!--      Cuando es vista de muchos botones -->
                        @if (isset($botonruta))
                        <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Editar <span
                                    class="caret"></span></button>
                            <ul class="dropdown-menu">
                                @foreach($botonruta as $keyboton => $botonesvalue)
                                <?php
                                                        eval("\$cade=\$value->".$botonvariable[$keyboton].";");
                                                        $rutaboton = str_replace("@variable",$cade, $botonesvalue);
                                                    //$rutaboton = str_replace("@variable", $value->$botonvariable[$keyboton], $botonesvalue);
                                                    ?>
                                <li><a href="{{ URL::to($rutaboton)}}" target="_blank"
                                        class="font-bold">{{ $botoncaption[$keyboton] }} </a></li>
                                <li class="divider"></li>
                                @endforeach
                            </ul>
                        </div>
                        @else
                        @php
                        // dd($configuraciongeneral);
                        @endphp
                        <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id) }}" class="divpopup"
                            target="_blank"">
                                            <i class=" fa fa-newspaper-o"></i></a>&nbsp;&nbsp;
                        @if(!isset($edit))
                        <a href="{{ URL::to($configuraciongeneral[1]."/".$value->id."/edit") }}"><i
                                class="fa fa-pencil-square-o"></i></a>
                        @endif&nbsp;
                        @if(isset($editfinalizartramite))
                        <a href="{{ URL::to("tramites/finalizartramite?id=".$value->id) }}"><i
                                class="fa fa-pencil-square-o"></i></a>
                        @endif&nbsp;

                        @if(isset($delete))
                        @if($delete=='si')
                        <a href="javascript::" onclick="eliminar({{ $value->id }})"><i class="fa fa-trash"></i>
                        </a>
                        @endif
                        @endif

                        @if(isset($imprimir))

                        <a href="vertramite/{{ $value->id }}" class="divpopup" target="_blank""><i class=" fa
                            fa-print"></i> </a>

                        @endif
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
            </tfoot>

        </table>
        {{-- $tabla->links() --}}
    </div> <!-- ibox-content -->
</div> <!-- ibox float-e-margins -->
@if(isset($delete))
@if($delete=='si' )
<div style="display: none;">
    @foreach($tabla as $key => $value)
    {!! Form::open(['route' => [str_replace("/",".",$configuraciongeneral[7]).'.destroy', $value->id], 'method'
    =>
    'delete','id'=>'frmElimina'.$value->id,'class' => 'pull-right']) !!}
    {!! Form::submit('Eliminar', array('class' => 'btn btn-small btn-warning')) !!}
    {!! Form::close() !!}
    @endforeach
</div>
@endif
@endif
@stop
