<?php
    $images = explode("|",ConfigSystem("backgroundlogin"));
    $c = count($images)-1;
    $imgbk = $images[rand(0,$c)];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset("favicon.ico") }} " rel="shortcut icon" type="image/vnd.microsoft.icon" />
    <title>@yield('titulo')</title>

    {{ HTML::style('css/bootstrap.min.css') }}
    {{ HTML::style('font-awesome/css/font-awesome.css') }}

    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

    {{ HTML::script('https://code.jquery.com/jquery-3.3.1.js') }}
    {{ HTML::script('/js/bootstrap.min.js') }}

    {{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::script('js/plugins/toastr/toastr.min.js') }}

    {{ HTML::style('css/plugins/sweetalert/sweetalert.css') }}
    {{ HTML::script('js/plugins/sweetalert/sweetalert.min.js') }}

    {{ HTML::style('css/jquery-gallery.css') }}
    {{ HTML::script('js/jquery-gallery.js') }}
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

<style type="text/css">
    .bgbody {
        background: url({{ asset($imgbk) }}) no-repeat 0px 0px;
        background-size: cover;
    }
.middle-box h1 {
    font-size: 100px !important;
}
#myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%;
  min-height: 100%;
  filter:opacity(30%);
}
/* .container { margin: 150px auto; max-width: 600px; } */
@media (min-aspect-ratio: 16/9) {
    #myVideo {
        width:100%;
        height: auto;
    }
}
@media (max-aspect-ratio: 16/9) {
    #myVideo {
        width:auto;
        height: 100%;
    }
}

.fade{
    opacity: 1 !important;
}
</style>
<script type="text/javascript">
    $(function() {
  $('.gallery-slideshow').slideshow({
    // default: 2000
    interval: 3000,
    // default: 500
    width: 850,
    // default: 350
    height: 620
  });
});

</script>
</head>

<body class="gray-bglogin bgbody">
<video autoplay muted loop id="myVideo">
  <source src="{{ asset("img/videobg.mp4") }}" type="video/mp4">
  Your browser does not support HTML5 video.
</video>

{{--    <div class="form-row" >--}}
{{--        <div class="form-group col-md-12 col-lg-12" style="    margin-top: 2%;">--}}
            <div class="middle-box text-center loginscreen animated fadeInDown">
                <div>
                    <div>

                       <!-- logomanta.jpg -->
                        <h1>
                            {{ HTML::image('img/logo.png','LOGO',array('style'=>'width: 180px;')) }}
                            </h1>


                    </div>
                    <h3 class="titulo">{{ config('app.name') }}</h3>

                    <p class="tab">@yield('cabecera')</p>
                                   @yield('errores')
                    <!--form class="m-t" role="form" action="index.html"-->
                        @yield('formulario')
                        <br>
                    <p class="m-tLogin"> <small>{!! trans('html.main.copyright') !!}</small> </p>
                </div>
            </div>
{{--        </div>--}}
{{--        <div class="form-group col-md-8 col-lg-8" style="    margin-top: 2%;"> --}}

{{--            <div class="animated fadeInDown">--}}
{{--                <?php $imagenes= explode('|',ConfigSystem('imagenes-slide')) ?>--}}

{{--                <ul class="gallery-slideshow">--}}
{{--                    @foreach ($imagenes as $key => $item)--}}
{{--                       <li><img src="{{asset('img_gallery/'.$item)}}" width="100%" height="auto"/></li>--}}
{{--                    @endforeach--}}
{{--                 --}}
{{--                </ul>--}}
{{--              </div>--}}

{{--        </div>--}}

{{--    </div>--}}
{{ HTML::script('js/jquery.snow.min.1.0.js') }}
@if(date("m")==12)
    {{ HTML::script('js/cursor-con-nieve.js') }}
@endif
<script>
    $(document).ready( function(){
        var d = new Date();// Capturo Fecha Actual
        var n = d.getMonth();//Capturo Mes de la Fecha Actual
        //0: Enero, 1: Febrero, 2:Marzo, etc.
        //console.log(n);
        if(n==11)//Diciembre
            $.fn.snow();
    });
</script>
</body>

</html>
