<?php
  $images=explode("|",ConfigSystem("backgroundlogin"));
  $c=count($images)-1;
  $imgbk=$images[rand(0,$c)];
?>
<!DOCTYPE html>
<html>

<head>

    <meta charset='utf-8'/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="{{ asset('favicon.ico') }} " rel="shortcut icon" type="image/vnd.microsoft.icon" />
   <title>@yield('titulo')</title>

    {!! HTML::style('css/bootstrap.min.css') !!}
    {!! HTML::style('font-awesome/css/font-awesome.css') !!}

   <!-- Data Tables -->
    {{ HTML::style('css/plugins/dataTables/dataTables.bootstrap.css') }}
    {{ HTML::style('css/plugins/dataTables/jquery.dataTables.min.css') }}
    {{ HTML::style('css/plugins/dataTables/responsive.dataTables.min.css') }}
    {{ HTML::style('css/plugins/dataTables/dataTables.tableTools.min.css') }}
    {{ HTML::style('css/plugins/dataTables/buttons.dataTables.min.css') }}


<!-- Mensaje -->

{{ HTML::style('css/plugins/toastr/toastr.min.css') }}
    {{ HTML::style('css/animate.css') }}
    {{ HTML::style('css/style.css') }}

    <!--Wizard-->
{{-- {{ HTML::style('css/plugins/steps/jquery.steps.css')}} --}}
{{-- {{ HTML::script('css/plugins/iCheck/custom.css')}} --}}

    @yield('estilos')
<!-- Mainly scripts -->
{{ HTML::script('js/jquery-2.1.1.js') }}
    {{-- HTML::script('https://code.jquery.com/jquery-3.3.1.js') --}}
{{ HTML::script('js/bootstrap.min.js') }}
{{ HTML::script('js/plugins/metisMenu/jquery.metisMenu.js') }}
{{ HTML::script('js/plugins/slimscroll/jquery.slimscroll.min.js') }}

{!! HTML::style('css/plugins/blueimp/css/blueimp-gallery.min.css'); !!}
{!! Html::script('js/plugins/blueimp/jquery.blueimp-gallery.min.js') !!}

<!-- Custom and plugin javascript -->
{{ HTML::script('js/inspinia.js') }}
{{ HTML::script('js/plugins/pace/pace.min.js') }}
{{ HTML::script('js/plugins/steps/jquery.steps.min.js') }}
{{ HTML::script('js/plugins/validate/jquery.validate.min.js') }}

 <!-- Data Tables -->


 <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" type="text/javascript"></script>

 {{ HTML::script('js/plugins/dataTables/jquery.dataTables.min.js') }}
    {{ HTML::script("js/plugins/dataTables/dataTables.buttons.min.js") }}
    {{ HTML::script("js/plugins/dataTables/buttons.flash.min.js") }}
    {{ HTML::script("js/plugins/dataTables/jszip.min.js") }}

<!-- {{ HTML::script("js/plugins/dataTables/pdfmake.min.js") }} -->
    {{ HTML::script("js/plugins/dataTables/vfs_fonts.js") }}
    {{ HTML::script("js/plugins/dataTables/buttons.html5.min.js") }}

    {{ HTML::script("js/plugins/dataTables/buttons.print.min.js") }}

    {{ HTML::script("js/plugins/dataTables/buttons.colVis.min.js") }}

    {{ HTML::script("js/plugins/dataTables/dataTables.bootstrap.js") }}
    {{ HTML::script("js/plugins/dataTables/dataTables.responsive.min.js") }}

    {{ HTML::script("js/plugins/dataTables/dataTables.tableTools.min.js") }}






 <!-- Mensaje -->
 {{ HTML::script('js/plugins/toastr/toastr.min.js') }}

      <!-- CSS Notificacion -->
    {{ HTML::style('ventanas-modales/ventanas-modales.css') }}
    <!-- CSS Notificacion -->
    {{ HTML::script('ventanas-modales/ventanas-modales.js') }}
    <!-- Graficos estadissticos -->
{{ HTML::style('css/plugins/morris/morris-0.4.3.min.css') }}
{{-- {{ HTML::script('js/plugins/morris/raphael-2.1.0.min.js') }} --}}
{{ HTML::script('js/plugins/morris/raphael.js') }}
{{ HTML::script('js/plugins/morris/morris.js') }}
<!-- Chosen -->
    {{ HTML::style('chosen/docsupport/prism.css') }}
    {{ HTML::style('chosen/chosen.css') }}

    {{ HTML::script('chosen/chosen.jquery.js') }}
    {{ HTML::script('chosen/docsupport/prism.js') }}
<!-- ColorBox -->
    {{ HTML::style('colorbox/colorbox.css') }}
    <style type="text/css">
        /* To change position of close button to Top Right Corner */
        #colorbox #cboxClose
        {
            top: 0;
            right: 0;
        }
        #cboxLoadedContent{
            margin-top:28px;
            margin-bottom:0;
        }
        #mapa {
            height: 500px;
            border-style: none;
            width: auto;
            border: none;
            text-align: center;
        }

    </style>
    {{ HTML::script('colorbox/jquery.colorbox.js') }}
    <!-- Jquery Validate -->
    {{ HTML::script('js/jquery.validate.min.js') }}

        <!-- DatePicker -->
        {{ HTML::style('bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}
        {{ HTML::script('bootstrap-datepicker/bootstrap-datepicker.js') }}
        {{ HTML::script('bootstrap-datepicker/locales/bootstrap-datepicker.es.js') }}

        <!-- DateTimePicker -->
        {{ HTML::style('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') }}
        {{ HTML::script('js/plugins/datetimepicker/moment.min.js') }}
        {{ HTML::script('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}
    <!-- Selective -->
    {!! HTML::style('selective/selectize.bootstrap3.css'); !!}
    {!! Html::script('selective/selectize.js') !!}
    <!-- Tree -->
    {!! HTML::style('css/plugins/jsTree/style.css'); !!}
    {!! Html::script('js/plugins/jsTree/jstree.js') !!}

    <!-- dropzone -->
    {!! HTML::style('css/plugins/dropzone/dropzone.css'); !!}
    {!! Html::script('js/plugins/dropzone/dropzone.js') !!}




    {{-- Html::script('js/plugins/highcharts/highcharts.js') --}}

    {!! HTML::style('css/plugins/c3/c3.min.css'); !!}
    {!! Html::script('js/plugins/d3/d3.min.js') !!}
    {!! Html::script('js/plugins/c3/c3.min.js') !!}

    {!! Html::script('js/plugins/sparkline/jquery.sparkline.min.js') !!}
    {!! Html::script('js/jquery.blockUI.js') !!}

    {!! HTML::style('lightgallery/css/lightgallery.css'); !!}
    {!! Html::script('lightgallery/js/lightgallery.min.js') !!}


    {{ HTML::style('css/jquery-gallery.css') }}
    {{ HTML::script('js/jquery-gallery.js') }}

      <!--sweetalert-->
      {{ HTML::style('css/plugins/sweetalert/sweetalert.css')}}
      {{ HTML::script('js/plugins/sweetalert/ciudadano/sweetalert.min.js') }}

      {{ HTML::style('fullcalendar/fullcalendar.min.css')}}
      {{ HTML::script('fullcalendar/fullcalendar.min.js') }}
      {{ HTML::script('fullcalendar/es-us.js') }}

      {{ HTML::style('pg-calendar/css/pignose.calendar.css')}}
      {{ HTML::script('pg-calendar/js/pignose.calendar.js') }}
<!--Loader-->
    {{ HTML::script('js/jquery.queryloader2.min.js') }}
    {{ HTML::script('js/popper.min.js') }}
    {{ HTML::script('js/tooltip.min.js') }}





    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.css">

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/main.js">   </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/daygrid/main.js">   </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/timegrid/main.js">   </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/list/main.js">   </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/core/locales-all.js">   </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/4.2.0/bootstrap/main.min.js">   </script>

    <script src="{{asset('js/numeral.min.js')}}" type="text/javascript"></script>
    <!-- <script src="{{asset('js/highcharts.js')}}"></script> -->
    <!-- <script src="{{asset('js/modules/exporting.js')}}"></script> -->


<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>




   {{-- ///editor  --}}
   {!! HTML::style('trumbowyg/dist/ui/trumbowyg.min.css'); !!}
   {!! Html::script('trumbowyg/dist/trumbowyg.min.js') !!}
   {!! Html::script('trumbowyg/dist/langs/es.min.js') !!}





    {{ HTML::style('css/select2.min.css')}}
    {{ HTML::script('js/select2.min.js') }}
    {!! Html::script('js/randomColor.js') !!}


  {{-- mapbox --}}
  <link href="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.css" rel="stylesheet" />
  <link rel="stylesheet"
      href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css"
      type="text/css" />
  <script src="https://api.mapbox.com/mapbox-gl-js/v1.9.1/mapbox-gl.js"></script>
  <script src="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js"></script>
  <script src="{{ asset('js/mapbox.js').'?v='.rand(1,1000)  }}"></script>





    <link href="{{ asset('plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/summernote/summernote.min.css')}}" rel="stylesheet">

    <!--Markdown [ OPTIONAL ]-->
    <script src="{{ asset('plugins/bootstrap-markdown/js/markdown.js')}}"></script>
    <!--ToMarkdown [ OPTIONAL ]-->
    <script src="{{ asset('plugins/bootstrap-markdown/js/to-markdown.js')}}"></script>
    <!--Bootstrap Markdown [ OPTIONAL ]-->
    <script src="{{ asset('plugins/bootstrap-markdown/js/bootstrap-markdown.js')}}"></script>
    <!--Summernote [ OPTIONAL ]-->
    <script src="{{ asset('plugins/summernote/summernote.min.js')}}"></script>




    <script type="text/javascript">
        window.addEventListener('DOMContentLoaded', function() {
            QueryLoader2(document.querySelector("body"), {
                barColor: "#00000",
                backgroundColor: "#ffffff75",
                percentage: true,
                barHeight: 1,
                minimumTime: 200,
                fadeOutTime: 1000
            });
        });
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };
        $(document).ready(function() {
            $("a").click(
                function () {
                    var bad = this.href.lastIndexOf('#') >= 0 || this.href.indexOf('javascript') >= 0;
                    if(!bad) {
                        if (isMobile.any()) {
                            QueryLoader2(document.querySelector("body"), {
                                barColor: "#ffffff",
                                backgroundColor: "#ffffff75",
                                //percentage: true,
                                barHeight: 1,
                                minimumTime: 200,
                                fadeOutTime: 1000
                            });
                            $("#qLoverlay").html("<div class='centerdiv'><div class=\"sk-spinner sk-spinner-double-bounce\">\n" +
                                "                                    <div class=\"sk-double-bounce1\"></div>\n" +
                                "                                    <div class=\"sk-double-bounce2\"></div>\n" +
                                "                                </div></div>");
                        }
                    }
                }
            );
        });
    </script>
<style type="text/css">
    .centerdiv{
        position: absolute;
        height: 50px;
        width: 50px;
        /*background:red;*/
        top:calc(50% - 50px/2); /* height divided by 2*/
        left:calc(50% - 50px/2); /* width divided by 2*/
    }
    @media only screen and (max-width: 600px) {
        .fa-newspaper-o:before {
                font-size:  3em !important;
            }
        .fa-edit:before, .fa-pencil-square-o:before {
            font-size:  3em !important;
        }
        .vertical-timeline-block .vertical-timeline-content{
            width: 100% !important;
        }
}
    .bgbody {
        background: url({{ asset($imgbk) }}) no-repeat 0px 0px !important;
        background-size: cover !important;
        /*filter:opacity(30%);       */
    }
    .imgback {
    background-image: initial;
    background-position-x: initial;
    background-position-y: initial;
    background-size: initial;
    background-repeat-x: initial;
    background-repeat-y: initial;
    background-attachment: initial;
    background-origin: initial;
    background-clip: initial;
    background-color: rgba(255, 255, 255, 0.6);
    }
    .page-break {
    page-break-after: always;
}
.swal2-confirm {
                margin: 0 0.5rem;
            }

            .swal2-cancel {
                margin: 0 0.5rem;
            }
    @media only screen and (max-width: 600px) {
        #divusuario{
            display: none;
        }
    }



    .swal-height-show-agenda{
        width: 65%;
        /* height: 90%; */
    }

    /* .sweet_containerImportant{
        width: 50%;
        height: 70%;
    } */
    .sweet_popupImportant{
        /* width: 50%; */
        /* height: 70%; */
    }

    .swal2-content{
        height: 100%;
    }
    .modal-backdrop {
        position: relative;
    }


</style>

@yield('scripts')

 <script type="text/javascript">


function popup(obj)
{
    //console.log(obj);
    //console.log($(obj).parent().parent());
    var row= $(obj).parent().parent();
    row.css("background-color","#cdf7c8");
    $(obj).colorbox({fixed:true,iframe:true, innerWidth:screen.width -(screen.width * 0.20), innerHeight:screen.height -(screen.height * 0.25)});
}

function cargaatender()
        {

            $.ajax({
                type: "GET",
                url: '{{ URL::to("getnotificatodos") }}',
                data: '',
                error: function(objeto, quepaso, otroobj){
                    $("#txttodos").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
                },
                success: function(datos){
                    $("#alertas").html(datos);
                },
                statusCode: {
                    404: function() {
                        $("#txttodos").html("<div class='alert alert-danger'>No existe URL</div>");
                    }
                }
            });
        }
        function NotificacionTotal()
        {
            $.ajax({
                type: "GET",
                url: '{{ URL::to("getnotificacuenta") }}',
                data: '',
                error: function(objeto, quepaso, otroobj){
                    $("#txtcontar").html("<div class='alert alert-danger'>Error: "+quepaso+"</div>");
                },
                success: function(datos){
                    $("#numeroNotificaciones").html(datos);
                },
                statusCode: {
                    404: function() {
                        $("#txtcontar").html("<div class='alert alert-danger'>No existe URL</div>");
                    }
                }
            });

            setTimeout(NotificacionTotal, 30000);
            //cargaatender();
        }
        setTimeout(NotificacionTotal, 1000);

        function desactivarnoti(id)
        {
            console.log("ok");
            $.ajax({
                type: "GET",
                url: '{{ URL::to("desactivarNoti") }}/'+id,
                data: '',
                error: function(objeto, quepaso, otroobj){

                },
                success: function(datos){
                    console.log("ok");
                },
                statusCode: {
                    404: function() {

                    }
                }
            });

            setTimeout(NotificacionTotal, 30000);
            //cargaatender();
        }
 </script>

</head>
<?php
    //Estas líneas hacen que el menú Inicio tenga la ruta Inicio según el modulo
    $rucu=Request::path(); //Route::getCurrentRoute()->getPrefix(); //Route::getCurrentRoute()->getPath();
    $vermodulo=explode("/",$rucu);
    //str_replace("/","",)
    //($vermodulo);
    //$mod=App\ModulosModel::where("ruta",$vermodulo[0])->first();
    $rutamod=App\MenuModuloModel::where("ruta","like",$vermodulo[0])->first();
    if(!$rutamod)
        $mod=App\ModuloModel::where("ruta",$vermodulo[0])->first();
    else
        $mod=App\ModuloModel::find($rutamod->id_modulo);
    // show($vermodulo);

    $rutamod="/";
    if($mod)
        $rutamod=$mod->ruta;
    /*else
    {
        $mod=App\UsuarioModuloModel::join("ad_modulos as a","a.id","=","ad_usuario_modulo.id_modulo")
        ->where("ad_usuario_modulo.id_usuario",Auth::user()->id)
        ->select("a.nombre","a.id")
        ->first();
    }*/
    // dd($vermodulo);
?>
<body class="skin-1 {{ (($vermodulo[0]=="cambiardatos" || (isset($vermodulo[1]) && $vermodulo[1]=="agendavirtual") || (isset($vermodulo[1]) && $vermodulo[1]=="indicadoresgestion") || (isset($vermodulo[1]) && $vermodulo[1]=="dashboard")) ? 'pace-done body-small mini-navbar' : '') }}">
    <div id="gif_loader" hidden>
        <img
            style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 9999; background: url(/img/loading36.gif) center no-repeat #ffffffad;">
    </div>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                            <img src="https://ciudadanodigital.manta.gob.ec/local/public/logo.png" class=" img-responsive" alt="">
                        </span>

                    </div>
                    <div class="logo-element">
                        MENÚ
                    </div>
                </li>
                <li {{ (Request::is('/') ? 'class="active"' : '') }}>
                    <a href="{{ URL::to($rutamod) }}"><i class="fa fa-home"></i> <span class="nav-label">Inicio</span></a>
                </li>
<!-- Permisos segun el usuario-->
<?php
   $id_tipo_pefil=App\User::join("ad_perfil as ap","ap.id","=","users.id_perfil")->select("ap.id","ap.tipo")->where("users.id",Auth::user()->id)->first();
    // $rucu=$rucu=Route::currentRouteName(); //Route::getCurrentRoute()->getPath();
    //     $vermodulo=explode("/",$rucu);
        //show($vermodulo);
        //$mod=App\MenuModulosModel::where("ruta",$vermodulo[0])->first();
        //show($mod);
        //show($mod);
             if ($id_tipo_pefil->tipo==1)
             {

                $tablamen=App\MenuModuloModel::join("ad_menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("ad_menu_modulo.nivel",1)->orderby("ad_menu_modulo.idmain")->get();



            }
             else {
               $tablamen=App\MenuModuloModel::join("ad_menu as a","a.id","=","ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as b","b.id_menu_modulo","=","ad_menu_modulo.id")
                ->select("ad_menu_modulo.*","a.menu")
                ->where("ad_menu_modulo.nivel",1)
                ->where("b.id_perfil",$id_tipo_pefil->id);
                if($mod){
                    $tablamen=$tablamen->where("ad_menu_modulo.id_modulo",$mod->id);
                }

                $tablamen=$tablamen->orderby("ad_menu_modulo.orden")->get();
              }


             //$sw=0;
            //show($tablamen);
              if($vermodulo[0]=="cambiardatos")
                $tablamen=array();

            //dd($tablamen);
             ?>


            @foreach($tablamen as $key => $value)
                <?php
                    if ($id_tipo_pefil->tipo==1){

                        $tbnivel=App\MenuModuloModel::join("ad_menu as a","a.id","=","ad_menu_modulo.id_menu")
                            ->select("ad_menu_modulo.*","a.menu")
                            ->where("ad_menu_modulo.visible","SI")
                            ->where("ad_menu_modulo.nivel","<>",1) //->groupBy("ad_menu_modulo.id_menu");
                           ->where("ad_menu_modulo.idmain",$value->id)->get();

                    }
                    else{

                        $tbnivel=App\MenuModuloModel::join("ad_menu as a","a.id","=","ad_menu_modulo.id_menu")
                        ->join("ad_menu_perfil as bb","bb.id_menu_modulo","=","ad_menu_modulo.id")
                        ->join("ad_modulos as m","m.id","=","ad_menu_modulo.id_modulo")
                        ->join("ad_usuario_modulo as um","um.id_modulo","=","m.id")
                            ->join("users as u","u.id","=","um.id_usuario")
                            ->select("ad_menu_modulo.*","a.menu")
                            ->where("ad_menu_modulo.nivel","<>",1)
                            ->where("ad_menu_modulo.idmain",$value->id)
                            ->where("ad_menu_modulo.visible","SI")
                            ->where("u.id",Auth::user()->id)
                            //->where("u.id_perfil",Auth::user()->id)
                            ->where("bb.id_perfil",$id_tipo_pefil->id)
                            ->orderby("ad_menu_modulo.orden")
                            ->get();
                    }
                    $clase="";
                    foreach($tbnivel as $cla => $val)
                    {
                        //die(Request::path()."-".$val->ruta);
                        if(Request::is($val->ruta.'*'))
                                $clase="class=active";
                    }
                ?>
                <li {{ $clase }}>
                    @if($value->ruta=='#')
                        <a href="javascript::">
                    @else
                        <a href="{{ URL::to($value->ruta) }}" {{ $value->adicional }}>
                    @endif
                            <i class="{{ $value->icono }}"></i> <span class="nav-label">{{ $value->menu }}</span> <span class="fa arrow"></span></a>
                    @if(!empty($tbnivel))
                        <ul class="nav nav-second-level">
                            @foreach($tbnivel as $clave => $valor)
                            <li {{ (Request::is($valor->ruta.'*') ? 'class=active' : '') }}>
                                @if($valor->ruta=='#')
                                    <a href="javascript::" {{ $valor->adicional }}>
                                @else
                                    <a href="{{ URL::to($valor->ruta) }}" {{ $valor->adicional }}>
                                @endif
                                <i class="{{ $valor->icono }}"></i>
                                {{ $valor->menu }}</a>
                            </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
<!-- Fin Permisos -->
                     <li {{ (Request::is('salir') ? 'class="active"' : '') }}>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form2').submit();">
                            <i class="fa fa-sign-out"></i>
                            Salir
                        </a>

                        <form id="logout-form2" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
            </ul>

        </div>
    </nav>
<div id="page-wrapper" class="gray-bg bgbody">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <!--form role="search" class="navbar-form-custom" method="post" action="#">
                        <div class="form-group">
                            <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                        </div>
                    </form-->
                </div>
                <ul class="nav navbar-top-links navbar-right">
                        <?php
                        $user = App\User::
                        join("ad_perfil as p","p.id","=","users.id_perfil")->first();


                    ?>
                    @if($user->tipo==4 || $user->tipo==1)

                    <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#" onclick="cargaatender()">
                                <i class="fa fa-bell"></i>  <span class="label label-primary" id="numeroNotificaciones">0</span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts" id="alertas">

                            </ul>
                        </li>
                @endif
                    <li id="divusuario">
                        @if(Auth::user()->id==1)
                        <a href="#">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @else
                        <a href="{{ URL::to('cambiardatos/'.Auth::user()->id.'/edit') }}">
                            <i class="fa fa-user"></i> {{ Auth::user()->name }}
                        </a>
                        @endif
                    </li>
                    <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-cubes"></i> Módulos
                            </a>
                            <?php
                            $modulos=App\UsuarioModuloModel::
                                join("ad_modulos as m","m.id","=","ad_usuario_modulo.id_modulo")
                                ->select("m.*")
                                ->where("ad_usuario_modulo.id_usuario",Auth::user()->id)
                                ->get();
                            ?>
                            <ul class="dropdown-menu dropdown-alerts">
                                @foreach ($modulos as $key => $item)
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ URL::to($item->ruta) }}">
                                            <div>
                                                <i class="fa fa-cube"></i> {{$item->nombre}}
                                            </div>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                @endforeach

                            </ul>
                        </li>
                        @if (Session::has('current-user'))
                            <li>
                                <a href="{{ route('revertir') }}">
                                    <i class="fa fa-user"></i>
                                    Revertir Sesión
                                </a>
                            </li>
                        @endif
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i>
                            Salir
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="imgback">
                        @yield('contenido')
                    </div>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal fade in">
                <div class="modal-dialog">
                    <div class="modal-content" style="    width: fit-content;">
                        <div class="modal-header">
                            <span class="modal-title"
                                style="    font-size: x-large;"><?php echo ConfigSystem('nombre_slide'); ?> </span>
                            <button type="button" class="close fa fa-times-circle" style="    font-size: x-large;"
                                data-dismiss="modal" aria-hidden="true"></button>
                        </div>
                        <div class="modal-body">
                            <div class="animated">
                                <?php $imagenes= explode('|',ConfigSystem('imagenes-slide')) ?>
                                <ul class="gallery-slideshow">
                                    @foreach ($imagenes as $key => $item)
                                    <li><img src="{{asset('img_gallery/'.$item)}}" width="100%" height="100%" /></li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        <div class="modal-footer"> {{ trans('html.main.sistema') }}</div>
                    </div>
                </div>
            </div>

        <div class="footer">
            <div class="pull-right">
                <!--10GB of <strong>250GB</strong> Free. -->
                {{ Auth::user()->name }}
            </div>
            <div>
                {!! trans('html.main.copyright') !!}
            </div>
        </div>

    </div>
</div>
@if(date("m")==12)
    {{ HTML::script('js/cursor-con-nieve.js') }}
@endif
{{ HTML::script('js/jquery.snow.min.1.0.js') }}
<script>
    $(document).ready( function(){
        var d = new Date();// Capturo Fecha Actual
        var n = d.getMonth();//Capturo Mes de la Fecha Actual
        //0: Enero, 1: Febrero, 2:Marzo, etc.
        //console.log(n);
        if(n==11)//Diciembre
            $.fn.snow();
    });
</script>
</body>



</html>

