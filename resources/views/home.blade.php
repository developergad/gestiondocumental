@extends ('layouts.layout')
@section ('titulo') {{ trans('html.main.sistema') }} @stop
@section ('scripts')
@stop
@section ('contenido')
	<h1 class="titulo">
		Bienvenido
	</h1>
	<h1 class="titulo2">
                {{ trans('html.main.sistemasub') }}
	</h1>
@if (Session::has('message'))
    <script>
$(document).ready(function() {
    //toastr.succes("{{ Session::get('message') }}");
    toastr["success"]("{{ Session::get('message') }}");
    //$.notify("{{ Session::get('message') }}","success");
});
</script>
	   <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
@if (isset($estadistica)) <!-- [0]=Titulo [1]=Ruta [2]=Valor [3]=Numero hasta 4 [4]=Clase Texto: success info danger warning -->
    <div class="col-lg-2">
        <div class="ibox float-e-margins">
            <div class="ibox-title  boton-{{ $estadistica[3] }}">
                <h5>{{ $estadistica[0] }}</h5>
            </div>
            <div class="ibox-content cuadro-{{ $estadistica[3] }}" style="text-align: center;">
                @if(Auth::user()->id==1)
                    <a href="{{ URL::to($estadistica[1]) }}">
                        <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1> <!-- La clase de text-info se cambia a text-success, etc -->
                    </a>
                @else
                    <h1 class="no-margins"><div class="font-bold text-{{ $estadistica[4] }}">{{ $estadistica[2] }}</div></h1>
                @endif
            </div>
        </div>
    </div>
@endif
@stop
