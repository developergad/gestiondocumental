<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_perfil', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_perfil');
            $table->integer('tipo');
            $table->enum('estado', ['ACT', 'INA'])->default('ACT');
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_perfil')->constrained('ad_perfil');
            $table->string('name');
            $table->string('cedula')->unique();
            $table->string('email')->unique();
            $table->string('telefono');
            $table->date('fecha_nacimiento');
            $table->string('cargo')->nullable();
            $table->enum('estado', ['ACT', 'INA']);
            $table->integer('nuevo')->default(1);
            $table->timestamp('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('ad_perfil');
    }
}
