<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdMenuPerfilTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_menu_perfil', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_perfil')->constrained('ad_perfil');
            $table->foreignId('id_menu_modulo')->constrained('ad_menu_modulo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_menu_perfil');
    }
}
