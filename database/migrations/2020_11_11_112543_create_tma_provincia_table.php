<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmaProvinciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tma_provincia', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_provincia', 100);
            $table->string('capital_provincia', 100);
            $table->text('descripcion_provincia');
            $table->integer('poblacion_provincia');
            $table->double('superficie_provincia');
            $table->double('latitud_provincia');
            $table->double('longitud_provincia');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tma_provincia');
    }
}
