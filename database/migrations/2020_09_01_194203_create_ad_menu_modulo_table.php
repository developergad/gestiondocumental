<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdMenuModuloTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_menu_modulo', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_modulo')->constrained('ad_modulos');
            $table->foreignId('id_menu')->constrained('ad_menu');;
            $table->integer('idmain')->nullable();
            $table->string('ruta');
            $table->string('icono')->nullable();
            $table->integer('nivel');
            $table->double('orden', 8, 2);
            $table->enum('visible', ['SI', 'NO'])->default('SI');
            $table->string('adicional')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_menu_modulo');
    }
}
