<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTmoCantonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tmo_canton', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_provincia')->constrained('tma_provincia');
            $table->string('nombre_canton', 100);
            $table->text('descripcion_canton');
            $table->integer('poblacion_canton');
            $table->double('superficie_canton');
            $table->double('latitud_canton');
            $table->double('longitud_canton');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tmo_canton');
    }
}
