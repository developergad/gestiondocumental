<?php

use App\MenuModel;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuModel::create([
            'menu' => 'Administración'
        ]);

        MenuModel::create([
            'menu' => 'Menú de Opciones'
        ]);

        MenuModel::create([
            'menu' => 'Módulos de Sistema'
        ]);

        MenuModel::create([
            'menu' => 'Asignar Menú a Módulo'
        ]);

        MenuModel::create([
            'menu' => 'Usuarios'
        ]);

        MenuModel::create([
            'menu' => 'Perfiles'
        ]);

        MenuModel::create([
            'menu' => 'Permisos por perfiles'
        ]);

        MenuModel::create([
            'menu' => 'Usuarios del Sistema'
        ]);

        MenuModel::create([
            'menu' => 'Gestión de trámites internos'
        ]);

        MenuModel::create([
            'menu' => 'Elementos enviados'
        ]);

        MenuModel::create([
            'menu' => 'Asignaciones'
        ]);

        MenuModel::create([
            'menu' => 'Recibidos'
        ]);

        MenuModel::create([
            'menu' => 'Con copia'
        ]);

        MenuModel::create([
            'menu' => 'Archivados'
        ]);

        MenuModel::create([
            'menu' => 'Borradores'
        ]);

        MenuModel::create([
            'menu' => 'Direcciones'
        ]);

        MenuModel::create([
            'menu' => 'Autorizaciones'
        ]);

        MenuModel::create([
            'menu' => 'Parroquias'
        ]);

        MenuModel::create([
            'menu' => 'Barrios'
        ]);
    }
}
