<?php

use App\ModuloModel;
use Illuminate\Database\Seeder;

class ModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ModuloModel::create([
            'nombre' => 'GENERAL',
            'detalle' => 'GENERAL',
            'ruta' => '/'
        ]);

        ModuloModel::create([
            'nombre' => 'Módulo de Trámites Externos e Internos',
            'detalle' => 'Módulo de Trámites Externos e Internos',
            'ruta' => 'tramites',
            'icono' => 'rendimiento-actividades.png'
        ]);
    }
}
