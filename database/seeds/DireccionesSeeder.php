<?php

use Illuminate\Database\Seeder;
use Modules\Tramites\Entities\direccionesModel;

class DireccionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        direccionesModel::create([
            'direccion' => 'DIRECCIÓN DE DESARROLLO, INNOVACIÓN E INFRAESTRUCTURA TECNOLÓGICA',
            'alias' => 'TECNOLOGIA',
            'mta' => 'DIIT',
        ]);

        direccionesModel::create([
            'direccion' => 'DIRECCIÓN DE COMPRAS PÚBLICAS',
            'alias' => 'CCPP',
            'mta' => 'DCPU',
        ]);
    }
}
