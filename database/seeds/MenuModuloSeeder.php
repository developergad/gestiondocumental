<?php

use App\MenuModuloModel;
use Illuminate\Database\Seeder;

class MenuModuloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 1,
            'ruta' => '#',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 1,
            'orden' => 1.00,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 2,
            'idmain' => 1,
            'ruta' => 'menu',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.01,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 3,
            'idmain' => 1,
            'ruta' => 'modulos',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.02,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 4,
            'idmain' => 1,
            'ruta' => 'menumodulos',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.03,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 5,
            'ruta' => '#',
            'icono' => 'fa fa-users',
            'nivel' => 1,
            'orden' => 2.00,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 6,
            'idmain' => 5,
            'ruta' => 'perfil',
            'icono' => 'fa fa-users',
            'nivel' => 2,
            'orden' => 2.01,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 7,
            'idmain' => 5,
            'ruta' => 'menuperfil',
            'icono' => 'fa fa-users',
            'nivel' => 2,
            'orden' => 2.02,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 8,
            'idmain' => 5,
            'ruta' => 'usuarios',
            'icono' => 'fa fa-users',
            'nivel' => 2,
            'orden' => 2.02,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 9,
            'ruta' => '#',
            'icono' => 'fa fa-file-text',
            'nivel' => 1,
            'orden' => 3.00,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 10,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.01,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 11,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos/asignados',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.02,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 12,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos/recibidos',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.03,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 13,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos/copia',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.04,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 14,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos/archivados',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.05,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 15,
            'idmain' => 9,
            'ruta' => 'tramites/tramitesinternos/borradores',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.06,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 16,
            'idmain' => 1,
            'ruta' => 'tramites/direcciones',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.04,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 18,
            'idmain' => 1,
            'ruta' => 'parroquias',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.05,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 1,
            'id_menu' => 19,
            'idmain' => 1,
            'ruta' => 'barrios',
            'icono' => 'fa fa-pencil-square-o',
            'nivel' => 2,
            'orden' => 1.06,
            'visible' => 'SI'
        ]);

        MenuModuloModel::create([
            'id_modulo' => 2,
            'id_menu' => 17,
            'idmain' => 9,
            'ruta' => 'tramites/autorizaciones',
            'icono' => 'fa fa-file-text',
            'nivel' => 2,
            'orden' => 3.07,
            'visible' => 'SI'
        ]);
    }
}
