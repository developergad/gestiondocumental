<?php

use App\PerfilModel;
use App\User;
use Illuminate\Database\Seeder;

class PerfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PerfilModel::create([
            'nombre_perfil' => 'ADMINISTRADOR',
            'estado' => 'ACT',
            'tipo' => 1
        ]);

        PerfilModel::create([
            'nombre_perfil' => 'DIRECTOR',
            'estado' => 'ACT',
            'tipo' => 3
        ]);

        PerfilModel::create([
            'nombre_perfil' => 'ANALISTA',
            'estado' => 'ACT',
            'tipo' => 3
        ]);

        User::create([
            'id_perfil' => 1,
            'name' => 'ADMINISTRADOR',
            'cedula' => 'admin',
            'email' => 'tidesarrollo@manta.gob.ec',
            'telefono' => '0000000000',
            'fecha_nacimiento' => date('Y-m-d'),
            'cargo' => 'ADMINISTRADOR',
            'password' => config('app.env') == 'local' ? bcrypt('desarrollo') : bcrypt('TI@Firm3s*sis')
        ]);
    }
}
