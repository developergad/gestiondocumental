<?php

use App\SysConfigModel;
use Illuminate\Database\Seeder;

class SysconfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SysConfigModel::create([
            'campo' => 'correos',
            'valor' => 'SI'
        ]);
    }
}
