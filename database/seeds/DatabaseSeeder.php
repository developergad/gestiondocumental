<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PerfilSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(ModuloSeeder::class);
        $this->call(MenuModuloSeeder::class);
        $this->call(DireccionesSeeder::class);
        $this->call(SysconfigSeeder::class);
    }
}
