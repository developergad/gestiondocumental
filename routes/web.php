<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('index');
Route::get('/home', 'HomeController@home')->name('home');

Route::resource('menu', 'MenuController', ['only' => ['index', 'store', 'destroy']]);
Route::resource("modulos", "ModulosController");

Route::get('modulosajax', 'ModulosController@modulosajax');
Route::get("moduleschoice", "ModulosController@moduleschoice");

Route::resource("menumodulos", "MenuModulosController");

Route::resource('usuarios', 'UsuariosController');
Route::get('usuariosajax', 'UsuariosController@usuariosajax');

Route::resource('perfil', 'PerfilController');
Route::get('perfilesajax', 'PerfilController@perfilesajax');

Route::resource('menuperfil', 'MenuPerfilControler', ['only' => ['index', 'create', 'store']]);
Route::get("getmenumodulostree", "MenuModulosController@getmenumodulostree");

Route::resource('parroquias', 'ParroquiaController');
Route::get('getparroquia', 'ParroquiaController@getparroquia');
Route::get("parroquiaajax", "ParroquiaController@parroquiaajax");

Route::resource('barrios', 'BarrioController');
Route::get('getBarrio', 'BarrioController@getBarrio');
Route::get('barrioajax', 'BarrioController@barrioajax');

Route::resource('cambiardatos', 'CambiaDatosController', ['only' => ['edit', 'update']]);

Route::get('/soap', 'SoapController@soap');
Route::post('/soap_subida', 'SoapController@soap');
Route::get('/soapFirma', ['as' => 'soapFirma', 'uses' => 'SoapController@soapFirma']);

Route::get("getnotificatodos", "NotificacionesController@getnotificatodos");
Route::get("getnotificacuenta", "NotificacionesController@getnotificacuenta");
Route::get("desactivarNoti/{id}", "NotificacionesController@desactivarNoti");
Route::get("consultardatosusuario", "UsuariosController@consultarDatosUsuario");


