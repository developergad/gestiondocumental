<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioModuloModel extends Model
{
    protected $table = 'ad_usuario_modulo';
    protected $fillable = ['id_usaurio', 'id_modulo'];
}
