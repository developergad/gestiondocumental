<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditoriaModel extends Model
{
    protected $table = 'ad_auditoria';
    protected $fillable = ['idusuario', 'urlmenu', 'accion', 'ip', 'nompc'];
}
