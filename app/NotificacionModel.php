<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificacionModel extends Model
{
    protected $table = 'tmo_notificaciones';
    protected $fillable = ['users_id', 'notificacion', 'ruta', 'estado', 'color'];
}
