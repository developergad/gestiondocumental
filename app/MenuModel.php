<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected $table = 'ad_menu';
    protected $fillable = ['menu', 'adicional', 'icono'];
}
