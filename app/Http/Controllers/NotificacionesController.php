<?php

namespace App\Http\Controllers;

use App\User;
use App\NotificacionModel;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class NotificacionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    function notificacionesweb($notificacion, $ruta, $user_id, $color)
    {
        NotificacionModel::create([
            'notificacion' => $notificacion,
            'ruta' => $ruta,
            'users_id' => $user_id,
            'color' => $color,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }

    function getnotificacuenta()
    {
        $total = NotificacionModel::where("users_id", "=", Auth::user()->id)
            ->where("estado", "=", "ACT")
            ->whereBetween("created_at", ['"' . date('Y-m-d') . ' 00:00:00"', date('Y-m-d ') . '23:59:59'])
            ->count();
        return $total;
    }

    function desactivarNoti($id)
    {
        NotificacionModel::where("id", "=", $id)->update(["estado" => 'INA']);
    }

    function getnotificatodos()
    {
        $notificaciones = NotificacionModel::where("users_id", "=", Auth::user()->id)
            ->whereBetween("created_at", ['"' . date('Y-m-d') . ' 00:00:00"', date('Y-m-d ') . '23:59:59'])
            ->orderBy("id", "desc")
            ->get();

        $html = '';
        foreach ($notificaciones as $notificacion)
        {
            $estilo = null;
            $estilo = 'style="background: #' . $notificacion->color . '; color:white;"';
            if ($notificacion->estado == "INA")
            {
                $estilo = '';
            }

            $html .= '<li class="divider"></li>
                <li ' . $estilo . ' >
                    <div class="link-block" style="text-align: justify;">

                        <a href="' . URL::to($notificacion->ruta) . '" onclick="desactivarnoti(' . $notificacion->id . ')">
                            <span> 🔔' . $notificacion->notificacion . ' 📨 </span>

                        </a>
                    </div>
                </li>';
        }

        return $html;
    }

    public function EnviarEmail($email, $asunto, $tipo, $detalle, $ruta, $vista = "vistas.email", $bonton = "SI")
    {
        $objetos = '[
            {"Tipo":"tipo","Descripcion":"Notificación","Nombre":"tipo","Valor":"Null"},
            {"Tipo":"mensaje","Descripcion":"Detalle","Nombre":"detalle","Valor":"Null"},
            {"Tipo":"buttonlink","Descripcion":"Ver","Nombre":"botonlink","Valor":"Null"}
        ]';

        $objetos = json_decode($objetos);
        $objetos[0]->Valor = $tipo;
        $objetos[1]->Valor = $detalle;
        $objetos[2]->Valor = config('app.url') . '/' . $ruta;
        if($bonton == 'NO')
        {
            unset($objetos[2]);
        }

        $usuario = User::where('email', $email)->first();
        $name = '';
        if ($usuario)
        {
            $name = $usuario->name;
        }

        $correos = ConfigSystem("correos");
        if ($correos == "SI")
        {
            Mail::send($vista, [
                'objetos' => $objetos,
                'usuario' => $name
            ], function ($m) use ($email, $asunto) {
                $m->from("sistemasic@manta.gob.ec", "SistemasIC");
                $m->to($email)->subject($asunto);
            });
        }
    }
}
