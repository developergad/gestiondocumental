<?php namespace App\Http\Controllers;

use App\BarrioModel;
use App\parroquiaModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class barrioController extends Controller
{
	var $configuraciongeneral = ['Asignar barrios', 'barrios', 'index', 6 => 'barrioajax', 7 => 'barrios'];
    var $escoja = [null => 'Escoja opción...'];
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Nombre del barrio","Nombre":"barrio","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"select","Descripcion":"Parroquia ","Nombre":"id_parroquia","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
    ]';

    var $validarjs = [
        "barrio"=>"parroquia: {
            required: true
        }",
        "barrio"=>"parroquia: {
            required: true
        }"
    ];

    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function getBarrio()
    {
        $id = request()->parroquia_id;
        $barrio = BarrioModel::where("id_parroquia",$id)->orderBy("barrio")->pluck("barrio","id")->all();
        return $barrio;
    }

	public function index()
	{
		$objetos = json_decode($this->objetos);
		$objetos[1]->Nombre = 'parroquia';
        $objetos = array_values($objetos);

        $id_tipo_pefil = User::join('ad_perfil as ap','ap.id','=','users.id_perfil')
        ->select('ap.tipo')->where(['users.id' => Auth::user()->id])->first();

        $delete = '';
        $create = '';

        switch($id_tipo_pefil->tipo)
        {
            case 1:
                $delete = 'si';
                $create = 'si';
                $edit = '';
                break;
            case 2:
                $delete = 'no';
                $create = 'no';
                $edit = '';
                break;
            case 3:
                $delete = 'si';
                $create = 'si';
                $edit = '';
                break;
            case 4:
                $delete = 'si';
                $create = 'si';
                $edit = '';
                break;
        }

        $tabla = [];

		return view('vistas.index', [
            'objetos' => $objetos,
            'tabla' => $tabla,
            'configuraciongeneral' => $this->configuraciongeneral,
            'delete' => $delete,
            'create' => $create
        ]);
    }

    public function barrioajax(Request $request)
    {
        $columns = [
            0 => 'id',
            1 => 'barrio',
            2 => 'parroquia',
            3 => 'acciones',
        ];

        $totalData = barrioModel::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = barrioModel::select('barrios.*','p.parroquia')
            ->join('parroquias as p','p.id','=','barrios.id_parroquia')
            ->where('p.estado','ACT')->offset($start)->limit($limit)
            ->orderBy($order,$dir)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $posts =  barrioModel::select('barrios.*','p.parroquia')
                ->join('parroquias as p','p.id','=','barrios.id_parroquia')
                ->where('p.estado','ACT')
                ->where(function($query)use($search){
                    $query->where('barrio.id','LIKE',"%{$search}%")
                    ->orWhere('barrio', 'LIKE',"%{$search}%")
                    ->orWhere('parroquia', 'LIKE',"%{$search}%");
                })->offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();

            $totalFiltered = barrioModel::select("barrios.*","p.parroquia")
                ->join("parroquias as p","p.id","=","barrios.id_parroquia")
                ->where("p.estado","ACT")
                ->where(function($query)use($search){
                    $query->where('barrio.id','LIKE',"%{$search}%")
                    ->orWhere('barrio', 'LIKE',"%{$search}%")
                    ->orWhere('parroquia', 'LIKE',"%{$search}%");
                })->count();
        }

        $data = [];
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $aciones=link_to_route(''.$this->configuraciongeneral[7].'.show','', array($post->id), array('class' => 'fa fa-newspaper-o divpopup','target'=>'_blank', 'onclick'=>'popup(this)')).'&nbsp;&nbsp;'.
                link_to_route(''.$this->configuraciongeneral[7].'.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a>
                <div style="display: none;">
                <form method="POST" action="'.$this->configuraciongeneral[7].'/'.$post->id.'" accept-charset="UTF-8" id="frmElimina'.$post->id.'" class="pull-right"><input name="_method" type="hidden" value="DELETE">
                    <input name="_token" type="hidden" value="'.csrf_token().'">
                    <input class="btn btn-small btn-warning" type="submit" value="Eliminar">
                </form>
                </div>';

                $nestedData['id'] = $post->id;
                $nestedData['barrio'] = $post->barrio;
                $nestedData['parroquia'] = $post->parroquia;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }

        $json_data = [
            'draw'            => intval($request->input('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        ];

        return response()->json($json_data);
    }

	public function guardar($id)
    {
        $input = request()->all();
        $ruta = $this->configuraciongeneral[1];

        if($id == 0)
        {
            $ruta.="/create";
            $guardar= new barrioModel;
            $msg="Registro Creado Exitosamente...!";
            $msgauditoria="Registro Variable de Configuración";
        }
        else
        {
            $ruta.="/$id/edit";
            $guardar= barrioModel::find($id);
            $msg="Registro Actualizado Exitosamente...!";
            $msgauditoria="Edición Variable de Configuración";
        }

        $input = request()->all();
        $arrapas = array();

        $validator = Validator::make($input, barrioModel::rules($id));

        if ($validator->fails())
        {
            return Redirect::to("$ruta")->withErrors($validator)->withInput();
        }
        else
        {
            foreach($input as $key => $value)
            {
                if($key != "_method" && $key != "_token")
                {
                    $guardar->$key = $value;
                }
            }

            $guardar->save();
            // Auditoria($msgauditoria." - ID: ".$id. "-".Input::get($guardar->barrio));
        }

        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objetos = json_decode($this->objetos);
        $objetos[1]->Nombre = "parroquia";
        $objetos = array_values($objetos);
        $tabla = barrioModel::join('parroquia as p','barrio.id_parroquia','=','p.id')
            ->select('barrio.*','p.parroquia')->where('barrio.estado','ACT')->first();

		return view('vistas.show', [
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral
        ]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$this->configuraciongeneral[2] = "crear";
		$seleccionar_parroquia = parroquiaModel::where("estado", "ACT")->pluck("parroquia","id")->all();
		$objetos = json_decode($this->objetos);
        $objetos[1]->Valor = $this->escoja + $seleccionar_parroquia;

		return view('vistas.create', [
            "objetos"=>$objetos,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs
		]);
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->guardar(0);
	}


	/*
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->configuraciongeneral[2]="editar";
        $seleccionar_parroquia=parroquiaModel::where("estado", "ACT")->pluck("parroquia","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor=$this->escoja + $seleccionar_parroquia;
        $tabla=barrioModel::find($id);
        return view('vistas.create',[
            "tabla" => $tabla,
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "validarjs" => $this->validarjs
        ]);
	}

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tabla = barrioModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
