<?php

namespace App\Http\Controllers;

use App\MenuModuloModel;
use App\MenuPerfilModel;
use App\ModuloModel;
use App\PerfilModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class MenuPerfilControler extends Controller
{
    var $configuraciongeneral = ["Menú Perfil", "menuperfil", "crear","getmenumodulostree"];
    var $escoja = [null=>"Escoja opción..."];
    var $objetos = '[
        {"Tipo":"select2","Descripcion":"Módulo","Nombre":"id_modulo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI"  },
        {"Tipo":"select2","Descripcion":"Perfil","Nombre":"id_perfil","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" }
    ]';

    var $validarjs =array(
        "id_perfil"=>"id_perfil: {
            required: true
        }",
        "id_modulo"=>"id_modulo: {
            required: true
        }"
    );

    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modulo = ModuloModel::orderby("nombre")->pluck("nombre","id")->toarray();//->all();
        $main = PerfilModel::orderby("nombre_perfil")->pluck("nombre_perfil","id")->toarray();//->all()
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor= $this->escoja + $main;
        $objetos[0]->Valor= $this->escoja + $modulo;
        $tabla = array();

        return view('permisosmenu.createindex',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>array(),
            "create"=>'si',
            "delete"=>'si'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $idbus = intval(request()->id);
        $modulo = ModuloModel::orderby("nombre")->pluck("nombre","id")->all();
        $main = PerfilModel::orderby("nombre_perfil")->pluck("nombre_perfil","id")->all();
        $objetos=json_decode($this->objetos);
        $objetos[1]->Valor= $this->escoja + $main;
        $objetos[0]->Valor= $this->escoja + $modulo;
        $objetos[0]->ValorAnterior= $idbus;

        $tabla=array();//->paginate(500);

        return view('permisosmenu.createindex',[
            "objetos"=>$objetos,
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "checkmenu"=>"si",
            "validarjs"=>array(),
            "menutodo"=>"si",
            "idbus"=>$idbus
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ruta = $this->configuraciongeneral[1];
        $ruta.="/create";
        $guardar= new MenuPerfilModel;
        $msg="Registro Creado Exitosamente...!";
        $msgauditoria="Registro Variable de Configuración";

        $input = request()->all();
        $arrapas=array();

        $validator = Validator::make($input, MenuPerfilModel::rules(0));

        if ($validator->fails()) {
            //die($ruta);
            return Redirect::to("$ruta")
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $msg="No ha escogido menú de Opciones.";
            if(request()->has("idmenumain"))
            {
                $menuper=request()->idmenumain;
                $del=MenuPerfilModel::join("ad_menu_modulo as a","a.id","=","ad_menu_perfil.id_menu_modulo")
                ->where("ad_menu_perfil.id_perfil",request()->id_perfil)
                ->where("a.id_modulo",request()->id_modulo)
                ->delete();
                $sw=array();
                foreach ($menuper as $key => $value) {
                    $guardar= new MenuPerfilModel;
                    $guardar->id_perfil=request()->id_perfil;
                    $guardar->id_menu_modulo=$value;
                    $guardar->save();
                    $msg="Registro Creado Exitosamente...!";
                    $sw[]=$guardar->id;
                }
                if(count($sw))
                {
                    $ver = MenuModuloModel::join("ad_menu_perfil as a","a.id_menu_modulo","=","ad_menu_modulo.id")
                    ->wherein("a.id",$sw)
                    ->select("ad_menu_modulo.id","ad_menu_modulo.idmain",DB::raw("count(*) as total"))
                    ->groupby("ad_menu_modulo.id","ad_menu_modulo.idmain")
                    ->get();
                    //show($ver);
                    foreach ($ver as $key => $value) {
                        # code...
                        $guardar=MenuPerfilModel::where("id_perfil",request()->id_perfil)->where("id_menu_modulo",$value->idmain)->first();
                        if(!$guardar)
                            $guardar=new MenuPerfilModel;
                        $guardar->id_perfil=request()->id_perfil;
                        $guardar->id_menu_modulo=$value->idmain;
                        $guardar->save();
                    }
                }

            }
        }

        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
