<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        $user->update([
            'last_login_at' => date("Y-m-d")." ".date("H:i:s"),
            'last_login_ip' => $request->ip()
        ]);
        if($user->nuevo == 1)
            return redirect()->intended("cambiardatos/".$user->id."/edit");
        return redirect()->intended($this->redirectPath());
    }

    protected function sendLoginResponse(Request $request)
    {
        $cedula = $request->cedula;
        $usuario = User::where('cedula', $cedula)->first();
        if($usuario->id_perfil == 13)
        {
            Auth::logout();
            return redirect('login')->withErrors(['Su usuario ha sido inhabilitado, por favor comuníquese con el administrador.'])->withInput();
        }
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }

    public function username()
    {
        return 'cedula';
    }
}
