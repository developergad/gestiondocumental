<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;

class FirmaElectronicaController extends Controller
{
    public function pruebaDomcumento()
    {
        $documento = public_path() . '/archivos_sistema/alcaldia_1_1590681559.pdf';
        $nombre_documento = 'alcaldia_1_1590681559.pdf';
        return $this->firmaToken($nombre_documento, $documento, '1309029708');
    }

    public function firmaToken($nombre_documento, $documento, $cedula, $tipo_firma = 1)
    {
        $sistema = 'epam';
        $x_api_key = "7d9e00cce6ec8f793873e52bdd7de31ad1c0626fb88c245b19c3f64a43258623";
        $sizeNombreDocumento = 180;
        $sizeDocumento = 10485760;

        // $cedula = "1309029708";
        $documento_base64 = trim(base64_encode(file_get_contents($documento)));
        if (is_file($documento)) {
            unlink($documento);
        }
        // dd($documento_base64);
        if (config('app.env') == 'local') {
            // $urlws = 'http://127.0.0.1:8080/servicio/documentos';
            // $sistema = 'sistemasic_local';
        }
        $urlws = 'http://webserver.manta.gob.ec:8265/servicio/documentos';
        // $urlws = 'http://194.170.14.38:8265/servicio/documentos';
        $certificadoDigital = "&tipo_certificado=" . $tipo_firma;
        $estampado = "&llx=260&lly=100&estampado=QR&razon=SistemasIC";
        // $estampado = "&llx=260&lly=91&estampado=information1";
        if (strlen($nombre_documento) >= $sizeNombreDocumento) {
            return ['estado' => false, 'msg' => 'Nombre del documento es muy extenso y no debe exeder' . $sizeNombreDocumento . 'caracteres'];
        }
        $pattern =  '/["!@#$%&\/()]/';
        if (preg_match($pattern, $nombre_documento)) {
            return ['estado' => false, 'msg' => 'Nombre del documento contiene caracteres especiales no permitidos'];
        }
        $repeatBody = "{
			\"nombre\": \"" . $nombre_documento . "\",
			\"documento\": \"" . "$documento_base64" . "\"
        }";

        $body = "{
            \"cedula\": \"" . $cedula . "\",
            \"sistema\": \"" . $sistema . "\",
            \"documentos\":[" . $repeatBody . "]
        }";

        // dd(json_decode($body));

        if (strlen($body) <= $sizeDocumento) {
            $headers = array("Content-Type: application/json", "X-API-KEY: " . $x_api_key);
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $urlws);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $token = curl_exec($curl);

            Log::info($token);
            // dd($token);
            if ($token == '' || $token == null) {
                if (config('app.env') == 'local') {
                    return ['estado' => false, 'documento' => $nombre_documento, 'msg' => 'No se pudo generar la acción, intente nuevamente por favor.'];
                } else {
                    return ['estado' => false, 'documento' => null, 'msg' => 'No se pudo generar la acción, intente nuevamente por favor.'];
                }
            }
            $pos = strpos($token, '<html>');
            if ($pos !== false) {
                if (config('app.env') == 'local') {
                    return ['estado' => false, 'documento' => $nombre_documento, 'msg' => 'No se pudo generar la acción, intente nuevamente por favor.'];
                } else {
                    return ['estado' => false, 'documento' => null, 'msg' => 'No se pudo generar la acción, intente nuevamente por favor.'];
                }
            }
            curl_close($curl);
        } else {
            $mb = 1024;
            $size_limit = ($sizeDocumento - 3145728) / $mb / $mb;
            return ['estado' => false, 'documento' => null,  'msg' => 'Documento(s) exede(n) peso permitido para firmar, máximo hasta ' . $size_limit . ' MB'];
        }

        return ['estado' => true, 'documento' => $nombre_documento, 'msg' => 'firmaecmanta://' . $sistema . '/firmar?token=' . $token . $certificadoDigital . $estampado];
    }
}
