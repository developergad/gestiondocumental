<?php

namespace App\Http\Controllers;

use App\MenuModel;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Input;

class MenuController extends Controller
{
    var $configuraciongeneral = ["Menú de Opciones", "menu", "index"];
    var $escoja = [null => "Escoja opción..."];

    var $objetos = '[
        {"Tipo":"text","Descripcion":"Nombre del Menú","Nombre":"menu","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "SI" },
        {"Tipo":"text","Descripcion":"Icono","Nombre":"icono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO"  },
        {"Tipo":"text","Descripcion":"Adicional","Nombre":"adicional","Clase":"Null","Valor":"Null","ValorAnterior" :"Null","Requerido" : "NO"  }
    ]';

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objetos = json_decode($this->objetos);
        $objetos = array_values($objetos);

        $tabla = MenuModel::orderby("id","asc")->get();

        return view('permisosmenu.menuindex', [
            "objetos" => $objetos,
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "si",
            "urlmenu" => "permisostipousu",
            "create" => 'si',
            "delete" => 'si'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datosenvio = $request->datosenvio;
        $id = trim($datosenvio[0]);

        $objetos = json_decode($this->objetos);
        $objetos = array_values($objetos);

        foreach($objetos as $keycam => $valuecam)
        {
          if ($datosenvio[$keycam + 1] == "-" || $datosenvio[$keycam + 1] == "" )
           {
               if ($valuecam->Requerido == "SI")
                 {
                   return "Por Favor Ingrese correctamente en el campo " . $valuecam->Nombre;
                 }
           }
           if($valuecam->Nombre=="menu" && $id=="-")
            {
                $verificarexisteid = MenuModel::where("menu",trim($datosenvio[$keycam + 1]))->first();
                if($verificarexisteid)
                    return "El menú '".trim($datosenvio[$keycam + 1])."' ya existe.";
            }
        }

        $verificarexisteid = MenuModel::find($id);


        if (!$verificarexisteid)
        {
            $guardar= new MenuModel;

        }
        else
        {
            $guardar= MenuModel::find($id);
        }

        foreach($objetos as $keycam => $valuecam)
        {
            $dato=trim($datosenvio[$keycam + 1]);
            if($dato=="-")
                $dato="";
            $nombre=$valuecam->Nombre;
            $guardar->$nombre= $dato;
        }

        $guardar->save();

        return $guardar->id;
    }
}
