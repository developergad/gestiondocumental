<?php

namespace App\Http\Controllers;

use App\User;
use App\ModuloModel;
use App\MenuModuloModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ModulosController extends Controller
{
    var $configuraciongeneral = ["Módulos del Sistema", "modulos", "index",6=>"modulosajax"];
    var $objetos = '[
        {"Tipo":"text","Descripcion":"Nombre","Nombre":"nombre","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Detalle","Nombre":"detalle","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Ruta","Nombre":"ruta","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Icono","Nombre":"icono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" }
    ]';

    var $validarjs = [
        "nombre"=>"nombre: {
            required: true
        }",
        "detalle"=>"detalle: {
            required: true
        }"
    ];

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function moduleschoice()
    {
        $id_tipo_pefil = User::join("ad_perfil as ap","ap.id","=","users.id_perfil")
            ->select("ap.tipo")->where("users.id",Auth::user()->id)->first();

        if($id_tipo_pefil->tipo==1)
            return redirect("/");

        $tabla = MenuModuloModel::join("ad_menu as b","b.id","=","ad_menu_modulo.id_menu")
        ->join("ad_modulos as m","ad_menu_modulo.id_modulo","=","m.id")
        ->join("ad_usuario_modulo as m_us","m.id","=","m_us.id_modulo")
        ->select("m.nombre","m.detalle","m.ruta","m.icono", DB::raw("count(*) as total"),DB::raw("max(m_us.id_usuario) as usu"))
        ->groupby("m.nombre","m.detalle","m.ruta", 'm.icono')
        ->where("m_us.id_usuario",Auth::user()->id)
        ->where("ad_menu_modulo.nivel","<>",1)
        ->get();

        Session::put("escoger","1");

        return view('vistas.moduleschoice', ["tabla" => $tabla]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabla=[];

        return view('vistas.index', [
            "objetos" => json_decode($this->objetos),
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral,
            "delete" => "no",
            "create" => 'si'
        ]);
    }

    public function modulosajax(Request $request)
    {
        $columns = array(
            0 =>'id',
            1 =>'nombre',
            2=> 'detalle',
            3=> 'ruta',
            4=> 'icono',
            7=> 'acciones',
        );

        $totalData = ModuloModel::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = ModuloModel::where("estado","ACT")
                ->offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $posts =  ModuloModel::where("estado","ACT")
                ->where(function($query)use($search){
                    $query->where('id','LIKE',"%{$search}%")
                    ->orWhere('nombre', 'LIKE',"%{$search}%")
                    ->orWhere(DB::raw("detalle"), 'LIKE',"%{$search}%")
                    ->orWhere(DB::raw("ruta"), 'LIKE',"%{$search}%");
                })->offset($start)->limit($limit)
                ->orderBy($order,$dir)->get();

            $totalFiltered = ModuloModel::where("estado","ACT")
                ->where(function($query)use($search){
                    $query->where('id','LIKE',"%{$search}%")
                    ->orWhere('nombre', 'LIKE',"%{$search}%")
                    ->orWhere(DB::raw("detalle"), 'LIKE',"%{$search}%")
                    ->orWhere(DB::raw("ruta"), 'LIKE',"%{$search}%");
                })->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $aciones=link_to_route('modulos.show','', array($post->id), array('class' => 'fa fa-newspaper-o divpopup','target'=>'_blank', 'onclick'=>'popup(this)')).'&nbsp;&nbsp;'.
                link_to_route('modulos.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> ';

                $nestedData['id'] = $post->id;
                $nestedData['nombre'] = $post->nombre;
                $nestedData['detalle'] = $post->detalle;
                $nestedData['ruta'] = $post->ruta;
                $nestedData['icono'] = $post->icono;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
            "objetos"=>json_decode($this->objetos),
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs
        ]);
    }

    public function guardar($request, $id)
    {
        $input = $request->all();
        $ruta = $this->configuraciongeneral[1];

        if($id == 0)
        {
            $ruta.= "/create";
            $guardar = new ModuloModel;
            $msg = "Registro Creado Exitosamente...!";
        }
        else
        {
            $ruta .= "/$id/edit";
            $guardar = ModuloModel::find($id);
            $msg = "Registro Actualizado Exitosamente...!";
        }

        $input = $request->all();
        $arrapas=array();

        $validator = Validator::make($input, ModuloModel::rules($id));

        if ($validator->fails())
        {
            return Redirect::to("$ruta")->withErrors($validator)->withInput();
        }
        else
        {
            foreach($input as $key => $value)
            {
                if($key != "_method" && $key != "_token")
                {
                    $guardar->$key = $value;
                }
            }

            $guardar->save();
        }

        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->guardar($request, 0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tabla = ModuloModel::find($id);
        return view('vistas.show',[
            "objetos" => json_decode($this->objetos),
            "tabla" => $tabla,
            "configuraciongeneral" => $this->configuraciongeneral
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->configuraciongeneral[2]="editar";
        $tabla = ModuloModel::find($id);
        return view('vistas.create',[
            "objetos"=>json_decode($this->objetos),
            "configuraciongeneral"=>$this->configuraciongeneral,
            "tabla"=>$tabla,
            "validarjs"=>$this->validarjs
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tabla = ModuloModel::find($id);
        $tabla->estado = 'INA';
        $tabla->save();
        Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
