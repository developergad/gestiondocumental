<?php

namespace App\Http\Controllers;

use App\PerfilModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PerfilController extends Controller
{
    var $configuraciongeneral = ["Perfiles de usuario", "perfil", "index",6=>"perfilesajax"];
    var $objetos = '[ {"Tipo":"text","Descripcion":"Perfil","Nombre":"nombre_perfil","Clase":"mayuscula","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"TIpo","Nombre":"tipo","Clase":"solonumeros","Valor":"Null","ValorAnterior" :"Null" }
    ]';

    var $validarjs =array(
        "perfil"=>"perfil: {
            required: true
        }"
    );

    public function __construct()
    {
        $this->middleware(['auth','admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabla=PerfilModel::where("estado","ACT")->orderby("id","desc")->get();//->paginate(500);
        return view('vistas.index',[
            "objetos"=>json_decode($this->objetos),
            "tabla"=>$tabla,
            "configuraciongeneral"=>$this->configuraciongeneral,
            "delete"=>"no",
            "create"=>"si"
        ]);
    }


    public function perfilesajax(Request $request)
    {
        $columns = [
            0 =>'id',
            1 =>'nombre_perfil',
            2=> 'tipo',
            4=> 'acciones',
        ];

        $totalData = PerfilModel::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = PerfilModel::where("estado","ACT")
                        ->offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  PerfilModel::where("estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                            ->orWhere('nombre_perfil', 'LIKE',"%{$search}%")
                            ->orWhere(DB::raw("tipo"), 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();

            $totalFiltered = PerfilModel::where("estado","ACT")
                            ->where('id','LIKE',"%{$search}%")
                             ->orWhere('nombre_perfil', 'LIKE',"%{$search}%")
                             ->orWhere(DB::raw("tipo"), 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            //show($posts);
            foreach ($posts as $post)
            {
                $aciones=link_to_route('perfil.show','', array($post->id), array('class' => 'fa fa-newspaper-o divpopup','target'=>'_blank', 'onclick'=>'popup(this)')).'&nbsp;&nbsp;'.
                link_to_route('perfil.edit','', array($post->id), array('class' => 'fa fa-pencil-square-o')).'&nbsp;&nbsp;<a onClick="eliminar('.$post->id.')"><i class="fa fa-trash"></i></a> ';

                $nestedData['id'] = $post->id;
                $nestedData['nombre_perfil'] = $post->nombre_perfil;
                $nestedData['tipo'] = $post->tipo;
                $nestedData['acciones'] = $aciones;
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        return response()->json($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->configuraciongeneral[2]="crear";
        return view('vistas.create',[
            "objetos"=>json_decode($this->objetos),
            "configuraciongeneral"=>$this->configuraciongeneral,
            "validarjs"=>$this->validarjs
        ]);
    }

    public function guardar($request, $id)
    {
        $input = $request->all();

        $ruta = $this->configuraciongeneral[1];

        if($id==0)
        {
            $ruta.="/create";
            $guardar= new PerfilModel;
                $msg="Registro Creado Exitosamente...!";
                $msgauditoria="Registro Variable de Configuración";
        }
        else{
            $ruta.="/$id/edit";
            $guardar= PerfilModel::find($id);
            $msg="Registro Actualizado Exitosamente...!";
            $msgauditoria="Edición Variable de Configuración";
        }

        $input= $request->all();
        $arrapas=array();

        // $validator = Validator::make($input, PerfilModel::rules($id));

        Validator::make($input, PerfilModel::rules($id))->validate();

        // if ($validator->fails()) {
        //     return Redirect::to("$ruta")
        //         ->withErrors($validator)
        //         ->withInput();
        // }else {
            foreach($input as $key => $value)
            {
                if($key != "_method" && $key != "_token")
                {
                    $guardar->$key = $value;
                }
            }
                $guardar->save();
        // }

        Session::flash('message', $msg);
        return Redirect::to($this->configuraciongeneral[1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->guardar($request, 0);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tabla = PerfilModel::find($id);
        return view('vistas.show',[
                "objetos"=>json_decode($this->objetos),
                "tabla"=>$tabla,
                "configuraciongeneral"=>$this->configuraciongeneral
                ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->configuraciongeneral[2]="editar";
        $tabla = PerfilModel::find($id);
        return view('vistas.create',[
                "objetos"=>json_decode($this->objetos),
                "configuraciongeneral"=>$this->configuraciongeneral,
                "tabla"=>$tabla,
                "validarjs"=>$this->validarjs
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        return $this->guardar($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tabla=PerfilModel::find($id);
            //->update(array('estado' => 'INACTIVO'));
        $tabla->estado='INA';
        $tabla->save();
            Session::flash('message', 'Registro dado de Baja!');
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
