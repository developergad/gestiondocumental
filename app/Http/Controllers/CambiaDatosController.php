<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Modules\Tramites\Entities\direccionesModel;

class CambiaDatosController extends Controller
{
    var $configuraciongeneral = ["Datos del Usuario", "cambiardatos", "editar"];

    var $objetos = '[ {"Tipo":"text","Descripcion":"Cédula","Nombre":"cedula","Clase":"Null","Valor":"Null","ValorAnterior" :"Null" },
        {"Tipo":"text","Descripcion":"Nombres","Nombre":"name","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Teléfono","Nombre":"telefono","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Email","Nombre":"email","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"datetext","Descripcion":"Fecha de Nacimiento","Nombre":"fecha_nacimiento","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"password","Descripcion":"Contraseña","Nombre":"password","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"password","Descripcion":"Confirmar Contraseña","Nombre":"password_confirmation","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"select","Descripcion":"Dirección / Área","Nombre":"id_direccion","Clase":"chosen-select","Valor":"Null","ValorAnterior" :"Null"},
        {"Tipo":"text","Descripcion":"Cargo","Nombre":"cargo","Clase":"Null","Valor":"Null","ValorAnterior" :"Null"}
    ]';

    var $validarjs =array(
        /*"campo"=>"campo: {
                        required: true
                    }",
        "valor"=>"valor: {
                        required: true
                    }"*/
    );

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tabla = User::find($id);
        $objetos = json_decode($this->objetos);
        $direccion = direccionesModel::where("estado","ACT")->pluck("direccion","id")->all();
        $objetos[7]->Valor = array(null=>"Escoja opción...") + $direccion;

        return view('vistas.create', [
            "objetos" => $objetos,
            "configuraciongeneral" => $this->configuraciongeneral,
            "tabla" => $tabla,
            "validarjs" => $this->validarjs,
            "create" => "si",
            "variableControl" => "",
            "save" => 'si',
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->guardar($id);
    }

    public function guardar($id)
    {
        $input = request()->all();
        $ruta = $this->configuraciongeneral[1];

        if($id==0)
        {
            $ruta.="/create";
            $guardar= new User;
            $msg="Registro Creado Exitosamente...!";
            $msgauditoria="Registro Variable de Configuración";
        }
        else
        {
            $ruta.="/$id/edit";
            $guardar= User::find($id);
            $msg="Registro Actualizado Exitosamente...!";
            $msgauditoria="Edición Variable de Configuración";
        }

        $input=request()->all();
        $arrapas=array();

        $validator = Validator::make($input, User::rulescon($id));

        if ($validator->fails())
        {
            return Redirect::to("$ruta")->withErrors($validator)->withInput();
        }
        else
        {
            foreach($input as $key => $value)
            {
                if($key != "_method" && $key != "_token" && $key != "password_confirmation" && $key != "password")
                {
                    $guardar->$key = $value;
                }
            }

            $guardar->password=bcrypt(request()->password);
            $guardar->nuevo=0;
            $guardar->save();
        }

        Session::flash('message', $msg);
        return Redirect::to("moduleschoice");
        return Redirect::to($this->configuraciongeneral[1]);
    }
}
