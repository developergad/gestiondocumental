<?php

namespace App\Http\Controllers;

use SoapFault;
use SoapServer;
use App\SubidaDocumento;
use Illuminate\Support\Facades\Log;

class SoapController extends Controller
{
    public function soap(SubidaDocumento $servicio)
    {
        try {

            ini_set('display_errors', '1');
            error_reporting(E_ALL);
            Log::info('0 Llega');
            $dir = public_path() . '/firma_digital_prod.wsdl';

            $soapServer = new SoapServer($dir, array("trace" => true, "exceptions" => true));
            $soapServer->setObject($servicio);


            Log::info('00 Llega');
            ob_start();
            Log::info('0X0 Llega');
            // $soapServer->xdebug_disable();
            // Log::info($soapServer);
            $soapServer->handle();
            // Log::info($soapServer);
            Log::info('0c0 Llega');
            $response = response(ob_get_clean(), 200, [
                'Content-Type' => 'text/xml; charset=utf-8'
            ]);
            Log::info('0cc0 Llega');

            return $response;
        } catch (SoapFault $e) { // Do NOT try and catch "Exception" here
            // echo 'sorry... our service is down';
            Log::info($e);
        }
    }
}
