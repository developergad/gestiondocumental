<?php

namespace App\Http\Controllers;

use App\MenuModuloModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo")->where("users.id", Auth::user()->id)->first();
        if (Auth::user()->id == 1)
            return view('home');
        if (Session::has("escoger") && $id_tipo_pefil->tipo != 1) {
            return redirect("moduleschoice");
        }
        else
        {
            $tabla = MenuModuloModel::join("ad_modulos as m", "m.id", "=", "ad_menu_modulo.id_modulo")
                ->join("ad_menu as me", "me.id", "=", "ad_menu_modulo.id_menu")
                ->join("ad_menu_perfil as mp", "mp.id_menu_modulo", "=", "ad_menu_modulo.id")
                ->join("ad_usuario_modulo as um", "um.id_modulo", "=", "m.id")
                ->select("me.menu", "ad_menu_modulo.adicional", "ad_menu_modulo.ruta", DB::raw("count(*) as total"), DB::raw("max(um.id_usuario) as usu"))
                ->groupby("me.menu", "ad_menu_modulo.adicional", "ad_menu_modulo.ruta")
                ->where("um.id_usuario", Auth::user()->id)
                ->where("ad_menu_modulo.nivel", "<>", 1)->get();
            $totalmod = $tabla->count();

            if ($totalmod == 1)
            {
                foreach ($tabla as $key => $value) {
                    return redirect($value->ruta);
                    break;
                }
            }
            else
            {
                return redirect("moduleschoice");
            }
        }
        return view('home');
    }

    public function home()
    {
        return view('home');
    }
}
