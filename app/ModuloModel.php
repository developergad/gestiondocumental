<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModuloModel extends Model
{
    protected $table = 'ad_modulos';
    protected $fillable = ['nombre', 'detalle', 'ruta', 'icono', 'estado'];

    public static function rules($id = 0, $merge = [])
    {
        return array_merge([
            'nombre'=>'required|unique:ad_modulos'. ($id ? ",id,$id" : ''),
            'detalle'=>"required",
            'ruta'=>"required"
        ], $merge);
    }
}
