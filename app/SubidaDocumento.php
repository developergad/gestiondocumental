<?php

namespace App;

use Illuminate\Support\Facades\Log;

class SubidaDocumento
{
    function grabar_archivos_firmados($usuario, $nombre_doc, $archivo, $datos_firmante, $fecha, $institucion, $cargo)
    {
        try {
            Log::info('----');
            //code...
            $carpeta  = public_path() . '/archivos_firmados';
            if (!file_exists($carpeta)) {
                mkdir($carpeta, 0777, true);
            }
            Log::info('----1 Llega');

            $ruta_destino_archivo = $carpeta . "/" . $nombre_doc;
            if (is_file($ruta_destino_archivo)) {
                unlink($ruta_destino_archivo);
            }
            Log::info('1 Llega');
            $archivo_ok = file_put_contents($ruta_destino_archivo, $archivo);
            Log::info('Llega');
            if ($archivo_ok) {
                return 1;
            } else {
                return 0;
            }
        } catch (\Throwable $th) {
            //throw $th;

            Log::info($th);
            return 0;
        }
    }
}
