<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parroquiaModel extends Model
{
    protected $table = 'parroquias';
    protected $fillable = ['parroquia', 'zona', 'estado'];

    public static function rules ($id=0, $merge=[])
    {
		return array_merge([
			'parroquia'=>'required|unique:parroquias'. ($id ? ",id,$id" : ''),
			'zona'=>'required'
		], $merge);
    }
}
