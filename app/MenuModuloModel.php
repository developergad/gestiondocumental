<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuModuloModel extends Model
{
    protected $table = 'ad_menu_modulo';
    protected $fillable = ['id_modulo', 'id_menu', 'idmain', 'ruta', 'icono', 'nivel', 'oreden', 'visible', 'adicional'];
}
