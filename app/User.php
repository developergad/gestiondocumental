<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const ANALISTA_DIRECTOR = [62, 67];

    const ID_SECRETARIA_GENERAL = [];
    const REVISAR_TRAMITES = [];
    const SUB_ANALISTAS = [];
    const SECRETARIO_GENERAL = 50;
    const UNIDADES = [];
    const USUARIOS_UNIDADES = [];
    const IMPUGNACIONES = [];
    const OTRO_TIPO = [];
    const SUBDIRECTORES = [31, 79];
    const ANALISTA_SUBAREA = [31, 76, 79, 81];


    const DIRECTORES = [
        2
    ];

    const ANALISTAS = [
        3
    ];

    const SOLO_DIRECTORES = [
        2
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'nuevo',
        'cedula', 'cargo', 'id_perfil', 'telefono',
        'nuevo', 'last_login_at', 'last_login_ip'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function rules($id=0, $merge=[])
    {
        return array_merge([
            'name'=>'required',
            'cedula'=>'required',
            'cargo'=>'required',
            'id_perfil'=>'required',
            'id_direccion'=>'required',
            'email'=>'required|email|unique:users'. ($id ? ",id,$id" : '')
        ], $merge);
    }

    public static function rulescon($id=0, $merge=[])
    {
        return array_merge([
            'email'=>'required|email|unique:users'. ($id ? ",id,$id" : ''),
            'password'=>'required|alpha_num|min:4|confirmed',
            'password_confirmation'=>'required|alpha_num|min:4',
            'name'=>'required',
            'id_direccion'=>'required',
            'cargo'=>'required'
        ], $merge);
    }

    public function direccion()
    {
        return $this->belongsTo(direccionesModel::class, 'id_direccion');
    }

    public function scopeDirectores($query)
    {
        return $query->join('tmae_direcciones as td', 'td.id', 'users.id_direccion')
            ->join('ad_perfil as ap', 'ap.id', 'users.id_perfil')
            ->select('users.id', 'users.name', 'users.email')
            ->whereIn('ap.id', [3, 5, 9, 15, 16, 22, 21, 25, 37, 49, 54]);
    }

    public function esDirector()
    {
        $director = Str::contains($this->cargo, 'DIRECTOR')
            // || Str::contains($this->cargo, 'VICEALCALDE')
            || Str::contains($this->cargo, 'SECRETARIO')
            || Str::contains($this->cargo, 'PROCURADOR');

        $esDirector = collect(self::DIRECTORES)->contains(Auth::user()->id_perfil);

        return $director && $esDirector ? true : false;
    }

    public static function esAnalistaSubarea()
    {
        return collect(self::ANALISTA_SUBAREA)->contains(Auth::user()->id_perfil);
    }

    public static function subdirector()
    {
        return collect(self::SUBDIRECTORES)->contains(Auth::user()->id_perfil);
    }
}
