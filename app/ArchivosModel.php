<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArchivosModel extends Model
{
    protected $table = 'tmov_archivos';
    protected $fillable = ['id_usuario', 'tipo', 'id_referencia', 'estado', 'tipo_archivo', 'nombre'];
}
