<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CantonModel extends Model
{
    protected $table = 'tmo_canton';
}
