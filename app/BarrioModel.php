<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BarrioModel extends Model
{
    protected $table = 'barrios';
    protected $fillable = ['id_parroquia', 'barrio', 'estado'];

    public static function rules ($id=0, $merge=[])
    {
		return array_merge([
            'id_parroquia' => 'required',
			'barrio' => 'required',
		], $merge);
    }
}
