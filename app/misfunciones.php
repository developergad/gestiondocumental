<?php

use App\AuditoriaModel;
use App\MenuModuloModel;
use App\SysConfigModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

function ConfigSystem($clave = "")
{
    $data = SysConfigModel::where("campo", $clave)->first();
    $res = '';
    if (!empty($data)) {
        $res = $data->valor;
    }
    return $res;
}

function Autorizar($url)
{
    $id_tipo_pefil = User::join("ad_perfil as ap", "ap.id", "=", "users.id_perfil")->select("ap.tipo", "ap.id")->where("users.id", Auth::user()->id)->first();
    $rucu = $url;
    Route::currentRouteName();
    $vermodulo = explode("/", $rucu);
    $mod = User::where("ruta", $vermodulo[0])->first();

    if (!$mod) {
        $urlar =  explode("/", $url);
        $url = $urlar[0];
    } else {
        $urlar =  explode("/", $url);
        $url = $urlar[0] . "/" . $urlar[1];
    }

    if ($url == "cambiardatos")
        return;
    if ($id_tipo_pefil->tipo != 1) {
        $tabla = MenuModuloModel::join("ad_modulos as m", "m.id", "=", "ad_menu_modulo.id_modulo")
            ->join("ad_usuario_modulo as um", "um.id_modulo", "=", "m.id")
            ->join("ad_menu_perfil as a", "a.id_menu_modulo", "=", "ad_menu_modulo.id")
            ->select("ad_menu_modulo.*", "um.id_usuario")
            ->where(function ($query) use ($url, $id_tipo_pefil) {
                $query->where("ad_menu_modulo.ruta", "like", $url . "%")
                    ->where("um.id_usuario", Auth::user()->id)
                    ->where("a.id_perfil", $id_tipo_pefil->id);
            })
            ->first();
        if (empty($tabla)) {
            Session::flash("msgacceso", "Se detectó un acceso no permitido el " .  fechas(2) . " a las " . date("H:i:s") . ".");

            Auditoria("Acceso no permitido 403");
            die(Response::view('errors/403', array(), 403));

            die("
                <html>
                    <head>
                        <meta charset='utf-8'>
                        <meta http-equiv='Refresh' content='5;url=" . URL::to(URL::previous()) . "'>
                    </head>
                <body>
                    <p><h1>Acceso denegado</h1><h2>Será redireccionado automáticamente a la página prinipal. Caso contrario haga clic <a href='" . URL::to(URL::previous()) . "'>aquí</a> para regresar</h2></p>
                </body>
                </html>");
        }
    }
}

function Auditoria($accion)
{
    $historial = new AuditoriaModel;
    $historial->idusuario = Auth::user()->id;
    $historial->urlmenu = request()->getRequestUri();
    $historial->accion = $accion;
    $historial->ip = request()->ip();
    $historial->nompc = request()->getHost();

    $historial->save();
}

function fechas($a = 0, $fecha = '')
{
    $mes = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'diciembre');
    $dia = array('Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado');
    switch ($a) {
        case 0: // Solo fecha actual y hora
            $fecha = date("Y-m-d") . " " . date("H:i:s");
            $fechar = $fecha;
            break;
        case 1: // Solo fecha actual
            $fecha = date("Y-m-d");
            $fechar = $fecha;
            break;
        case 2: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio";
            // dd($fecha);
            $fechar = utf8_decode($fecha);
            break;
        case 111: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio";
            // dd($fecha);
            $fechar = $fecha;
            break;
        case 3: // Devuelve la fecha en letras y la hora
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $hora = " a las " . date("H:i:s", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio $hora";
            $fechar = utf8_decode($fecha);
            break;
        case 600: // Devuelve la fecha en letras pero no el dia ni la hora
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            //  $hora=date("H:i:s",strtotime($fecha));
            $fecha = "$diad de " . $mes[$mesn - 1] . " del $anio";
            $fechar = utf8_decode($fecha);
            break;
        case 800: // Devuelve la edad
            if ($fecha != null) {
                list($Y, $m, $d) = explode("-", $fecha);
                $fechar = (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y) . " Años";
            } else {
                $fechar = null;
            }

            break;
        case 1100: // Devuelve la edad
            if ($fecha != null) {
                list($Y, $m, $d) = explode("-", $fecha);
                $fechar = (date("md") < $m . $d ? date("Y") - $Y - 1 : date("Y") - $Y);
            } else {
                $fechar = null;
            }

            break;
        case 900: //Devuelve solo el dia de la fecha
            if ($fecha == '')
                $fecha = date("d-m-Y");
            else
                $fecha = date("w", strtotime($fecha));
            $fechar = $dia[$fecha];
            break;
        case 1000:
            if ($fecha == '')
                $fecha = date("d-m-Y");
            else
                $fecha = date("H:i:s", strtotime($fecha));
            $fechar = $fecha;
            break;
        case 1200:
            if ($fecha == '')
                $fecha = date("H:i");
            else
                $fecha = date("H:i", strtotime($fecha));
            $fechar = $fecha;
            break;
        case 300: // Devuelve la fecha en letras cuando se le envie una fecha cualquiera
            if ($fecha == '')
                $fecha = date("Y-m-d") . " " . date("H:i:s");
            $mesn = date("n", strtotime($fecha));
            $dian = date("w", strtotime($fecha));
            $diad = date("d", strtotime($fecha));
            $anio = date("Y", strtotime($fecha));
            $hora = date("H:i:s", strtotime($fecha));
            $fecha = $dia[$dian] . ", $diad de " . $mes[$mesn - 1] . " de $anio a las $hora";
            $fechar = utf8_decode($fecha);
            break;
    }
    return $fechar;
}

function qrvcard($code, $tramite, $url, $datos = "")
{
    $qrch = "
    BEGIN:VCARD
    VERSION:3.0
    N:GAD;MANTA
    FN:DOCUMENTO DIGITAL CORRECTO: $code
    ORG:GAD MANTA
    TITLE:$tramite
    EMAIL;;INTERNET:ciudadanodigital@manta.gob.ec
    BDAY:Proceso validado exitosamente
    END:VCARD";
    $tramite = trim($tramite);
    $code = trim($code);
    $datos = trim(reemplazaespeciales($datos, "NO"));
    $fechahora = date("Y-m-d") . " " . date("H:i:s");
    $qrch = "GAD MANTA\n$tramite\nCODIGO: $code\n$datos\n$fechahora";
    return $qrch;
}

function reemplazaespeciales($string, $especiales = "SI")
{
    $string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C'),
        $string
    );
    if ($especiales == "SI") {
        //Esta parte se encarga de eliminar cualquier caracter extraño
        $string = str_replace(
            array(
                "\\", "¨", "º", "-", "~",
                "#", "@", "|", "!", "\"",
                "·", "$", "%", "&", "/",
                "(", ")", "?", "'", "¡",
                "¿", "[", "^", "`", "]",
                "+", "}", "{", "¨", "´",
                ">", "< ", ";", ",", ":",
                " "
            ),
            '',
            $string
        );
    }

    return $string;
}

function explodewords($string, $car = " ")
{
    $array = explode($car, $string);
    $retorno = array();
    foreach ($array as $key => $value) {
        # code...
        $retorno[$value] = $value;
    }
    return $retorno;
}
