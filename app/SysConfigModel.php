<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysConfigModel extends Model
{
    protected $table = 'sysconfig';
    protected $fillables = ['campo', 'valor', 'valorsub'];
}
